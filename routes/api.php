<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'cors'], function () {
	Route::group(['middleware' => 'auth:api' ], function(){

		//Airports
		
		Route::post('/createAirportRide', 'API\AirportRideController@createAirportRide');

		//User Dashboard
		Route::get('/getUserInformation', 'API\UserIonicController@getUserInformation');
		Route::get('/getProfileDetails', 'API\UserIonicController@getProfileDetails');
		Route::any('/saveProfileData', 'API\UserIonicController@saveProfileData');
		Route::any('/uploadImageProfile', 'API\UserIonicController@uploadImageProfile');
		Route::any('/getProfilePic', 'API\UserIonicController@getProfilePic');
		Route::any('/logout', 'API\UserIonicController@makeLogout');
		Route::any('/changePassword', 'API\UserSettingsController@changePassword');
		Route::any('/updatePaypalAccount', 'API\UserSettingsController@updatePaypalAccount');
		Route::any('/deleteAccount', 'API\UserIonicController@deleteAccount');
		Route::any('/sendVerification', 'API\UserIonicController@sendVerification');
		Route::any('/saveRideReminder', 'API\UserSettingsController@saveRideReminder');
		Route::any('/getRideReminders', 'API\UserSettingsController@getRideReminders');
		Route::any('/deleteRideReminder', 'API\UserSettingsController@deleteRideReminder');

		//Rides
		Route::any('/EditViewOfferRide', 'API\RidesController@EditViewOfferRide');
		Route::any('/checkRideOfferForm', 'API\RidesController@checkRideOfferForm');
		Route::any('/EditViewOfferRideForm', 'API\RidesController@EditViewOfferRideForm');
		Route::any('/updateEditRide', 'API\RidesController@updateEditRide');
		Route::any('/requestJoin', 'API\RidesController@requestJoin');
		Route::any('/createRide', 'API\RidesController@createRide');
		Route::any('/getRideData/{id}', 'API\RidesController@getRideData');
		Route::any('/updateRide/{id}', 'API\RidesController@updateRide');
		Route::any('/getCurrentRides/{page}', 'API\RidesController@getCurrentRides');
		Route::any('/getPastRides/{page}', 'API\RidesController@getPastRides');
		Route::any('/createRideRountrip', 'API\RidesController@createRideRountrip');
		Route::any('/reportProblem', 'API\RidesController@reportProblem');
		Route::any('/deleteRide', 'API\RidesController@deleteRide');
		Route::any('/repeatRide', 'API\RidesController@repeatRide');

		//Preferences
		Route::any('/getTheUserPreferences', 'API\PreferencesController@getTheUserPreferences');
		Route::any('/updatePreferences', 'API\PreferencesController@updatePreferences');
		Route::get('/getPreferenceOptions', 'API\PreferencesController@getPreferenceOptions');
		Route::get('/getOtherUserPreferences/{id}', 'API\PreferencesController@getOtherUserPreferences');


		//BookedRides
		Route::any('/getCurentBooked/{page}', 'API\BookedRidesController@getCurentBooked');
		Route::any('/getPastBooked/{page}', 'API\BookedRidesController@getPastBooked');
		Route::any('/cancelBookedRide/{id}', 'API\BookedRidesController@cancelBookedRide');
		Route::any('/rideBooking', 'API\BookedRidesController@rideBooking');
		Route::any('/reviewDriver', 'API\BookedRidesController@reviewDriver');
		Route::any('/deleteUserRide', 'API\BookedRidesController@deleteUserRide');
		Route::any('/reviewPassenger', 'API\BookedRidesController@reviewPassenger');
		
		//Payments
		Route::any('/paymentSucess', 'API\PaymentController@paymentSucess');
		Route::any('/getUserPayments', 'API\PaymentController@getUserPayments');

		//Driver
		Route::any('/gettingDriverInfo', 'API\DriverController@gettingDriverInfo');
		Route::any('/acceptPassenger', 'API\UserIonicController@acceptPassenger');
		Route::any('/saveCarImage', 'API\DriverController@saveCarImage');
		Route::any('/saveInsurance', 'API\DriverController@saveInsurance');
		Route::any('/saveRegistration', 'API\DriverController@saveRegistration');
		Route::any('/denyPassenger', 'API\UserIonicController@denyPassenger');
		Route::any('/getDriverInfo', 'API\DriverController@getDriverInfo');

		//Reviews
		Route::any('/getUserReviews/{id}', 'API\UserReviewsController@getUserReviews');

		//Notifications
		Route::any('/getNotifications', 'API\NotificationController@getUserNotifications');
		Route::any('/getNotificationActions', 'API\NotificationController@getNotificationActions');
		Route::any('/getNotificationMessages','API\NotificationController@getNotificationMessages');
		Route::any('/getSingleNotificationMessage/{id}', 
			'API\NotificationController@getSingleNotificationMessage');
		Route::any('/notificationCounts', 'API\NotificationController@notificationCount');

		//Messages
		Route::any('/getMessagesCount', 'API\MessagesController@getMessCount');
		Route::any('/getAllMessages', 'API\MessagesController@getAllMessages');
		Route::any('/getSingleMessage/{id}', 'API\MessagesController@getSingleMessage');
		Route::any('/sendMessage/{id}/{reciver_id}', 'API\MessagesController@sendMessage');
		Route::any('/deleteSingleMessage/{id}', 'API\MessagesController@deleteSingleMessage');
		Route::any('/getUserToContact', 'API\MessagesController@getUserToContact');
		Route::any('/getSingleUserContact', 'API\MessagesController@getSingleUserContact');
		Route::any('/storeNewMessage', 'API\MessagesController@storeMessageNew');
		Route::any('/messageDriver/{id}/{ride}', 'API\MessagesController@messageDriver');

		//OpenRides
		Route::get('/fetchPurposes', 'API\FetchController@fetchPurposes');
		Route::any('/createOpenRides', 'API\OpenRidesController@createOpenRides');
        Route::get('/getOpenRides/{page}', 'API\OpenRidesController@getOpenRides');
        Route::any('/changeStatus/{id}', 'API\OpenRidesController@changeStatus');
        Route::any('/deleteOpenRide/{id}', 'API\OpenRidesController@delete');
        Route::post('/editOpenRide/{id}', 'API\OpenRidesController@editOpenRides');
        Route::any('/acceptOpenRidePassenger', 'API\OpenRidesController@acceptPassenger');
        Route::any('/denyPassengerOpenRide', 'API\OpenRidesController@denyPassenger');
        Route::any('/getOpenRideDates/{id}', 'API\OpenRidesController@getDates');
        Route::any('/bookOpenRide/{id}', 'API\OpenRidesController@requestjoin');
	});

	//Fetch Data
	Route::any('/getCountries', 'API\FetchController@getCountries');
	Route::any('/getStates', 'API\FetchController@getStates');
	Route::any('/countryCodes', 'API\FetchController@countryCodes');

	//User Login / Registration
	Route::any('/registerUser', 'IonicController@registerUser');
	Route::any('/makeLogin', 'API\UserIonicController@login');
	Route::any('/loginFacebook', 'IonicController@loginFacebook');
	Route::any('/getPublicProfile/{id}', 'IonicController@getPublicProfile');

	//Rides Routes
	Route::get('/setCurrentUser' , 'IonicController@setCurrentUser');

	//Car Details 
	Route::get('/getCarMake', 'IonicController@getCarMake');

	//Rides
	Route::any('/findRides', 'IonicController@findRides');
	Route::any('/getAirportRides', 'IonicController@getAirportRides');
	Route::any('/findRideInfo', 'IonicController@findRideInfo');
	Route::any('/rideDetails' , 'IonicController@rideDetails');
	

	//Settings
	Route::any('/forgotPassword', 'IonicController@forgotPassword');
	Route::get('/getAirportList', 'API\AirportRideController@getAirportList');

});