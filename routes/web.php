<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

//Errors
Route::get('error500', function () {
    throw new \Exception('Error 500');
});

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login/facebook', 'Auth\LoginController@redirectToProvider');

Route::get('/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/rides/search', 'RidesController@search')->name('rides.search');

Route::post('/rides/list', 'RidesController@list');

Route::get('/verifyemail/{token}', 'Auth\LoginController@verify');

Route::get('/about' , 'HomeController@about')->name('company.about');

Route::get('/contact' , 'HomeController@contact')->name('company.contact');

Route::get('/faq' , 'HomeController@faq')->name('company.faq');

Route::get('/privacy' , 'HomeController@privacy')->name('company.privacy');

Route::get('/code-of-conduct' , 'HomeController@codeofconduct')->name('company.code-of-conduct');

Route::get('/terms-of-use' , 'HomeController@terms')->name('company.terms');

Route::get('/press' , 'HomeController@press')->name('company.press');

Route::get('/opportunities' , 'HomeController@opportunities')->name('company.opportunities');

Route::post('/contact', 'HomeController@contact_update')->name('company.contact_update');

Route::get('/send_report', 'HomeController@send_report')->name('error.send_report');

Route::post('/post_report', 'HomeController@post_report')->name('error.post_report');

Route::get('/feedback', 'HomeController@feedback')->name('company.feedback');

Route::post('/post_feedback', 'HomeController@post_feedback')->name('company.post_feedback');

Route::get('/howtoridesurf', 'HomeController@howtoridesurf')->name('company.howtoridesurf');

Route::get('/homebass', 'HomeController@homebass')->name('search.homebass');

Route::get('/synapse', 'HomeController@searchSynapse')->name('search.synapse');

Route::post('/synapse/getRides', 'HomeController@searchSynapseRides');

Route::get('/homebass/filter', 'HomeController@searchHomeBase')->name('search.filterHomeBass');

Route::get('/synapse/filter', 'HomeController@searchSynapse')->name('search.filtersynapse');

Route::get('/rides/getAirports', 'RidesController@getAirports');

Route::post('/rides/searchAirport', 'RidesController@searchAirport');


/*=============================== Promotions ===============================*/
Route::get('/tailgate', 'LandingPageController@tailgatePage')->name('promotions.tailgate');


/*=============================== IONIC REST API JSON ===============================*/




/* ========== Auth ========== */
Route::group(['middleware' => ['auth']], function() {
    
    Route::get('adminlogsreviewing', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::resource('drivers','DriverController');

    Route::group(['prefix' => 'locations'], function () {
        Route::resource('locations', 'LocationController');
        Route::post('/fetch' , 'LocationController@fetch')->name('locations.fetch');
    });

    Route::group(['prefix' => 'connections'], function () {
        Route::resource('connections','ConnectionsController');
        Route::post('/fetch' , 'ConnectionsController@fetch')->name('connections.fetch');
    });



    Route::group(['prefix' => 'rides'], function () {
        Route::resource('rides','RidesController');
        Route::post('/destroy/{id}', 'RidesController@destroy');
        Route::get('/getCurrentRides', 'DashboardUserController@getCurrentRides');
        Route::get('/getPastRides', 'DashboardUserController@getPastRides');
        Route::post('/fetch' , 'RidesController@fetch')->name('rides.fetch');
        Route::post('/createRideForm', 'RidesController@createRideForm')->name('rides.createRideForm');
        Route::post('/StoreInformation', 'RidesController@StoreInformation')->name('rides.StoreInformation');
        Route::post('/join/{id}', 'RidesController@join')->name('rides.join');
        Route::put('/storejoin/{id}', 'RidesController@storejoin')->name('rides.storejoin');
        Route::delete('/destroyUserRide/{id}', 'RidesController@destroyUserRide')->name('rides.destroyUserRide');
        Route::get('/findride', 'RidesController@findride')->name('rides.findride');
        Route::get('/payment/{id}', 'RidesController@payment')->name('rides.payment');
        Route::get('/successpayment', 'RidesController@successpayment')->name('rides.successpayment'); 
        Route::post('/request/{id}/{price}', 'RidesController@request')->name('rides.request'); 
        Route::post('/acceptPassenger/{id}/{user}', 'RidesController@acceptPassenger')->name('rides.acceptPassenger');
        Route::post('/denyPassenger/{id}/{user}', 'RidesController@denyPassenger')->name('rides.denyPassenger');
        Route::get('/viewUserProfile/{id}', 'DashboardUserController@viewUserProfile')->name('rides.viewUserProfile');
        Route::resource('open-rides', 'OpenRideController');
        Route::get('/fetchPurpose', 'OpenRideController@fetchPurposes');
        Route::get('/openRide/join/{id}', 'OpenRideController@join')->name('rides.openRides');
        Route::post('/openRide/join/{id}','OpenRideController@postJoin');
        Route::any('/openRide/destroy/{id}','OpenRideController@destroy');
        Route::post('/openRide/accept/{id}','OpenRideController@acceptPassenger')
        ->name('rides.acceptOpenRide');
        Route::post('/openRide/deny/{id}','OpenRideController@denyPassenger')->name('rides.denyOpenRide');
        Route::post('/openRide/status/{id}', 'OpenRideController@statusChange');
        Route::post('/open-rides/update/{id}', 'OpenRideController@update');
        //Airports
        Route::resource('airports', 'AirportRideController');
    });


  
    Route::resource('/ride_request', 'RideRequestController');


    Route::group(['prefix' => 'dashboarduser'], function () {

        //Fetch Information
        Route::resource('dashboarduser','DashboardUserController');
        Route::post('/fetchVehicleMake','DashboardUserController@fetchVehicleMake')
        ->name('dashboarduser.fetchVehicleMake');
        Route::post('/fetchModel','DashboardUserController@fetchModel')
        ->name('dashboarduser.fetchModel');


        Route::post('/uploadProfileImage','DashboardUserController@uploadProfileImage')
        ->name('dashboarduser.uploadProfileImage');
        Route::post('/fetchType','DashboardUserController@fetchType')
        ->name('dashboarduser.fetchType');
        Route::post('/fetchColor','DashboardUserController@fetchColor')
        ->name('dashboarduser.fetchColor');
        Route::get('/fetchAllData', 'DashboardUserController@fetchAllData')->name('dashboarduser.fetchAllData');
        Route::get('/booked', 'DashboardUserController@booked')->name('dashboarduser.booked');
        Route::get('/payments', 'DashboardUserController@payments')->name('dashboarduser.payments');
        Route::get('/reportRide/{id}', 'DashboardUserController@reportRide')->name('dashboarduser.reportRide');
        Route::post('/storeReport/{id}', 'DashboardUserController@storeReport')->name('dashboarduser.storeReport');
        Route::get('/message', 'DashboardUserController@message')->name('dashboarduser.message');
        Route::get('/profile', 'DashboardUserController@profile')->name('dashboarduser.profile');
        Route::get('/reviews', 'DashboardUserController@reviews')->name('dashboarduser.reviews');
        Route::resource('user-reviews', 'ReviewsController');

        Route::get('/showCancelride/{id}', 'DashboardUserController@showCancelride')
        ->name('dashboarduser.cancelUserRide');
        Route::get('/openRides', 'DashboardUserController@openRides')->name('dashboarduser.openRides');
        Route::get('/ridesCurrent', 'DashboardUserController@ridesCurrent')->name('dashboarduser.ridesCurrent');
        Route::get('/ridesPast', 'DashboardUserController@ridesPast')->name('dashboarduser.ridesPast');
        Route::get('/becomeDriver', 'DashboardUserController@becomeDriver')->name('dashboarduser.becomeDriver');
        Route::post('/createDriver', 'DashboardUserController@createDriver')->name('dashboarduser.createDriver');
        Route::post('/saveRegistration', 'DriverController@saveRegistration');
        Route::post('/saveInsurance', 'DriverController@saveInsurance');
        Route::post('/saveLicense', 'DriverController@saveLicense');
        Route::post('/saveCarImage', 'DriverController@saveCarImage');
        Route::post('/saveDriver', 'DriverController@store');
        Route::put('/updateDriver', 'DriverController@update');

        //Preferences 
        Route::resource('user_prefereces', 'UserPreferencesController');
        Route::get('/editPreferences', 'UserPreferencesController@editPreferences')
        ->name('dashboarduser.editPreferences');
        Route::any('/updatePreferences', 'UserPreferencesController@updatePreferences')
        ->name('dashboarduser.updatePreferences');

        //Verifications
        Route::get('/sendverification', 'DashboardUserController@sendVerification')
        ->name('dashboarduser.sendverification');

        //Passwords
        Route::get('/updatepassword', 'DashboardUserController@updatepassword')->name('dashboarduser.updatepassword');
        Route::get('/cardetails', 'DashboardUserController@cardetails')->name('dashboarduser.cardetails');
        Route::post('/patchpassword', 'DashboardUserController@patchpassword')
        ->name('dashboarduser.patchpassword');


    });        

    Route::group(['prefix' => 'messages'], function () {
        Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
        Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
        Route::get('contact/{id}', ['as' => 'messages.contact', 'uses' => 'MessagesController@contact']);
        Route::get('contactUser/{id}/{ride}', ['as' => 'messages.contactUser', 
            'uses' => 'MessagesController@contactUser']);
        Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
        Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
        Route::get('filter/{number}', ['as' => 'messages.filter', 'uses' => 'MessagesController@filter']);
        Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
        Route::delete('{id}', ['as' => 'messages.delete', 'uses' => 'MessagesController@delete']);
    });

    Route::group(['prefix' => 'reviews'], function () {
        Route::resource('reviews', 'ReviewsController');
        Route::get('/rate/{user}/{ride}', 'ReviewsController@rate')->name('reviews.rate');
        Route::put('storeRate', 'ReviewsController@storeRate')->name('reviews.storeRate');
    });


    /*========================= ADMIN DASHBOARD =========================*/
    Route::group(['prefix' => 'dashboardadmin'], function () {

        Route::resource('dashboardadmin', 'DashboardAdminController');
        Route::get('/language-label', 'DashboardAdminController@languageLabel')
               ->name('dashboardadmin.language-label');
        Route::get('/seachLabel', 'DashboardAdminController@seachLabel')->name('dashboardadmin.seachLabel');
        Route::get('/addNewLabel', 'DashboardAdminController@addNewLabel')->name('dashboardadmin.addNewLabel');
        Route::post('/createNewLabel', 'DashboardAdminController@createNewLabel')->name('dashboardadmin.createNewLabel');
        Route::get('/editLanguageLabel/{id}', 'DashboardAdminController@editLanguageLabel')->name('dashboardadmin.editLanguageLabel');
        Route::put('/updateLanguageLabel/{id}', 'DashboardAdminController@updateLanguageLabel')->name('dashboardadmin.updateLanguageLabel');
        Route::delete('/deleteLanguageLabel/{id}', 'DashboardAdminController@deleteLanguageLabel')->name('dashboardadmin.deleteLanguageLabel');
        Route::get('/contactList', 'DashboardAdminController@contactList')->name('dashboardadmin.contactList');
        Route::get('/viewContactMessage/{id}', 'DashboardAdminController@viewContactMessage')->name('dashboardadmin.viewContactMessage');
        Route::post('/replyMessage/{id}', 'DashboardAdminController@replyMessage')->name('dashboardadmin.replyMessage');
        Route::resource('/roles','RoleController');
        Route::resource('/contact','ContactController');


        Route::get('/getAiports', 'AirportRideController@getList')->name('airport.list');
        Route::get('/createAirport', 'AirportRideController@createNewAirport')->name('airport.createNewAirport');
        Route::get('/airport/edit/{id}',  'AirportRideController@editSingleAirport')->name('airport.editSingle');
        Route::post('/airport/store',  'AirportRideController@storeNewAirport')->name('airport.storeNewAirport');
        Route::post('/aiport/update/{id}', 'AirportRideController@updateAirport')->name('airport.updateAirport');
//        Route::get('/create-airport-ride', 'AirportRideController@create');

        //Users 
        Route::resource('/users','UserController');


        Route::get('/requestDriversList', 'UserController@requestDriversList')->name('dashboardadmin.requestDriversList');
        Route::get('/filterUser', 'UserController@filterUser')->name('users.filterUser');
        Route::post('/importUsers', 'UserController@importUsers')->name('users.importUsers');
        Route::delete('/users/destroyFromAdmin/{id}', 
            'UserController@destroyFromAdmin')->name('users.destroyFromAdmin');
        Route::post('/updateDriverInformation/{id}', 'UserController@updateDriverInformation')
              ->name('users.updateDriverInformation');
        Route::any('/updateUsers/', 'UserController@updateUsers')->name('users.updateUsers');

        //Driver
        Route::any('/driverList', 'DriverController@driverlist')->name('driver.list');
        Route::any('/emptyDriver/{id}', 'DriverController@emptyDriver')->name('driver.emptyDriver');
        Route::any('/updateSingleImage/{id}', 'UserController@updateSingleImage')->name('driver.updateImage');
        Route::any('/restateDriver/{id}' ,'DriverController@reApproveDriver')->name('driver.acceptDriver');
        Route::any('/exportDrivers', 'DashboardAdminController@DriverExport')->name('driver.export');

        //Rides - Payments 
        Route::get('/payToDrivers','DashboardAdminController@payToDrivers')->name('dashboardadmin.payToDrivers');
        Route::post('/exportPayUser','DashboardAdminController@exportPayUser')->name('dashboardadmin.exportPayUser');
        Route::post('/driverPaid','DashboardAdminController@driverPaid')->name('dashboardadmin.driverPaid');
        Route::get('/listRides','DashboardAdminController@listRides')->name('dashboardadmin.listRides');
        Route::get('/filterRides','DashboardAdminController@filterRides')->name('dashboardadmin.filterRides');
        Route::get('/manageUsers/{id}','DashboardAdminController@manageUsers')->name('dashboardadmin.manageUsers');

        // Makes
        Route::get('/addMake','DashboardAdminController@addMake')->name('dashboardadmin.addMake');
        Route::get('/filterMake','DashboardAdminController@filterMake')->name('dashboardadmin.filterMake');
        Route::post('/createMake','DashboardAdminController@createMake')->name('dashboardadmin.createMake');
        Route::get('/listMake','DashboardAdminController@listMake')->name('dashboardadmin.listMake');
        Route::get('/editMake/{id}','DashboardAdminController@editMake')->name('dashboardadmin.editMake');
        Route::put('/updateMake/{id}','DashboardAdminController@updateMake')->name('dashboardadmin.updateMake');
        Route::delete('/destroyMake/{id}','DashboardAdminController@destroyMake')->name('dashboardadmin.destroyMake');

        //Model
        Route::resource('/model', 'ModelController');
        Route::get('/model/filterModel', 'ModelController@filterModel')->name('model.filterModel');

        //Reports
        Route::get('/listReport', 'DashboardAdminController@listReport')->name('reports.index');
        Route::get('/reportReply/{id}', 'DashboardAdminController@reportReply')->name('reports.reportReply');
        Route::get('/filterReport', 'DashboardAdminController@filterReport')->name('reports.filterReport');
        Route::post('/updateReport/{id}' , 'DashboardAdminController@updateReport' )->name('reports.updateReport');

        //Ride Request
        Route::get('/rideRequest', 'RideRequestController@showAll')->name('request.showAll');
        Route::get('/showRideRequest/{id}', 'RideRequestController@showRequest')->name('request.showsingle');

        //IP Request
        Route::get('/iprequest', 'IPRequestController@index')->name('ip.index');

        //Preferences
        Route::resource('preference', 'PreferencesController');
        Route::any('/assignPreferneces', 'PreferencesController@assignPreferneces')
        ->name('assignPreferneces');

        //SearchLogs
        Route::get('/search-log', 'SearchLogController@index')->name('search.index');

        //Statistics
        Route::get('/weekly', 'StatisticController@weekly')->name('stat.weekly');

        //Trip Purposes
        Route::get('/trip-purposes', 'TripPurposeController@index')->name('purpose.index');
        Route::get('/trip-purposes/create', 'TripPurposeController@create')->name('purpose.create');
        Route::get('/trip-purposes/edit/{id}', 'TripPurposeController@edit')->name('purpose.edit');
        Route::post('/trip-purposes/store', 'TripPurposeController@store')->name('purpose.store');
        Route::put('/trip-purposes/update/{id}', 'TripPurposeController@update')->name('purpose.update');
        Route::delete('/trip-purposes/delete/{id}', 'TripPurposeController@delete')->name('purpose.delete');
    });

    Route::post('/users/approveDriver/{id}', 'UserController@approveDriver')->name('users.approveDriver');
    Route::post('/users/denyDriver/{id}', 'UserController@denyDriver')->name('users.denyDriver');
    

    /******  Delete Users ******/
    Route::delete('/dashboardadmin/admindeleteuser/{id}', 'DeleteUsersController@destroyFromAdmin')
    ->name('dashboardadmin.admindeleteuser');

    Route::delete('/dashboarduser/deleteuser/{id}', 'DeleteUsersController@destroyFromUser')
           ->name('dashboarduser.deleteuser');


    /* ================================== IONIC APP ================================== */


});
