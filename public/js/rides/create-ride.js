/* This is the Create Ride JS 
*/

// Global Variables
var map;
var stops = 0; // Keeps tracks of how many stops 
var inputMyDateFrom = document.querySelector('input#dateFrom');
let arrayStop = {};
let arrayStartStop = {}


//Start the Map
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      mapTypeControl: false,
      center: {lat: 27.6648, lng: -81.5158},
      zoom: 5
    });

    new AutocompleteDirectionsHandler(map);
}
/**
* @constructor
*/
function AutocompleteDirectionsHandler(map) {
	this.map = map;
	this.originPlaceId = null;
	this.destinationPlaceId = null;
	this.travelMode = 'DRIVING';
	var originInput = document.getElementById('from-fetch');
	var destinationInput = document.getElementById('to-fetch');
	this.directionsService = new google.maps.DirectionsService;
	this.directionsDisplay = new google.maps.DirectionsRenderer;
	this.directionsDisplay.setMap(map);

	var originAutocomplete = new google.maps.places.Autocomplete(
	originInput);
	var destinationAutocomplete = new google.maps.places.Autocomplete(
	destinationInput);


	this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
	this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

}

// Sets a listener on a radio button to change the filter type on Places
// Autocomplete.
AutocompleteDirectionsHandler.prototype.setupClickListener = function(id, mode) {
	var radioButton = document.getElementById(id);
	var me = this;
	radioButton.addEventListener('click', function() {
	  me.travelMode = mode;
	  me.route();
	});
};

AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {

	var me = this;
	autocomplete.bindTo('bounds', this.map);
	autocomplete.addListener('place_changed', function() {
	  var place = autocomplete.getPlace();
	  if (!place.place_id) {
	    window.alert("Please select an option from the dropdown list.");
	    return;
	  }
	  if (mode === 'ORIG') {
	    me.originPlaceId = place.place_id;
	  } else {
	    me.destinationPlaceId = place.place_id;
	  }
	  me.route();
	});

};

//Route handler when there are no stop overs
AutocompleteDirectionsHandler.prototype.route = function() {
	if (!this.originPlaceId || !this.destinationPlaceId) {
	  return;
	}
	var me = this;
	var waypts = [];

	if(stops > 0){
		var stopInput = document.getElementById('StopOver');

		var autocomplete = new google.maps.places.Autocomplete(stopInput);
	    autocomplete.inputId = $(this).attr('id');

	    
	    //Google Map function after input has been added
	    google.maps.event.addListener(autocomplete, 'place_changed', function () {
	        var geocoder = new google.maps.Geocoder();

	        var place = autocomplete.getPlace();
	        var stop = place.formatted_address;
	        var waypts = [];

	        var start = document.getElementById("from-fetch").value;
	        var end = document.getElementById("to-fetch").value;
	        
	        if(end){
	        	//codeAddress(geocoder, map, stop);
	        	var latlng;
	        	
			    // Make route and display new route
	        	
	        	geocoder.geocode({'address': stop}, function(results, status) {
			      if (status === 'OK') {

			        var latitude = results[0].geometry.location.lat();
		            var longitude = results[0].geometry.location.lng();
		            latlng = new google.maps.LatLng(latitude, longitude);

		            waypts.push({
				        location: {lat: latitude, lng: longitude},
				        stopover: true,
				      });
			       	
			       	setTimeout(function() {
			       		calcRoute(start, waypts, end, stop)
			       	}, 2000);


			      } else {
			        console.log('Geocode was not successful for the following reason: ' + status);
			      }
			    });
				  

	        }
	        else{
	        	var data = '<div class="alert alert-danger" id="displayError" role="alert">'+
								'<p> Please add an End Destination </p>'+
							'</div>';
				jQuery('#alertError').append(data).fadeIn(1000);
				jQuery('#displayError').fadeOut(4500);
	        }
	    });
	}// End of if there stop overs 


	this.directionsService.route({

	  origin: {'placeId': this.originPlaceId},
	  destination: {'placeId': this.destinationPlaceId},
	  waypoints : waypts,
	  travelMode: this.travelMode

	}, function(response, status) {
	  if (status === 'OK') {
	    me.directionsDisplay.setDirections(response);

	    var distance = me.directionsDisplay.directions.routes[0].legs[0].distance.text;
	    var distance_num = me.directionsDisplay.directions.routes[0].legs[0].distance.value;
	    var duration = me.directionsDisplay.directions.routes[0].legs[0].duration.text;
	    var duration_num = me.directionsDisplay.directions.routes[0].legs[0].duration.value;
	    var start = me.directionsDisplay.directions.routes[0].legs[0].start_address;
	    var end = me.directionsDisplay.directions.routes[0].legs[0].end_address;

	    console.log(duration_num);
	    console.log(duration);

	    jQuery('#infoTrip').remove();
	    jQuery('#infoInput').remove();
	    jQuery('#stopOver').remove();

	    var cityFrom = parseLocation(start);
	    var cityTo = parseLocation(end);

	    var data = '<div id="infoTrip"><p><br />Duration: <strong id="completeDistance">'
	    			+duration+' </strong> </p>' +
	    			'<p> Distance: <strong id="durationMiles"> '+distance+'</strong> </p> <br> </div>';
	    var form = '<div id="infoInput"> <input name="duration" id="duration" value="'+duration_num+'" style="visibility:hidden; display:none;">'+
	    			'<input name="distance" id="distance_num" value="'+distance_num+'" style="visibility:hidden; display:none;">'+
	    			'<input name="cityFrom" id="cityFrom" value="'+cityFrom+'" style="visibility:hidden; 	display:none;">'+
	    			'<input name="cityTo" id="cityTo" value="'+cityTo+'" style="visibility:hidden; 	display:none;">'+
	    			'</div>';
	    jQuery('#ChangeMap').append(data);
	    jQuery('#from').append(form);

	    arrayStartStop ={
	    	'start': cityFrom,
	    	'stop' : cityTo,
	    	'distance' : distance_num
	    };

	  } else {
	    window.alert('Directions request failed due to ' + status);
	  }
	});

};// End of Route Handler

      	
//Add Marker of Stop Over
function codeAddress(geocoder, address) {

	var waypts = [];

    geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
        var latlng = new google.maps.LatLng(latitude, longitude);

        waypts.push({
	        location: {lat: latitude, lng: longitude},
	        stopover: true,
	      });


      } else {
        console.log('Geocode was not successful for the following reason: ' + status);
      }
    });

    return waypts;
}



//jQuery
$(document).ready(function(){

	// When there is a start and end 
	// User can add stop over
	$('#to-fetch').on('change', function() {
		$('#addStopOver').prop('disabled', false);
	});

	$(function(){
	    var dtToday = new Date();
	    
	    var month = dtToday.getMonth() + 1;
	    var day = dtToday.getDate() + 1;
	    var year = dtToday.getFullYear();
	    if(month < 10)
	        month = '0' + month.toString();
	    if(day < 10)
	        day = '0' + day.toString();
	    
	    var maxDate = year + '-' + month + '-' + day;
	    
	    $('#dateFrom').attr('min', maxDate);
	});

	//Create Stop Over Input
	$('#addStopOver').on('click', function(e){
		e.preventDefault();
		
		var data =  '<div class="form-with-label" id="stop-'+stops+'">'+
					'<span class="ride-label"> Stop Over</span>'+
					'<div class="frm-ride first" >'+
					'<input type="text" id="StopOver-'+stops+'" class="ride-to chunkypuff"' +
					'placeholder="City, town, or public location">'+
				    '</div> </div>';
		

		$('#stopsAppend').append(data);

		var stopInput = document.getElementById('StopOver-' + stops);

		var autocomplete = new google.maps.places.Autocomplete(stopInput);
	    autocomplete.inputId = $(this).attr('id');
	    stops++; // Increase how many stops are
	    
	    //Google Map function after input has been added
	    google.maps.event.addListener(autocomplete, 'place_changed', function () {
	        var geocoder = new google.maps.Geocoder();

	        var place = autocomplete.getPlace();
	        var stop = place.formatted_address;
	        var start = document.getElementById("from-fetch").value;
	        var end = document.getElementById("to-fetch").value;
	        var waypts = [];
	        		      		        
	        
	        if(end){
	        	//codeAddress(geocoder, map, stop);
	        	var latlng;
	        	
			    // Make route and display new route
	        	// Route Map when there are stop overs 
	        	geocoder.geocode({'address': stop}, function(results, status) {
			      if (status === 'OK') {
			        var latitude = results[0].geometry.location.lat();
		            var longitude = results[0].geometry.location.lng();
		            latlng = new google.maps.LatLng(latitude, longitude);

		            for(var i=0; i < stops; i++){
		            	
		            	waypts.push({
					        location: document.getElementById("StopOver-"+i).value,
					        stopover: true,
					      });
		            }
			       	
			       	calcRoute(start, waypts, end, stop);


			      } else {
			        console.log('Geocode was not successful for the following reason: ' + status);
			      }
			    });
				  
	        	/*
	        	var marker = new google.maps.Marker({

	                map: map,
	                position: latlng,
	                title: 'Stop Over'

	            });*/

	        }
	        else{
	        	var data = '<div class="alert alert-danger" id="displayError" role="alert">'+
								'<p> Please add an end location </p>'+
							'</div>';
    			jQuery('#alertError').append(data).fadeIn(1000);
    			jQuery('#displayError').fadeOut(4500);
	        }
	    });

	});

	$('#roundtrip').on('click', function(){
	 	
	 	
	 	//Check that the input if on or off
	 	if($('#roundtrip').prop("checked") == true){
		 	var datesData = '<div class="form-with-label" id="twowayDiv">'+
                        '<span class="ride-label">Return Date</span>'+
                        '<div class="frm-ride second">'+
                         '<input class="departure chunkypuff" name="dateTo" type="date" id="dateTo">'+
                        '</div> </div>';
            var check = jQuery('#price').val();
            if(check != null){
            	var clock = '<span class="ride-label">Return Time</span>' +
			         		'<div class="frm-ride fifth"> <input type="time" name="returning" id="returning"'+             
			         		'class="timing chunkypuff"> </div>';
			    $('#returnTime').append(clock).fadeIn(150);
            }
			$('#dateChange').html(datesData).hide().fadeIn(150);



			var inputMyDateTo = document.querySelector('input#dateTo');

			var hrs='';
		    var mins='';
		    for(var i = 0; i < 24; i++)
		    {
		       hrs += '<option value="'+(i+1)+'">'+(i+1)+'</option>';
		    }
		    $('.hours').html(hrs);

		    for(var x = 0; x < 60; x++)
		    {
		      mins += '<option value="'+x+'">'+x+'</option>';
		    }
		    $('.minutes').html(mins); 

		    var dtToday = new Date();
		    var month = dtToday.getMonth() + 1;
		    var day = dtToday.getDate()+1;
		    var year = dtToday.getFullYear();
		    if(month < 10)
		        month = '0' + month.toString();
		    if(day < 10)
		        day = '0' + day.toString();
		    
		    var maxDate = year + '-' + month + '-' + day;
		    
		    $('#dateTo').attr('min', maxDate);
		    
	    }
	    else if($('#roundtrip').prop("checked") == false){
	    	console.log('is off');
	    	// Turn off Return date

	    	jQuery('#dateTo').val("");
	    	jQuery('#dateTo').prop('disabled', true);
	    	jQuery('#dateTo').css('background-color','#29ca8e');
	    	var check = jQuery('#price').val();
            if(check != null){
            	jQuery('#returning').prop('disabled', true);
            	jQuery('#returning').css('background-color','#29ca8e');
            }
	    }


	});

	$('#changeTrip').change(function(){
		
		$('#changeTrip').fadeOut();
		$('#twowayDiv').fadeOut();
		$('#round-one-trip').html('<span id="changeTrip"></span>');
		var dataDates = '<div class="form-group" id="roundtripDiv">'+
						'<label>Round Trip?</label>'+
						'<input class="form-control" name="roundtrip" type="checkbox" id="roundtrip">'+
						'</div>';
		$('#dateChange').html(dataDates).hide().fadeIn(100);
	});

});

function refreshMap(){	
	
	location.reload();
}	
//Create the second part of ride creation
// After user clicks on continue
function createRideForm(){

	var fromDestination = document.getElementById('from-fetch').value;
	var toDestination = document.getElementById('to-fetch').value;
	var check = jQuery('#dateTo').val();
	var dateFrom = jQuery('#dateFrom').val();
	
	if(fromDestination == "" || toDestination == "" || dateFrom == ""){
		var data = "Please complete all the input values";
		var data = '<div class="alert alert-danger" id="displayError" role="alert">'+
							'<p> Please fill out all fields </p>'+
						'</div>';
		jQuery('#alertError').append(data).fadeIn(1000);
		jQuery('#displayError').fadeOut(4000);
		if(fromDestination == ""){
			jQuery('#from-fetch').addClass('errorClass');
		}
		if(toDestination == ""){
			jQuery('#to-fetch').addClass('errorClass');
		}
		if(dateFrom == ""){
			jQuery('#dateFrom').addClass('errorClass');
		}
	}
	else{
		jQuery('#from-fetch').removeClass('errorClass');
		jQuery('#to-fetch').removeClass('errorClass');
		jQuery('#dateFrom').removeClass('errorClass');
        jQuery('#buttonsChange').fadeOut();
        
         
        var html = '<span class="ride-label"> Seats Offered </span> <div class="frm-ride third">'+
         			'<input class="seats-av chunkypuff" name="seats" id="seats" type="number" value="1" min="1" max="4" required> </div>'+
         			
         			'<span class="ride-label"> Suggested Seat Price </span> <div class="frm-ride fourth">'+
         			'<input  class="pricing chunkypuff" id="price" name="price" min="1" max="200" type="number" required>'+
         			'</div>';

         			for(var i=0; i < stops; i++){
         				html = html + '<span class="ride-label">' +
         				'Suggested Price from '+fromDestination+' to ' + arrayStop[i]['name'] +': </span> '+
         				'<div class="frm-ride fourth">'+
         				'<input  class="pricing" value="'+ arrayStop[i]['price_from'] +'"'+
         				'id="price_stop_over_from-'+i+'" type="number"  min="1" max="200" required>'+
         				'</div>'+
         				'<span class="ride-label">' +
         				'Suggested Price From '+ arrayStop[i]['name'] +' to '+toDestination+' </span> <div class="frm-ride fourth">'+
         				'<input  class="pricing" min="1" max="200" id="price_stop_over_to-'+i+'" value="'+arrayStop[i]['price_to']+'" type="number" required>'+
         				'</div>';
         			}
         			html = html + '<span class="ride-label"> Luggage Size </span> <div class="frm-ride six">'+
         			'<select name="luggage_size" id="luggage_size" class="form-control chunkypuff" style="padding: 5px !important;">'+
	         			'<option selected value="Small"> Small </option>'+
	         			'<option value="Medium"> Medium </option>'+
	         			'<option value="Large"> Large </option>'+
         			'</select>'+
         			'</div>'+

         			'<span class="ride-label"> Luggage Amount</span> <div class="frm-ride seven">'+
         			'<input  class="luggage_amount chunkypuff" min="1" max="5" id="luggage_amount" name="luggage_amount" value="1" type="number">'+
         			'</div>'+

         			'<span class="ride-label"> Departure Time</span>'+ 
         			'<div class="frm-ride fifth">' +
         			'<input type="time" id="time" name="time" class="timing chunkypuff" required> </div>'+
         			'<div id="returnTime"></div>'+
         			'<div class="frm-ride">';
         			if(check != null){

         				html = html + '<span class="ride-label">Return Time</span>' +
         				'<div class="frm-ride fifth">'+
         				'<input type="time" name="returning" id="returning"  class="timing" required> </div>';

         			} 
         			if(gender != 'Male'){
	         			html = html + '<span class="ride-label">Women only?</span>'+
	         			'<div class="frm-ride six"> '+
	         			'<select name="ladies_only" id="ladies_only" class="form-control">'+
	         			'<option value="No" selected> No </option>'+
	         			'<option value="Yes"> Yes </option> </select> </div>';
         			}else{
         				html = html + '<input type="text" id="ladies_only" name="ladies_only" style="display:none">';
         			}
         			html = html +'<span class="ride-label">Trip Info:</span>'+
         			'<textarea class="form-control pufftext" id="description" name="description" required cols="5">' +
         			'</textarea> </div>';

         var buttonContinue = '<button type="submit" class="btn btn-primary" onclick="StoreInformation();">'+
         					   'Continue</button> ';

        var distance_num = document.getElementById('distance_num').value;
     	
        var numbers = distance_num * 0.000621371192;

        
        var tripPurpose = document.getElementById('tripPurpose').value;
        if(tripPurpose == 8){
        	var price = Math.ceil(numbers * 0.85);
        }else{
        	var price = Math.ceil(numbers * 0.09);
        }

        // TODO 
        // Create an algorithim with small incremets of 100, 200, 500, 1000, 1500

        jQuery('#extraInfo').append(html).hide().fadeIn(200);
        jQuery('#buttonsChange').html(buttonContinue).hide().fadeIn(200);
        jQuery('#price').val(price).fadeIn(400);


    }// End of checking if variables are null
    jQuery("#time").val("10:00:00");
    if(check != null ){
    	jQuery("#returning").val("10:00:00");
    }

}//End of Create Ride

	function StoreInformation(){

		var dateFrom = document.getElementById('dateFrom').value;
		if(jQuery('#dateTo').length ){
			var dateTo =  document.getElementById('dateTo').value;
			var returning = document.getElementById('returning').value;
		}
		var fromDestination = document.getElementById('from-fetch').value;
		var toDestination = document.getElementById('to-fetch').value;
		var price = document.getElementById('price').value;
		var seats = document.getElementById('seats').value;
		var description = document.getElementById('description').value;
		var time = document.getElementById('time').value;
		var cityFrom = document.getElementById('cityFrom').value;
		var cityTo = document.getElementById('cityTo').value;
		var luggage_amount = document.getElementById('luggage_amount').value;
		var luggage_size = document.getElementById('luggage_size').value;
		var ladies_only = document.getElementById('ladies_only').value;
		var duration = document.getElementById('duration').value;
		var tripPurpose = document.getElementById('tripPurpose').value;
		for(var i=0; i < arrayStop.length; i++){
			arrayStop[i]['price_from'] = document.getElementById('price_stop_over_from-'+i).value;
			arrayStop[i]['price_to'] =  document.getElementById('price_stop_over_to-'+i).value;
		}

		console.log(description);

		if(description == null || description == ""){
			var data = '<div class="alert alert-danger" id="displayError" role="alert">'+
								'<p> Please add a description </p>'+
							'</div>';
    			jQuery('#alertError').append(data).fadeIn(1000);
    			jQuery('#displayError').fadeOut(4500);
			return false;
		}
		$.ajax({
			type:"POST",
			headers: {
			  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:sentUrl,
			method:"POST",
			dataType: "json",
			data:{
				fromDestination : fromDestination,
				toDestination : toDestination, 
				dateFrom : dateFrom,
				dateTo : dateTo,
				seats : seats,
				price : price,
				description : description,
				time : time,
				returning : returning,
				cityFrom : cityFrom,
				cityTo : cityTo,
				duration : duration,
				stopOvers: arrayStop,
				luggage_amount:luggage_amount,
				luggage_size:luggage_size,
				ladies_only:ladies_only,
				tripPurpose:tripPurpose,
				_token : $('meta[name="csrf-token"]').attr('content')
			},
			success:function(data){

				jQuery('#dashboard').fadeOut();
				var info = '<div class="col-md-12 simple-page-pad">';
				if(data.state != 'FL' && data.state != 'CA'){
					info = info + '<div class="florida-launch">'+
									'<p> This ride is not in our official launch markets, be aware that you might not get any passengers.</p>' +
									'<p> Help us bring Ridesurf to your area! </p>'+
									'<p> (Click <a href="http://eepurl.com/ggIG1b">here</a> to bring Ridesurf to your state!)</p>'+
								   '</div>';
					
				}

				info = info + '<div class="text-center"> <h4> Your trip has been posted!</h4>' +
							'<a href="/dashboarduser/dashboarduser" class="btn btn-primary">Return to Dashboard</a></div></div>';
				jQuery('#form-sidebar').append(info);
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			},
			error: function (xhr, status, error) {

				console.log(xhr.responseText);
				var err = JSON.parse(xhr.responseText);
				var data = '<div class="alert alert-danger" id="displayError" role="alert">'+
								'<p> '+err.message+'</p>'+
							'</div>';
    			jQuery('#alertError').append(data).fadeIn(1000);
    			jQuery('#displayError').fadeOut(4500);
			}
		});
	}


// Calculate Route and Display when there is a stop over
function calcRoute(start,stop, end, cityAddress) {
	 var directionsService = new google.maps.DirectionsService();
	 var request = {
	    origin:start,
	    destination:end,
	    waypoints:stop,
	    travelMode: google.maps.TravelMode.DRIVING
	  };
	  var directionsDisplay = new google.maps.DirectionsRenderer;
	  var map = new google.maps.Map(document.getElementById('map'), {
	          center: {lat: -37.0902, lng: 95.7129},
	          zoom: 6
	        });
	  directionsService.route(request, function(result, status) {
	    if (status == google.maps.DirectionsStatus.OK) {

	      directionsDisplay.setMap(map);
	      directionsDisplay.setDirections(result);
	      console.log(result.routes);

	      var cityStopOver = parseLocation(cityAddress);

	      var totoalTime = result.routes[0].legs[1].duration.value + result.routes[0].legs[0].duration.value;

		  var miles = Math.ceil(arrayStartStop['distance'] * 0.000621371192) + " Miles";

		  jQuery('#durationMiles').html(miles);
		  jQuery('#completeDistance').html(formatted);
		  jQuery('#duration').html(arrayStartStop['distance']);

 

		  //Distance Stop Over to Point B
		  var distance_to = result.routes[0].legs[stops].distance.value * 0.000621371192; // Transform Meters to Miles

		  //Distance Point A to Stop Over
		  var distance_stop_from = (arrayStartStop['distance'] - result.routes[0].legs[stops].distance.value)  * 0.000621371192; // Transform Meters to Miles

		  arrayStop[stops-1] = {
		  	'price_from' : Math.ceil(distance_stop_from * 0.09),
		  	'price_to' : Math.ceil(distance_to * 0.09),
		  	'duration' : result.routes[0].legs[0].duration.value,
		  	'distance_stop_from' : distance_stop_from,
		  	'distance_stop_to' : distance_to,
		  	'name' : cityStopOver,
		  	'address' : cityAddress
		  };

		  console.log(arrayStop);
	      
	    }
  });

}

function parseLocation(city){
	var parseFrom = city.split(',');

  	if(parseFrom.length == 5){
    	var parse = parseFrom[2] + ", "+ parseFrom[3].match(/[a-zA-Z]+/g);
  	}
  	if(parseFrom.length == 4){  
    	var parse = parseFrom[1] + ", "+ parseFrom[2].match(/[a-zA-Z]+/g);
  	}
  	if(parseFrom.length == 3){
    	var parse = parseFrom[0] + ", "+ parseFrom[1].match(/[a-zA-Z]+/g);
  	}else{
  		var parse = city;
  	}
	return parse;
}