<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
	protected $fillable = ['make_id', 'model_id', 'type_id', 'color_id', 'year', 
						   'vehicle_registration', 'vehicle_tag'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    
    }
}
