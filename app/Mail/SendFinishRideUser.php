<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendFinishRideUser extends Mailable
{
    use Queueable, SerializesModels;

    public $driver, $driver_id, $name,$rideTo,$rideFrom,$date,$rides_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($driver, $driver_id, $name,$rideTo,
        $rideFrom,$date,$rides_id)
    {   
        $this->driver = $driver;
        $this->driver_id = $driver_id;
        $this->name = $name;
        $this->rideTo = $rideTo;
        $this->rideFrom = $rideFrom;
        $this->date = $date;
        $this->rides_id = $rides_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Next steps once your ride is over')
                    ->view('email.rideFinishedUser');
    }
}
