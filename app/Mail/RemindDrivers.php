<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemindDrivers extends Mailable
{
    use Queueable, SerializesModels;
    public $driver, $expired;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($driver, $expired)
    {
        $this->driver = $driver;
        $this->expired = $expired;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Driver Information is Expired')
                    ->view('email.driverReminder');
    }
}
