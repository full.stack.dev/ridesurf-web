<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preferences extends Model
{
    protected $fillable = ['title', 'description', 'category', 'status', 'action'];

    public function userPreference(){
        return $this->hasMany('App\UserPreferences');
    
    }
}
