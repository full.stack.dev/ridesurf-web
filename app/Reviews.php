<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    public function reviewFrom(){
        return $this->belongsTo('App\User', 'user_rating_id');
    
    }

    public function reviewTo(){
        return $this->belongsTo('App\User', 'user_rate_id');
    
    }
}
