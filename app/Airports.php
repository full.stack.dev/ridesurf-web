<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airports extends Model
{
    protected $fillable = ['name', 'address','city','zip','state'];

    public function meetings(){
    	return $this->hasMany('App\AirportMeetings', 'airport_id');
    }
}
