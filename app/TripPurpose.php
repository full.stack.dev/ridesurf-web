<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripPurpose extends Model
{
    protected $fillable = ['type', 'description'];

    public function openRide(){
        return $this->belongsTo('App\OpenRide', 'trip_purpose_id');
    }

    public function ride(){
        return $this->belongsTo('App\Rides', 'trip_purpose_id');
    }
}
