<?php

namespace App\Providers;
use App\Http\Traits\Notifications;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Guard;
use App\User;
use View;

class AppServiceProvider extends ServiceProvider
{

    use Notifications;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Guard $auth) {

        view()->composer('*', function($view) use ($auth) {
            // get the current user
            $currentUser = $auth->user();
            $notific = $this->getNotifications($currentUser);
            $message =  $this->getNotificationMessages();
            $cMessages = count($message);
//            $notify_menu = $notific + $cMessages;

            // pass the data to the view
            $view->with('cMessages', $cMessages);
            $view->with('notify_menu', $notific);
        }); 
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
