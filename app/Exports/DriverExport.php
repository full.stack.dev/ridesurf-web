<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class DriverExport implements FromQuery
{

    use Exportable;

    public function query()
    {	

        return User::query()->join('drivers', 'drivers.user_id', '=', 'users.id')->select('first_name', 'last_name', 'email', 'drivers.status');
    }
}