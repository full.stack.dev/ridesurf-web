<?php

namespace App\Exports;

use App\UserRides;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;


class UsersExport implements FromQuery
{
    use Exportable;

    public function __construct(string $dateFrom, string $dateTo)
    {
      
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function query()
    { 
        return UserRides::query()->join('rides', 'rides.id', '=' ,'user_rides.rides_id')
                      ->join('users as driver','rides.user_id', '=', 'driver.id')
                      ->selectRaw('driver.email as email, sum(rides.totalToPay) as total')
                      ->where('paid_driver', '=', 'No')
                      ->where('rides.dateFrom' , '>=', $this->dateFrom)
                      ->where('rides.dateFrom', '<=', $this->dateTo)
                      ->where('booker_payment_paid', '=', 'Yes')
                      ->groupBy('email');      
                                        
    }
}
