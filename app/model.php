<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class model extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'model';

    /**
     * @var array
     */
    protected $fillable = ['iModelId', 'iMakeId', 'vTitle', 'eStatus'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function make()
    {
        return $this->belongsTo('make', 'iMakeId');
    }
}
