<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class make extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'make';

    /**
     * @var array
     */
    protected $fillable = ['iMakeId', 'vMake', 'eStatus'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function model()
    {
        return $this->hasMany('model', 'iMakeId');
    }

}
