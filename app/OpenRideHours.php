<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenRideHours extends Model
{
    public function openRide(){
        return $this->belongsTo('App\OpenRide', 'open_rides_id');
    }
}
