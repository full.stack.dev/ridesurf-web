<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPreferences extends Model
{
    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function preference(){
    	return $this->belongsTo('App\Preferences');
    }
}
