<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AirportDetails extends Model
{
	protected $fillable = ['airport_id', 'type','airport','terminal','parking'];

    public function ride(){
    	return $this->belongsTo('App\Rides', 'ride_id', 'id');
    }
}
