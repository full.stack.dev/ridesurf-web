<?php

namespace App\Imports;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Session, DB;

class UsersImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
    	$count =0;
        foreach($collection as $user){

        	if($user[3] != null && $user[17] != null ){

        		$randomPass = "Z3fdasddw442dd";

        		$newUser = new User();


	        	$newUser->facebook_id = $user[1];
	        	$newUser->first_name = $user[3];
	        	$newUser->last_name = $user[4];
	        	$newUser->description = $user[6];
	        	$newUser->gender = $user[7];
	        	$newUser->birthday = $user[9];
	        	$newUser->address = $user[11];
	        	$newUser->city = $user[12];
	        	$newUser->state = $user[13];
	        	$newUser->country = $user[14];
	        	$newUser->zipcode = $user[15];
	        	$newUser->phone_number = $user[16];
	        	$newUser->email = $user[17];
	        	$newUser->password = bcrypt($randomPass);
	        	//$newUser->created_at = $user[19];
	        	$newUser->last_login = $user[23];
	        	$newUser->verified = $user[29];
	        	$newUser->imported = true;
	        	$newUser->email_token = str_random(40);

	        	$newUser->save();

	        	$newUser->assignRole('User');

	        	$prefenreces = DB::table('preferences')->where('eStatus', '=', 'Active')->get();
	        	// Add Preferemces
		        foreach($prefenreces as $pref){
		            DB::table('user_preferences')->insert(
		                ['user_id' => $newUser->id, 'preferences_id' => $pref->iPreferencesId, 'option' => $pref->vNO_Lable_EN,
		                'created_at' => Carbon::now()]
		            );
		        }

	        	

	        	$count++;
        	}
        }

        Session::flash('message', 'Upload successfully updated ' . $count);

        return redirect()->back();
    }
}
