<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOpenRide extends Model
{
    public function open_ride(){
        return $this->belongsTo('App\OpenRide', 'open_ride_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');

    }
}
