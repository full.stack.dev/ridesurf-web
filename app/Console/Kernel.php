<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\RideFinishedUsers',
        'App\Console\Commands\RegisteredUsers',
        'App\Console\Commands\ImportCars',
        'App\Console\Commands\DeleteMessages',
        'App\Console\Commands\FixImages',
        'App\Console\Commands\SeedTestData',
        'App\Console\Commands\MissingInfo',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('rideFinished:users')
                   ->timezone('America/New_York')
                   ->withoutOverlapping()
                   ->daily();

        $schedule->command('registered:users')
                 ->timezone('America/New_York')
                 ->withoutOverlapping()
                 ->daily();
                 
        $schedule->command('command:sendridereminder')
                 ->timezone('America/New_York')
                  ->withoutOverlapping()
                 ->daily();

        $schedule->command('driver:expireInfo')
                 ->timezone('America/New_York')
                 ->withoutOverlapping()
                 ->daily();

        $schedule->command('openrides:deactivate')
                ->timezone('America/New_York')
                ->withoutOverlapping()
                ->dailyAt('2:00');

        $schedule->command('backup:clean')->dailyAt('01:30');
        $schedule->command('backup:run --only-db')->dailyAt('01:35');
    }

    //* * * * * cd /Users/eliecervera/Documents/localhost/car-o/ && php artisan schedule:run >> /dev/null 2>&1
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

