<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Traits\MailAdmins;
use Carbon\Carbon;

class RegisteredUsers extends Command
{

    use MailAdmins;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registered:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email sent to Admins to Register users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $yesterday = Carbon::yesterday();
        $today = Carbon::today();
        
        $totalUsers = \DB::table('users')
                        ->whereBetween('created_at', [$yesterday,$today])->count();

        $reason = "Daily User Registration";

        $reply = "The total User count for yesterday is " . $totalUsers;

        $emails = explode(',', env('ADMIN_EMAILS'));

        $this->mailCurrentAdminds($emails,$reply,$reason);
    }
}
