<?php

namespace App\Console\Commands;

use App\Mail\SendReminderInfo;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class MissingInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'missing:info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When a member signs up and they havent compete their profile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $nowStart = Carbon::now()->subDays(7)->startOfDay();
        $nowEnd = Carbon::now()->subDays(5)->endOfDay();

        $users = User::select('first_name','last_name','phone_number','address','city','state','zipcode', 'created_at')
                 ->where('created_at','>=', $nowStart->format('Y-m-d H:i:s'))
                 ->where('created_at','<=', $nowEnd->format('Y-m-d H:i:s'))
                 ->get();

        foreach($users as $user){
            if($user->hasFourEmpty()){
                Mail::to('eliman341@gmail.com')->send(new SendReminderInfo($user->first_name));            }
        }

    }
}
