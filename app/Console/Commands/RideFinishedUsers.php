<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendFinishRideUser;
use App\Rides;
use App\UserRides;
use Carbon\Carbon;
use ArrayObject;

class RideFinishedUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rideFinished:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email when A ride has finished';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $yesterday = Carbon::yesterday()->toDateString();
        //$time = Carbon::now()->format('H:i:s');

        $rides = Rides::where('dateFrom', '=', $yesterday)->get();
        $ride_info = new ArrayObject();
        
        foreach($rides as $ride){
            
            if(count($ride->user_ride) > 0){
                $driver =  $ride->user->name;
                $driver_id = $ride->user->id;
                $rideTo = $ride->location_to;
                $rideFrom = $ride->location_from;
                $date = $ride->dateFrom;
                $rides_id = $ride->id;

                $user_rides = $ride->user_ride;
                foreach($user_rides as $uRide){
                    if($uRide->booker_payment_paid == 'Yes' && $uRide->transaction_id != null){
                        $email = $uRide->user->email;
                        $name = $uRide->user->name;
                        Mail::to($email)
                        ->send(new SendFinishRideUser($driver,$driver_id,$name,$rideTo,$rideFrom,$date,$rides_id));
                    }
                }
                
            }
                
        }
    }


}
