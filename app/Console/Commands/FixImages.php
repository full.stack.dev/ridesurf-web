<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\ProfileImages;

class FixImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command will fix image in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        $count = 0;
        foreach($users as $user){
            if( count($user->profileImages) > 0){
                $user->avatar = $user->profileImages[0]->path;
                $user->update();
                $count++;
            }
        }
        print_r('\n');
        print_r($count);

    }
}
