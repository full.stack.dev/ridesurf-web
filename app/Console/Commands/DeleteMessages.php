<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;

class DeleteMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will delete all messages from tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Message::truncate();
        Participant::truncate();
        Thread::truncate();
    }
}
