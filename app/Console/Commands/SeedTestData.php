<?php

namespace App\Console\Commands;

use Illuminate\Database\Console\Seeds\SeedCommand;
use Illuminate\Database\DatabaseManager;

class SeedTestData extends SeedCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command that apply test data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DatabaseManager $databaseManager)
    {

        parent::__construct($databaseManager);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Testing 
        $this->call('UsersTableSeeder');
        $this->call('ReviewsTestSeeder');
        $this->call('RideTableSeeder');
    }
}
