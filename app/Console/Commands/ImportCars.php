<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class ImportCars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:cars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Cars from SQL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::unprepared(file_get_contents('database/sql/car_colour.sql'));
        DB::unprepared(file_get_contents('database/sql/car_type.sql'));
        DB::unprepared(file_get_contents('database/sql/make.sql'));
        DB::unprepared(file_get_contents('database/sql/model.sql'));
        DB::unprepared(file_get_contents('database/sql/countries.sql'));
    }
}
