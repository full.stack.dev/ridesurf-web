<?php

namespace App\Console\Commands;

use App\OpenRide;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeactivateRides extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'openrides:deactivate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::now();
        $openRides = OpenRide::where('open_date_to' ,'<', $today->format('Y-m-d'))->where('active','=',1)->get();
        foreach($openRides as $ride){
            $ride->active = false;
            $ride->update();
        }
    }
}
