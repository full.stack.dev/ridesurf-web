<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Driver;
use App\Mail\RemindDrivers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class checkDriverStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:expireInfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check that driver does not have an expired document';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $drivers = Driver::where('status', '=', 'Approved')->get();

        $today = Carbon::now()->format('Y-m-d');

        foreach($drivers as $driver){
            if(!empty($driver->user)){
                $messageForDriver = null;
                $sendMail = false;

                if($driver->registration_expire <= $today && $driver->registration_expire){
                    $messageForDriver = $messageForDriver . ' registration ';
                    $sendMail = true;
                }
                if($driver->license_expire <= $today && $driver->license_expire){
                    $messageForDriver =  $messageForDriver. ' drivers license ';
                    $sendMail = true;
                }
                if($driver->insurance_expire <= $today && $driver->insurance_expire){
                    $messageForDriver =  $messageForDriver. ' insurance';
                    $sendMail = true;
                }


                if($sendMail == true){
                    Log::info('User Denied '. $driver->user->first_name);
                    $driver->status = 'Denied';
                    $driver->update();

                    Mail::to($driver->user->email)
                    ->send(new RemindDrivers($driver->user->first_name, $messageForDriver));
                }
            }
        }
    }
}
