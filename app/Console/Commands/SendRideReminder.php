<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Traits\LocationHelper;
use App\User;
use App\Rides;
use App\RideRequest;
use Carbon\Carbon;
use Auth, Mail, Log;


class SendRideReminder extends Command
{
    use LocationHelper;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendridereminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Rides in which user wants to know';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $ride_request = RideRequest::all();

        foreach($ride_request as $rideR){
            if($rideR->date != null){
                $date = Carbon::today()->format('Y-m-d');
            }else{
                $date = $rideR->date;
            }
            $array1 = explode(',', $rideR->city_from);
            $array2 = explode(',', $rideR->city_to);
            $city_from = $this->parseLocation($array1, $rideR->city_from);
            $city_to = $this->parseLocation($array2, $rideR->city_to);
            
            $rides = Rides::where('city_from', 'like', '%' . $city_from  . '%')
                            ->where('city_to', 'like', '%' . $city_to  . '%')
                            ->where('dateFrom', '>', $date)
                            ->where('seats' ,'!=', 0)
                            ->get();

            if(count($rides) > 0){
                $user = User::find($rideR->user_id);
                if($user != null){
                    $email = $user->email;
                    $from = $rideR->city_from;
                    $to = $rideR->city_to;
                    $info_mail = array(
                        'rides' => $rides,
                        'user_name' => $user->first_name,
                        'from' => $from,
                        'to' => $to,
                    );
                    $rides = $rides->filter(function ($item) use ($rides, $user) {

                        if($item->user->id != $user->id){
                            return $item;
                        } 

                    })->values()->all();
                    Mail::send('email.sendreminder', $info_mail, function($message) use ($email, $to) {
                            $message->to($email)->subject("Ridesurf: Ride Reminder to " . $to);
                    });
                }
            }
        }

    }
}
