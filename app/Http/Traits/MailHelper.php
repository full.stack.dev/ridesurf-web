<?php
namespace App\Http\Traits;

use App\Rides;
use App\RidesStop;
use App\Vehicle;
use App\UserRides;
use App\User;
use App\Driver;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Input;
use Mail, Session, Redirect,OneSignal;

trait MailHelper{

    private $admin;
    public function __construct()
    {
        $this->admin = User::where('first_name', '=', 'admin')->get()->first();
    }

    public function mailRequest_ToDriver($user, $ride){
        $message = "You have a new request from " . $user->first_name . " for your trip to " . $ride->location_to . ". Please review them and approve or deny the ride as soon as possible.";

        $thread = Thread::create([
            'subject' => 'New passenger request for ' .$ride->location_to,
            'rides_id' => $ride->id,
            'isNotification' => 'Yes',
        ]);

        $admin = User::where('first_name', '=', 'admin')->get()->first();
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $ride->user->id,
            'last_read' => new Carbon,
        ]);
        //App Notification to Driver
        OneSignal::sendNotificationUsingTags(
            $message,
            array(
                ["field" => "tag","key"=>"userid", "relation" => "=", "value" => $ride->user_id],
            ),
            $url = null,
            $data = null,
            $buttons = null,
            $schedule = null
        );
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($ride->user->id);
        }
        $rating = $user->raiting;
        if($rating == null){
            $rating = "User has no ratings yet.";
        }
        $driver_name = $ride->user->first_name;
        $email = $ride->user->email;
        $info = array('driver'=>$driver_name, "user" => $user->first_name, "rideTime" => $ride->startHour, "rideDate" => $ride->dateFrom, "rideFrom" => $ride->location_from, "rideTo" => $ride->location_to, "userRaiting"=>
            $rating, "userVerify" => $user->verified);

        Mail::send('email.RequestJoin', $info, function($message) use ($email) {
            $message->to($email)->subject('A passenger has requested to join your ride');
        });
    }

}