<?php
namespace App\Http\Traits;
use Intervention\Image\ImageManagerStatic as Image;

use App\Rides;
use App\RidesStop;
use App\Vehicle;
use App\UserRides;
use App\User;
use App\Driver;
use Carbon\Carbon;
use App\ProfileImages;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\Sale;
use PayPal\Api\Payment;
use Mail, File;


trait UploadImage {

	public function upload($file, $user_id, $control, $inner_path){

		//Uncropped Image
         if($control == 0){
            return false;
        }else{

            //Cropped Image
            $data = $file;
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $ext = explode( '/', $type);
            $data = base64_decode($data);

            $encrypt = encrypt($user_id);


            $path = public_path().$inner_path;

            if(!File::exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }

            $name = $path."/".$encrypt .".".$ext[1];

            file_put_contents($name, $data);
            $user = User::find($user_id);

            if($user->avatar == null){

               $user->avatar = $inner_path.'/'.$encrypt .".".$ext[1];
               $user->update();
              }
            else{
				if( file_exists(public_path().$user->avatar) ){
					unlink(public_path().$user->avatar);
				}
              $user->avatar = $inner_path.'/'.$encrypt .".".$ext[1];
              $user->update();
            }

           return true;
        }//End of File creation
	}

	public function uploadCarImage($file, $user_id, $inner_path,$driver,$make,$model,$year){


        //Cropped Image
        $data = $file;

        list($type, $data) = explode(';', $data);

        list(, $data)      = explode(',', $data);
        $ext = explode( '/', $type);
        $data = base64_decode($data);


        $encrypt = str_random(40);

        $path = public_path().$inner_path;

        if(!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $name = $path."/".$encrypt .".".$ext[1];

        file_put_contents($name, $data);


        $path_image=$inner_path.'/'.$encrypt .".".$ext[1];


        return $path_image;
	}


  public function resizeImage($file, $path){

    if(!File::exists(public_path().$path)) {
        File::makeDirectory(public_path().$path, $mode = 0777, true, true);
    }
    $filename = str_random(40);
    $ext  = $file->getClientOriginalExtension();
    $image_resize = Image::make($file->getRealPath())->orientate();

    $image_resize->resize(800, 800);
    $image_resize->save(public_path().$path. '/' .$filename. '.'.$ext);

    return ($path .'/'. $filename.'.'.$ext);

  }

}
