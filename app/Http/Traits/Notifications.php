<?php
namespace App\Http\Traits;

use App\OpenRide;
use App\Rides;
use App\RidesStop;
use App\UserOpenRide;
use App\Vehicle;
use App\UserRides;
use App\User;
use App\Driver;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Mail, File, DB, Auth;
use Illuminate\Support\Facades\Log;

trait Notifications {

  public function getNotifications($user){

      $today = strtotime(Carbon::now());

      if($user != null ){
        $rides = Rides::where('user_id', '=', $user->id)->get();
        $openRides = OpenRide::where('user_id', '=', $user->id)->get();
      }

      else{
        $rides = null;
          $openRides = null;
      }

      $notifications = 0;


      if($rides != null){

        foreach($rides as $ride){
          $time_ride = strtotime($ride->dateFrom  ." ".$ride->startHour);
          if($time_ride >= $today){
            $urides =  DB::table('user_rides')
                         ->where('rides_id', '=', $ride->id)
                         ->where('status', '=', 'Requested')
                         ->get();
            if(count($urides) > 0){

              foreach($urides as $user){
                $notifications++;

              }

            }
          }
        }
      }


      if($user != null){
        //Add Changes
        $user_rides = UserRides::where('user_id', '=', $user->id)->get();
      }else{
        $user_rides = null;
      }

      if($user_rides != null){

          foreach($user_rides as $user_ride){
             if(isset($user_ride->ride)){
                $time_ride = strtotime($user_ride->ride->dateFrom  ." ".$user_ride->ride->startHour);
                if($user_ride->status == 'Approved' && $user_ride->booker_payment_paid == 'No'
                  && $time_ride >= $today){
                    $notifications++;
                }
              }
          }
      }

    $threads = Thread::forUser(Auth::id())->latest('updated_at')
               ->where('isNotification','=', 'Yes')->get();

    $count = 0;

    foreach($threads as $thread){
        $count += Message::where('thread_id', '=', $thread->id)
                           ->where('user_id', '!=', Auth::id())
                           ->where('last_seen', '=', null )->count();

    }

    $notifications = $notifications;

    return $notifications;
  }


  // Get the actions of the user ( Driver and Passanger Notifications)
  public function getActions($user, $today){
    $notifications = null;
    $i = 0;
    
    //Driver notifications
    if(count($user->ride)){
        $rides = Rides::where('user_id', '=', $user->id)->get();
        $offer = count($user->ride);

        foreach($rides as $ride){

            if(count($ride->user_ride) > 0){
                foreach($ride->user_ride as $user_ride){

                    $time_ride = strtotime($ride->dateFrom  ." ".$ride->startHour);

                    if($user_ride->status == 'Requested' && $time_ride > $today ){
                        
                        $user_ride = UserRides::find($user_ride->id);
                        $user_ride->user_name = $user_ride->user->first_name;
                        $user_ride->user_image = $user_ride->user->avatar;

                        $notifications[$i++] = $user_ride;
                    }
                }

            }

        }

        $openRides = OpenRide::where('user_id', '=', $user->id)->get();
        $now = Carbon::now();

        foreach($openRides as $open){
            if( count($open->user_ride) > 0 ){
                foreach($open->user_ride as $user_ride){
                    if($user_ride->status == 'Requested' &&
                        $user_ride->date >= $now->format('Y-m-d') ){
                        $new_user_ride = UserOpenRide::find($user_ride->id);
                        $new_user_ride->user_name = $new_user_ride->user->first_name;
                        $new_user_ride->user_image = $new_user_ride->user->avatar;
                        $new_user_ride->hour =  date('h:i a', strtotime($new_user_ride->hour)); 
                        $new_user_ride->isOpenRide = true;
                        $new_user_ride->city_from = $open->city_from;
                        $new_user_ride->city_to = $open->city_to;
                        $notifications[$i++] = $new_user_ride;

                    }

                }

            }

        }
    }
    

    //Passenger Notifications
    if(count($user->UserRide)){
        $userRide = $user->UserRide;

        foreach($userRide as $uRide){

            //Get the time of each user ride
            if( !empty($uRide->ride) ) {
                if( strlen($uRide->ride) > 0) {
                    $time_ride = strtotime($uRide->ride->dateFrom . " " . $uRide->ride->startHour);
                    //Review User
                    if ($uRide->status == 'Approved' && $time_ride < $today) {
                        if ($uRide->reviewed == null && $uRide->booker_payment_paid == 'Yes') {

                            $user_ride = UserRides::find($uRide->id);
                            $user_ride->isReview = true;
                            $user_ride->time_ride = $time_ride;
                            $user_ride->date = $user_ride->ride->dateFrom;
                            $user_ride->user_name = $user_ride->user->first_name;
                            $user_ride->driver = $user_ride->ride->user;
                            $user_ride->user_image = $user_ride->user->avatar;
                            $notifications[$i] = $user_ride;
                            $i++;
                        }
                    }
                }
            }



          // Make Payment
          if($uRide->status == 'Approved' && ( $uRide->booker_payment_paid == 'No' || $uRide->booker_payment_paid == null) 
            && $time_ride > $today){
                
              $user_ride = UserRides::find($uRide->id);
            
              if( ($user_ride->ride->user) ){
                  $user_ride->date = $user_ride->ride->dateFrom;
                  $user_ride->isReview = false;
                  $user_ride->driver = $user_ride->ride->user;
                  $notifications[$i] = $user_ride;
                  $i++;
              }
          }


          
        }

      }

    return $notifications;
  }

  public function getNotificationMessages(){

      $threads = Thread::forUser(Auth::id())->latest('updated_at')
                 ->where('isNotification','=', 'Yes')->get();

      foreach($threads as $key => $num){
        foreach($num->messages as $message){
          if($message->last_seen == null){
            $num->otherUser = User::where('first_name', '=', 'admin')->first();
          }else{
            $threads->forget($key);
          }
        }
      }

      return $threads;         
  }

  public function messageCount(){

    $threads = Thread::forUser(Auth::id())->latest('updated_at')
               ->where('isNotification','=', 'No')->get();

    $count = 0;
    
    foreach($threads as $thread){
        $count += Message::where('thread_id', '=', $thread->id)
                           ->where('user_id', '!=', Auth::id())
                           ->where('last_seen', '=', null)->count();
    }
    
    return $count;
  }


}
