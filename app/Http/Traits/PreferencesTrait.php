<?php
namespace App\Http\Traits;

use App\User;
use App\Preferences;
use App\UserPreferences;

trait PreferencesTrait {

	public function getUserPreferences($user){

		$preferences = UserPreferences::where('user_id', '=', $user->id)->get();

		return $preferences;
	}
}