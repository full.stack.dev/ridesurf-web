<?php
namespace App\Http\Traits;

use App\Rides;
use App\RidesStop;
use App\Vehicle;
use App\UserRides;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Log;

trait Formating{

	public function FormatPhone($phone){
		$temp = explode('-', $phone);
		
		$returnPhone = null;
		foreach($temp as $item){
			$returnPhone = $returnPhone . $item;
		}
		
		return $returnPhone;
	}
}