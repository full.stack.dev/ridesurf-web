<?php
namespace App\Http\Traits;

use App\Rides;
use App\RidesStop;
use App\Vehicle;
use App\UserRides;
use App\User;
use App\Driver;
use Carbon\Carbon;
use App\ProfileImages;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Mail, File;

trait MailAdmins{

	public function mailCurrentAdminds($admin_emails, $reply, $reason){

		$info = array(
            'reply' => $reply,
            'reason' => $reason,
        );
		foreach($admin_emails as $email){

	        Mail::send('email.notifyadmins', $info, function($message) use ($email, $reason) {
	            $message->to($email)->subject($reason);
	        });

        }
	}
}