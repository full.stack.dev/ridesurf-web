<?php

namespace App\Http\Traits;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth; 
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;

use App\Rides;
use App\UserRides;
use App\User;
use Carbon\Carbon;
use OneSignal;

trait MessageTrait {


    /* This healper trait will send a new message and email
    *  whenever communications haven't been stablish by
    */ 
    public function StoreNewMessage( $ride_id, $otherUser_id, $body, $before_pay = null){

        $ride = Rides::find($ride_id);

        $find = Thread::where('rides_id','=', $ride_id)->first();

        //If User is messaging the other user for first time
        if(empty($find)){

            $thread = Thread::create([
                'subject' => 'Messager',
                'rides_id' => $ride->id,
                'isNotification' => 'No',
            ]);

            $thread->isNotification = 'No';
            if($before_pay)
                $thread->before_pay = $before_pay;
            $thread->rides_id = $ride->id;
            $thread->update();

            // Message
            $mes = Message::create([
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
                'body' => $body,
            ]);

            $mes->last_seen = null;
            $mes->update();

            // Sender
            Participant::create([
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
                'last_read' => new Carbon,
            ]);
            
            // Reciver
            Participant::create([
                'thread_id' => $thread->id,
                'user_id' => $otherUser_id,
                'last_read' => new Carbon,
            ]);

            //App Notification to Receiver
//            OneSignal::sendNotificationUsingTags(
//                $mes,
//                array(
//                    ["field" => "tag","key"=>"userid", "relation" => "=", "value" => $otherUser_id],
//                ),
//                $url = null,
//                $data = null,
//                $buttons = null,
//                $schedule = null
//            );

        }else{
            // Message
            $mes = Message::create([
                'thread_id' => $find->id,
                'user_id' => Auth::id(),
                'body' => $body,
            ]);
            $mes->last_seen = null;
            $mes->update();

            $thread = $find;

        }

        $sender = Auth::user();

        $reciver = User::find($otherUser_id);

        $email = $reciver->email;

        $subject_to = "New Message from " . $sender->first_name . ". ";

        $info_user = array(
            'reciver' => $reciver,
            'reply' => $body,
            'sender' => $sender,
            'thread_id' => $thread->id,
        );

        Mail::send('email.sendMessage', $info_user, function($message) use ($email, $subject_to) {
            $message->to($email)->subject($subject_to);
        });
    }

}