<?php
namespace App\Http\Traits;

use App\TripPurpose;
use Illuminate\Pagination\Paginator;
use App\Rides;
use App\RidesStop;
use App\OpenRide;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Log;
trait LocationHelper{

    public function parseLocation($array, $place){

        if(count($array) == 5){
            $city = $array[2] .",". preg_replace('/[0-9]+/', '',  $array[3]);
        }
        else if(count($array) == 4){
            $city = $array[1] .",". preg_replace('/[0-9]+/', '',  $array[2]);
        }
        else if(count($array) == 3){
            $city = $array[0] .",". preg_replace('/[0-9]+/', '', $array[1]);
        }else{
            $city = $place;
            
        }
        if(!empty($city)){
            if($city[0] == ' ' && strlen($city) > 0){
                $city = substr($city, 1);
            }
        }

        return $city;
    }

    public function getState($city){

        $address = explode(',', $city);

        if(count($address) == 5){
            $state =   preg_replace('/[^a-z-]/i', '',$address[3]);
        }
        else if(count($address) == 4){
            $state =  preg_replace('/[^a-z-]/i', '',$address[2]);
        }
        else if(count($address) == 3){
            $state =  preg_replace('/[^a-z-]/i', '', $address[1]);
        }else if(count($address) == 2){

            $state =  preg_replace('/[^a-z-]/i', '', $address[1]);
        }else{
            $state = false;
        }

        
        return $state;
    }

    public function getCity($string){
        $city = explode(',', $string);
        $newCity = explode(' ', $city[0]);

        $returnCity = null;
        foreach($newCity as $item){
            if($returnCity != null){
                $returnCity =  $returnCity . '+' .  $item;
            }else{
                $returnCity = $item;
            }
        }
        return $returnCity;
    }

    public function parseState($state){
        $temp = DB::table('states')->where('name', 'like', '%' . $state . '%')->first();

        return $temp->code;
    }

    public function queryArrayCities($city){
        try{
            // get geocode object as array from The Google Maps Geocoding API
            $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);

            // get latitude and longitude from geocode object
            $latitude = $geocodeObject['results'][0]['geometry']['location']['lat'];
            $longitude = $geocodeObject['results'][0]['geometry']['location']['lng'];

            // set request options
            $responseStyle = 'short'; // the length of the response
            $citySize = 'cities15000'; // the minimal number of citizens a city must have
            $radius = 50; // the radius in KM
            $maxRows = 25; // the maximum number of rows to retrieve
            $username = 'roska541'; // the username of your GeoNames account

            // get nearby cities based on range as array from The GeoNames API
            $nearbyCities = json_decode(file_get_contents('http://api.geonames.org/findNearbyPlaceNameJSON?lat='.$latitude.'&lng='.$longitude.'&style='.$responseStyle.'&cities='.$citySize.'&radius='.$radius.'&maxRows='.$maxRows.'&username='.$username, true));

        }catch(\Exception $e){
            Log::info($e);
            $nearbyCities = null;
        }

        return $nearbyCities;
    }

    public function getOpenRides($city_from, $city_to, $date, $price_control, $womenOnly){

        $city_start = $this->getCity($city_from);
        $city_end =  $this->getCity($city_to);
        $stateFrom = $this->getState($city_from);
        $stateTo = $this->getState($city_to);

        $extentDate = Carbon::parse($date);


        $openRides = OpenRide::where('city_from', 'like', '%'. $city_from . '%')
                              ->where('city_to', 'like', '%' .  $city_to . '%')
                              ->Where('open_date_from', '<=',  $extentDate->format('Y-m-d') )
                              ->Where('open_date_to', '>=', $extentDate->format('Y-m-d'))
                              ->where('active', '=', 1)
                              ->get();

        //Get the Driver information
        foreach($openRides as $ride){
            $user = User::find($ride->user_id);
            $purpose = TripPurpose::find($ride->trip_purpose_id);
            $ride->purpose = $purpose;

            if(count($user->driver) > 0){
                $ride->make = DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake;
                $ride->model = DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle;
                $ride->color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                            ->get()->first()->vColour_EN;
                $ride->type = DB::table('car_type')
                ->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;
                $ride->year = $user->driver[0]->year;
                $ride->car_image = $user->driver[0]->path_image;
            }
        }

        return $openRides;
    }

    public function getRides($city_from, $city_to, $date, $price_control, $womenOnly){

        $extentDate = Carbon::parse($date);

        $city_start = $this->getCity($city_from);
        $city_end =  $this->getCity($city_to);
        $stateFrom = $this->getState($city_from);
        $stateTo = $this->getState($city_to);

        //Get an array of nearby cities
        $nearbyCitiesFrom = $this->queryArrayCities($city_start);
        $nearbyCitiesTo = $this->queryArrayCities($city_end);

        $ids = array(
          '0', '1'
        );
        $stopsId = array(
            '0', '1'
        );
        $i =0;
        $j=0;
        //Get all normal Rides
        //$city_from, $city_to, $date, $extentDate,$womenOnly, $price_control
        $rides = $this->queryRidesSimple($city_from, $city_to, $date, 
            $extentDate, $womenOnly, $price_control, $ids);


        //From Point A to Stop Over
        $ride_stops_from = $this->queryRideStopFrom($city_from, $city_to, $date, 
            $extentDate, $womenOnly,$price_control, $stopsId);

        //From Stop Over to Point B
        $ride_stops = $this->queryRidesStopTo($city_from, $city_to, $date, 
            $extentDate, $womenOnly,$price_control, $stopsId);


        if($nearbyCitiesFrom != null){
            foreach($nearbyCitiesFrom->geonames as $nearbyFrom) {
                $newCityFrom = $nearbyFrom->name . ', ' . $stateFrom;

                foreach ($nearbyCitiesTo->geonames as $nearbyTo) {
                    $newCityTo = $nearbyTo->name . ', ' . $stateTo;

                    if ( strcmp($newCityFrom, $city_from) !== 0  && strcmp($newCityTo, $city_to) !== 0) {


                        //Get all from New City From and New City To
                        $AllCitiesRides = $this->queryRidesSimple($newCityFrom, $newCityTo, $date,
                                          $extentDate, $womenOnly, $price_control, $ids);

                        if (count($AllCitiesRides) > 0) {
                            foreach ($AllCitiesRides as $ride){
                                $ids[$i++] = ($ride->id);
                                $rides->push($ride);
                            }
                        }
                        //Get all normal Rides
                        $NewridesFrom = $this->queryRidesSimple($newCityFrom, $city_to, $date,
                            $extentDate, $womenOnly, $price_control, $ids);


                        if (count($NewridesFrom) > 0 ) {
                            foreach ($NewridesFrom as $ride){
                                $ids[$i++] = ($ride->id);
                                $rides->push($ride);
                            }
                        }

                        //From Point A to Stop Over
                        $Newride_stops_from = $this->queryRideStopFrom($city_to, $newCityFrom, $date,
                            $extentDate, $womenOnly, $price_control, $stopsId);

                        if (count($Newride_stops_from) > 0) {
                            foreach ($Newride_stops_from as $stop){
                                $stopsId[$j++] = ($stop->id);
                                $ride_stops_from->push($stop);
                            }

                        }

                        //From Stop Over to Point B
                        $Newride_stops = $this->queryRidesStopTo($newCityFrom, $city_to, $date,
                            $extentDate, $womenOnly, $price_control, $stopsId);

                        if (count($Newride_stops) > 0) {
                            foreach ($Newride_stops as $stop){
                                $stopsId->push($stop->id);
                                $ride_stops->push($stop);
                            }

                        }

                        //Get all normal Rides
                        $NewridesTo = $this->queryRidesSimple($city_from, $newCityTo, $date,
                            $extentDate, $womenOnly, $price_control, $ids);

                        if($nearbyTo->name == 'San Mateo'){
                        }

                        if (count($NewridesTo) > 0) {
                            foreach ($NewridesTo as $ride){
                                $ids[$i++] = ($ride->id);
                                $rides->push($ride);
                            }
                        }



                        //From Point A to Stop Over
                        $Newride_stops_To = $this->queryRideStopFrom($city_from, $newCityTo, $date,
                            $extentDate, $womenOnly, $price_control, $stopsId);

                        if (count($Newride_stops_To) > 0) {
                            foreach ($Newride_stops_To as $stop){
                                $stopsId->push($stop->id);
                                $ride_stops_from->push($stop);
                            }
                        }

                        //From Stop Over to Point B
                        $Newride_stopsTo = $this->queryRidesStopTo($newCityTo, $city_to, $date,
                            $extentDate, $womenOnly, $price_control, $stopsId);

                        if (count($Newride_stopsTo) > 0) {
                            foreach ($Newride_stopsTo as $stop){
                                $stopsId->push($stop->id);
                                $ride_stops->put($stop);
                            }
                        }
                    }
                }
            }
        }



        foreach($ride_stops as $stop){
            $stop->stop = true;
            $start = strtotime($stop->startHour);
            
            $sum = $stop->duration + $start;
            $endStop = date("h:i a", $sum);

            $stop->startHour = $endStop;
            $rides->put('item',$stop);
        }
        foreach ($ride_stops_from as $stop) {
            $stop->stop = true;
            $rides->put('item', $stop);
        }
        
        
        //Get the Driver information 
        foreach($rides as $ride){

            $user = User::find($ride->user_id);

            if(count($user->driver) > 0){
                $ride->make = DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake;
                $ride->model = DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle;
                $ride->color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                            ->get()->first()->vColour_EN;
                $ride->type = DB::table('car_type')
                ->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;
                $ride->year = $user->driver[0]->year;
                $ride->car_image = $user->driver[0]->path_image;

            }
            if(!empty($ride->airport_details)>0){
                $ride->airport = $ride->airport_details;
            }
        }

        //Only get the rides where seats are more than one
        $rides = $rides->filter(function ($item) use ($rides) {

            if($item->total == null){
                return $item;
            }else{
                return $item->total > 0;
            }

        })->values();
        
        return $rides;
    }

    public function queryRidesSimple( $city_from, $city_to, $date, $extentDate, 
                    $womenOnly, $price_control, $ids ){

        $rides = Rides::leftJoin('users as user', 'user.id', '=', 'rides.user_id')
                 ->leftJoin('rides_stops as stop', 'stop.rides_id', '=', 'rides.id')
                 ->select('rides.id','rides.location_from','rides.location_to','rides.price', 'rides.seats_available', 'rides.dateFrom', 'rides.description', 'rides.startHour', 'user.first_name', 'user.last_name','user.id as user_id', 'user.rating', 'rides.ladies_only', DB::raw('(stop.seats_from)+(stop.seats_to) as total'), 'user.avatar', 'rides.duration')
                 ->where('rides.city_from' ,'like', '%'. $city_from . '%')
                 ->where('rides.city_to','like', '%'. $city_to . '%')
                 ->whereRaw('CONCAT(dateFrom," ",startHour) >= NOW()')
//                 ->where('dateFrom', '>=', $extentDate->format('Y-m-d'))
//                 ->whereRaw('CONCAT(dateFrom," ",startHour) <= ($extentDate->addDays(7))')
                 ->where('dateFrom', '<=', $extentDate->addDays(7)->format('Y-m-d') )
                 ->whereNotIn('rides.id', $ids)
                 ->where(function($query) use ($womenOnly, $price_control){
                     if($womenOnly == 'on'){
                        $query->where('rides.ladies_only', 'yes');
                     }  
                     if($price_control != null){
                        $query->where('rides.price', '<=', $price_control);
                     }
                 })
                 ->where('seats_available' ,'>', 0)
                 ->orderBy('dateFrom' , 'ASC')
                 ->groupBy('rides.id')
                 ->paginate();

        return $rides;
    }

    public function queryRideStopFrom($city_from, $city_to, $date, $extentDate, 
                    $womenOnly, $price_control, $stopsId){

        $rides = Rides::leftJoin('rides_stops as stop', 'stop.rides_id', '=', 'rides.id')
             ->leftJoin('users as user', 'user.id', '=', 'rides.user_id')
             ->select('rides.id','stop.address as location_to','rides.location_from','stop.price_from as price','rides.dateFrom', 'rides.description', 'rides.startHour', 'user.first_name','user.last_name', 'user.id as user_id', 'user.rating', 'rides.ladies_only', DB::raw('(stop.seats_from+stop.seats_to) AS total'), 'rides.seats_available', 'user.avatar', 'rides.duration')
            ->where('rides.city_from' ,'like', '%'. $city_from . '%')
            ->where('rides.city_to','like', '%'. $city_to . '%')
             ->where('dateFrom', '>=', $date)
             ->where('stop.seats_from' ,'>', 0)
             ->whereNotIn('stop.id', $stopsId)
             ->where('dateFrom', '<=', $extentDate->addWeek(1)->format('Y-m-d') )
             ->where(function($query) use ($womenOnly, $price_control){
                     if($womenOnly == 'on'){
                        $query->where('rides.ladies_only', 'yes');
                     }  
                     if($price_control != null){
                        $query->where('rides.price', '<=', $price_control);
                     }
             })
             ->orderBy('dateFrom' , 'ASC')
             ->paginate();

        return $rides;
    }

    public function queryRidesStopTo($city_from, $city_to, $date, $extentDate, 
                    $womenOnly, $price_control, $stopsId ){

        $rides = Rides::leftJoin('rides_stops as stop', 'stop.rides_id', '=', 'rides.id')
                 ->leftJoin('users as user', 'user.id', '=', 'rides.user_id')
                 ->select('rides.id','stop.address as location_from','rides.location_to','stop.price_to as price','rides.dateFrom', 'rides.description', 'user.id as user_id', 'user.first_name','user.last_name', 'user.id as user_id', 'user.rating', 'rides.ladies_only', DB::raw('(stop.seats_from+stop.seats_to) AS total'), 'rides.seats_available', 'rides.startHour', 'stop.duration', 'user.avatar', 'rides.duration')
                 ->where('rides.city_from' ,'like', '%'. $city_from . '%')
                 ->where('rides.city_to','like', '%'. $city_to . '%')
                 ->where('dateFrom', '>=', $date)
                 ->where('dateFrom', '<=', $extentDate->addWeek(1)->format('Y-m-d') )
                 ->whereNotIn('stop.id', $stopsId)
                 ->where(function($query) use ($womenOnly, $price_control){
                     if($womenOnly == 'on'){
                        $query->where('rides.ladies_only', 'yes');
                     }
                     if($price_control != null){
                        $query->where('rides.price', '<=', $price_control);
                     }
                 })
                 ->orderBy('dateFrom' , 'ASC')
                 ->paginate();

        return $rides;

    }
}