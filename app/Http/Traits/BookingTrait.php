<?php
namespace App\Http\Traits;

use App\Rides;
use App\RidesStop;
use App\Vehicle;
use App\UserRides;
use Carbon\Carbon;
use DB;

trait BookingTrait{

	public function populateUserRides($user_ride){
		
		foreach($user_ride as $uRide){
            //Get values for List, Single and Cancel
            $uRide->driver = $uRide->ride->user->first_name ." ". substr($uRide->ride->user->last_name, 0, 1);
            $uRide->driver_profile = $uRide->ride->user->avatar;

            $uRide->make = DB::table('make')->where('iMakeId','=', $uRide->ride->user->driver[0]->make_id)
                         ->get()->first()->vMake;

            $uRide->model = DB::table('model')->where('iModelId','=', $uRide->ride->user->driver[0]->model_id)
                          ->get()->first()->vTitle;

            $uRide->color = DB::table('car_colour')->where('iColourId','=', $uRide->ride->user->driver[0]->color_id)
                          ->get()->first()->vColour_EN;
            $uRide->vehicle_tag = $uRide->ride->user->driver[0]->vehicle_tag;
            $uRide->year = $uRide->ride->user->driver[0]->year;
            $uRide->start = date("g:i a", strtotime($uRide->ride->startHour));
            $time =  strtotime($uRide->ride->startHour);
            $uRide->end = date("h:i a", ($time + $uRide->ride->duration));
            $uRide->date =  date('j M Y', strtotime($uRide->ride->dateFrom));
            $uRide->car = $uRide->ride->user->driver[0]->path_image;
            $uRide->control = ($uRide->ride->dateFrom ." ". $uRide->ride->startHour);
        }

        return $user_ride;
	}
}