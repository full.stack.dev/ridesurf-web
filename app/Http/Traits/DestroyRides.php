<?php
namespace App\Http\Traits;

use App\Rides;
use App\RidesStop;
use App\Vehicle;
use App\UserRides;
use App\User;
use App\Driver;
use Carbon\Carbon;
use App\CancelUserRides;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\Sale;
use PayPal\Api\Payment;
use App\AirportDetails;

use Mail, Session, Redirect;


trait DestroyRides {

	private $app_debug;
	private $paypal_client;
	private $paypal_secret;
	function __construct()
    {
    	$this->app_debug= env('APP_DEBUG');
    	$this->paypal_client = env('PAYPAL_CLIENT_ID');
    	$this->paypal_secret = env('PAYPAL_SECRET');
    }


    public function destroySimpleRideDriver($ride_id, $user_id, $reason) {

        $user_rides = UserRides::where('rides_id', '=', $ride_id)->get();

        $ride = Rides::find($ride_id);

        $today = strtotime(Carbon::now());

        if(!empty($user_rides)){

        	foreach($user_rides as $user_ride){
        		$time_ride = strtotime($user_ride->ride->dateFrom  ." ".$user_ride->ride->startHour);

		        //If the Driver cancel after user paying and the ride has not been completed.
		        if($user_ride->status == 'Approved' && $user_ride->booker_payment_paid == 'Yes'
		         	&& $time_ride <= $today && $ride->rated == null && $user_ride->sale_id != null){

		           	try{
		           	if($this->app_debug == true){

			            $apiContext = new \PayPal\Rest\ApiContext(
			              new \PayPal\Auth\OAuthTokenCredential(
			              	$this->paypal_client,
             				$this->paypal_secret
							//Production
			                //'ASW6BK8mGYilUfGKRkSH1YoXQHp8gnGoJxwdj2qlJm_x8t_r0btHnsVAOyIWAU_dzBWBrNjzY2nQl51A',
			                //'EJLiy3Zr9_PQ4oNtAJkDSqufgBE5n-0h9QS6n6ENshTsfpcqiR1oNRvDu_6e9tTFyy37__Gr2aNYO6yo'

			                //Sandbox
			                //'Aa-bqaGAlJYzwSub0o0DiXflk475Y2mvPnLjNzV4huHQQHXnGx9hxRODLbPgf9KIQmy_viZSW9ovm-pi',
			               	//'EK8EuONCIg1FQSuJq8_8BqbSiUr9nIBuQVUhWvC-TBQfie3LuhuvNuktn5SZN0kO6vh0PHtr2yD9v4ox'
			              )
			            );
		            }else{
		            	$apiContext = new \PayPal\Rest\ApiContext(
			              new \PayPal\Auth\OAuthTokenCredential(

			                //Production
			                $this->paypal_client,
             				$this->paypal_secret

			                //Sandbox
			                //'Aa-bqaGAlJYzwSub0o0DiXflk475Y2mvPnLjNzV4huHQQHXnGx9hxRODLbPgf9KIQmy_viZSW9ovm-pi',
			                //'EK8EuONCIg1FQSuJq8_8BqbSiUr9nIBuQVUhWvC-TBQfie3LuhuvNuktn5SZN0kO6vh0PHtr2yD9v4ox'
			              )
			            );
		            }

		            if($this->app_debug == false){
			            $apiContext->setConfig(
					        array(
					            'mode' => 'live',
					        )
					    );
				    }


		            $price = ($user_ride->price_paid) - 1.5;

		            $amt = new Amount();
		            $amt->setTotal($price)
		              ->setCurrency('USD');

		            $refund = new Refund();
		            $refund->setAmount($amt);

		            $sale = new Sale();
		            $sale->setId($user_ride->sale_id);

		            try {
		              $refundedSale = $sale->refund($refund, $apiContext);
		            } catch (PayPal\Exception\PayPalConnectionException $ex) {
		              echo $ex->getCode();
		              echo $ex->getData();

		              Session::flash('error' ,'There was an error trying to refund your passenger, please contact us to resolve the issue.');
		              return Redirect::back();
		            } catch (Exception $ex) {
		            	Session::flash('error', 'There was an error trying to refund your passenger, please contact us to resolve the issue.');
		              	return Redirect::back();
		            }
		        }catch(Exception $ex){
		        	Session::flash('error' ,'There was an error trying to refund your passenger, please contact us to resolve the issue.');
		            return Redirect::back();
		        }

		        }

		        if(is_array($user_ride->user)){
                    if( count($user_ride->user) > 0){
                        $email = $user_ride->user->email;

                        $info_user = array(
                            'location_to' => $user_ride->ride->location_to,
                            'user_ride' => $user_ride,
                            'name' => $user_ride->user->first_name,
                        );


                        //Send Email to User about Cancel Ride
                        Mail::send('email.deleteRideDriver', $info_user, function($message) use ($email) {
                                        $message->to($email)->subject("Ride Cancelled");
                        });

                        $message = "The driver for your trip to " . $ride->location_to .
                        " had to cancel the trip. We're very sorry for any inconvience this may cause you. Please check available rides for the same destination.";

                        $thread = Thread::create([
                            'subject' => 'Cancel Ride to ' .$ride->location_to,
                            'rides_id' => $ride_id,
                            'isNotification' => 'Yes',
                        ]);

                        // Message
                        Message::create([
                            'thread_id' => $thread->id,
                            'user_id' => 9,
                            'body' => $message,
                        ]);

                        Participant::create([
                                'thread_id' => $thread->id,
                                'user_id' => $user_ride->user->id,
                                'last_read' => new Carbon,
                        ]);
                    }
                }
	       		$user_ride->delete();

	       		//Delete messages
	       		$threads = Thread::where('rides_id', '=', $ride->id)->get();
	       		try{
	       			foreach($threads as $t){
		       			$message = $t->messages;
		       			$participants = $t->participants;
		       			foreach($participants as $p){
		       				$p->delete(); 
		       			}
		       			foreach($messages as $m){
		       				$m->delete();
		       			}
		       			$t->delete();
		       		}
	       		}catch(\Exception $e){
	       			Log::error('ERROR DR0001 trying to delete messages');
	       		}
	       		

        	}
        	if(count($ride->ride_stop) > 0){
        		$stops = RidesStop::where('rides_id', '=', $ride->id)->get();
        		foreach($stops as $stop){
        			$stop->destroy();
        		}
        	}

        	if(!empty($ride->airport_details) > 0){
        		$airport = AirportDetails::where('ride_id', $ride->id)->get();
        		foreach($airport as $a){
        			$a->delete();
        		}

        	}

        	$cancel_ride = new CancelUserRides;
		    $cancel_ride->rides_id = $ride->id;
		    $cancel_ride->price_refound = $ride->price;
		    $cancel_ride->reason = $reason;
		    $cancel_ride->save();
            $ride->delete();

            return true;
    	}
    } //End of method

    public function destroyUserRidePostPay($user, $user_ride, $reason, $ride){

    	
    	$is24 = null;
    	$price = null;
    	
    	//If the user cancel after paying
     	if($user_ride->status == 'Approved' && $user_ride->booker_payment_paid == 'Yes'
 		&& $user_ride->sale_id != null){

	        $apiContext = new \PayPal\Rest\ApiContext(
	           new \PayPal\Auth\OAuthTokenCredential(
	             $this->paypal_client,
	             $this->paypal_secret
	           )
	        );


	         $date = strtotime($user_ride->ride->dateFrom ." ". $user_ride->ride->startHour);

	         //Its less than 24 hours
	         if($date < time() + 86400){
	             $is24 = true;
	             $price = ($user_ride->price_paid * 0.50) - 1.5;

	             $amt = new Amount();
	             $amt->setTotal($price)
	               ->setCurrency('USD');
	         }
	         else{
	             $is24 = false;
	             //It's more than 24 hours from ride
	             $price = ($user_ride->price_paid) - 1.5;

	             $amt = new Amount();
	             $amt->setTotal($price)
	               ->setCurrency('USD');
	         }

	         $refund = new Refund();
	         $refund->setAmount($amt);

	         $sale = new Sale();
	         $sale->setId($user_ride->sale_id);

	         try {
	           $refundedSale = $sale->refund($refund, $apiContext);
	         } catch (PayPal\Exception\PayPalConnectionException $ex) {
	           echo $ex->getCode();
	           echo $ex->getData();
	           die($ex);
	         } catch (Exception $ex) {
	           die($ex);
	         }
    	}

    	$threads = Thread::where('rides_id', '=', $ride->id)->get();
   		try{
   			foreach($threads as $t){
       			$message = $t->messages;
       			$participants = $t->participants;
       			foreach($participants as $p){
       				$p->delete(); 
       			}
       			foreach($messages as $m){
       				$m->delete();
       			}
       			$t->delete();
       		}
   		}catch(\Exception $e){
   			Log::error('ERROR DR0001 trying to delete messages');
   		}

	    $cancel_ride = new CancelUserRides;
	    $cancel_ride->rides_id = $ride->id;
	    $cancel_ride->passenger_id = $user_ride->user->id;
	    $cancel_ride->sales_id = $user_ride->sale_id;
	    $cancel_ride->price_refound = $price;
	    $cancel_ride->is24Hours = $is24;
	    $cancel_ride->reason = $reason;
	    $cancel_ride->save();

	    $email = $user_ride->user->email;
	    $info_user = array(
	         'ride' => $ride,
	         'price_return' => $price,
	         'user_ride' => $user_ride);

	     //Send Email to User about Cancel Ride
	     Mail::send('email.cancelUserRide', $info_user, function($message) use ($email) {
	                     $message->to($email)->subject("You cancelled your ride");
	     });

	     //Add back the seats for ride
	     if(count($ride->ride_stop) > 0){
	         //It is a stop
	         if($user_ride->isStop == true){
	             //It is a stop Point A to Stop
	             if($ride->city_from == $user_ride->city_from){
	                 $ride_stop = RidesStop::find($ride->ride_stop[0]->id);
	                 $ride_stop->seats_from = $ride_stop->seats_from+ 1;
	                 $ride_stop->update();
	             }else{
	                 //Is a stop Stop to Point B
	                 $ride_stop = RidesStop::find($ride->ride_stop[0]->id);
	                 $ride_stop->seats_to = $ride_stop->seats_to+ 1;
	                 $ride_stop->update();
	             }
	         }else{
	             $ride->seats_available = $ride->seats_available + 1;
	             if(count($ride->ride_stop) > 0){
	                 $ride_stop = RidesStop::find($ride->ride_stop[0]->id);
	                 $ride_stop->seats_to =  $ride_stop->seats_to+ 1;
	                 $ride_stop->seats_from = $ride_stop->seats_from+ 1;
	                 $ride_stop->update();
	             }
	             $ride->update();
	         }
	     }else{
	     	//Is a simple ride
	        $ride->seats_available = $ride->seats_available + 1;
	        $ride->update();
	     }

    }

    public function destroyUserRidePrePaid($user, $user_ride, $reason, $ride){

    	$cancel_ride = new CancelUserRides;
	    $cancel_ride->rides_id = $ride->id;
	    $cancel_ride->passenger_id = $user_ride->user->id;
	    $cancel_ride->sales_id = "N/A";
	    $cancel_ride->price_refound = $ride->price;
	    $cancel_ride->reason = $reason;
	    $cancel_ride->save();

	    $threads = Thread::where('rides_id', '=', $ride->id)->get();
   		try{
   			foreach($threads as $t){
       			$message = $t->messages;
       			$participants = $t->participants;
       			foreach($participants as $p){
       				$p->delete(); 
       			}
       			foreach($messages as $m){
       				$m->delete();
       			}
       			$t->delete();
       		}
   		}catch(\Exception $e){
   			Log::error('ERROR DR0001 trying to delete messages');
   		}


    	if(count($ride->ride_stop) > 0){
	         //It is a stop
	         if($user_ride->isStop == true){
	             //It is a stop Point A to Stop
	             if($ride->city_from == $user_ride->city_from){
	                 $ride_stop = RidesStop::find($ride->ride_stop[0]->id);
	                 $ride_stop->seats_from = $ride_stop->seats_from+ 1;
	                 $ride_stop->update();
	             }else{
	                 //Is a stop Stop to Point B
	                 $ride_stop = RidesStop::find($ride->ride_stop[0]->id);
	                 $ride_stop->seats_to = $ride_stop->seats_to+ 1;
	                 $ride_stop->update();
	             }
	         }else{
	             $ride->seats_available = $ride->seats_available + 1;
	             if(count($ride->ride_stop) > 0){
	                 $ride_stop = RidesStop::find($ride->ride_stop[0]->id);
	                 $ride_stop->seats_to =  $ride_stop->seats_to+ 1;
	                 $ride_stop->seats_from = $ride_stop->seats_from+ 1;
	                 $ride_stop->update();
	             }
	             $ride->update();
	         }
	     }else{
	     	//Is a simple ride
	        $ride->seats_available = $ride->seats_available + 1;
	        $ride->update();
	     }
    }

}
