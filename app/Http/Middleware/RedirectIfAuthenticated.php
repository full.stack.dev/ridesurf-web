<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Request;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {   
        // Handle verificatn request
        if(Auth::user()){
            if(Auth::user()->verified == 0 && "/verifyemail/".Auth::user()->email_token ==Request::getPathInfo())
            {

                $user = Auth::user();

                $user->verified = 1;
                $user->update();
                return redirect()->intended('/');
                return $next($request);

            }
            else{
                return redirect()->intended('/');
                return $next($request);
            }


        }
        if (Auth::guard($guard)->check()) {

            return redirect()->intended('/');

        }

        return $next($request);
    }
}
