<?php

namespace App\Http\Controllers;

use App\Airports;
use App\AirportDetails;
use App\Http\Traits\DestroyRides;
use App\Http\Traits\LocationHelper;
use App\Http\Traits\MailHelper;
use Cartalyst\Stripe\Stripe;
use App\Rides;
use App\RidesStop;
use App\Vehicle;
use App\UserRides;
use App\User;
use App\SearchLog;
use Carbon\Carbon;
use App\Reviews;
use App\TripPurpose;
use Illuminate\Http\Request;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use stdClass;
use App\Location;
use App\CancelUserRides;
use App\Connections;
use DB, Auth, Mail;
use Response, Redirect;
use DateTime, ArrayObject;


class RidesController extends Controller
{
    use DestroyRides, LocationHelper, MailHelper;

    private $admin;
    private $app_debug;
    private $paypal_client;
    private $paypal_secret;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __contruct()
    {

        $this->middleware('permission:driver-list', ['only' => ['view'] ] );
        $this->middleware('permission:driver-create', ['only' => ['create','store']]);
        $this->middleware('permission:driver-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:driver-delete', ['only' => ['destroy']]);

    }

    public function __construct()
    {
        $this->admin = User::where('first_name', '=', 'admin')->get()->first();
        $this->app_debug= env('APP_DEBUG');
        $this->paypal_client = env('PAYPAL_CLIENT_ID');
        $this->paypal_secret = env('PAYPAL_SECRET');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $states = DB::table('states')->get();
        $airports = Airports::all();
        $control = true;
        return view('rides.search', compact('states', 'airports', 'control'));

    }

    public function list(Request $request){
        ini_set('max_execution_time', 200);
        $body = $request->get('body');
        $from = $body['start'];
        $to = $body['stop'];
        $dateSearch = $body['date'];

        $data = null;
        $women_only = $body['women_only'];
        $price_control = $body['price'];


        //Parse data to get Town
        if($from != null && $to != null){

            $date = substr($dateSearch, 0, 10);
            
            $arrayFrom = explode(',', $from);

            $city_from = $this->parseLocation($arrayFrom, $from);
            $city_name = $this->getCity($city_from);

            $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);


            if($geocodeObject['status'] = "OK"){
                $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
                $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
                $city_from = $this->parseLocation($temp, $from);
            }

            //Check if the name is not a state
            $checker = DB::table('states')->where('name','=',$city_name)->count();
            if($checker > 0 ){

                $response = array(
                    'action' => 0 ,
                    'error' => 'Address cannot be a state, it must be a city name',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }


            if(count($arrayFrom) <= 1){
                $response = array(
                    'action' => 0 ,
                    'error' => 'Error getting Address',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }

            $arrayTo = explode(',', $to);
            $city_to = $this->parseLocation($arrayTo, $to);
            $city_name =  $this->getCity($city_to);

            $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);

            if($geocodeObject['status'] = "OK"){
                $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
                $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
                $city_to = $this->parseLocation($temp, $from);
            }

            //Check if the name is not a state
            $checker = DB::table('states')->where('name','=',$city_name)->count();
            if($checker > 0 ){

                $response = array(
                    'action' => 0 ,
                    'error' => 'Address cannot be a state, it must be a city name',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }

            if(count($arrayTo) <= 1){
                $response = array(
                    'action' => 0 ,
                    'error' => 'Error please provide a valid place',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }


            if($city_from == null || $city_to == null){

                $response = array(
                    'action' => 0 ,
                    'error' => 'Error getting Address please try again',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }

            if($women_only == true){
                $women_only = 'on';
            }else{
                $women_only = null;
            }

            //Add the logs
            $log = new SearchLog;
            $log->city_from = $city_from;
            $log->city_to = $city_to;
            $log->date_asked = $date;
            $log->save();


            //Use Location Parser trait to get the rides
            $rides = $this->getRides($city_from, $city_to, $date, $price_control, $women_only);
            $openRides = $this->getOpenRides($city_from, $city_to, $date, $price_control, $women_only);

            $currentUser = Auth::user();
            $isStop = false;
            // Add extra information
            if($rides || $openRides){
                if(count($rides) > 0 || count($openRides) > 0){
                    foreach($rides as $ride){

                        if(count($ride->ride_stop) > 0){
                            $ride->ride_stop = $ride->ride_stop;
                            $ride->isStop = true;
                        }else{
                            $ride->isStop = false;
                        }
                        $ride->date = date('j M Y', strtotime($ride->dateFrom));
                        $ride->start = date("h:i a", strtotime($ride->startHour));
                        $sum = (strtotime($ride->startHour) + ($ride->duration));
                        $ride->end = date("h:i a", $sum);

                    }
                    foreach($openRides as $ride){
                        $user = $ride->user;
                        $ride->open_date_from = date('j M Y', strtotime($ride->open_date_from));
                        $ride->open_date_to = date('j M Y', strtotime($ride->open_date_to));
                        $ride->hours = $ride->hours;
                        $ride->first_name = $user->first_name;
                        $ride->avatar = $user->avatar;
                        $ride->rates = $user->rating;
                        $ride->car_image = $user->driver[0]->path_image;
                        $ride->path_image = $user->driver[0]->path_image;
                    }
                    $data['openRides'] = $openRides;
                    $data['rides'] = $rides;
                    $data['action'] = 1;
                }else{
                    $data = array(
                        'action' => 2,
                        'currentUser' => $currentUser,
                        'message' => 'No Rides',
                        'from' => $from,
                        'to' => $to
                    );
                }
            }
        }
        else{
            $data = array(
                'action' => 0,
                'error' => 'No data provided');
        }

        header('Content-Type: application/json');
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $purposes = TripPurpose::orderBy('type', 'asc')->get();
        return view('rides.create', compact('user', 'purposes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_ride = $request->get('ride');
        $type = $request->get('type');
        $input_stop = $request->get('stops');

        $user = Auth::user();

        if($type == 'oneway'){
            //is one way
            $ride = new Rides();
            $ride->fill($input_ride);
            $ride->user_id = $user->id;
            $ride->save();

            if($input_stop){
                foreach($input_stop as $stop){
                    $ride_stop = new RidesStop;
                    $ride_stop->fill($stop);
                    $ride_stop->rides_id = $ride->id;
                    $ride_stop->user_id = $user->id;
                    $ride_stop->seats_from = $ride->seats;
                    $ride_stop->seats_to = $ride->seats;
                    $ride_stop->save();
                }
            }

            return response()->json('success');
        }else if($type == 'round'){
            // is Round Trip

            $input_ride['dateFrom'] = $input_ride['firstDate'];
            $input_ride['startHour'] = $input_ride['firstHour'];
            $ride = new Rides();
            $ride->fill($input_ride);
            $ride->user_id = $user->id;
            $ride->save();

            if($input_stop){
                foreach($input_stop as $stop){
                    $ride_stop = new RidesStop;
                    $ride_stop->fill($stop);
                    $ride_stop->rides_id = $ride->id;
                    $ride_stop->user_id = $user->id;
                    $ride_stop->seats_from = $ride->seats;
                    $ride_stop->seats_to = $ride->seats;
                    $ride_stop->save();
                }
            }

            $input_ride['dateFrom'] = $input_ride['returnDate'];
            $input_ride['startHour']  = $input_ride['returnHour'];
            //Return 
            $returning = new Rides();
            $returning->fill($input_ride);
            $returning->location_from = $ride->location_to;
            $returning->location_to = $ride->location_from;
            $returning->city_from = $ride->city_to;
            $returning->city_to = $ride->city_from;
            $returning->user_id = $user->id;

            $returning->save();

            if($input_stop){
                foreach($input_stop as $stop){
                    $ride_stop = new RidesStop;
                    $ride_stop->fill($stop);
                    $ride_stop->rides_id = $returning->id;
                    $ride_stop->user_id = $user->id;
                    $ride_stop->seats_from = $ride->seats;
                    $ride_stop->seats_to = $ride->seats;
                    $ride_stop->save();
                }
            }
            
            return response()->json('success');
            
        }else{
            //Is Airport
            $ride = new Rides();
            $ride->fill($input_ride);
            $ride->user_id = $user->id;
            $ride->save();

            $input_air = $request->get('airport');
            $airport = new AirportDetails();
            $airport->airport_id = $input_air['id'];
            $airport->airport = $input_air['name'];
            $airport->type = 'Picking';
            $airport->ride_id = $ride->id;
            $airport->save();

            return response()->json('success');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rides  $rides
     * @return \Illuminate\Http\Response
     */
    public function show(Rides $rides, $id)
    {
        $ride = Rides::find($id);

        $start = date("g:i a", strtotime($ride->startHour));

        $time =  strtotime($ride->startHour);

        $sum = ($time + $ride->duration);

        $end = date("h:i a", $sum);

        $locationFrom = $ride->location_from;

        $locationTo = $ride->location_to;

        if(!empty($ride->user_ride) && count($ride->user_ride) > 0){
            $usersRiding = UserRides::where('rides_id', '=', $ride->id)
                           ->where('status', '=', 'Approved')
                           ->get();

            $usersRiding = $usersRiding->filter(function ($item) use ($rides) {

                $user = User::where('id', '=', $item->user_id)->get()->first();
                if($user != null){
                     return $item;
                }

             })->values()->all();
             if(empty($usersRiding)){
                $usersRiding = null;
             }

        }
        else{
            $usersRiding = null;
        }

        return view('rides.show', compact('ride', 'locationFrom', 'locationTo', 'start', 'end', 'usersRiding') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rides  $rides
     * @return \Illuminate\Http\Response
     */
    public function edit(Rides $rides, $id)
    {
        $user = Auth::user();

        if($user->hasRole('Driver') || $user->hasRole('Admin')){
            $ride = Rides::find($id);

            $start = date("g:i a", strtotime($ride->startHour));

            $time =  strtotime($ride->startHour);

            $sum = ($time + $ride->duration);

            $end = date("h:i a", $sum);

            $locationFrom = $ride->location_from;
            $locationTo = $ride->location_to;

            if(!empty($ride->user_ride)){
                $usersRiding = UserRides::where('rides_id', '=', $ride->id)
                           ->where('status', '=', 'Approved')
                           ->get();
                $usersRiding = $usersRiding->filter(function ($item) use ($rides) {

                $user = User::where('id', '=', $item->user_id)->get()->first();
                    if($user != null){
                         return $item;
                    }

                 })->values()->all();
                 if(empty($usersRiding)){
                    $usersRiding = null;
                 }
            }
            else{
                $usersRiding = null;
            }


            return view('rides.edit', compact('ride', 'locationFrom', 'locationTo', 'start', 'end', 'usersRiding') );
        }
        else{
            $exception = 'You have no permissions to view this page';
            return view('errors.pagenotfound', compact('exception'));
        }
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rides  $rides
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rides $rides, $id)
    {


        $data = Rides::find($id);
//        $from = $request->get('from-fetch');
//        $to = $request->get('to-fetch');
        $dateFrom = $request->get('date');
        $price = $request->get('price');
        $seats = $request->get('seats');
        $time = $request->get('startHour');
        $description = $request->get('description');
        $acceptUsers = $request->get('acceptUsers');
        $cityStopOver = $request->get('city_stop');
        $stopOverDuration = $request->get('stopOverDuration');
        $addressStopOver = $request->get('stopOver');
        $priceStopOver_From = $request->get('StopPrice_From');
        $priceStopOver_To = $request->get('StopPrice_To');
        $ladies_only =$request->get('ladies_only');

        $passengers = UserRides::where('rides_id', '=', $id)->get()->count();

        $seats_available = $seats - $passengers;

//        $data->location_from = $from;
//        $data->location_to = $to;
        $data->dateFrom = $dateFrom;

        $data->startHour = $time;
        $data->description = $description;
        $data->acceptUsers = $acceptUsers;
        $data->seats_available = $seats_available;
        $data->city_from = $request->get('city_from');
        $data->city_to = $request->get('city_to');
        $data->luggage_size = $request->get('luggage_size');
        $data->luggage_amount = $request->get('luggage_amount');
        $data->ladies_only = $ladies_only;



        if(!empty($cityStopOver)){

            if(count($data->ride_stop) > 0){
                $ride_stop = RidesStop::where('rides_id', '=', $data->id)->get()->first();

                $ride_stop->rides_id = $data->id;
                $ride_stop->address = $addressStopOver;
                $ride_stop->city_name = $cityStopOver;
                $ride_stop->price_from = $priceStopOver_From;
                $ride_stop->price_to = $priceStopOver_To;
                if(count($data->user_ride) > 0){

                    if($data->seats != $seats){

                        if($ride_stop->seats_from == 0){

                            $ride_stop->seats_from = $seats - $data->seats;

                        }else{
                            $ride_stop->seats_from = $seats - $ride_stop->seats_from;
                            if($ride_stop->seats_from < 0){
                                Session::flash('error', "you cannot change
                                    to offer less seats when you have passengers");
                                return redirect()->back();
                            }
                        }
                        if($ride_stop->seats_to == 0){
                            $ride_stop->seats_to = $seats - $data->seats;

                        }else{
                            $ride_stop->seats_to = $seats - $ride_stop->seats_to;
                            if($ride_stop->seats_to < 0){

                                Session::flash('error', "you cannot change
                                    to offer less seats when you have passengers");
                                return redirect()->back();
                            }
                        }
                    }
                }

                $ride_stop->update();
            }else{
                $ride_stop = new RidesStop;
                $ride_stop->rides_id = $data->id;
                $ride_stop->address = $addressStopOver;
                $ride_stop->duration = $stopOverDuration;
                $ride_stop->city_name = $cityStopOver;
                $ride_stop->price_from = $priceStopOver_From;
                $ride_stop->price_to = $priceStopOver_To;
                $ride_stop->seats_from = $seats;
                $ride_stop->seats_to = $seats;

                $ride_stop->save();
            }


        }

        $data->price = $price;
        if(count($data->user_ride)>0){
            if($data->seats == $seats){

            }else{
                $data->seats = $seats;
                $data->seats_available = $seats - $data->seats_available;
                if($data->seats_available <= 0){

                    Session::flash('error', "you cannot change to offer less seats when you have passengers");
                    return redirect()->back();
                }
            }
        }else{
            $data->seats = $seats;
        }

        $data->update();


        //If it has passenger send emails to passenger
        if(count($data->user_ride) > 0){

            //Send mail to passengers
            foreach($data->user_ride as $passenger){

                $user_name = $passenger->user->first_name;
                $email = $passenger->user->email;
                $rideTo = $passenger->ride->location_to;
                $user_id = $passenger->user->id;
                $info_user = array(
                    'name'=>$user_name,
                    'rideFrom' => $passenger->ride->location_from,
                    'rideTo' => $passenger->ride->location_to,
                    'driver' => $passenger->ride->user->first_name,
                    'date' => $passenger->ride->dateFrom,
                    'hour' => $passenger->ride->startHour,
                    'description' => $passenger->ride->description,
                    'ride_id' => $passenger->ride->id
                );

                if($user_id != null){
                    $message = "The driver on your Ridesurf trip to " . $rideTo .
                    " has made some changes";

                    $thread = Thread::create([

                        'subject' => 'Your ride to ' . $rideTo .' has changed' ,
                        'rides_id' => $info_user['ride_id'],
                        'isNotification' => 'Yes',
                    ]);
                    $thread->isNotification = "Yes";
                    $thread->update();

                    // Message
                    Message::create([
                        'thread_id' => $thread->id,
                        'user_id' => $this->admin->id,
                        'body' => $message,
                    ]);

                    Participant::create([
                        'thread_id' => $thread->id,
                        'user_id' => $user_id,
                        'last_read' => new Carbon,
                    ]);


                    // Recipients
                    if (Input::has('recipients')) {
                        $thread->addParticipant(8);
                    }

                    Mail::send('email.changeRide', $info_user, function($message) use ($email) {
                        $message->to($email)->subject("Your ride has been modified");
                    });
                }
                else{
                    Session::flash('error', "There was an error sending messages to the passengers");
                    return back();
                }
            }
            Session::flash('message', "We've updated your ride and sent your passengers the new info.");

        }else{

            Session::flash('message', "Successfully edited ride");
        }


        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rides  $rides
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {

        $ride = Rides::find($id);
        $user = Auth::user();
        $response = $this->destroySimpleRideDriver($ride->id, $user->id, null);

        if($response == true){
            $response = array(
                'message' => "Ride deleted, if you had passengers we contacted them to let them know.",
                'status' => 'success',
            );
            return \Response::json($response);
        }
        else{
            $response = array(
                'message' => "Error deleting ride, make sure you have no current passengers booked, if the problem persists please contact us",
                'status' => 'error'
            );
            return \Response::json($response);
        }
    }

    //Cancel Ride from User
    public function destroyUserRide(Request $request, $id){

        $user = Auth::user();
        $user_ride = UserRides::where('rides_id', '=', $id)
                    ->where('user_id', '=', $user->id)->get()->first();
        $ride = Rides::find($id);
        $reason = $request->get('cancel_reason');

        if($user_ride->booker_payment_paid == "Yes"){
            $this->destroyUserRidePostPay($user, $user_ride, $reason, $ride);
        }
        else{
            $this->destroyUserRidePrePaid($user, $user_ride, $reason, $ride);
        }
        //Send Message to Driver that User cancel:
        $message = "The passenger " . $user_ride->user->first_name. " on your ride " .$user_ride->ride->location_to . " has canceled.";

        $thread = Thread::create([
             'subject' => 'Passenger has cancelled ride to ' .$user_ride->ride->location_to,
             'rides_id' => $ride->id,
             'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
             'thread_id' => $thread->id,
             'user_id' => $this->admin->id,
             'body' => $message,
        ]);


        Participant::create([
             'thread_id' => $thread->id,
             'user_id' => $user_ride->ride->user->id,
             'last_read' => new Carbon,
        ]);

        // Recipients
        if (Input::has('recipients')) {
             $thread->addParticipant(9);
        }

        $email = $user_ride->ride->user->email;
        $info_user = array(
             'location_to' => $user_ride->ride->location_to,
             'ride' => $ride,
             'user_ride' => $user_ride,
             'name' => $user_ride->ride->user->first_name,
             'passenger_name' => $user_ride->user->first_name,
        );


        //Send Email to Driver about Cancel Ride
        Mail::send('email.cancel_userRide_driver', $info_user, function($message) use ($email) {

                         $message->to($email)->subject("Your passenger has cancelled the ride");
        });

        $user_ride->delete();

        Session::flash('success', 'Successfully cancelled the ride and contacted the driver');

        return redirect('/dashboarduser/booked');

    }
    /**
     * the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {

         if($request->get('query'))
         {

          $query = $request->get('query');

          $data = DB::table('locations')
            ->where('city', 'LIKE', "%{$query}%")->take(20)
            ->get();
          $output = '<ul class="dropdown-menu" style="display:block; position:relative">';

          foreach($data as $row)
          {
           $output .= '
           <li><a href="#" value='.$row->id.'>'.$row->name. '</a></li>
           ';
          }
          $output .= '</ul>';
          echo $output;
         }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function join(Request $request, $id){

        if(!Auth::user()){
            return response()->json(['message' => 'Please log in to join ride'], 301);
        }

        $from = $request->from;
        $to = $request->to;
        $price = $request->price;
        $isStop = $request->isStop;
        $ride = Rides::find($id);

        $user = Auth::user();

        $exisits = UserRides::where('rides_id', '=', $ride->id)
                   ->where('user_id', '=', $user->id)->count();

        if($user->avatar == null || $user->gender == null || $exisits > 0 ||
           $user->verified == 0 ||  $user->id == $ride->id ){
            $allow = false;
            $reason = $user->avatar == null ? ' No Profile picture, ': null;
            $reason = $user->gender == null ?  ' No Gender, ' : null;
            $reason = $user->verified == null ? ' Verify email, ' : null;
            $reason = $exisits > 0 ? ' You already book this ride ' : null;
        }else{
            $reason = null;
            $allow = true;
        }

        if(!empty($ride->user_ride) && count($ride->user_ride) > 0){
            $usersRiding = UserRides::where('rides_id', '=', $ride->id)
                           ->where('status', '=', 'Approved')
                           ->get();

            $usersRiding = $usersRiding->filter(function ($item) use ($ride) {

                $user = User::where('id', '=', $item->user_id)->get()->first();
                if($user != null){
                     return $item;
                }

             })->values()->all();
             if(empty($usersRiding)){
                $usersRiding = null;
             }

        }
        else{
            $usersRiding = null;
        }
        if($isStop){
            if($ride->location_from == $from){
                $start = date("g:i a", strtotime($ride->startHour));
            }else{
                $timing = $ride->ride_stop[0]->duration + strtotime($ride->startHour);
                $start = date("g:i a", $timing);
            }
        }else{
            $start = date("g:i a", strtotime($ride->startHour));
        }

        $time =  strtotime($ride->startHour);
        $sum = ($time + $ride->duration);
        $end = date("h:i a", $sum);

        $user = $ride->user;

        $vehicle = [
            'make' => DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake,
            'model' => DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle,
            'color' => DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)->get()->first()->vColour_EN,
            'type' => DB::table('car_type')->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN,
            'year' => $user->driver[0]->year,
            'path_image' => $user->driver[0]->path_image,
        ];

        $ride->user = $ride->user;
        $ride->end = $end;
        $ride->start = $start;

        if($isStop){
            $ride->totalSeats = 0;
            if($ride->ride_stop[0]->seats_from > 0){
                if($ride->location_from == $from){
                    for($i=1; $i <= $ride->ride_stop[0]->seats_from; $i++){
                        $ride->totalSeats++;
                    }

                }
            }else{
                if($ride->ride_stop[0]->seats_to > 0){
                    for($i=1; $i <= $ride->ride_stop[0]->seats_to; $i++){
                        $ride->totalSeats++;
                    }
                }
            }
        }

        if($ride->aiport){
            $ride->airport = $ride->airport;
        }

        

        return response()->json([
            'isStop' => $isStop,
            'allow' => $allow,
            'usersRiding' => $usersRiding,
            'from' => $from,
            'to' => $to,
            'ride' => $ride,
            'price' => $price,
            'vehicle' => $vehicle,
            'reason' => $reason,
        ]);

//        return view('rides.join', compact('user', 'ride', 'vehicle', 'usersRiding', 'start', 'end',
//            'isStop', 'from', 'to', 'exisits', 'price', 'isStop'));

    }

    //Store the information when a Passenger Pays
    public function storejoin(Request $request, $id){

     $seats = $request->get('seats');
     $data = $request->get('data');
     $sale_id = $request->get('sale_id');

     $user = Auth::user();

     $ride = Rides::find($id);

     $user_ride = UserRides::where('user_id', '=', $user->id)->where('rides_id', '=', $id)->get()->first();

     $booking_number = "BCCS" . $user->id ."-". $id;
     $user_ride->booking_number = $booking_number;
     $user_ride->booking_date = Carbon::now();
     $user_ride->booker_payment_paid = 'Yes';
     $user_ride->transaction_id = $data['paymentID'];
     $user_ride->payerID = $data['payerID'];
     $user_ride->orderID = $data['orderID'];
     $user_ride->paymentToken = $data['paymentToken'];
     $user_ride->returnUrl = $data['returnUrl'];
     $user_ride->sale_id = $sale_id;
     $user_ride->price_paid = $request->get('amount');

     $user_ride->update();

     $message = "Congratulations! You have a passenger for your trip to " . $ride->location_to . ". Your passenger's name is " . $user->first_name . ", please contact them to arrange a safe pickup and dropoff location. \n Happy travels!";

     $thread = Thread::create([
         'subject' => 'New passenger for your trip to ' .$ride->location_to,
         'rides_id' => $ride->id,
         'isNotification' => 'Yes',
     ]);
     $thread->isNotification = "Yes";
     $thread->update();

     // Message
     Message::create([
         'thread_id' => $thread->id,
         'user_id' => $this->admin->id,
         'body' => $message,
     ]);

     // Sender
     Participant::create([
         'thread_id' => $thread->id,
         'user_id' => $ride->user->id,
         'last_read' => new Carbon,
     ]);

     // Recipients
     if (Input::has('recipients')) {
         $thread->addParticipant($ride->user->id);
     }

     //Send to Driver
     $driver_name = $ride->user->first_name;
     $email = $ride->user->email;
     $info = array('name'=>$driver_name, "user" => $user->first_name, "rideTime" => $ride->startHour,
        "rideDate" => date('j M Y', strtotime($ride->dateFrom)), "rideFrom" => $user_ride->city_from, "rideTo" => $user_ride->city_to);

     Mail::send('email.bookedDriver', $info, function($message) use ($email) {
         $message->to($email)->subject('Passenger paid for your ride');
     });

     //Send to Passenger
     $user_name = $user->first_name;
     $email = $user->email;
     $info_user = array('name'=>$user_name, "user" => $user->first_name, "rideTime" => $ride->startHour,
        "rideDate" => date('j M Y', strtotime($ride->dateFrom)), "rideFrom" => $ride->location_from, "rideTo" => $ride->location_to, "driver" => $driver_name,
         "booking_confirm" => $booking_number, "price" => $user_ride->price_paid);

     Mail::send('email.bookedUser', $info_user, function($message) use ($email) {
         $message->to($email)->subject('Confirmation of your upcoming trip');
     });

     $response['status'] = "Success";
     $response['rideFrom'] = $user_ride->city_from;
     $response['rideTo'] = $user_ride->city_to;
     $response['driver'] = $ride->user->first_name;
     $response['message'] = "You are confirmed to go to " . $ride->location_to ." with " .$ride->user->first_name .".";

     $response['booking_number'] = $booking_number;
     return \Response::json($response);

    }

    //Passenger Request to join a ride
    public function request(Request $request, $id, $price){

        $body = $request->get('body');

        if(empty($body)){
            return response()->json(['message' => 'There was a server problem, if this persist please contact us']);
        }
        $user = Auth::user();
        $ride = Rides::find($id);
        $check = UserRides::where('user_id', '=', $user->id)->where('rides_id', '=', $ride->id)->count();

        if($check > 0 ){

            return \Response::json(['message' => 'Sorry, you already requested to join this ride'], 401);
        }

        $user_ride = new UserRides;

        $user_ride->user_id = $user->id;
        $user_ride->rides_id = $ride->id;
        $user_ride->seats = $body['seats'];
        $user_ride->description = $body['description'];
        $user_ride->status = 1;
        $user_ride->ride_cost = $price;
        $user_ride->city_from = $body['city_from'];
        $user_ride->city_to = $body['city_to'];
        $user_ride->isStop = $body['isStop'];
        $user_ride->booker_payment_paid = 'No';

        $user_ride->save();

        $this->mailRequest_ToDriver($user, $ride);

        return \Response::json(['message' => 'Success'], 200);
    }



    // Create a new Ride
    public function StoreInformation(Request $request){

        if(!empty($request->get('toDestination')) || !empty($request->get('fromDestination'))){
            if($request->get('cityTo') == null || $request->get('cityFrom') == null ||
                $request->get('duration') == null || $request->get('time') == null ){

                $returnData = array(
                    'status' => 'error',
                    'message' => 'Error: Some inputs are blank, please refresh the page and try again.'
                );
                return response()->json($returnData, 404);
            }

            $nameFrom = $request->get('fromDestination');
            $nameTo = $request->get('toDestination');
            $dateFrom = $request->get('dateFrom');
            $price = $request->get('price');
            $seats = $request->get('seats');
            $description = $request->get('description');
            $time = $request->get('time');
            $checkbox = $request->get('acceptUsers');
            $city_from = $request->get('cityFrom');
            $city_to = $request->get('cityTo');
            $luggage_size = $request->get('luggage_size');
            $luggage_amount = $request->get('luggage_amount');
            $ladies_only = $request->get('ladies_only');
            $duration = $request->get('duration');
            $stopOvers = $request->get('stopOvers');
            $TripPurpose = $request->get('tripPurpose');
            $state = $this->getState($city_from);
            $state_to = $this->getState($city_to);


            if(strlen($state) > 2 && strlen($state_to) > 2){
                $returnData = array(
                    'status' => 'error',
                    'message' => 'Please make sure the states are in the two letter format, ex: "FL" for Florida'
                );
                return response()->json($returnData, 404);
            }

            if($ladies_only == null){
                $ladies_only = 'No';
            }
            if(!empty($request->get('dateTo'))){
                $dateTo = $request->get('dateTo');
                $returning = $request->get('returning');
            }
            else{
                $dateTo = null;
                $return = null;
            }

        }
        else{
            $returnData = array(
                'status' => 'error',
                'message' => 'Please enter all information'
            );
            return response()->json($returnData, 500);
        }

        $user = Auth::user();
        //create the instance of rides
        $ride = new Rides;
        $ride->dateFrom = $dateFrom;
        $ride->price = $price;
        $ride->seats = $seats;
        $ride->description = $description;
        $ride->startHour = $time;
        $ride->user_id = $user->id;
        $ride->seats_available = $seats;
        $ride->location_from = $nameFrom;
        $ride->location_to = $nameTo;
        $ride->city_from = $city_from;
        $ride->city_to = $city_to;
        $ride->duration = $request->get('duration');
        $ride->luggage_size = $luggage_size;
        $ride->luggage_amount = $luggage_amount;
        $ride->ladies_only = $ladies_only;
        $ride->completed = 'Ongoing';
        $ride->trip_purpose_id = $TripPurpose;

        if($dateTo != null){

            $Newride = new Rides;
            $Newride->dateFrom = $dateTo;
            $Newride->price = $price;
            $Newride->seats = $seats;
            $Newride->description = $description;
            $Newride->user_id = $user->id;
            $Newride->startHour = $returning;
            $Newride->seats_available = $seats;
            $Newride->location_from = $nameTo;
            $Newride->location_to = $nameFrom;
            $Newride->luggage_size = $luggage_size;
            $Newride->luggage_amount = $luggage_amount;
            $Newride->ladies_only = $ladies_only;
            $Newride->trip_purpose_id = $TripPurpose;
            $ride->completed = 'Ongoing';
            //It is the opposive for the return
            $Newride->city_from = $city_to;
            $Newride->city_to = $city_from;
            $Newride->duration = $request->get('duration');
            $Newride->save();
        }

        $ride->save();
        if(!empty($stopOvers)){
            for($i=0; $i < count($stopOvers); $i++){
                $ride_stop = new RidesStop;
                $ride_stop->city_name = $stopOvers[$i]['name'];
                $ride_stop->rides_id = $ride->id;
                $ride_stop->user_id = $user->id;
                $ride_stop->address = $stopOvers[$i]['address'];
                $ride_stop->duration = $stopOvers[$i]['duration'];
                $ride_stop->seats_from = $seats;
                $ride_stop->seats_to = $seats;
                $ride_stop->price_from = $stopOvers[$i]['price_from'];
                $ride_stop->price_to = $stopOvers[$i]['price_to'];
                $ride_stop->save();
                if($dateTo != null && !empty($Newride)){
                    $ride_stop = new RidesStop;
                    $ride->stop->city_name = $stopOvers[$i]['name'];
                    $ride_stop->rides_id = $ride->id;
                    $ride_stop->user_id = $user->id;
                    $ride_stop->address = $stopOvers[$i]['address'];
                    $ride_stop->duration = $stopOvers[$i]['duration'];
                    $ride_stop->seats_from = $seats;
                    $ride_stop->seats_to = $seats;
                    $ride_stop->price_from = $stopOvers[$i]['price_to'];
                    $ride_stop->price_to = $stopOvers[$i]['price_from'];
                    $ride_stop->save();
                }
            }
        }


        $returnData = array(
                    'status' => 'success',
                    'message' => 'Ride has been created',
                    'state' => $state,
                );
        return response()->json($returnData, 200);
    }

    public function payment(Request $request, $id){


        $ride = Rides::find($id);

        $user = Auth::user();

        $paypalToken = $this->paypal_client;

        $mode = env('PAYPAL_MODE');

        $user_ride = UserRides::where('user_id', '=', $user->id)->where('rides_id', '=', $ride->id)->get()->first();

        if($user_ride->status == 'Approved' && $user_ride->booker_payment_paid == 'Yes'){

            Session::flash('message', "You already paid for this ride.");

            return redirect('/dashboarduser/dashboarduser');
        }


        return view('rides.payment', compact('user', 'ride', 'user_ride', 'paypalToken', 'mode'));

    }

    public function denyPassenger(Request $request, $id, $user){

        $ride = Rides::find($id);

        $user = User::find($user);

        $user_ride = UserRides::where('user_id', '=', $user->id)->where('rides_id', '=', $ride->id)->get()->first();

        $user_ride->status = 'Denied';

        $user_ride->update();

        $ride->seats_available = $ride->seats_available + $user_ride->seats;
        //Send Message to Driver

        $message = "Sorry, the driver has denied your request to join the trip. We encourge you to look for another one!";

        $thread = Thread::create([
            'subject' => 'New passenger request for ' .$ride->location_to,
            'rides_id' => $ride->id,
            'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $user->id,
            'last_read' => new Carbon,
        ]);

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($ride->user->id);
        }


        $driver_name = $user->first_name;
        $email = $user->email;
        $info = array("user" => $user->first_name, "rideTo" => $ride->location_to);

        Mail::send('email.denyPassenger', $info, function($message) use ($email) {
            $message->to($email)->subject('Ride Request Update');
        });

        Session::flash('message', "Request for driver has been updated");

        return redirect()->back();

    }

    public function acceptPassenger(Request $request, $id, $user){

        $ride = Rides::find($id);

        if($ride->seats_available == 0){

            Session::flash('error', 'Sorry you cannot accept more riders, your car is full! ');

            return Redirect::back();
        }

        $user = User::find($user);

        $user_ride = UserRides::where('user_id', '=', $user->id)->where('rides_id', '=', $ride->id)->get()->first();
        $seats = $user_ride->seats;

        $user_ride->status = 'Approved';

        $user_ride->update();
        if($user_ride->isStop == true){
            //Check that the ride book is a stop over
            $ride_stop = RidesStop::find($ride->ride_stop[0]->id);

            //Check if the ride is From Point A to Stop Over
            if($user_ride->city_from == $ride->location_from){
                $ride_stop->seats_from = $ride_stop->seats_from - $seats;
                if($ride_stop->seats_from < 0){
                    Session::flash('error', 'Sorry you cannot accept more riders, your car is full! ');

                    return Redirect::back();
                }

            }else{
                //The ride is from Stop Over to Point B
                $ride_stop->seats_to = $ride_stop->seats_to - $seats;
                if($ride_stop->seats_to < 0){
                    Session::flash('error', 'Sorry you cannot accept more riders, your car is full! ');

                    return Redirect::back();
                }
            }



            $ride_stop->update();
        }else{
            $ride->seats_available = $ride->seats_available - $user_ride->seats;
            if($user_ride->isStop == true){
                $ride_stop = RidesStop::find($ride->ride_stop[0]->id);
                $ride_stop->seats_to = $ride_stop->seats_to - $seats;
                $ride_stop->seats_from = $ride_stop->seats_from - $seats;
                if($ride_stop->seats_to < 0 || $ride_stop->seats_from < 0){
                    Session::flash('error', 'Sorry you cannot accept more riders, your car is full! ');

                    return Redirect::back();
                }
                $ride_stop->update();
            }
            if($ride->seats_available < 0){
                Session::flash('error', 'Sorry you cannot accept more riders, your car is full! ');

                return Redirect::back();
            }
            $ride->update();
        }



        $ride->update();
        $message = "Congratulations, you are going to ".$user_ride->ride->location_to."! The driver has approved your request to join the ride. Please go to your booking page and pay asap.";

        $thread = Thread::create([
            'subject' => 'Approved for ride to ' .$ride->location_to,
            'rides_id' => $ride->id,
            'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $user->id,
            'last_read' => new Carbon,
        ]);

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($ride->user->id);
        }

        $driver_name = $user->first_name;
        $email = $user->email;

        $info = array("user" => $user->first_name, "rideTo" => $ride->location_to, "driver" => $driver_name, "ride_id" => $ride->id);

        Mail::send('email.acceptPassenger', $info, function($message) use ($email) {
            $message->to($email)->subject('Ride Request Update');
        });

        Session::flash('message', "Request for driver has been updated");

        return redirect()->back();
    }

    public function getAirports(){
        $airports = Airports::orderBy('name', 'desc')->get();

        return $airports;
    }

    public function searchAirport(Request $request){

        $body = $request->body;
        $date = $body['date'];
        $time_today = strtotime($date);
        $airport = $body['airport'];

        $from = $airport['city'] . ', ' . $airport['state'];

        $date = substr($date, 0, 10);

        $rides = Rides::with(array('airport_details'=>function($query) use($airport) {
            $query->where('airport_id', '=', $airport['id']);
        }))
        ->where('city_from','like', '%'.$from. '%')
        ->where('ride_type', '=', 'Airport')
        ->where('dateFrom', '>=', $date)
        ->get();

        $rides = ($rides->filter(

            function ($item) use ($rides,$time_today) {
                $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
                if($time_ride >= $time_today){
                    return $item;
                }

            }
        ));

        foreach($rides as $ride){
            if(count($ride->ride_stop) > 0){
                $ride->ride_stop = $ride->ride_stop;
            }
            $ride->date = date('j M Y', strtotime($ride->dateFrom));
            $ride->start = date("h:i a", strtotime($ride->startHour));
            $sum = (strtotime($ride->startHour) + ($ride->duration));
            $ride->end = date("h:i a", $sum);

            $user = User::find($ride->user_id);

            if(count($user->driver) > 0){
                $ride->make = DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake;
                $ride['model'] = DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle;
                $ride->color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                    ->get()->first()->vColour_EN;
                $ride->type = DB::table('car_type')
                    ->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;
                $ride->year = $user->driver[0]->year;
                $ride->car_image = $user->driver[0]->path_image;
                $ride->rating = $ride->rated == null ? 0 : $ride->rated;

            }
            $ride->user = $ride->user;
            $ride->avatar = $ride->user->avatar;

        }

        if( count($rides) == 0){
            $data['openRides'] = null;
            $data['rides'] = null;
            $data['action'] = 0;
        }else{
            $data['openRides'] = null;
            $data['rides'] = $rides;
            $data['action'] = 1;
        }



        header('Content-Type: application/json');
        return response()->json($data);
    }

}
