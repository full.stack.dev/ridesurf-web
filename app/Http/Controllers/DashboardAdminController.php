<?php

namespace App\Http\Controllers;

use App\DashboardAdmin;
use App\OpenRide;
use Illuminate\Http\Request;
use App\User;
use App\Vehicle;
use App\CancelUserRides;
use App\UserRides;
use App\Driver;
use App\Contact;
use App\Reviews;
use Carbon\Carbon;
use App\Rides;
use App\Report;
use App\Exports\UsersExport;
use App\Exports\DriverExport;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Input;
use Cmgmyr\Messenger\Models\Message;
use PayPal\Api\Payout;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use Auth, Storage, DB, Response, File, Mail;
use Session, Redirect, Hash;

class DashboardAdminController extends Controller
{

    private $admin;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:Administer roles & permissions');
        $this->middleware('permission:role-list');
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        $this->admin = User::where('first_name', '=', 'admin')->get()->first();

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->apiContext = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->apiContext->setConfig($paypal_conf['settings']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = Carbon::now();
        $users = User::all()->count();
        $lmUsers = User::where('created_at', '>=', $now->startOfMonth()->subMonth())
                       ->where('created_at', '<=', $now->endOfMonth()->subMonth())
                       ->count();

        $monthUsers = User::where('created_at', '>=', $now->startOfMonth())->count();
         
        if($monthUsers > 0)
          $statsUsers = ( ($monthUsers - $lmUsers) / $monthUsers) * 100; 
        else
          $statsUsers = 0;     



        $rides = Rides::all()->count();

        $booked = UserRides::all()->count();

        $drivers = Driver::where('status' ,'=' , 'Approved')->count();

        $lmDrivers = Driver::where('created_at', '>=', $now->startOfMonth()->subMonth())
                       ->where('created_at', '<=', $now->endOfMonth()->subMonth())
                       ->count();

        $monthDriver = Driver::where('created_at', '>=', $now->startOfMonth())->count(); 

        if($monthDriver > 0)
          $statsDrivers = ( ($monthDriver - $lmDrivers) / $monthDriver) * 100; 
        else
          $statsDrivers = 0;         

        $admins = User::whereHas('roles', function ($query) {
                    $query->where('name', '=', 'Admin');
         })->get();

        $loggedUsers = User::orderBy('last_login', 'DESC')->get()->take(10);

        $LastestRides = Rides::with('user')->orderBy('created_at', 'DESC')->get()->take(10);

        $lastOpenRides = OpenRide::orderBy('created_at', 'DESC')->get()->take(10);

        return view('dashboardadmin.index', compact('users', 'rides', 'booked', 'drivers', 
            'admins', 'loggedUsers', 'LastestRides', 'lastOpenRides', 'statsUsers', 'statsDrivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /*********** LABELS *******************/

    public function addNewLabel(){

        return view('dashboardadmin/settings/addNewLabel');
    }

    public function createNewLabel(Request $request){

        DB::table('language_label')->insert(
            ['vLabel' => $request->get('vLabel'), 'vValue' => $request->get('vValue'), 
            'vCode' => $request->get('vCode')]
        );

        $datas = DB::table('language_label')->orderBy('vLabel', 'ASC')->paginate(35);

        Session::flash('message', "Successfully added the label" . $request->get('vLabel'));

        return Redirect::to('dashboardadmin/language-label');

    }

    public function languageLabel(){

        $datas = DB::table('language_label')->orderBy('vLabel', 'ASC')->paginate(35);

        return view('dashboardadmin.settings.languageLabel', compact('datas'));
    }
    
    public function seachLabel(Request $request){

        if($request->get('searchLabel')){
            $query = $request->get('searchLabel');
          
            $datas = DB::table('language_label')
            ->where('vLabel', 'LIKE', "%{$query}%")->paginate(35);
        }
        else if($request->get('searchLabel') == ""){
            $datas = DB::table('language_label')->orderBy('vLabel', 'ASC')->paginate(35);

            return view('dashboardadmin.settings.languageLabel', compact('datas'));
        }
        else{
            return Redirect::back()->withErrors(['msg', 'Something went wrong']);
        }

        return view('dashboardadmin/settings/languageLabel', compact('datas'));
    }

    public function editLanguageLabel($LanguageLabelId){
        $data = DB::table('language_label')->where('LanguageLabelId', '=', $LanguageLabelId)->get()->first();

        return view('dashboardadmin/settings/editLanguageLabel', compact('data'));
    }



    public function updateLanguageLabel(Request $request, $LanguageLabelId){

        DB::table('language_label')->where('LanguageLabelId', '=', $LanguageLabelId)
                ->update(array(
                    'LanguageLabelId'=> $LanguageLabelId,
                    'vLabel'=>$request->get('vLabel'),
                    'vValue'=>$request->get('vValue'),
                    'vCode'=>$request->get('vCode'),
        ));

        Session::flash('message', "Successfully updated label");

        $data = DB::table('language_label')->where('LanguageLabelId', '=', $LanguageLabelId)->get()->first();


        return view('dashboardadmin/settings/editLanguageLabel', compact('data'));
    }

    public function deleteLanguageLabel(Request $request, $LanguageLabelId){


        DB::table('language_label')->where('LanguageLabelId', $LanguageLabelId)->delete();

        Session::flash('message', "Successfully deleted the label");

        return Redirect::to('dashboardadmin/language-label');

    }

    public function payToDrivers(Request $request){

        $now = Carbon::today();

        $rides = UserRides::join('rides', 'rides.id', '=' ,'user_rides.rides_id')
                      ->where('paid_driver', '=', 'No')
                      ->where('booker_payment_paid', '=', 'Yes')
                      ->where('rides.dateFrom' ,'<=', Carbon::today())
                      ->groupBy('rides_id')
                      ->get();

        $user_rides = UserRides::join('rides', 'rides.id', '=' ,'user_rides.rides_id')
                      ->join('users as driver','rides.user_id', '=', 'driver.id')
                      ->where('paid_driver', '=', 'No')
                      ->where('booker_payment_paid', '=', 'Yes')
                      ->where('rides.dateFrom', '<=', Carbon::today()->format('Y-m-d'))
                      ->select('rides.id', 'driver.first_name as first_name', 'driver.email', 'price', 'dateFrom', 'driver.id as user_id', 'user_rides.price_paid')
                      ->orderBy('dateFrom', 'DESC')
                      ->get();

        $UpdateRides = Rides::join('user_rides', 'user_rides.rides_id', '=', 'rides.id')
                      ->where('paid_driver', '=', 'No')
                      ->where('user_rides.booker_payment_paid', '=', 'Yes')
                      ->where('rides.dateFrom', '<=' ,Carbon::today())
                      ->get();

        $cancelRides = CancelUserRides::where('created_at', '<=', $now)
                        ->where('is24Hours', '=', true)->get();

        
        $totalPayout = array();

        $i = 0;
        //Set up Objects with rides id
        foreach($rides as $uRide){
            $totalPayout[$uRide->rides_id] = new totalPayout();
            $totalPayout[$uRide->rides_id]->rides_id = $uRide->rides_id;
            $totalPayout[$uRide->rides_id]->price = 0;
        }
        

        foreach($user_rides as $uRide){
            $totalPayout[$uRide->id]->user_id = $uRide->user_id;
            $totalPayout[$uRide->id]->first_name = $uRide->first_name;
            $totalPayout[$uRide->id]->email = $uRide->email;
            $totalPayout[$uRide->id]->dateFrom = $uRide->dateFrom;
            if($uRide->price_paid > 0 && $uRide->price_paid != null){
              array_push($totalPayout[$uRide->id]->price_pay, $uRide->price_paid);
              $temp = round( (($uRide->price_paid - 1) * 0.89), 2);
              array_push($totalPayout[$uRide->id]->transactions, $temp);

              $totalPayout[$uRide->id]->price = round( ($totalPayout[$uRide->id]->price + ($uRide->price_paid - 1)) * 0.89, 2);
            }
            else{
              array_push($totalPayout[$uRide->id]->transactions,0);
              array_push($totalPayout[$uRide->id]->price_pay, 0);
            }
            
        }

        

        foreach($cancelRides as $cancel){
          if(!empty($cancel)){
            
            if($totalPayout[$cancel->rides_id] != null){
              $totalPayout[$cancel->rides_id]->price = round( ($totalPayout[$uRide->id]->price + 
                                                       ($cancel->price_refound)), 2 );
            }else{
              $totalPayout[$cancel->rides_id] = new totalPayout();
              $totalPayout[$uRide->id]->user_id = $cancel->ride->user->id;
              $totalPayout[$cancel->rides_id]->rides_id = $uRide->rides_id;
              $totalPayout[$cancel->rides_id]->price = round($cancel->price_refound, 2);
              $totalPayout[$cancel->rides_id]->first_name = $cancel->ride->user->first_name;
              $totalPayout[$cancel->rides_id]->email = $cancel->ride->user->email;
              $totalPayout[$cancel->rides_id]->dateFrom = $cancel->ride->dateFrom;
            }
          }

        }

        //Import to Database
        foreach($rides as $uRide){
            DB::table('rides')->where('id', '=', $uRide->id )
            ->update(['totalToPay' => $totalPayout[$uRide->id]->price]);
        }

        return view('dashboardadmin/rides/payToDrivers', compact('totalPayout'));
    }

    public function exportPayUser(Request $request){
        $dateFrom = $request->get('dateFrom');
        $dateTo = $request->get('dateTo');
        return (new UsersExport($dateFrom, $dateTo))->download('invoices.xlsx');

    }

    public function driverPaid(Request $request){

        $input = $request->except('_token');

        $payouts = new \PayPal\Api\Payout();
        $senderBatchHeader = new \PayPal\Api\PayoutSenderBatchHeader();
        $senderBatchHeader->setSenderBatchId(uniqid())
        ->setEmailSubject("You have a payment from Ridesurf");
        $drivers = array();
        $payment = array();

        foreach($input as $key=>$value){

            $senderItem = new \PayPal\Api\PayoutItem();
            $ride_id = explode('-', $key);
            $ride = Rides::find($ride_id[1]);
            $drivers[$ride->user->id] = $ride->user->id;

            if(array_key_exists($ride->user->id, $payment)){
              $payment[$ride->user->id] = $payment[$ride->user->id] + $ride->totalToPay;
            }else{

              $payment[$ride->user->id] = $ride->totalToPay;
            }

            $ride->paid_driver = 'Yes';
            $ride->update();
        }

        foreach($drivers as $driver){

          $user = User::find($driver);
          $senderItem->setRecipientType('Email')
            ->setNote('Thanks you.')
            ->setReceiver($user->email)
            ->setSenderItemId("Rides" . uniqid() )
            ->setAmount(new \PayPal\Api\Currency('{
                                "value":'.$payment[$user->id].',
                                "currency":"USD"
                            }'));
            $payouts->setSenderBatchHeader($senderBatchHeader)
                        ->addItem($senderItem);

            $email = $user->email;
            $info = array(
              'reply' => "Payment is on the way! Paypal will send you a message regarding the payment.",
              'first_name' => $user->first_name,
              'total' => $payment[$user->id]
            );
            Mail::send('email.payToDriver', $info, function($message) use ($email) {
            $message->to($email)->subject('Ridesurf Payment Processed');
            });
        }

        $request = clone $payouts;

        try {

            $output = $payouts->create(null, $this->apiContext);

        } catch (Exception $e) {

            return $e->getMessage;
        }


        Session::flash('message', 'Driver payment status changed to paid');

        return redirect()->back();
    }

    public function listRides(){
      $rides = Rides::orderBy('dateFrom', 'ASC')->paginate(25);

      return view('dashboardadmin.rides.listRides', compact('rides'));
    }

    public function filterRides(Request $request){
      $date = $request->get('date');
      $name = $request->get('name');
      $id = $request->get('rides_id');

      $rides = Rides::query();

      if($date != null){
        $rides = $rides->where('dateFrom', '>=', $date);
      }
      if($name != null){
        $rides = $rides->join('users', 'users.id', '=', 'rides.user_id')->where('users.first_name',  'like', '%' . $name . '%');

      }
      if($id != null){
        $rides = $rides->where('id', '=', $id);
      }

      $rides = $rides->paginate(25);

      return view('dashboardadmin.rides.listRides', compact('rides'));
    }
    
    public function addMake(){
      return view('dashboardadmin.cars.addMake');
    }

    public function createMake(Request $request){


      DB::table('make')->insert(
        ['vMake' => $request->get('vMake'), 'eStatus' => $request->get('eStatus')]
      );

      Session::flash('message', 'Successfully created make');

      return redirect()->route('dashboardadmin.listMake');

    }
    public function listMake(){

      $makes = DB::table('make')->orderBy('vMake', 'ASC')->paginate(15);

      return view('dashboardadmin.cars.make', compact('makes'));
    }

    public function filterMake(Request $request){

     if($request->get('eMake'))
      $makes = DB::table('make')->where('vMake', '=' , $request->get('eMake'))->paginate(5);
      else
        $makes = DB::table('make')->orderBy('vMake', 'ASC')->paginate(15);

      return view('dashboardadmin.cars.make', compact('makes'));
    }

    public function editMake(Request $request, $id){

      $make = DB::table('make')->where('iMakeId', '=', $id)->get()->first();

      return view('dashboardadmin.cars.editMake', compact('make'));
    }

    public function updateMake(Request $request, $id){

      $make = DB::table('make')->where('iMakeId', '=', $id)
              ->update(['vMake'=>$request->get('vMake'), 'estatus' =>$request->get('eStatus')]);

      Session::flash('message', 'Car make successfully changed');
      return redirect()->back();
    }

    public function destroyMake(Request $request, $id){

      DB::table('make')->where('iMakeId', '=', $id)->delete();

      Session::flash('message', 'Successfully deleted make');
      return redirect()->back();

    }

    public function listReport(Request $request){

      $reports = Report::orderBy('created_at', 'DESC')->get();


      return view('dashboardadmin.reports.index', compact('reports'));
    }

    public function filterReport(Request $request){

      $user_name = User::where('first_name','=', $request->get('first'))->get()->first();

      $ride = Rides::where('id', '=', $request->get('rides_id'))->get()->first();
      if($user_name != null && $ride != null){
        $reports = Report::where('passenger_id','=', $user_name->id)->get();
      }
    }

    public function reportReply(Request $request, $id){

      $report = Report::find($id);

      return view('dashboardadmin.reports.reply', compact('report'));
    }

    public function updateReport(Request $request, $id){

      $report = Report::find($id);

      $report->replied = true;
      $report->update();

      $reply = $request->get('reply');

      $thread = Thread::create([
                'subject' => 'Ridesurf Resolution Center',
                'rides_id' => $report->ride->id,
                'isNotification' => 'Yes',
      ]);
      $thread->isNotification = "Yes";
      $thread->update();
      
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $reply,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $report->user_passenger->id,
            'last_read' => new Carbon,
        ]);


        $driver_name = $report->ride->user->first_name; 
        $email = $report->user_passenger->email;
        $info = array('driver_name' => $driver_name, 
          'first_name' => $report->user_passenger->first_name, 'message' => $reply, 'report' => $report);

        Mail::send('email.replyReport', $info, function($message) use ($email) {
            $message->to($email)->subject('Ridesurf Support: About recent report');
        });

        return Redirect::route('reports.index');
    }

    public function manageUsers(Request $request, $id){
      
      $ride = Rides::find($id);

      return view('dashboardadmin.rides.manageUsers', compact('ride'));
    }

    public function contactList(Request $request){

      $contacts = Contact::orderBy('created_at', 'DESC')->paginate(25);

      return view('dashboardadmin.contact.list', compact('contacts'));
    }

    public function viewContactMessage(Request $request, $id){
      $contact = Contact::find($id);

      return view('dashboardadmin.contact.reply', compact('contact'));
    }

    public function replyMessage(Request $request, $id){

      $message_reply = $request->get('message');

      $contact = Contact::find($id);


      $contact->replied = true;
      $contact->update();
      $info = array('name' => $contact->name, 'message_reply' => $message_reply);
      
      $email = $contact->email;
      Mail::send('email.replyContactForm', $info, function($message) use ($email) {
            $message->to($email)->subject("Reply to your Ridesurf inquiry");
        });

      return Redirect::to('dashboardadmin/contactList');

    }

    public function DriverExport(Request $request){

        return Excel::download(new DriverExport, 'drivers.xlsx');
    }

}

class totalPayout{
    public $rides_id;
    public $price;
    public $name;
    public $dateFrom;
    public $email;
    public $user_id;
    public $passenger;
    public $transactions = array();
    public $price_pay = array();
}
