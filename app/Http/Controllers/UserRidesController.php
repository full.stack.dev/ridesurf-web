<?php

namespace App\Http\Controllers;
use App\UserRides;
use Illuminate\Http\Request;



class UserRidesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserRides  $userRides
     * @return \Illuminate\Http\Response
     */
    public function show(UserRides $userRides)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserRides  $userRides
     * @return \Illuminate\Http\Response
     */
    public function edit(UserRides $userRides)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserRides  $userRides
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserRides $userRides)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserRides  $userRides
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRides $userRides)
    {
        //
    }


}
