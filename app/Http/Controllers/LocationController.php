<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use DB, HTML;


class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Location::orderBy('name', 'ASC')->paginate(25);

        return view('locations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = DB::table('cities')->where('region', '=', 'FL')->get();

        return view('locations.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);


        $input = $request->all();

        $location = new Location;
        $location->name = $request->input('name');
        $location->address = $request->input('address');
        $location->description = $request->input('description');
        $location->city = $request->input('city');

        $location->save();

        return redirect()->route('locations.index')
                        ->with('success','Location ' . $location->name .' created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
   
        $data = Location::find($location->id);
        $cities = DB::table('cities')->where('region', '=', 'FL')->get();

        return view('locations.edit', compact('data', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);


        $input = $request->all();

        $location = Location::find($location->id);
        $location->name = $request->input('name');
        $location->address = $request->input('address');
        $location->description = $request->input('description');
        $location->city = $request->input('city');

        $location->update();

        return redirect()->route('locations.index')
                        ->with('success','Location ' . $location->name .' Edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $data = Location::find($location->id);
        $name = $data->name;

        $data->delete();

        return redirect()->back()->with('success', 'Location ' . $name .' Successfully Deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {
        

         if($request->get('query'))
         {

          $query = $request->get('query');
          
          $data = DB::table('cities')
            ->where('region' , '=' , 'FL')
            ->where('city', 'LIKE', "%{$query}%")->distinct('city')
            ->take(20)
            ->get();
          $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
          foreach($data as $row)
          {
           $output .= '
           <li><a href="#">'.$row->city. ' ' .$row->region . '</a></li>
           ';
          }
          $output .= '</ul>';
          echo $output;
         }
    }
}
