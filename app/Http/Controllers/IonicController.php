<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Traits\LocationHelper;
use App\Http\Traits\PreferencesTrait;
use Illuminate\Support\Facades\Log;

use App\User;
use App\Vehicle;
use App\UserRides;
use App\Driver;
use App\Reviews;
use App\Rides;
use App\Preferences;
use App\UserPreferences;
use Carbon\Carbon;
use App\SearchLog;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Jobs\SendVerificationEmail;
use Illuminate\Auth\Events\Registered;
use App\Providers\HelperServiceProvider;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Input;
use Auth, Storage, DB, Response, File, Mail;
use Session,Redirect, ArrayObject;
use Newsletter;



class IonicController extends Controller
{
    private $url;
    private $adminEmails;

    use PreferencesTrait;
    use LocationHelper;
    use RegistersUsers;
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->url = "http://127.0.0.1:8000";

        $this->adminEmails = explode(',', env('ADMIN_EMAILS'));
    }

    public function getCarMake(Request $request){

        $makes = DB::table('make')->where('eStatus','=','Active')->get();

        return response()->json($makes);

    }

    public function getAirportRides(Request $request){

        $date = $request->date;
        $time_today = strtotime($date);
        $airport = $request->airport;
        

        $rides = Rides::with(array('airport_details' => function($query) use($airport) {
                    $query->where('airport_id', '=', $airport['id']);
                }))
                ->where('ride_type', '=', 'airport')
                ->where('dateFrom', '>=', $date)
                ->get();

        $rides = ($rides->filter(

            function ($item) use ($rides,$time_today) {
                $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
                if($time_ride >= $time_today){
                    return $item;
                }

            }
        ));

        foreach($rides as $ride){
            if(count($ride->ride_stop) > 0){
                $ride->ride_stop = $ride->ride_stop;
            }
            $ride->date = date('j M Y', strtotime($ride->dateFrom));
            $ride->start = date("h:i a", strtotime($ride->startHour));
            $sum = (strtotime($ride->startHour) + ($ride->duration));
            $ride->end = date("h:i a", $sum);

            $user = User::find($ride->user_id);

            if(count($user->driver) > 0){
                $ride->make = DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake;
                $ride['model'] = DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle;
                $ride->color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                    ->get()->first()->vColour_EN;
                $ride->type = DB::table('car_type')
                    ->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;
                $ride->year = $user->driver[0]->year;
                $ride->car_image = $user->driver[0]->path_image;
                $ride->first_name = $user->first_name;
            }
            if(!empty($ride->airport_details)>0){
                $ride->airport = $ride->airport_details;
            }
            $ride->user = $ride->user;
            $ride->avatar = $ride->user->avatar;
            $ride->rating = $ride->user->rating;

        }

        if( count($rides) == 0){
            $rides = null;
        }

        $data['openRides'] = null;
        $data['rides'] = $rides;
        if($rides)
            $data['action'] = 1;
        else
            $data['action'] = 2;
        header('Content-Type: application/json');
        return response()->json($data);
    }

    //Find ride ******
    public function findRideInfo(Request $request){

        $from = $request->get('from');
        $to = $request->get('to');
        $date = $request->get('date');
        $data = null;
        $women_only = $request->get('women_only');
        $price_control = $request->get('price');

        //Parse data to get Town
        if($from != null && $to != null){
            $arrayFrom = explode(',', $from);

            $city_from = $this->parseLocation($arrayFrom, $from);
            $city_name = $this->getCity($city_from);

            $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);


            if($geocodeObject['status'] = "OK"){
                $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
                $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
                $city_from = $this->parseLocation($temp, $from);
            }

            //Check if the name is not a state
            $checker = DB::table('states')->where('name','=',$city_name)->count();
            if($checker > 0 ){

                $response = array(
                    'action' => 0 ,
                    'error' => 'Address cannot be a state, it must be a city name',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }

            if(count($arrayFrom) <= 1){
                $response = array(
                    'action' => 0 ,
                    'error' => 'Error getting Address',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }



            $arrayTo = explode(',', $to);
            $city_to = $this->parseLocation($arrayTo, $to);
            $city_name =  $this->getCity($city_to);

            $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);

            if($geocodeObject['status'] = "OK"){
                $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
                $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
                $city_to = $this->parseLocation($temp, $from);
            }

            //Check if the name is not a state
            $checker = DB::table('states')->where('name','=',$city_name)->count();
            if($checker > 0 ){

                $response = array(
                    'action' => 0 ,
                    'error' => 'Address cannot be a state, it must be a city name',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }

            if(count($arrayTo) <= 1){
                $response = array(
                    'action' => 0 ,
                    'error' => 'Error please provide a valid place',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }


            if($city_from == null || $city_to == null){

                $response = array(
                    'action' => 0 ,
                    'error' => 'Error getting Address please try again',
                );

                header('Content-Type: application/json');

                return response()->json($response);
            }

            if($women_only == true){
                $women_only = 'on';
            }else{
                $women_only = null;
            }

            //Add the logs
            $log = new SearchLog;
            $log->city_from = $city_from;
            $log->city_to = $city_to;
            $log->date_asked = $date;
            $log->save();


            //Use Location Parser trait to get the rides
            $rides = $this->getRides($city_from, $city_to, $date, $price_control, $women_only);
            $openRides = $this->getOpenRides($city_from, $city_to, $date, $price_control, $women_only);

            // Add extra information
            if($rides || $openRides){
                if(count($rides) > 0 || count($openRides) > 0){
                    foreach($rides as $ride){
                        if(count($ride->ride_stop) > 0){
                            $ride->ride_stop = $ride->ride_stop;
                        }
                        $ride->date = date('j M Y', strtotime($ride->dateFrom));
                        $ride->start = date("h:i a", strtotime($ride->startHour));
                        $sum = (strtotime($ride->startHour) + ($ride->duration));
                        $ride->end = date("h:i a", $sum);

                    }
                    foreach($openRides as $ride){
                        $user = $ride->user;
                        $ride->open_date_from = date('j M Y', strtotime($ride->open_date_from));
                        $ride->open_date_to = date('j M Y', strtotime($ride->open_date_to));
                        $ride->hours = $ride->hours;
                        $ride->first_name = $user->first_name;
                        $ride->avatar = $user->avatar;
                        $ride->rates = $user->rating;
                    }
                    $data['openRides'] = $openRides;
                    $data['rides'] = $rides;
                    $data['action'] = 1;
                }else{
                    $data = array(
                        'action' => 2,
                        'message' => 'No Rides',
                        'from' => $from,
                        'to' => $to
                    );
                }
            }
        }
        else{
            $data = array(
                'action' => 0,
                'error' => 'No data provided');
        }

        header('Content-Type: application/json');
        return response()->json($data);
    }


    public function getCarmodel(Request $request){

        $model = DB::table('model')
            ->join('make', 'make.iMakeId', '=', 'model.iMakeId')
            ->where('make.iMakeIdV', '=', $request->get('value'))
            ->get()->first();
        header('Content-Type: application/json');
        return Response::json($model);
    }

    public function registerUser(Request $request){

        try{
            $data = $request->get('data');
            $check = User::where('email', '=',$data['email'])->count();

            if($check > 0){
                $response = array(
                    'action' => 2,
                    'message' => 'Email Already Exists'
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
            if($data['first_name'] == null){
                $response = array(
                    'action' => 2,
                    'message' => 'Please enter a First Name'
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
            if($data['last_name'] == null){
                $response = array(
                    'action' => 2,
                    'message' => 'Please enter a Last Name'
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
            if($data['email'] == null){
                $response = array(
                    'action' => 2,
                    'message' => 'Please enter a Valid email'
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
            if(strpos($data['email'], '@') == false){
                $response = array(
                    'action' => 2,
                    'message' => 'Please enter a Valid email'
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }

            $user = new User();
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            $user->email_token = str_random(40);
            $user->created_from = 'Mobile';
            $user->save();

            $preferences = Preferences::all();

            // Add Preferemces
            foreach($preferences as $preference){
                if($preference->id % 2 == 0){
                    $user_preference = new UserPreferences;
                    $user_preference->user_id = $user->id;
                    $user_preference->preference_id = $preference->id;
                    $user_preference->save();
                }
            }

            $user->assignRole('User');

//            if ( ! Newsletter::isSubscribed($user->email) ) {
//                Newsletter::subscribe($user->email);
//            }

            $to_name = $data['first_name'];
            $to_email = $data['email'];
            $info = array('name'=>$to_name, "body" => "New User");

            Mail::send('email.newuser', $info, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('New User');
                $message->from('no-reply@ridesurf.io','Ridesurf');
            });

            dispatch(new SendVerificationEmail($user));
            $token = $user->createToken('MyApp')->accessToken;
      
            $response = array(
                'action' => 1,
                'user' => $user,
                'driver' => false,
                'role' => $user->getRoleNames(),
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'user_id' => $user->id,
                'email' => $user->email,
                'verified' => $user->verified,
                'avatar' => $user->avatar,
                'paypal' => $user->paypal_email,
                'token' => $token,
                'image' => false,
            );

            return response()->json($response);
        }catch(\Exception $e){
            $response = array(
                'action' => 2,
                'message' => 'There was an error trying to sign in'
            );
            Log::info('Error Signing up Mobile ' .$e);
            header('Content-Type: application/json');
            return response()->json($response);
        }

    }

    /**
     * Get the data to display of User
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            $user = User::where('email', '=', $credentials['email'])->get()->first();


            $response = array(
                'action' => 1,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'id' => $user->id,
                'verified' => $user->verified,
                'email' => $user->email,
                'imageUrl' => $user->avatar,
                'gender' => $user->gender,
                'vLanguageCode' => 'EN',
                'csrfToken' => csrf_token(),
            );
            header('Content-Type: application/json');
            return response()->json($response);
        }
        else{

            $response =  array(
                'success' => false,
                'message' => 'Incorrect username or password, please try again.'
            );
            header('Content-Type: application/json');
            return response()->json($response);
        }

    }

    public function loginFacebook(Request $request){


        $id = $request->get('id');
        if( !empty($id['email']) ){
            $email = $id['email'];
        }else{
            $response = array(
                'status' => '2',
                'message' => 'Error trying to login, Your facebook does not have an Email');

            header('Content-Type: application/json');
            return response()->json(['response' => $response], 201);
        }

        $user = User::where('email', '=', $email)->count();

        if($email != null){
            if ($user > 0) {
                $user = User::where('email', '=', $email)->get()->first();

            }
            else{
                $picture = $request->get('picture');

                //Create a new User
                $user = new User;
                $user->first_name = $request->get('first_name');
                $user->last_name = $request->get('last_name');
                $user->verified = 1;
                $user->email = $email;
                $user->facebook_id = $id['id'];
                $user->created_from = 'Mobile';
                $user->save();

                $preferences = Preferences::all();

                // Add Preferemces
                foreach($preferences as $preference){
                    if($preference->id % 2 == 0){
                        $user_preference = new UserPreferences;
                        $user_preference->user_id = $user->id;
                        $user_preference->preference_id = $preference->id;
                        $user_preference->save();
                    }
                }

                $user->assignRole('User');

                if ( ! Newsletter::isSubscribed($user->email) ) {
                    Newsletter::subscribe($user->email);
                }
            }
        }else{
            $response = array(
                'status' => '2',
                'message' => 'Error trying to login, Email not provided');

            header('Content-Type: application/json');
            return response()->json(['response' => $response], 201);
        }


        if(count($user->driver) > 0){
            $driver = true;
        }else{
            $driver = false;
        }
        if($user->avatar == null){
            $image = false;
        }else{
            $image = true;
        }
        $response = array(
            'action' => 1,
            'user' => $user,
            'driver' => $driver,
            'role' => $user->getRoleNames(),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'user_id' => $user->id,
            'email' => $user->email,
            'verified' => $user->verified,
            'avatar' => $user->avatar,
            'paypal' => $user->paypal_email,
            'token' => $user->createToken('MyApp')->accessToken,
            'image' => $image,
        );
        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function forgotPassword(Request $request){

        $this->sendResetLinkEmail($request);

    }

    public function getPublicProfile($id){

        $user = User::find($id);

        $preferences = $this->getUserPreferences($user);

        foreach($preferences as $uPref){
            $uPref->preference = $uPref->preference;
        }

        $user->imageUrl = $user->avatar;
        $user->preferences = $preferences;

        $response = array(
            'user' => $user,
            'state' => $user->state,
            'country' => $user->country,
            'role' => $user->getRoleNames(),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'user_id' => $user->id,
            'email' => $user->email,
            'verified' => $user->verified,
            'avatar' => $user->avatar,
            'status' => 200);
        header('Content-Type: application/json');
        return response()->json($response);
    }


}