<?php

namespace App\Http\Controllers;


use App\TripPurpose;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\UploadImage;
use App\Http\Traits\MailAdmins;
use App\Http\Traits\PreferencesTrait;
use App\Http\Traits\Notifications;
use App\User;
use App\UserRides;
use App\Driver;
use App\Reviews;
use App\Rides;
use App\Report;
use App\State;
use App\Preferences;
use App\OpenRide;
use Carbon\Carbon;
use App\UserPreferences;
use App\Providers\HelperServiceProvider;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Input;
use App\Http\Traits\Formating;
use App\Mail\EmailVerification, Validator;
use Illuminate\Pagination\LengthAwarePaginator;



//Extras
use View,Storage, DB, Response, File, Mail;
use Session,Redirect, ArrayObject, Hash;


class DashboardUserController extends Controller
{
    use UploadImage, MailAdmins, PreferencesTrait, Notifications, Formating;

    private $adminEmails, $admin, $messageCount;
    private $user, $user_preference, $now;

    public function __contruct()
    {
        $this->middleware('auth');
        
    }
    public function __construct(Request $request)
    {

        $this->admin = User::where('first_name', '=', 'admin')->get()->first();

        $this->adminEmails = explode(',', env('ADMIN_EMAILS'));

        $this->middleware(function ($request, $next) {
            
            $this->user = Auth::user();
            $this->user_preference = $this->getUserPreferences($this->user);
            $this->settings =  DB::table('preferences')->get();
            $this->now = Carbon::now();
            $this->messageCount = $this->messageCount();

            View::share( 'user_preference', $this->user_preference );
            View::share( 'now', $this->now );
            View::share( 'user', $this->user );
            View::share( 'messageCount', $this->messageCount);
            View::share( 'settings', $this->settings);
            return $next($request);
        });
        

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {

        $user = Auth::user();

        $i=0;
        $notifications = null;
        $date_today = Carbon::today()->format('Y-m-d');

        $today = strtotime(Carbon::now());

        $notifications = $this->getActions($user, $today);

        $messages = $this->getNotificationMessages();

        if(!empty($messages) && !empty($notifications)){
          $totalNotif = count($notifications) + count($messages);
        }else{
          $totalNotif = 0;
        }

        if(!empty($user)){

            $rides = Rides::where('user_id', '=', $user->id)->get();
            if(!empty($user->ride)){
                $offer = count($user->ride);
            }else{
                $offer =0;
            }
            
        }else{
            $rides = null;
            $offer = 0;
            $counting = 0;
        }
        $counting = 0;

        $booked = UserRides::where('user_id', '=', Auth::id())
                  ->where('booker_payment_paid', '=', 'Yes')
                  ->where('status', '=', 'Approved')->count();


        if($rides != null){
            $pastRides = $rides->where('dateFrom', '<', $this->now);
        }
        else{
            $pastRides = null;
        }

        

        $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')
                   ->where('isNotification', '=', 'No')->get();
        if(empty($threads)){
            $threads = null;
        }

        foreach( $threads as $num){
            
            $temp = DB::table('participants')->where('thread_id', '=', $num->id)
                                         ->where('user_id','!=',$user->id)
                                        ->get()->first();
            
            if($temp != null){
              $num->otherUser = User::find($temp->user_id);
            }else{
              $num->otherUser = null; 
            }
        }

        return view('dashboarduser.index', compact('rides', 'counting', 'messages', 'totalNotif',
             'pastRides', 'offer', 'booked', 'threads','notifications', 'i', 'today'));
    }

    public function viewUserProfile(Request $request, $id){

        $user = User::find($id);

        $reviews = Reviews::where('user_rate_id', '=', $user->id)->get();


        return view('rides.publicProfile', compact('user', 'reviews'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'regex:/[0-9]{9}/',
            'email' => 'required|email|unique:users,email,'.$id,

        );

        $input = $request->get('body');
        $user = $input['user'];


        $temp_area = explode('+',$user['area_code']);

        $this->user->fill($user);
        $this->user->area_code = $temp_area[1];
        $this->phone_number = $this->FormatPhone($user['phone_number']);
        $this->user->paypal_email = $user['paypal_email'];
        $this->user->description = $user['description'];
        $this->user->update();

        $response = array(
            'message' => 'Account successfully updated',
            'status' => 'success'
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteuser($id)
    {
        $user = User::findOrFail($id);
        $rides = Rides::where('user_id', '=', $this->user->id)->get();
        $reason = 'Delete Account';
        
        foreach($rides as $ride){
            $this->destroySimpleRideDriver($ride->id, $user->id, $reason);
        }

        $booked = UserRides::where('user_id', '=', $this->user->id)->get();
        
        if(count($user->driver) > 0){
            $driver = Driver::where('user_id', '=', $user->id)->first();
            $driver->delete();
        }

        foreach($booked as $book){
            if($book->booker_payment_paid == 'Yes'){
              Session::flash('error', 'Error deleting account, 
                              you have paid booked rides, please delete them and try again.' );
              return redirect()->back();
            }

            $book->delete();
        }

        $preferences = UserPreferences::where('user_id', '=', $id)->get();
        foreach($preferences as $pref){
          $pref->delete();
        }

        $threads = Thread::forUser(Auth::id())->get();
        foreach($threads as $t){
          //Todo Save them in another place
          $t->delete();
        }
        $messages = Message::where('user_id', '=', $this->user->id)-get();
        foreach($messages as $messasge){
          $messasge->delete();
        }

        $check = DeleteUsers::where('user_id', '=', $id)->get()->first();
        
        $dir = public_path('/images/user_information/' . $id);

        $this->rrmdir($dir);
        
        if(count($check) > 0){
            Session::flash('error', 'We are having problems deleting your account, please contact us.' );
            return redirect()->back();
        }
        else{
            $DeleteUser = new DeleteUsers;
            $DeleteUser->name = $this->user->first_name;
            $DeleteUser->email = $this->user->email;
            $DeleteUser->user_id = $this->user->id;
            $DeleteUser->delete_date = Carbon::now();

            $DeleteUser->save();
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $this->user->delete();

            Auth::logout();
            Session::flash('success',"logout");
            return \Redirect::back();
        }
    }
    //Delete files and folders 
    public function rrmdir($dir) {
       if (is_dir($dir)) {
         $objects = scandir($dir);
         foreach ($objects as $object) {
           if ($object != "." && $object != "..") {
              if (filetype($dir."/".$object) == "dir") 
                $this->rrmdir($dir."/".$object); 
              else 
                unlink($dir."/".$object);
           }
         }
         reset($objects);
         rmdir($dir);
       }
    }
    public function uploadProfileImage(Request $request){

      $user = Auth::user();
      $inner_path = '/images/user_information/'.$this->user->id;
      $input = $request->get('body');

      try {
        $upload = $this->upload($input['image'],$this->user->id,true,$inner_path);
      }
      catch (\Exception $e) {
          $response = array(
            'status ' => 'error',
            'message' => 'Error uploading image',
            'error' => $e,
          );
          header('Content-Type: application/json');

          return response()->json($response, 401);
      }
      if($upload == false){
          $response = array(
              'status' => 'Error',
              'message' => 'There was an error uploading the image ',
          );
          header('Content-Type: application/json');
          return response()->json($response, 401);
      }

      $response = array(
        'status' => 'success',
        'message' => 'Successfully saved image',
      );

      header('Content-Type: application/json');
      return response()->json($response);
    }

    public function fetchAllData(Request $request){
     $make = $request->get('make');

     $model = DB::table('model')
              ->where('iMakeId', '=', $make)
              ->where('eStatus', '=', 'Active')
              ->orderBy('vTitle', 'ASC')
              ->get();
      $modeltype = DB::table('car_type')
              ->where('eStatus', '=', 'Active')
              ->orderBy('vTitle_EN')
              ->get();
      $color = DB::table('car_colour')
               ->where('eStatus', '=', 'Active')
               ->orderBy('vColour_EN', 'ASC')
               ->get();

      $response = array(
        'model' => $model,
        'modeltype' => $modeltype,
        'carcolor' => $color,
      );

      return Response::json( $response );

    }
    public function fetchVehicleMake(Request $request){
        if($request->get('query'))
        {

            $query = $request->get('query');

            $vehicles = DB::table('make')
                        ->where('vMake', 'LIKE', "%{$query}%")->limit(10)
                        ->get();

              $output = '<ul class="dropdown-menu" style="display:block; position:relative">';

              foreach($vehicles as $row)
              {
               $output .= '
               <li><a href="#" value='.$row->iMakeId.'>'.$row->vMake. '</a></li>
               ';
              }
              $output .= '</ul>';
              echo $output;
        }
    }

    public function fetchModel(Request $request){


          $make = $request->get('make');

          $vehicles = DB::table('model')
                        ->where('iMakeId', '=', $make)
                        ->where('eStatus', '=', 'Active')
                        ->orderBy('vTitle', 'ASC')
                        ->get();

          return Response::json( $vehicles );

    }

    public function fetchType(Request $request){


          $vehicles = DB::table('car_type')
                        ->where('eStatus', '=', 'Active')
                        ->orderBy('vTitle_EN')
                        ->get();

          return Response::json( $vehicles);
    }

    public function fetchColor(Request $request){

         $vehicles = DB::table('car_colour')
                          ->where('eStatus', '=', 'Active')
                          ->orderBy('vColour_EN', 'ASC')
                          ->get();


          return Response::json( $vehicles );

    }



    public function reviews(Request $request){

        if(count($this->user->ride)){
            $rides = $this->user->ride;
            $offer = count($this->user->ride);
        }
        else{
            $rides = null;
            $offer = 0;
        }

        if(count($this->user->UserRide)){
            $userRide = $this->user->UserRide;
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }


        $user = Auth::user();

        return view('dashboarduser.reviews', compact('offer','rides', 'booked', 'userRide', 'user'));
    }

    public function payments(Request $request){

        if(count($this->user->driver) > 0){
           $driver = Driver::where('user_id', '=', $this->user->id)->get()->first();
        }else{
          $driver = null;
        }
        if(count($this->user->ride)){
            $rides = Rides::where('user_id', '=', $this->user->id)->get();
            $offer = count($this->user->ride);
            foreach($rides as $ride){
               $passengers = UserRides::where('rides_id', '=', $ride->id)
                             ->where('booker_payment_paid', '=', 'Yes')->count();

               $ride->passengers = $passengers;

            }
        }
        else{
            $rides = null;
            $offer = 0;
            $passengers = 0;
        }

        if(count($this->user->UserRide)){
            $userRide = UserRides::where('user_id', '=', $this->user->id)->get();
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }


        return view('dashboarduser.payments', compact('userRide', 'offer', 'rides', 'driver', 'booked' , 'passangers'));
    }

    public function booked(Request $request){


        $hour = Carbon::now()->toTimeString();

        if(count($this->user->ride)){
            $rides = $this->user->ride;
            $offer = count($this->user->ride);
        }
        else{
            $rides = null;
            $offer = 0;
        }

        if(count($this->user->UserRide)){
            $userRide = UserRides::where('user_id', '=', $this->user->id)->get();
            foreach($userRide as $uride){
                $ride = Rides::find($uride->rides_id);
                if($ride){
                    $uride->date = $ride->dateFrom . " " . $ride->startHour;
                }else{
                    $uride->date = null;
                }
            }
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }


        return view('dashboarduser.booked', compact('userRide', 'booked', 'offer', 'hour'));
    }

    public function profile(Request $request){

        $user = Auth::user();
        $user->driver = $user->driver;

        $preferences = DB::table('user_preferences as user')
                       ->where('user.user_id', '=', $this->user->id)
                       ->get();

        $countries = DB::table('countries')->select('name', 'calling_code', 'flag')->whereNotNull('flag')->orderBy('name', 'ASC')->get();

        if(count($this->user->driver) > 0){
            
            if($this->user->driver[0]->status == 'Approved'){
              $make = DB::table('make')->where('iMakeId','=', $this->user->driver[0]->make_id)->get()->first()->vMake;
              $model = DB::table('model')->where('iModelId','=', $this->user->driver[0]->model_id)->get()->first()->vTitle;
              $color = DB::table('car_colour')->Where('iColourId', '=', $this->user->driver[0]->color_id)
                          ->get()->first()->vColour_EN;
              $type = DB::table('car_type')->where('iCarTypeId', '=', $this->user->driver[0]->type_id)->get()->first()->vTitle_EN;
              $vehicle = new ArrayObject();
              $vehicle->make = $make;
              $vehicle->model = $model;
              $vehicle->color = $color;
              $vehicle->type = $type;
              $vehicle->year = $this->user->driver[0]->year;
              $vehicle->vehicle_tag = $this->user->driver[0]->vehicle_tag;

              $vehicle->vehicle_registration = $this->user->driver[0]->vehicle_registration;
              $vehicle->path_image = $this->user->driver[0]->path_image;
            }
        }
        else{
            $vehicle = null;
        }

        if(count($this->user->ride)){
            $rides = $this->user->ride;
            $offer = count($this->user->ride);
        }
        else{
            $rides = null;
            $offer = 0;
        }

        if(count($this->user->UserRide)){
            $userRide = UserRides::where('user_id', '=', $this->user->id)->get();
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
      }

      $state = State::all('name', 'code');

      return view('dashboarduser.profile',  compact('vehicle', 'booked', 'userRide', 'offer', 'preferences', 'countries', 'state'));
    }

    public function ridesCurrent(Request $request){
        $user = Auth::user();

        $perPage = 5;

        $today = Carbon::today()->format('Y-m-d');
        $time_today = strtotime(Carbon::now());

        if(count($this->user->ride)){
            $rides = Rides::where('user_id', '=', $user->id )
                    ->where('dateFrom','>=', $today)
                    ->paginate(25);

            $rides= $rides->setCollection($rides->getCollection()->filter(

              function ($item) use ($rides,$time_today) {
                $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
                if($time_ride >= $time_today){
                    return $item;
                }


            }));
            $offer = count($rides);
        }
        else{
            $rides = null;
            $offer = 0;
        }

        if(count($this->user->UserRide)){
            $userRide = $this->user->UserRide;
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }


        $rides = json_encode($rides);

        return view('dashboarduser.rides', compact('rides', 'userRide', 'booked', 'offer'));
    }

    public function getCurrentRides(Request $request){
        $user = Auth::user();

        $today = Carbon::today()->format('Y-m-d');
        $time_today = strtotime(Carbon::now());

        if(count($user->ride)){
            $rides = Rides::where('user_id', '=', $user->id )
                    ->orderBy('dateFrom', 'asc')
                    ->where('dateFrom','>=', $today)
                    ->paginate(50);

            foreach($rides as $ride){
                if( count($ride->ride_stop ) > 0){
                    $ride->stop = $ride->ride_stop;
                    
                }
                if(count($ride->user_ride) > 0){
                    foreach($ride->user_ride as $uRide){
                        if($uRide->booker_payment_paid == 'Yes'){
                            $ride->passengerCount++;
                            $uRide->user = $uRide->user;

                        }

                    }
                }else{
                     $ride->passengerCount = 0;
                }
                $ride->date = date('j M Y', strtotime($ride->dateFrom));
                $ride->start = date("g:i a", strtotime($ride->startHour));
                $time =  strtotime($ride->startHour);
                $ride->end = date("h:i a",($time + $ride->duration));
                $ride->user = $ride->user;
            }        
            $rides= $rides->setCollection($rides->getCollection()->filter(

                function ($item) use ($rides,$time_today) {
                    $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
                    if($time_ride >= $time_today){
                        return $item;
                    }

                }
            ));
            
        }else{
            $rides = null;
        }



        header('Content-Type: application/json');
        return response()->json($rides);

    }
    public function getPastRides(Request $request){
        $user = Auth::user();

        $today = Carbon::today()->format('Y-m-d');
        $time_today = strtotime(Carbon::now());

        if(count($user->ride)){
            $rides = Rides::where('user_id', '=', $user->id )
                    ->orderBy('dateFrom', 'asc')
                    ->where('dateFrom','<=', $today)
                    ->paginate(50);

            foreach($rides as $ride){
                if( count($ride->ride_stop ) > 0){
                    $ride->stop = $ride->ride_stop;
                    
                }
                if(count($ride->user_ride) > 0){
                    foreach($ride->user_ride as $uRide){
                        if($uRide->booker_payment_paid == 'Yes'){
                            $ride->passengerCount++;
                            $uRide->user = $uRide->user;

                        }

                    }
                }else{
                     $ride->passengerCount = 0;
                }
                $ride->date = date('j M Y', strtotime($ride->dateFrom));
                $ride->start = date("g:i a", strtotime($ride->startHour));
                $time =  strtotime($ride->startHour);
                $ride->end = date("h:i a",($time + $ride->duration));
                $ride->user = $ride->user;
            }        
            $rides= $rides->setCollection($rides->getCollection()->filter(

                function ($item) use ($rides,$time_today) {
                    $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
                    if($time_ride <= $time_today){
                        return $item;
                    }

                }
            ));
            
        }else{
            $rides = null;
        }

        header('Content-Type: application/json');
        return response()->json($rides);
    }
    public function ridesPast(Request $request){

        $user = Auth::user();

        $time_today = strtotime(Carbon::now());
        $today = Carbon::today()->format('Y-m-d');

        if(count($this->user->ride)){
            $rides = Rides::where('user_id', '=', $this->user->id )
                     ->where('dateFrom','<=', $today)
                     ->paginate(10);
            $rides= $rides->setCollection($rides->getCollection()->filter(

              function ($item) use ($rides,$time_today) {
                $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
                if($time_ride <= $time_today){
                    return $item;
                }


            }));

          $offer = count($rides);
        }
        else{
            $rides = null;
            $offer = 0;
        }

        if(count($this->user->UserRide)){
            $booked = count($this->user->UserRide);
        }else{
            $booked = 0;
        }

        return view('dashboarduser.rides', compact('rides', 'userRide','booked', 'offer'));
    }

    public function openRides(Request $request){

      $user = Auth::user();

      $openRides = OpenRide::with('hours')->with('user')
                   ->where('user_id','=',$user->id)->get();

      foreach($openRides as $ride){
  
        if($ride['active'] == true ){
          $ride['active'] = true;
        }else{
          $ride['active'] = false;
        }

        $ride->trip_purpose = TripPurpose::find($ride->trip_purpose_id);
      }

      return response()->json($openRides, 200);
    }

    public function message(Request $request){

        $today = Carbon::today()->format('Y-m-d');

        $user = Auth::user();


        if(count($this->user->UserRide)){
            $userRide = $this->user->UserRide;
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }

        $control = 1;
        $threads = Thread::forUser(Auth::id())->where('isNotification','=', 'No')->latest('updated_at')->get();

        $thread = 0;
        foreach( $threads as $num){

            $thread += $num->userUnreadMessagesCount(Auth::id());

            $temp = DB::table('participants')->where('thread_id', '=', $num->id)
                                         ->where('user_id','!=',$user->id)
                                         ->get()->first();
            if(!empty($temp))                             
              $num->otherUser = User::find($temp->user_id);
            else
              $num->otherUser = null;
        }

        

        $preferences = DB::table('user_preferences as user')
                       ->where('user.user_id', '=', $this->user->id)
                       ->get();

        if(count($this->user->ride)){
            $rides = Rides::where('user_id', '=', $this->user->id )
                    ->where('dateFrom','>=', $today)
                    ->paginate(10);
            $offer = Rides::where('user_id', '=', $this->user->id )
                    ->where('dateFrom','>=', $today)->count();
        }
        else{
            $rides = null;
            $offer = 0;
        }



        return view('dashboarduser.message', compact('threads', 'control' ,
            'thread', 'preferences', 'booked', 'offer'));
    }

    public function becomeDriver(Request $request){


        $user = Auth::user();

        if(count($this->user->driver) > 0){
            $rides = $this->user->ride;
            $offer = count($this->user->ride);
            if(count($this->user->driver) > 0){
                $temp = Driver::where('user_id', '=', $this->user->id)->get()->first();
                $driver = json_encode($temp);

                $car = new ArrayObject();
                if($this->user->driver[0]->make_id)
                    $car->make = DB::table('make')->where('iMakeId','=', $this->user->driver[0]->make_id)->get()->first()->vMake;
                else $car->make = null;
                if($this->user->driver[0]->model_id)
                    $car->model = DB::table('model')->where('iModelId','=', $this->user->driver[0]->model_id)->get()->first()->vTitle;
                else $car->model = null;
                if($this->user->driver[0]->color_id)
                    $car->color = DB::table('car_colour')->Where('iColourId', '=', $this->user->driver[0]->color_id)->get()->first()->vColour_EN;
                else$car->color = null;
                if($this->user->driver[0]->type_id)
                    $car->type = DB::table('car_type')->where('iCarTypeId', '=', $this->user->driver[0]->type_id)->get()->first()->vTitle_EN;
                else$car->type = null;

            }
        }
        else{
            $rides = null;
            $offer = 0;
            $driver = null;
            $car = null;
        }

        if(count($this->user->UserRide)){
            $userRide = $this->user->UserRide;
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }

        $vehicles = DB::table('make')
                        ->where('eStatus', '=', 'Active')
                        ->orderBy('vMake', 'ASC')
                        ->get();

        $user_preference = $this->getUserPreferences($user);

        return view('dashboarduser.becomeDriver', compact('rides', 'car',
            'booked', 'userRide', 'offer', 'vehicles', 'driver', 'user_preference'));
    }



    public function createDriver(Request $request){

      $rules =  array(

            'make' => 'required',
            'model' => 'required',
            'type' => 'required',
            'color'  => 'required',
            'year'  => 'required',
            'driver_license'  => 'required',
            'vehicle_tag'  => 'required',
       );

      $validator = Validator::make(Input::all(), $rules);
      if($validator->fails()){
        return  redirect()->back()->withErrors($validator)->withInput();
      }


      $user = Auth::user();
      $roles = $this->user->getRoleNames();
      $tag = $request->get('vehicle_tag');



      if($roles[0] == 'Driver'){
        //Normal change of car
        if( count($this->user->driver) > 0 ){
              $driver = Driver::where('user_id', '=' , $this->user->id)->get()->first();
              $driver->make_id = $request->get('make');
              $driver->model_id = $request->get('model');
              $driver->type_id = $request->get('type');
              $driver->color_id = $request->get('color');
              $driver->year = $request->get('year');
              $driver->vehicle_tag = $tag;
              $driver->user_id = $this->user->id;
              $driver->status = 1;
              $driver->status_message = "The user has requested to change cars, please check the info and approve or deny the request.";



              $inner_path = '/images/user_information/'.$this->user->id.'/car_details';
              $file_car = $request->get('car_image');
              $file_registration = Input::file('registration_image');
              $file_insurance = Input::file('insurance_image');
              $file_license = Input::file('driver_license');

              try {
                if($file_license != null){
                  if(file_exists(public_path().$driver->driver_license && $driver->driver_license != null) ){
                    unlink(public_path().$driver->driver_license);
                  }
                  $driver->driver_license = $this->resizeImage($file_license,$inner_path);
                }

                if($file_registration != null){
                  if(file_exists(public_path().$driver->registration_path && $driver->registration_path != null) ){
                    unlink(public_path().$driver->registration_path);
                  }
                  $driver->registration_path = $this->resizeImage($file_registration,$inner_path);
                }

                if($file_insurance != null){
                  if(file_exists(public_path().$driver->insurance_path && $driver->insurance_path!=null) ){
                    unlink(public_path().$driver->insurance_path);
                  }
                  $driver->insurance_path = $this->resizeImage($file_insurance, $inner_path);
                }

                if($file_car != null){
                  if( file_exists(public_path().$driver->path_image && $driver->path_image != null) ){
                    unlink(public_path().$driver->path_image);
                  }
                  $driver->path_image = $this->uploadCarImage($file_car,$this->user->id,$inner_path,
                    $driver,$request->get('make'), $request->get('model'), $request->get('year') );
                }
              }
              catch (\Exception $e) {
                  Session::flash('error', 'Error uploading image, please try again');

                  return redirect()->back()->withInput();
              }

              if($driver->path_image == null){
                Session::flash('error', 'Car image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }else if($driver->insurance_path == null){
                Session::flash('error', 'Insurance image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }else if($driver->registration_path == null){
                Session::flash('error', 'Registration image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }

              $driver->update();

              Session::flash("message", "Your request to change your car was uploaded, we will review it and send you a decision as soon as possible.");

          }
          else{
              Session::flash('error', "There was an error trying to proccess your new car request, please contact us to resolve this issue.");

              return Redirect::to('/dashboarduser/cardetails');

          }

        }
        else{

        if(count($this->user->driver) > 0){

            //Driver Applied but was denied
            $driver = Driver::where('user_id', '=', $this->user->id)->get()->first();

            $driver->make_id = $request->get('make');
            $driver->model_id = $request->get('model');
            $driver->type_id = $request->get('type');
            $driver->color_id = $request->get('color');
            $driver->year = $request->get('year');
            $driver->vehicle_tag = $tag;
            $driver->user_id = $this->user->id;
            $driver->status=1;
            $driver->status_message = "The user has requested to be a driver, please check the data and approve or deny it";


            $inner_path = '/images/user_information/'.$this->user->id.'/car_details';
            $file_car = $request->get('car_image');
            $file_registration = Input::file('registration_image');
            $file_insurance = Input::file('insurance_image');
            $file_license = Input::file('driver_license');

              try {
                if($file_license != null){
                  if(file_exists(public_path().$driver->driver_license && $driver->driver_license != null) ){
                    unlink(public_path().$driver->driver_license);
                  }
                  $driver->driver_license = $this->resizeImage($file_license,$inner_path);
                }

                if($file_insurance != null){
                  if(file_exists(public_path().$driver->insurance_path && $driver->insurance_path != null) ){
                    unlink(public_path().$driver->insurance_path);
                  }
                  $driver->insurance_path = $this->resizeImage($file_insurance, $inner_path);
                }

                if($file_car != null){
                  if( file_exists(public_path().$driver->path_image && $driver->path_image != null) ){
                    unlink(public_path().$driver->path_image);
                  }
                  $driver->path_image = $this->uploadCarImage($file_car,$this->user->id,$inner_path,
                    $driver,$request->get('make'), $request->get('model'), $request->get('year') );
                }
            }
            catch (\Exception $e) {
                  Session::flash('error', 'Error uploading image, please try again' . $e);
                  //return redirect()->back()->withErrors()->withInput();
                  return redirect()->back()->withInput();
            }

            if($driver->path_image == null){
                Session::flash('error', 'Car image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }else if($driver->insurance_path == null){
                Session::flash('error', 'Insurance image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }else if($driver->registration_path == null){
                Session::flash('error', 'Registration image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }

            $driver->update();

            Session::flash("message", "Your request to change a car has been recieved, we will review it and 
                    send you a decision as soon as possible.");
          }else{


            //New Driver
            $driver = new Driver;
            $driver->make_id = $request->get('make');
            $driver->model_id = $request->get('model');
            $driver->type_id = $request->get('type');
            $driver->color_id = $request->get('color');
            $driver->year = $request->get('year');
            $driver->vehicle_tag = $tag;
            $driver->user_id = $this->user->id;
            $driver->status=1;
            $driver->status_message = "New user requesting to become a driver, please review information";

            $inner_path = '/images/user_information/'.$this->user->id.'/car_details';
            $file_car = $request->get('car_image');
            $file_registration = Input::file('registration_image');
            $file_insurance = Input::file('insurance_image');
            $file_license = Input::file('driver_license');


            try {
              $driver->driver_license = $this->resizeImage($file_license,$inner_path);

              $driver->registration_path = $this->resizeImage($file_registration,$inner_path);

              $driver->insurance_path = $this->resizeImage($file_insurance, $inner_path);

              $driver->path_image = $this->uploadCarImage($file_car,$this->user->id,$inner_path, $driver,$request->get('make'),
              $request->get('model'), $request->get('year') );
            }
            catch (\Exception $e) {
              if($driver->registration_path == null){
                Session::flash('error', 'Registration image was not upload correctly, please try again' );

                return Redirect::back()->withInput();
              }
              else if($driver->insurance_path == null){
                Session::flash('error', 'Insurance image was not upload correctly, please try again' );

                return Redirect::back()->withInput();
              }
              else if($driver->path_image == null){
                Session::flash('error', 'Car image was not cropped, please crop image' );

                return Redirect::back()->withInput();
              }

                Session::flash('error', 'Error uploading image, please try again ');
                //return redirect()->back()->withErrors()->withInput();
                return redirect()->back()->withInput();
            }

              if($driver->path_image == null){
                Session::flash('error', 'Car image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }else if($driver->insurance_path == null){
                Session::flash('error', 'Insurance image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }else if($driver->registration_path == null){
                Session::flash('error', 'Registration image was not cropped, please crop image');

                return Redirect::back()->withInput();
              }

            $driver->save();

            Session::flash("message", "Your request to become a driver has been recieved, we will review it and send you a decision as soon as possible.");

          }
      }


      $reason = "New driver request";
      $reply = "User: " . $this->user->first_name . " has requested to be a driver and/or change car";

      $this->mailCurrentAdminds($this->adminEmails,$reply,$reason);

      return Redirect::to('dashboarduser/cardetails');

    }


    public function cardetails(Request $request){

      $user = Auth::user();

      if(count($this->user->ride)){
          $rides = $this->user->ride;
          $offer = count($this->user->ride);
      }
      else{
          $rides = null;
          $offer = 0;
      }

      if(count($this->user->UserRide)){
          $userRide = $this->user->UserRide;
          $booked = count($this->user->UserRide);
      }else{
          $userRide = null;
          $booked = 0;
      }


      if(count($this->user->driver) == 0){
          return redirect('dashboarduser/becomeDriver');
      }else if($this->user->driver[0]->status == 'Requested'){
          return redirect('dashboarduser/becomeDriver');
      } else{
            $make = DB::table('make')->where('iMakeId','=', $this->user->driver[0]->make_id)->get()->first()->vMake;
            $model = DB::table('model')->where('iModelId','=', $this->user->driver[0]->model_id)->get()->first()->vTitle;
            $color = DB::table('car_colour')->Where('iColourId', '=', $this->user->driver[0]->color_id)
                        ->get()->first()->vColour_EN;
            $type = DB::table('car_type')->where('iCarTypeId', '=', $this->user->driver[0]->type_id)->get()->first()->vTitle_EN;
            $vehicle = new ArrayObject();
            $vehicle->make = $make;
            $vehicle->model = $model;
            $vehicle->color = $color;
            $vehicle->type = $type;
            $vehicle->year = $this->user->driver[0]->year;
            $vehicle->vehicle_tag = $this->user->driver[0]->vehicle_tag;
            $vehicle->registration_path = $this->user->driver[0]->registration_path;
            $vehicle->insurance_path = $this->user->driver[0]->insurance_path;
            $vehicle->vehicle_registration = $this->user->driver[0]->vehicle_registration;
            $vehicle->path_image = $this->user->driver[0]->path_image;
            $vehicle->driver_license = $this->user->driver[0]->driver_license;

            if(count($this->user->UserRide)){
                $booked = count($this->user->UserRide);
            }else{
                $booked = 0;
            }


            $preferences = DB::table('user_preferences')->where('user_id', '=', $this->user->id)->get();

        return view('dashboarduser.cardetails', compact('vehicle',
                'booked', 'preferences', 'offer', 'booked'));

      }

    }
    public function reportRide(Request $request, $id){

        if(count($this->user->ride)){
            $offer = count($this->user->ride);
        }
        else{
            $rides = null;
            $offer = 0;
        }

        if(count($this->user->UserRide)){
            $userRide = UserRides::where('user_id', '=', $this->user->id)->get();
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }

        $ride = Rides::find($id);

        return view('dashboarduser.report', compact('ride', 'booked', 'userRide','offer', 'preferences'));
    }

    public function storeReport(Request $request, $id){

        $ride = Rides::find($id);

        $reason = $request->get('reason');
        $description = $request->get('description');

        $report = new Report;

        $report->reason = $reason;
        $report->description = $description;
        $report->passenger_id = $this->user->id;
        $report->rides_id = $ride->id;
        $report->save();

        Session::flash("message", "Your report has been made, please allow us up to
            24 hours to respond.");

        return redirect::route('dashboarduser.booked');
    }

    public function showCancelride(Request $request, $id){

        $ride = Rides::find($id);

        $date = strtotime($ride->dateFrom ." ". $ride->startHour);

        if(count($this->user->ride)){
            $offer = count($this->user->ride);
        }
        else{
            $rides = null;
            $offer = 0;
        }

        if(count($this->user->UserRide)){
            $userRide = UserRides::where('user_id', '=', $this->user->id)->get();
            $booked = count($this->user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }

        if($date < time() + 86400) {

            $control = true;
            $return = ($ride->user_ride[0]->price_paid * 0.50) - 1.50;
         } else {


            $control = false;
            $return = $ride->user_ride[0]->price_paid - 1.50;
         }



        return view('dashboarduser.cancelUserRide', compact('ride', 'control', 'booked', 'userRide',
                'user','offer', 'return'));
    }

    public function sendVerification(Request $request){

        $email = new EmailVerification($this->user);

        Mail::to($this->user->email)->send($email);

        return Redirect::back();

    }

    public function updatepassword(Request $request){

      $preferences = DB::table('user_preferences as user')
                       ->where('user.user_id', '=', $this->user->id)
                       ->get();

      if(count($this->user->ride)){
          $offer = count($this->user->ride);
      }
      else{

          $offer = 0;
      }

      if(count($this->user->UserRide)){
          $booked = count($this->user->UserRide);
      }else{
          $userRide = null;
          $booked = 0;
      }

      return view('dashboarduser.updatepassword', compact('booked' , 'offer', 'preferences'));
    }

    public function patchpassword(Request $request){

      $input = $request->all();
    
      $user = Auth::user();

      if(!empty($input['password'])){
          $input['password'] = Hash::make($input['password']);
          $user->password = $input['password'];
      }

      $user->update();


      Session::flash('message', 'Password successfully changed');

      return Redirect::back();


    }

    //TODO create handler to finish ride
    public function rideFinished(Request $request){

    }

    public function encrypt($n) {
      return (((0x0000FFFF & $n) << 16) + ((0xFFFF0000 & $n) >> 16));
    }
}
