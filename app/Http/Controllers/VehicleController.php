<?php

namespace App\Http\Controllers;

use App\Vehicle;
use Illuminate\Http\Request;
use Gerardojbaez\Vehicle\Contracts\HasMake as HasMakeContract;
use Gerardojbaez\Vehicle\Contracts\HasModel as HasModelContract;
use Gerardojbaez\Vehicle\Contracts\HasModelYear as HasModelYearContract;
use Gerardojbaez\Vehicle\Contracts\HasVehicle as HasVehicleContract;
use Gerardojbaez\Vehicle\Traits\HasMake;
use Gerardojbaez\Vehicle\Traits\HasModel;
use Gerardojbaez\Vehicle\Traits\HasModelYear;
use Gerardojbaez\Vehicle\Traits\HasVehicle;

class Vehicle extends Model implements HasMakeContract, HasModelContract, HasModelYearContract, HasVehicleContract
{
    use HasMake, HasModel, HasModelYear, HasVehicle;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'make_id',
        'model_id',
        'year_id',
        'vehicle_id',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        //
    }
}
