<?php

namespace App\Http\Controllers;

use App\UserPreferences;
use Illuminate\Http\Request;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

use Cmgmyr\Messenger\Traits\Messagable;
use App\Http\Traits\PreferencesTrait;

use App\User;
use App\Preferences;
use Carbon\Carbon;
use App\Http\Traits\Notifications;
use View, DB, Response;
use Session,Redirect;

class UserPreferencesController extends Controller
{

    private $user;
    private $user_preference;
    private $now, $messageCount, $settings;

    use PreferencesTrait, Notifications;
    
    public function __construct(Request $request)
    {

        $this->middleware(function ($request, $next) {
            
            $this->user = Auth::user();
            $this->user_preference = $this->getUserPreferences($this->user);
            $this->now = Carbon::now();

            $this->settings =  DB::table('preferences')->get();
            $this->messageCount = $this->messageCount();

            View::share( 'messageCount', $this->messageCount);
            View::share( 'settings', $this->settings);
            View::share( 'user_preference', $this->user_preference );
            View::share( 'now', $this->now );
            View::share( 'user', $this->user );
            return $next($request);
        });
        

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserPreferences  $userPreferences
     * @return \Illuminate\Http\Response
     */
    public function show(UserPreferences $userPreferences)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserPreferences  $userPreferences
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPreferences $userPreferences)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserPreferences  $userPreferences
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPreferences $userPreferences)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserPreferences  $userPreferences
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPreferences $userPreferences)
    {
        //
    }

    public function editPreferences(Request $request){


      $user_preference_single = $this->getUserPreferences($this->user);
      $preferences = Preferences::all();

      if(count($this->user->ride)){
          $rides = $this->user->ride;
          $offer = count($this->user->ride);
      }
      else{
          $rides = null;
          $offer = 0;
      }

      if(count($this->user->UserRide)){
          $userRide = $this->user->UserRide;
          $booked = count($this->user->UserRide);
      }else{
          $userRide = null;
          $booked = 0;
      }

      $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();
      $thread = 0;
      foreach( $threads as $num){
          $thread += $num->userUnreadMessagesCount(Auth::id());
      }


      return view('dashboarduser.editPreferences', compact('offer', 'rides', 'thread', 
        'booked', 'userRide', 'user_preference_single', 'preferences'));
    }

    public function updatePreferences(Request $request){

      $input = $request->except('_token', '_method');

      $user_preference = $this->getUserPreferences($this->user);

      foreach($user_preference as $uPrefe){
        $uPrefe->preference_id = $input[$uPrefe->preference->category];
        $uPrefe->update();
      }
      Session::flash('message', "Preferences successfully updated");

      return Redirect::back();
    }
}
