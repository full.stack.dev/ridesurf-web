<?php

namespace App\Http\Controllers;

use App\User;
use App\Rides;
use App\UserRides;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Traits\MessageTrait;
use App\Http\Traits\Notifications;
use DB, Session, Mail;
use View;

class MessagesController extends Controller
{

    use MessageTrait, Notifications;

    private $messageCount;
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function __contruct()
    {
        $this->middleware('auth');
        
    }
    public function __construct(Request $request)
    {   
        $this->middleware(function ($request, $next) {
            $this->messageCount = $this->messageCount();
            View::share( 'messageCount', $this->messageCount);
            return $next($request);
        });

    }
    public function index()
    {
        $user = Auth::user();
        
        // All threads that user is participating in
        $threads = Thread::forUser(Auth::id())->where('isNotification', '=', 'No')
                   ->latest('updated_at')->get();

        foreach($threads as $thread){
            $otherUser = $thread->creator();
            if($otherUser->id == $user->id){
                $temp = DB::table('participants')->where('thread_id', '=', $thread->id)
                                             ->where('user_id','!=',$user->id)
                                             ->get()->first();
                $thread->otherUser = User::find($temp->user_id);
            }
        }
        
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();

        $control = 0;
        return view('messenger.index', compact('threads', 'control'));
    }
    public function filter(Request $request, $number)
    {
        $user = Auth::user();

        $today = Carbon::today()->format('Y-m-d');
        
        if($number == 1){
            // All threads that user is participating in
            $threads = Thread::forUser(Auth::id())->where('isNotification', '=', 'No')
                       ->latest('updated_at')->get();
            foreach($threads as $thread){
                $otherUser = $thread->creator();
                if($otherUser->id == $user->id){
                    $temp = DB::table('participants')->where('thread_id', '=', $thread->id)
                                                 ->where('user_id','!=',$user->id)
                                                 ->get()->first();
                    $thread->otherUser = User::find($temp->user_id);
                }
            }
           
        }   
        if($number == 2){
            $threads = Thread::forUser(Auth::id())
                       ->where('isNotification', '=', 'Yes')
                        ->latest('updated_at')->get();
            foreach($threads as $thread){
                $thread->otherUser = User::where('first_name', '=', 'admin')->get()->first();
                
            }

        }
        if($number == 3){
            $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')
                        ->where('isNotification', '=', 'No')->get();
        }
        $thread = 0;
        foreach( $threads as $num){
            $thread += $num->userUnreadMessagesCount(Auth::id());
        }

        $control = $number;

        if(count($user->UserRide)){
            $booked = count($user->UserRide);
        }else{
            $userRide = null;
            $booked = 0;
        }
        if(count($user->ride)){
            $offer = Rides::where('user_id', '=', $user->id )
                    ->where('dateFrom','>=', $today)->count();
        }
        else{
            $offer = 0;
        }
        // All threads that user is participating in
        //$threads = Thread::forUser(Auth::id())->latest('updated_at')->get();
        
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();
        
        
        return view('dashboarduser.message', compact('threads', 'control', 'user', 'thread', 'offer', 'booked'));
    }
    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        
        $user = Auth::user();

        foreach($thread->messages as $message){
            if( $message->user_id != $user->id){
                $message->last_seen = Carbon::now();
                $message->update();
            }
            if($thread->isNotification == 'Yes'){
                $message->last_seen = Carbon::now();
                $message->update();
            }
            $message->user = $message->user;
        }

        $otherUser = $thread->creator();
        if($otherUser->id == $user->id){
            $temp = DB::table('participants')->where('thread_id', '=', $id)
                                         ->where('user_id','!=',$user->id)
                                         ->get()->first();
            $otherUser = User::find($temp->user_id);
        }
        if( count($user->UserRide)){
            $ride = Rides::join('user_rides as urides', 'urides.rides_id', '=', 'rides.id')
                           ->where('urides.user_id', '=', $user->id)
                           ->where('rides.user_id', '=', $otherUser->id)
                           ->get()->first();
        }
        else{
            $ride = null;
        }

        return view('messenger.show', compact('thread', 'otherUser', 'user', 'ride'));
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {   
        $user = Auth::user();

        //own Ride 
        $users = UserRides::where('user_id', '=', $user->id)
                ->where('status','=', 'Approved')
                ->where('booker_payment_paid', '=', 'Yes')
                ->get();
        foreach($users as $uRide){
             $uRide->contact_user = $uRide->ride->user_id;
        }
        $rides = Rides::where('user_id','=',$user->id)->get();

        //Driver
        foreach($rides as $ride){
            if(count($ride->user_ride) > 0){
                foreach($ride->user_ride as $uRide){
                    if($uRide->booker_payment_paid == 'Yes'){
                        $uRide->contact_user = $ride->user_id;
                        $users->add($uRide);
                    }
                }
                
            }
        }
        

        return view('messenger.create', compact('users'));
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function contact($id)
    {   

        $ride = Rides::find($id);

        $users  = $ride->user;
                       
        return view('messenger.contact', compact('users', 'ride'));
    }
    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function contactUser($id, $ride)
    {   
        
        $ride = Rides::find($ride);
        $users  = User::find($id);
        
        
        return view('messenger.contactUser', compact('users', 'ride'));
    }
    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $info = $request->get('recipients');
        $pieces = explode(",", $info);

        $ride_id = $pieces[0];
        $otherUser_id = $pieces[1];
        $body = $request->get('message');

        //Trait from MessageTrait             
        $this->StoreNewMessage($ride_id, $otherUser_id,$body);

        Session::flash('message', "Successfully sent email");
        return redirect()->route('messages');
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        $thread->activateAllParticipants();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => Input::get('message'),
        ]);

        return redirect()->route('messages.show', $id);
    }

    public function delete($id){

        $data = Thread::find($id);

        $message = Message::find($data->messages);

        $message[0]->delete();
        $data->delete();

        // All threads, ignore deleted/archived participants
        $threads = Thread::getAllLatest()->get();

        Session::flash('message', "Successfully deleted message");

        return redirect()->back();
    }
}
