<?php

namespace App\Http\Controllers;

use App\Http\Traits\DestroyRides;
use App\DeleteUsers;
use App\User;
use App\Rides;
use App\Driver;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Socialite;
use Session;
use Redirect;
use DB;

class DeleteUsersController extends Controller
{
    use DestroyRides;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeleteUsers  $deleteUsers
     * @return \Illuminate\Http\Response
     */
    public function show(DeleteUsers $deleteUsers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeleteUsers  $deleteUsers
     * @return \Illuminate\Http\Response
     */
    public function edit(DeleteUsers $deleteUsers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeleteUsers  $deleteUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeleteUsers $deleteUsers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeleteUsers  $deleteUsers
     * @return \Illuminate\Http\Response
     */
    public function destroyFromUser(DeleteUsers $deleteUsers, $id)
    {

        $user = User::find($id);
 
        $check = DeleteUsers::where('user_id', '=', $id)->get()->first();

        if(count($check) > 0){
            

            $rides = Rides::where('user_id', '=', $user->id)->get();

            foreach($rides as $ride){
                $response = $this->destroySimpleRideDriver($ride->id, $user->id);
            }

            DB::table('user_preferences')->where('user_id', '=', $user->id)->delete();
            // TODO Send email saying we are sorry for the person deleteing account
            
            $driver = Driver::where('user_id', '=', $user->id)->get()->first();
            if(count($user->driver) > 0){
                $driver->delete();
            }
            $participants = DB::table('participants')->where('user_id', '=', $user->id)->get();
            foreach($participants as $part){
                DB::table('threads')->where('id','=', $part->thread_id)->delete();
                DB::table('messages')->where('thread_id', '=', $part->thread_id)->delete();
                DB::table('participants')->where('id','=', $part->id)->delete();
            }

            if($user->avatar != null){
                if (file_exists($user->avatar)) {
                    unlink($user->avatar);
                }
            }
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->delete();
            Auth::logout();

            Session::flash('success',"logout");
            return Redirect::to('/register');
        }
        else{
            $DeleteUser = new DeleteUsers;
            $DeleteUser->name = $user->first_name;
            $DeleteUser->email = $user->email;
            $DeleteUser->user_id = $user->id;
            $DeleteUser->delete_date = Carbon::now();

            $rides = Rides::where('user_id', '=', $user->id)->get();

            foreach($rides as $ride){
                $response = $this->destroySimpleRideDriver($ride->id, $user->id);
            }

            DB::table('user_preferences')->where('user_id', '=', $user->id)->delete();
            // TODO Send email saying we are sorry for the person deleteing account
            $DeleteUser->save();
            $driver = Driver::where('user_id', '=', $user->id)->get()->first();
            if(count($user->driver) > 0){
                $driver->delete();
            }
            $participants = DB::table('participants')->where('user_id', '=', $user->id)->get();
            foreach($participants as $part){
                DB::table('threads')->where('id','=', $part->thread_id)->delete();
                DB::table('messages')->where('thread_id', '=', $part->thread_id)->delete();
                DB::table('participants')->where('id','=', $part->id)->delete();
            }

            if($user->avatar != null){
                if (file_exists($user->avatar)) {
                    unlink($user->avatar);
                }
            }
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->delete();
            
            Auth::logout();
            Session::flash('success',"logout");
            return Redirect::to('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeleteUsers  $deleteUsers
     * @return \Illuminate\Http\Response
     */
    public function destroyFromAdmin(DeleteUsers $deleteUsers, $id)
    {
        $user = User::findOrFail($id);
        $user->active = false;
        

        $check = DeleteUsers::where('user_id', '=', $id)->get()->first();

        if(count($check) > 0){
            Session::flash('error', 'Account is already delelted');
            return Redirect::to('dashboardadmin.index');
        }
        else{
            $DeleteUser = new DeleteUsers;
            $DeleteUser->name = $user->first_name;
            $DeleteUser->email = $user->email;
            $DeleteUser->user_id = $user->id;
            $DeleteUser->delete_date = Carbon::now();

            $DeleteUser->save();

            $rides = Rides::where('user_id', '=', $user->id)->get();

            foreach($rides as $ride){
                $response = $this->destroySimpleRideDriver($ride->id, $user->id);
            }

            DB::table('user_preferences')->where('user_id', '=', $user->id)->delete();
            // TODO Send email saying we are sorry for the person deleteing account
            $DeleteUser->save();
            $driver = Driver::where('user_id', '=', $user->id)->get()->first();
            if(count($user->driver) > 0){
                $driver->delete();
            }
            $participants = DB::table('participants')->where('user_id', '=', $user->id)->get();
            foreach($participants as $part){
                DB::table('threads')->where('id','=', $part->thread_id)->delete();
                DB::table('messages')->where('thread_id', '=', $part->thread_id)->delete();
                DB::table('participants')->where('id','=', $part->id)->delete();
            }

            if($user->avatar){
                if (file_exists($user->avatar)) {
                    unlink($user->avatar);
                }
            }
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->delete();

            Session::flash('message', 'Successfully deleted user');
            return Redirect::to('dashboardadmin.index');
        }


    }
}
