<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\IpRequest;
use App\Preferences;
use App\User;
use App\UserPreferences;
use Auth;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Session;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = url()->previous();
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        Session::flash('success', "logout");
        return redirect('/login');
    }


    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }


    /**
     * Obtain the user information from facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $ip = new IpRequest;
        $ip->ip = request()->ip();
        $ip->url_visted = "login-facebook";
        $ip->save();

        $userSocial = Socialite::driver('facebook')->stateless()->user();

        $user = User::where(['email' => $userSocial->getEmail()])->first();

        if ($user) {
            Auth::login($user);
            return Redirect::intended();
        } else {
            $newUser = new User;
            $name = explode(" ", $userSocial->name);
            if (count($name) > 2) {
                $newUser->first_name = $name[0] . " " . $name[1];
                $newUser->last_name = $name[2];
            } else {
                $newUser->first_name = $name[0];
                $newUser->last_name = $name[1];
            }

            $newUser->email = $userSocial->email;
            $newUser->facebook_id = $userSocial->id;
            $newUser->avatar = $userSocial->avatar;
            $newUser->rating = 0;
            $newUser->verified = 1;
            $newUser->created_from = 'Web';
            $newUser->assignRole('User');

            $message = "";

            if ($newUser->avatar == null || $newUser->email == null) {
                if ($newUser->avatar == null) {
                    $message = "Profile Picture is Empty ";

                }
                if ($$newUser->email == null) {
                    $message = $message . " Email is empty";
                }
                Session::flash('error', 'An Error was encoutred trying to Sign Up with facebook because ' . $message . ', please try again');

                return redirect()->back();
            }
            $newUser->save();

            $to_name = $newUser->first_name;
            $to_email = $newUser->email;
            $info = array('name' => $to_name, "body" => "New User");

            $preferences = Preferences::all();

            // Add Preferemces
            foreach ($preferences as $preference) {
                if ($preference->id % 2 == 0) {
                    $user_preference = new UserPreferences;
                    $user_preference->user_id = $newUser->id;
                    $user_preference->preference_id = $preference->id;
                    $user_preference->save();
                }
            }

            Mail::send('email.newuser', $info, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('New User');
                $message->from('no-reply@ridesurf.io', 'Ridesurf');
            });

            auth()->login($newUser, true);
            return redirect()->to('/dashboarduser/dashboarduser');
        }

    }

    /**
     * Handle a registration request for the application.
     *
     * @param $token
     * @return \Illuminate\Http\Response
     */
    public function verify($token)
    {
        $user = User::where('email_token', $token)->first();
        if (Auth::user() == $user) {
            Auth::logout();
        }

        $user->verified = 1;

        if (Auth::check()) {
            if ($user->save()) {
                return view('email.emailconfirm', ['user' => $user]);
            }
        } else {
            if ($user->save()) {
                return redirect::to('/dashboarduser/profile');
            }
        }
    }
}
