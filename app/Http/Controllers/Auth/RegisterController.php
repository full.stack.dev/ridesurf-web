<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Preferences;
use App\UserPreferences;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Jobs\SendVerificationEmail;
use Carbon\Carbon;
use Config, DB;
use Redirect;
use Newsletter;
use App\IpRequest;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    private $isDebug;
    

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->isDebug= env('APP_DEBUG');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'zipcode' => 'required|max:5',
            'password' => ['required', 
                           'min:8', 
                           'regex:/^.*(?=.{2,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%*@&]).*$/', 
                           'confirmed'],
            'password_confirmation' => 'required|string|min:8',
        ];

        $messages = [
            'password.same' => 'Your passwords do not match.',
            'regex' => 'Please enter a password with at least one number, uppercase, lowercase letter and symbol as (!$#%*@&). 
                        Minimum length 8 characters',

        ];


        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

 
        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->zipcode = $data['zipcode'];
        $user->email_token = str_random(40);
        $user->created_from = 'Web';
        $user->save();


        $preferences = Preferences::all();

        // Add Preferences
        foreach($preferences as $preference){
            if($preference->id % 2 == 0){
                $user_preference = new UserPreferences;
                $user_preference->user_id = $user->id;
                $user_preference->preference_id = $preference->id;
                $user_preference->save();
            }
        }
        
        $user->assignRole('User');

        $to_name = $data['first_name'];
        $to_email = $data['email'];
        $info = array('name'=>$to_name, "body" => "New User");

        
        if(!$this->isDebug){
            if ( ! Newsletter::isSubscribed($user->email) ) {
                Newsletter::subscribe($user->email, 
                    ['FNAME'=> $user->first_name, 'LNAME'=> $user->last_name]);
            }
        }

        Mail::send('email.newuser', $info, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('New User');
            $message->from('no-reply@ridesurf.io','Ridesurf');
        });
        
        $ip = new IpRequest;
        $ip->ip = request()->ip();
        $ip->url_visted = "create-user";
        $ip->save();

        return $user;
    }

    /**
    * Handle a registration request for the application.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        dispatch(new SendVerificationEmail($user));

        return view('email.verified');
    }

    
}
