<?php

namespace App\Http\Controllers;

use App\RidesStop;
use App\Rides;
use App\Vehicle;
use App\UserRides;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use stdClass;
use App\Location;
use App\Connections;
use DB, Auth, Mail;
use Response;
use DateTime, ArrayObject;

class RidesStopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RidesStop  $ridesStop
     * @return \Illuminate\Http\Response
     */
    public function show(RidesStop $ridesStop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RidesStop  $ridesStop
     * @return \Illuminate\Http\Response
     */
    public function edit(RidesStop $ridesStop)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RidesStop  $ridesStop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RidesStop $ridesStop)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RidesStop  $ridesStop
     * @return \Illuminate\Http\Response
     */
    public function destroy(RidesStop $ridesStop)
    {
        //
    }
}
