<?php

namespace App\Http\Controllers;

use App\User;
use App\Driver;
use App\Rides;
use App\UserRides;
use App\DeleteUsers;
use Carbon\Carbon;
use App\Preferences;
use App\UserPreferences;
use App\Http\Traits\DestroyRides;
use App\Http\Traits\Notifications;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cmgmyr\Messenger\Traits\Messagable;
use App\Http\Traits\UploadImage;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Models\Role;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use DB, ArrayObject,Redirect, View;
use Hash, Session, Auth, Mail;

class UserController extends Controller
{
    use Messagable, DestroyRides, Notifications, UploadImage;
    private $admin;
    private $messageCount, $settings;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:Administer roles & permissions');
        $this->middleware('permission:role-list');
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        $this->admin = User::where('first_name', '=', 'admin')->get()->first();
        $this->middleware(function ($request, $next) {

            $this->settings =  DB::table('preferences')->get();
            $this->messageCount = $this->messageCount();

            View::share( 'messageCount', $this->messageCount);
            View::share( 'settings', $this->settings);
            return $next($request);
        });


    }

    
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(10);
        return view('dashboardadmin.users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('dashboardadmin.users.create',compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);


        $user = new User;
        $user->first_name =$request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->email_token = str_random(40);
        $user->save();

        $user->assignRole($request->input('roles'));
        
        $preferences = Preferences::all();

        // Add Preferemces
        foreach($preferences as $preference){
            if($preference->id % 2 == 0){
                $user_preference = new UserPreferences;
                $user_preference->user_id = $user->id;
                $user_preference->preference_id = $preference->id;
                $user_preference->save();
            }
        }
        

        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('dashboardadmin.users.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        $vehicles = DB::table('make')
                        ->where('eStatus', '=', 'Active')
                        ->orderBy('vMake', 'ASC')
                        ->get();


        if(count($user->driver) > 0){

            if($user->driver[0]->year != null && $user->driver[0]->make_id != null ){
                $make = DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake;
                $model = DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle;
                $color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                            ->get()->first()->vColour_EN;
                $type = DB::table('car_type')->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;
                $vehicle = new ArrayObject();
                $vehicle->make = $make;
                $vehicle->model = $model;
                $vehicle->color = $color;
                $vehicle->type = $type;
                $vehicle->year = $user->driver[0]->year;
                $vehicle->vehicle_tag = $user->driver[0]->vehicle_tag;
                $vehicle->vehicle_registration = $user->driver[0]->vehicle_registration;
            }else{
                $vehicle = new ArrayObject();
                $vehicle->make = null;
                $vehicle->model = null;
                $vehicle->color = null;
                $vehicle->type = null;
                $vehicle->year = null;
                $vehicle->vehicle_tag = null;
                $vehicle->vehicle_registration = null;
            }
        }
        else{
            $vehicle = null;
        }

        return view('dashboardadmin.users.edit',compact('user','roles',
            'userRole', 'vehicle', 'vehicles'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();

        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }

        $user = User::find($id);

        //Check Role change
        $roles = $request->input('roles');
        if($roles[0] == 'Driver'){
            //Update 
        }

        $user->fill($input);
        $user->description = $request->get('description');
        $user->update();

        DB::table('model_has_roles')->where('model_id',$id)->delete();


        $user->assignRole($request->input('roles'));

        Session::flash('message','User updated successfully');

        return redirect()->back();
    }

    public function updateDriverInformation(Request $request, $id){
        
        $input = $request->all();
       

        $driver = Driver::where('user_id', '=', $id)->get()->first();
        
        $driver->fill($input);
        $driver->registration_expire = $request->get('registration_expire');
        $driver->license_expire = $request->get('license_expire');
        $driver->insurance_expire = $request->get('insurance_expire');
        $driver->update();


        Session::flash('message','Driver Information Updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeleteUsers  $deleteUsers
     * @return \Illuminate\Http\Response
     */
    public function destroyFromAdmin(DeleteUsers $deleteUsers, $id)
    {
        $user = User::findOrFail($id);
        $rides = Rides::where('user_id', '=', $user->id)->get();

        foreach($rides as $ride){
            //TOOD
            //Send Message to Users who booked as well
            $ride->delete();

        }
        
        if(count($user->driver) > 0){
            $driver = Driver::where('user_id', '=', $user->id)->first();
            $driver->delete();
        }

        $booked = UserRides::where('user_id', '=', $user->id)->get();

        foreach($booked as $book){
            //TODO
            //Send Message to Drivers who has passengers
            $book->delete();
        }

        DB::table('user_preferences')->where('user_id','=', $user->id)->delete();

        $threads = Thread::forUser(Auth::id())->get();
        foreach($threads as $t){
          //Todo Save them in another place
          $t->delete();
        }
        $messages = Message::where('user_id', '=', $user->id)->get();
        foreach($messages as $messasge){
          $messasge->delete();
        }
        
        $check = DeleteUsers::where('user_id', '=', $id)->count();

        $dir = public_path('/images/user_information/' . $user->id);
        $this->rrmdir($dir);
        
        if($check > 0){
            Session::flash('error', 'Account is already delelted' );
            return redirect()->back();
        }
        else{
            $DeleteUser = new DeleteUsers;
            $DeleteUser->name = $user->first_name;
            $DeleteUser->email = $user->email;
            $DeleteUser->user_id = $user->id;
            $DeleteUser->delete_date = Carbon::now();

            $DeleteUser->save();
            DB::table('model_has_roles')->where('model_id',$user->id)->delete();
            $user->delete();

            Session::flash('message','User deleted successfully');
            return redirect()->back();
        }
    }
    //Delete files and folders 
    public function rrmdir($dir) {
       if (is_dir($dir)) {
         $objects = scandir($dir);
         foreach ($objects as $object) {
           if ($object != "." && $object != "..") {
              if (filetype($dir."/".$object) == "dir") 
                $this->rrmdir($dir."/".$object); 
              else 
                unlink($dir."/".$object);
           }
         }
         reset($objects);
         rmdir($dir);
       }
    }

    public function updateSingleImage(Request $request, $id){
        $driver = Driver::where('user_id','=', $id)->get()->first();

        $type = $request->get('type');
        $file = Input::file('file_input');

        if($file != null){
            $inner_path = '/images/user_information/'.$id.'/car_details';

            try{
                if(file_exists(public_path().$driver[$type] && $driver[$type] != null) ){
                    unlink(public_path().$driver[$type]);
                }

                $driver[$type] = $this->resizeImage($file,$inner_path);
                
                $driver->update();
            }catch (\Exception $e) {
                Session::flash('error', 'Error uploading image, please try again');

                return redirect()->back()->withInput();
            }
        }
        Session::flash('message', 'Image Uploaded');
        return redirect()->back();

    }
    public function approveDriver(Request $request,$id){

        $user = User::find($id);
        
        $Driver = Driver::where('id', '=', $user->driver[0]->id)->get()->first();

        $Driver->status = 'Approved';

        $Driver->status_message = "User has been approved";

        //Delete Previous roles
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole('Driver');

        $user->update();

        $Driver->update();
        //TODO IF IS ALREADY A DRIVER OR NEW DRIVER 
        $driver_name = $user->first_name;
        $email = $user->email;
        $info = array('name'=>$driver_name);

        Mail::send('email.approveDriver', $info, function($message) use ($email) {
            $message->to($email)->subject('You have been approved as a driver!');
        });

        // Send Internal Message 
        $message = "You have been approved as a driver, yay! Now go post your first ride!";

        $thread = Thread::create([
            'subject' => 'You have been approved as a driver!',
            'isNotification' => 'Yes',
            //'rides_id' => '',
        ]);

        $thread->isNotification = 'Yes';
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id, // From Admin of the company
            'body' => $message,
        ]);


        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $user->id,
            'last_read' => new Carbon,
        ]);



        Session::flash('message','Driver successfully approved');

        return redirect()->back();

    }


    public function denyDriver(Request $request, $id){

        $user = User::find($id);
        $Driver = Driver::where('id', '=', $user->driver[0]->id)->get()->first();
        $Driver->status = 'Denied';
        $info = $request->except('_token', 'reply_message');
        $reply = $request->get('reply_message');

        $Driver->status_message = "Oops, We need you to submit again some information";
        foreach($info as $data){

            $Driver->status_message = " -" . $Driver->status_message . " " . $data. ",";
        }
        
        
        $Driver->update();

        $driver_name = $user->first_name;
        $email = $user->email;
        $info = array(
            'name'=>$driver_name, 
            'reasons'=>$info,
            'reply' => $reply,
        );

        Mail::send('email.denyDriver', $info, function($message) use ($email) {
            $message->to($email)->subject('Ridesurf Driver Application Update');
        });

        $thread = Thread::create([
            'subject' => 'Ridesurf Driver Application Update',
            'isNotification' => 'Yes',
            //'rides_id' => '',
        ]);
        $thread->isNotification = 'Yes';
        $thread->update();
        
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => 9, // From Admin of the company
            'body' => $Driver->status_message,
        ]);


        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $user->id,
            'last_read' => new Carbon,
        ]);
        

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(8);
        }

        Session::flash('message','Driver request still needs more information or is missing information');

        return redirect()->back();

    }

    public function requestDriversList(Request $request){

        $drivers = Driver::where('status', '=', 1)->orderBy('updated_at', 'DESC')->paginate(25);

        $driverCount = Driver::where('status', '=', 1)->count();

        return view('dashboardadmin.requestDriversList', compact('drivers', 'driverCount'));
    }

    public function filterUser(Request $request){
        $name = $request->get('first');
        $data = User::orderBy('id','DESC')
                ->where('first_name', '=', $name)
                ->paginate(10);
        $i=0;
        return view('dashboardadmin.users.index', compact('data', 'i'));
    }

    public function importUsers(Request $request){

        $file = $request->file('users_import'); 

        Excel::import(new UsersImport, $file);

    }

    public function updateUsers(Request $request){

        $users = User::all();

        foreach($users as $user){
            if($user->imported != null){
                $user->verified = 1;
                $user->update();
            }
        }

        Session::flash('message','Users Updated');
        return redirect()->back();
    }
    
}