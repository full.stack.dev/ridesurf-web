<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Vehicle;
use App\ProfileImages;
use App\UserRides;
use App\Driver;
use App\Reviews;
use App\Rides;
use App\RidesStop;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth; 
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

use DB, Mail;

use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\Sale;
use PayPal\Api\Payment;


class PaymentController extends Controller
{

	private $admin, $paypal_client,$paypal_secret, $app_debug ;

	public function __construct()
    {
        $this->admin = User::where('first_name', '=', 'admin')->get()->first();
        $this->app_debug= env('APP_DEBUG');
        $this->paypal_client = env('PAYPAL_CLIENT_ID');
        $this->paypal_secret = env('PAYPAL_SECRET');
    }

    public function paymentSucess(Request $request){
    	
        $success = $request->get('success');
        $seats = $request->get('seats');
        $user = Auth::user();
        $ride = Rides::find($request->get('ride_id'));
        $user_ride = UserRides::where('user_id', '=', $user->id)->where('rides_id', '=', $ride->id)->get()->first();
        
        $payments = "";
        try{
            $apiContext = new \PayPal\Rest\ApiContext(
                      new \PayPal\Auth\OAuthTokenCredential(
                        $this->paypal_client,
                        $this->paypal_secret
                      ));
            if($this->app_debug == false){
                $apiContext->setConfig(
                    array(
                        'mode' => 'live',
                    )
                );
            }
            $payments = Payment::get($success['response']['id'], $apiContext);
            $payments->getTransactions();
        }catch(\Exception $e){
            Log::error('ERROR PY0001 error trying to process a payment');

            return response()->json(['message' => 'There was an error 
                trying to process the payment', 500]);
        }
        

        
        $obj = $payments->toJSON();
        $paypal_obj = json_decode($obj);
        $sale_id = $paypal_obj->transactions[0]->related_resources[0]->sale->id;

	    $user_ride->booking_number = "BCCS" . $user->id ."-". $ride->id;
	    $user_ride->booking_date = Carbon::now();
	    $user_ride->booker_payment_paid = 'Yes';
        $user_ride->sale_id = $sale_id;
	    $user_ride->transaction_id = $success['response']['id'];
	    $user_ride->price_paid = $request->get('price_paid');
        

        $message = "Congratulations! You have a passenger for your trip to " . $ride->location_to . " Your passenger's name is " . $user->first_name . ", please contact them to arrange a safe pickup and dropoff location. \n Happy travels!";

	    $thread = Thread::create([
	         'subject' => 'New passenger for your trip to ' .$ride->location_to,
	         'rides_id' => $ride->id,
             'isNotification' => 'Yes',
	    ]);
        $thread->isNotification = "Yes";
        $thread->update();

	    $thread2 = Thread::create([
            'subject' => 'Your ride to ' . $ride->city_to .' has changed' ,
            'rides_id' => $ride->id,
            'isNotification' => 'Yes',
        ]);
        $thread2->isNotification = "Yes";
        $thread2->update();
        
        // Message
        Message::create([
             'thread_id' => $thread2->id,
             'user_id' => $this->admin->id,
             'body' => $message,
        ]);

        // Sender
        Participant::create([
             'thread_id' => $thread2->id,
             'user_id' => $ride->user->id,
             'last_read' => new Carbon,
        ]);

	    // Message
	    Message::create([
	         'thread_id' => $thread->id,
	         'user_id' => $this->admin->id,
	         'body' => $message,
	    ]);

	    // Sender
	    Participant::create([
	         'thread_id' => $thread->id,
	         'user_id' => $ride->user->id,
	         'last_read' => new Carbon,
	    ]);

	    // Recipients
	    if (Input::has('recipients')) {
	         $thread->addParticipant($ride->user->id);
	    }

        // Recipients
        if (Input::has('recipients')) {
             $thread2->addParticipant($ride->user->id);
        }

        $updateThread = Thread::forUser(Auth::id())->where('rides_id', '=', $ride->id)
                        ->where('before_pay', '=', true)->get();

        foreach($updateThread as $t){
            $t->before_pay = false;
            $t->update();
        }                

        //Send to Driver
	    $driver_name = $ride->user->first_name;
	    $email = $ride->user->email;
	    $info = array('name'=>$driver_name, "user" => $user->first_name, "rideTime" => $ride->startHour, 
            "rideDate" => date('j M Y', strtotime($ride->dateFrom)), "rideFrom" => $user_ride->city_from, "rideTo" => $user_ride->city_to);

	    Mail::send('email.bookedDriver', $info, function($message) use ($email) {
	         $message->to($email)->subject('Passenger paid for your ride');
	     });

	    //Send to Passenger
	    $user_name = $user->first_name;
	    $email = $user->email;
	    $info_user = array('name'=>$user_name, "user" => $user->first_name, "rideTime" => $ride->startHour, 
            "rideDate" => date('j M Y', strtotime($ride->dateFrom)), "rideFrom" => $ride->location_from, "rideTo" => $ride->location_to, "driver" => $driver_name,
	         "booking_confirm" => $user_ride->booking_number, "price" => $user_ride->price_paid);

	    Mail::send('email.bookedUser', $info_user, function($message) use ($email) {
	        $message->to($email)->subject('Confirmation of your upcoming trip');
	    });

	    $response = array(
        	'success' => 200,
        	'message' => "You are confirmed to go to " . $ride->location_to ." with " .$ride->user->first_name .".",
        	'ride' => $ride,
        );
	    $user_ride->update();
        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function getUserPayments(){

    	$user = Auth::user();
    	$last_week_start = Carbon::now()->subWeek()->startOfWeek()->format('Y-m-d');
    	$last_week_end = Carbon::now()->subWeek()->endOfWeek()->format('Y-m-d');
    	$rides = Rides::where('user_id', '=', $user->id)
    			 ->where('dateFrom', '<=', $last_week_end)
    			 ->whereHas('user_ride', function ($q){
    			 	$q->where('booker_payment_paid', '=', 'Yes');
    			 })->get();

        foreach($rides as $ride){
            $ride->date = date('j M Y', strtotime($ride->dateFrom));
            $ride->start = date("g:i a", strtotime($ride->startHour));
            $ride->passengers = UserRides::where('rides_id','=',$ride->id)
                                ->where('booker_payment_paid','=', 'Yes')->count();
            $ride->ride_total = ($ride->price*$ride->passengers);
        }

        if(empty($rides)){
            $rides = 0;
        }
    	$currentPayout = Rides::where('user_id', '=', $user->id)
    			 		->whereBetween('dateFrom', [$last_week_start, $last_week_end])
    			 		->get();
    	$weekPayout = 0;
    	foreach($currentPayout as $item){
    		$weekPayout = $weekPayout + $item->totalToPay;
    	}
    	
    	$response = array(
    		'rides' => $rides,
    		'weekPayout' => $weekPayout
    	);

    	header('Content-Type: application/json');
        return response()->json($response);
    }

}
