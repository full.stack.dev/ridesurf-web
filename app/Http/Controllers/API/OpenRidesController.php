<?php

namespace App\Http\Controllers\API;

use App\Http\Traits\LocationHelper;
use App\Rides;
use App\UserRides;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\OpenRideHours;
use App\OpenRide;
use App\UserOpenRide;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Mail, Response;


class OpenRidesController extends Controller
{
    use LocationHelper;

    private $admin;

    public function __construct(Request $request)
    {

        $this->admin = User::where('first_name', '=', 'admin')->get()->first();
    }

    public function createOpenRides(Request $request){

    	$input = $request->all();
    

        $arrayFrom = explode(',', $input['data']['location_from']);
        $arrayTo = explode(',', $input['data']['location_to']);

        $city_from = $this->parseLocation($arrayFrom, $input['data']['location_from'] );
        $city_name = $this->getCity($city_from);
        $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);


        if($geocodeObject['status'] = "OK"){
            $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
            $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
            $city_from = $this->parseLocation($temp,  $input['data']['location_from']);
        }

        $city_to = $this->parseLocation($arrayTo, $input['data']['location_to']);
        $city_name = $this->getCity($city_to);
        $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);


        if($geocodeObject['status'] = "OK"){
            $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
            $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
            $city_to = $this->parseLocation($temp, $input['data']['location_to']);
        }

        $state = $this->getState($city_from);
        $state_to = $this->getState($city_to);

        if(strlen($state) > 2){
            $temp_from = $this->parseState($state);

            $city = $this->getCity($city_from);
            $city_from = $city ."," . $temp_from;
        }
        if( strlen($state_to) > 2){
            $temp_to = $this->parseState($state_to);
            $city = $this->getCity($city_to);
            $city_to = $city ."," . $temp_to;
        }



        $open_ride = new OpenRide;
        $open_ride->fill($input['data']);
        $open_ride->seats_available = $input['data']['seats']; 
        $open_ride->user_id = Auth::id();
        $open_ride->city_from =$city_from;
        $open_ride->city_to =$city_to;
        $open_ride->ladies_only = 'No';

        if($input['data']['trip_purpose_id'] == null ){
            header('Content-Type: application/json');
            return response()->json(['message' => 'Please fill out all the inputs'],401);
        }else{
            $open_ride->trip_purpose_id = $input['data']['trip_purpose_id'];
        }
        $open_ride->save();

        if($open_ride->trip_purpose_id == null){
            $open_ride->trip_purpose_id = 1;
        }
        if($open_ride->luggage_size == null){
            $open_ride->luggage_size = 'Small';
        }

        foreach($input['openHours'] as $hour){

            $oph = new OpenRideHours;
            $oph->open_rides_id = $open_ride->id;
            $carbon = new Carbon($hour);
            $oph->hour = $carbon->format('H:i:s');
            $oph->save();

        }

        $message = array(
            'message' => 'Successfully Created Ride',
            'open_ride' => $open_ride
        );
        header('Content-Type: application/json');
        return response()->json($message);
    }

    public function getOpenRides($page){
        $user = Auth::user();

        $openRides = OpenRide::with('hours')->with('user')
            ->where('user_id','=',$user->id)
            ->paginate(5,['*'], 'page', $page);

        foreach($openRides as $ride){
            foreach($ride['hours'] as $hour){
                $hour->hour = date("g:i a", strtotime($hour->hour));
            }
            
            if($ride['active'] == true ){
                $ride['active'] = true;
            }else{
                $ride['active'] = false;
            }
        }

        return response()->json($openRides, 200);
    }

    public function editOpenRides(Request $request, $id){
        $data = $request->get('data');
        $hours = $request->get('openHours');
        $hourSwtich = $request->get('hourSwtich');
        $openRide = OpenRide::find($id);
        $openRide->fill($data);
        $openRide->update();

        if(count($hours) > 0){
            $openRide->hours()->delete();
            foreach($hours as $hour){

                $oph = new OpenRideHours;
                $oph->open_rides_id = $openRide->id;

                if($hourSwtich){
                    $carbon = new Carbon($hour['time']);
                }else{
                    $carbon = new Carbon($hour['hour']);
                }
                $oph->hour = $carbon->format('H:i:s');
                $oph->save();

            }
        }

        $response = array(
            'message' => 'Your ride has been updated',
            'status' => 'success'
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function  changeStatus($id){
        $openRide = OpenRide::find($id);

        if($openRide->active == true){
            $openRide->active = false;
            $openRide->update();
            $response = array(
                'message' => 'Your Open Ride is now Inactive',
                'status' => 'success'
            );
            header('Content-Type: application/json');
            return response()->json($response);
        }else{
            $openRide->active = true;
            $openRide->update();
            $response = array(
                'message' => 'Your Open Ride is now Active',
                'status' => 'success'
            );

            header('Content-Type: application/json');
            return response()->json($response);
        }

        $response = array(
            'message' => 'There was an error trying to update the Open Ride, please try again ',
            'status' => 'error'
        );
        header('Content-Type: application/json');
        return response()->json($response, 400);
    }

    public function delete($id){
        $openRide = OpenRide::find($id);
        $hours = OpenRideHours::where('open_rides_id',$id)->get();
        foreach($hours as $hour){
            $hour->delete();
        }
        $openRide->delete();

        $response = array(
            'message' => "Open Ride successfully deleted",
            'status' => 'success',
        );
        return \Response::json($response);

    }

    public function acceptPassenger(Request $request){

        $userRide = UserOpenRide::find($request->get('user_ride_open_id'));

        $openRide = OpenRide::find($userRide->open_ride_id);

        $array = $openRide->toArray();

        //Switch open ride to ride
        $ride = new Rides;
        $ride->fill($array);
        $ride->user_id = $openRide->user_id;
        $ride->seats_available = $openRide->seats - $userRide->seats;
        $ride->city_from = $openRide->city_from;
        $ride->city_to = $openRide->city_to;
        $ride->dateFrom = $userRide->date;
        $ride->startHour = $userRide->hour;
        $ride->save();

        //Create new user ride to the normal ride
        $new_user_ride = new UserRides;
        $new_user_ride->rides_id = $ride->id;
        $new_user_ride->user_id = $userRide->user_id;
        $new_user_ride->status = 'Approved';
        $new_user_ride->city_from = $ride->city_from;
        $new_user_ride->city_to = $ride->city_to;
        $new_user_ride->booker_payment_paid = 'No';
        $new_user_ride->ride_cost = $openRide->price;
        $new_user_ride->seats = $userRide->seats;
        $new_user_ride->save();

        $message = "Congratulations, you are going to ".$new_user_ride->ride->location_to."! The driver has approved your request to join the ride. Please go to your booking page and pay asap.";

        $thread = Thread::create([
            'subject' => 'Approved for ride to ' .$ride->location_to,
            'rides_id' => $ride->id,
            'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $userRide->user_id,
            'last_read' => new Carbon,
        ]);

        $driver_name = $ride->user->first_name;
        $email = $userRide->user->email;

        $info = array("user" => $userRide->user->first_name, "rideTo" => $ride->location_to, "driver"    => $driver_name, "ride_id" => $ride->id);

        Mail::send('email.acceptPassenger', $info, function($message) use ($email) {
            $message->to($email)->subject('Ride Request Update');
        });

        $openRide->active = false;
        $openRide->update();
        $userRide->status = 'Approved';
        $userRide->update();

        $response = array(
            'message' => "Congratulations you have approve your passenger",
            'status' => 2,
        );
        return \Response::json($response);
    }

    public function denyPassenger(Request $request){

        $userRide = UserOpenRide::where('user_id','=' ,$request->get('user_id'))
                                 ->where('open_ride_id', '=', $request->get('ride_id'))
                                 ->first();
        $userRide->status = "Denied";
        $userRide->update();

        $message = "Sorry, the driver has denied your request to join the trip. We encourge you to look for another one!";

        $thread = Thread::create([
            'subject' => 'New passenger request for ' . $userRide->open_ride->location_to,
            'rides_id' => $userRide->open_ride->id,
            'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $userRide->user->id,
            'last_read' => new Carbon,
        ]);

        $email = $userRide->user->email;
        $info = array("user" => $userRide->user->first_name, 
                "rideTo" => $userRide->open_ride->location_to);

        Mail::send('email.denyPassenger', $info, function($message) use ($email) {
            $message->to($email)->subject('Ride Request Update');
        });

        $response = array(
            'message' => "Passenger has been sent an update",
            'status' => 'success',
        );
        return \Response::json($response);

    }


    /*public function getRangePeriod(Request $request){
        $openRide = OpenRide::find($request->get('ride_id'));

        $now  = Carbon::now();
        $end = Carbon::parse($openRide->open_date_to)->addDay(1);*/ 

    public function getDates($id){
        $openRide = OpenRide::find($id);

        $now  = Carbon::now();
        $end = Carbon::parse($openRide['open_date_to'])->addDay(1);


        //Check the times that
        if($now->format('Y-m-d') <= $openRide->open_date_from){
            if($openRide->open_date_from == $now->format('Y-m-d')){
                $period = CarbonPeriod::between($openRide->open_date_from, $end);
            }else{
                $period = CarbonPeriod::between($openRide->open_date_from, $end);
            }
        }
        else{
            $now  = Carbon::now()->addDay(1);
            $period = CarbonPeriod::between($now, $end);
        }
        $dates = [];

        // Iterate over the period
        foreach ($period as $date) {
            array_push($dates, $date->format('m/d/y'));
        }


        $hours = [];
        foreach($openRide->hours as $hour){
            array_push($hours, date("g:i a", strtotime($hour->hour)));
        }

        $response = array(
            'dates' => $dates,
            'hours' => $hours
        );
        return \Response::json($response);
    }

    public function requestjoin(Request $request, $id){

        $input = $request->all();
        $openRide = OpenRide::find($id);

        $hour = date("G:i", strtotime($input['hour']));
        $date =date("Y-m-d", strtotime($input['date']));

        $userRide = new UserOpenRide;
        $userRide->open_ride_id = $id;
        $userRide->user_id = Auth::id();
        $userRide->status = 'Requested';
        $userRide->hour = $hour;
        $userRide->date = $date;
        $userRide->seats = $input['seat'];
        $userRide->save();

        $message = "You have a new request for your open ride!";
        $thread = Thread::create([
            'subject' => 'Passenger request for the open ride to ' . $openRide->location_to ,
            'rides_id' => $openRide->id,
            'isNotification' => 'Yes',
        ]);

        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $openRide->user->id,
            'last_read' => new Carbon,
        ]);

        $email = $openRide->user->email;
        $info = array(
            'openRide' => $openRide,
            'userRide' => $userRide,
            'first_name' => $openRide->user->first_name,
        );

        Mail::send('email.openRideRequest', $info, function($Newmessage) use ($email) {
            $Newmessage->to($email)->subject('Passenger request for an open ride');
        });

        $response = 'Successfully requested ';
        header('Content-Type: application/json');
        return response()->json($response);
    }
}
