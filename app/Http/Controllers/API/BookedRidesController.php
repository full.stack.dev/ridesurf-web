<?php

namespace App\Http\Controllers\API;

use App\TripPurpose;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Vehicle;
use App\UserRides;
use App\Driver;
use App\Reviews;
use App\Rides;
use App\RidesStop;
use Carbon\Carbon;
use Validator;
use Mail, DB, Response;
use Illuminate\Support\Facades\Log;
use App\Http\Traits\DestroyRides;
use App\Http\Traits\BookingTrait;
use App\Http\Traits\PreferencesTrait;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;

class BookedRidesController extends Controller
{   
    use DestroyRides,BookingTrait, PreferencesTrait;

    private $url;
    private $adminEmails;
    private $admin;


    public function __construct()
    {
        $this->url = "http://127.0.0.1:8000";

        $this->adminEmails = explode(',', env('ADMIN_EMAILS'));

        $this->admin = User::where('first_name', '=', 'admin')->get()->first();

    }

    public function getCurentBooked(Request $request, $page){

    	$user = Auth::user();
    	$time_today = strtotime(Carbon::now());

//    	$userRide = UserRides::where('user_rides.user_id', '=', $user->id)
//            ->where('user_rides.status', '=', 'Approved')
//            ->with('ride')
//            ->whereHas('ride', function ($q) {
//                $q->orderBy('dateFrom', 'desc');
//            })
//            ->paginate(5,['*'], 'page', $page);

        $userRide = UserRides::where('user_rides.user_id', '=', $user->id)
                    ->leftJoin('rides', 'rides.id', '=', 'user_rides.rides_id')
                    ->orderBy('rides.dateFrom', 'asc')
                    ->where(function ($query) {
                        $query->whereRaw('CONCAT(rides.dateFrom, " ",rides.startHour) >= NOW()');
                    })
                    ->paginate(5,['*'], 'page', $page);


        //Populate the ride components
    	$userRide = $this->populateUserRides($userRide);

        /* Fix Ionic problem with not looping array
        *  when it starts from a number bigger than 0
        */ 	
//        $data = [];
//        $count = 0;
//        foreach($userRide as $uride){
//            $data[$count++] = $uride;
//        }
//        $message = array(
//            'userRide' => $userRide,
//            'data' => $data);

    	header('Content-Type: application/json');
        return response()->json($userRide);
    }

    public function getPastBooked(Request $request, $page){

        $user = Auth::user();
        $time_today = strtotime(Carbon::now());

        $userRide = UserRides::where('user_rides.user_id', '=', $user->id)
                    ->where('user_rides.status', '=', 'Approved')
                    ->with('ride')
                    ->paginate(5,['*'], 'page', $page);

        //Filter the rides that only happend from Now to the future
        $userRide = $userRide->setCollection($userRide->getCollection()->filter(

            function ($item) use ($userRide, $time_today) {
                $time_ride = strtotime($item->ride->dateFrom  ." ".$item->ride->startHour);

                if($time_ride <= $time_today){
                    return $item;
                }

            }
        )); 

        $userRide = $this->populateUserRides($userRide);

        if(empty($userRide)){
            $userRide = 0;
        }
        /* Fix Ionic problem with not looping array
        *  when it starts from a number bigger than 0
        */  
        $data = [];
        $count = 0;
        foreach($userRide as $uride){
            $data[$count++] = $uride;
        }
        $message = array(
            'userRide' => $userRide,
            'data' => $data);

        header('Content-Type: application/json');
        return response()->json($message);
    }

    public function rideBooking(Request $request){

        $ride = Rides::find($request->get('ride_id'));

        $ride->user_image = $ride->user->avatar;
        if($ride->trip_purpose_id)
            $ride->purpose = TripPurpose::find($ride->trip_purpose_id);
        else
            $ride->purpose = null;

        $user = User::find($ride->user_id);

        $preferences = $this->getUserPreferences($user);

        foreach($preferences as $uPref){
            $uPref->preference = $uPref->preference;
        }


        $carMake = DB::table('make')->where('iMakeId', '=', $ride->user->driver[0]->make_id)
                   ->get()->first()->vMake;

        $carModel = DB::table('model')->where('iModelId', '=', $ride->user->driver[0]->model_id)
                    ->get()->first()->vTitle;
                                                                
        $carImage = $ride->user->driver[0]->path_image;
        
        $date = date('m/d/Y', strtotime($ride->dateFrom));

        $check = UserRides::where('rides_id', '=', $ride->id)->where('user_id', '=', Auth::id())->count();

        $ride->time = date('g:i a', strtotime($ride->startHour));
        $ride->carModel = $carModel;
        $ride->date = $date;
        $ride->carMake = $carMake;
        $ride->carImage = $carImage;
        $ride->user_prefences = $preferences;
        $ride->check = $check;
        $ride->airport = $ride->airport_details;
        if(count($ride->ride_stop ) > 0){
            $ride->ride_stop = $ride->ride_stop;
        }
        header('Content-Type: application/json');
        return response()->json($ride);
    }

    public function reviewPassenger(Request $request){
        $userRide = UserRides::find($request->get('userRide_id'));

        $review = new Reviews;
        $review->user_rating_id = Auth::id();
        $review->user_rate_id = $userRide->user->id; // Passenger
        $review->rate = $request->get('rating');
        $review->description = $request->get('body');
        $review->review_from_name = Auth::user()->first_name;
        $review->save();

        $userRide->driver_rate = true;
        $userRide->update();

        //check that all users are reviewed
        $ride = Rides::find($userRide->rides_id);
        $count = count($ride->user_ride);
        $second = 0;

        foreach($ride->user_ride as $uRide){
            if($uRide->driver_rate == true){
                $second++;
            }
        }
        if($count == $second){
            $ride->completed = 'Completed';
            $ride->update();
        }

        $response = array(
            'message' => 'Thanks for your feedback!'
        );
        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function reviewDriver(Request $request){
        $userRide = UserRides::find($request->get('userRide_id'));

        $review = new Reviews;
        $review->user_rating_id = Auth::id();
        $review->user_rate_id = $userRide->ride->user->id; //Driver
        $review->rate = $request->get('rating');
        $review->description = $request->get('body');
        $review->review_from_name = Auth::user()->first_name;
        $review->save();

        $userRide->reviewed = true;
        $userRide->update();

        $response = array(
            'message' => 'Thanks for your feedback!'
        );
        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function deleteUserRide(Request $request){

        $user_ride = UserRides::find($request->get('uRide_id'));
        $user = Auth::user();
        $reason = $request->get('reason');
        $ride = Rides::find($user_ride->rides_id);

        if($user_ride->booker_payment_paid == 'Yes'){
            $this->destroyUserRidePostPay($user, $user_ride, $reason, $ride);
        }else{
            $this->destroyUserRidePrePaid($user, $user_ride, $reason, $ride);
        }


        //Send Message to Driver that User cancel
        $message = "Your passenger " . $user_ride->user->first_name. " for your drive to " .$user_ride->ride->location_to . " has cancelled.";

        $thread = Thread::create([
             'subject' => 'Passenger has cancelled ride to ' .$user_ride->ride->location_to,
             'rides_id' => $ride->id,
             'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
             'thread_id' => $thread->id,
             'user_id' => $this->admin->id,
             'body' => $message,
        ]);


        Participant::create([
             'thread_id' => $thread->id,
             'user_id' => $user_ride->ride->user->id,
             'last_read' => new Carbon,
        ]);


        $email = $user_ride->ride->user->email;
        $info_user = array(
             'location_to' => $user_ride->ride->location_to,
             'ride' => $ride,
             'user_ride' => $user_ride,
             'name' => $user_ride->ride->user->first_name,
             'passenger_name' => $user_ride->user->first_name,
        );


        //Send Email to Driver about Cancel Ride
        Mail::send('email.cancel_userRide_driver', $info_user, function($message) use ($email) {
                         $message->to($email)->subject("Your passenger has canceled the ride");
        });

        $user_ride->delete();

        $response = array(
            'message' => 'Successfully deleted booked ride.',
        );
        header('Content-Type: application/json');
        return response()->json($response);
    }
}
