<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Storage, DB, Response, File, Mail;
use Session,Redirect, ArrayObject;
use App\User;
use App\Vehicle;
use App\ProfileImages;
use App\UserRides;
use App\Driver;
use App\Reviews;
use App\Rides;
use App\State;
use App\TripPurpose;
use Carbon\Carbon;

class FetchController extends Controller
{
    public function getCountries(){

    	$countries = DB::table('countries')->select('name')->orderBy('name', 'ASC')->get();

    	header('Content-Type: application/json');
	    return response()->json($countries);
    }

    public function getStates(){

    	$state = State::all('name', 'code');
    	
    	header('Content-Type: application/json');
	    return response()->json($state);
    }

    public function countryCodes(){

        $countries = DB::table('countries')->select('name','calling_code', 'iso_3166_2', 'flag')
                    ->whereNotNull('flag')
                    ->orderBy('name', 'ASC')->get();
                    
        return response()->json($countries);
    }

    public function fetchPurposes(){
        $purpose = TripPurpose::orderBy('type','asc')->groupby('id')->get();

        header('Content-Type: application/json');
        return response()->json($purpose);
    }
}
