<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\Http\Traits\MailHelper; 
use App\Http\Traits\Notifications; 
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use App\User;
use App\Vehicle;
use App\ProfileImages;
use App\UserRides;
use App\Driver;
use App\Reviews;
use App\Rides;
use App\RidesStop;
use Carbon\Carbon;

class NotificationController extends Controller
{

	use MailHelper, Notifications;

	public $successStatus = 200;

	private $url;
	private $adminEmails;
	private $admin;

    public function __construct()
    {
        $this->url = "http://127.0.0.1:8000";

        $this->adminEmails = explode(',', env('ADMIN_EMAILS'));

        $this->admin = User::where('first_name', '=', 'admin')->get()->first();

    }
    public function getUserNotifications(Request $request){

        $user = Auth::user();
        $notifcations = $this->getNotifications($user);

        header('Content-Type: application/json');
        return response()->json($notifcations);
    }

    public function getNotificationActions(Request $request){

        $user = Auth::user();
        $today = strtotime(Carbon::now());

        $notifications = $this->getActions($user,$today);

        if($notifications == null){
            $notifications = 0;

        }
        
        header('Content-Type: application/json');
        return response()->json($notifications);
    }

    public function getNotificationMessages(){
        $threads = Thread::forUser(Auth::id())->latest('updated_at')
                  ->where('isNotification','=', 'Yes')->get();

        foreach($threads as $thread){
            $thread->user = $thread->creator();
        }

        header('Content-Type: application/json');
        return response()->json($threads);          
    }

    public function getSingleNotificationMessage($id){
        try {
            $thread = Thread::findOrFail($id);
            $user = Auth::user();
            $thread->markAsRead($user->id);
            $thread->messages = $thread->messages;
            $thread->user = $thread->creator();

            foreach($thread->messages as $message){
                $message->last_seen = Carbon::now();
                $message->update();
            }
            
        }catch(ModelNotFoundException $e){
            $reponse = array(
                'message' => $e,
                'status' => 'Error');
            header('Content-Type: application/json');
            return response()->json($threads);
        }

        header('Content-Type: application/json');
        return response()->json($thread);
    }

    public function notificationCount(){
        $user = Auth::user();
        $today = strtotime(Carbon::now());

        $actions = $this->getActions($user,$today);

        $message = $threads = Thread::forUser(Auth::id())->latest('updated_at')
                  ->where('isNotification','=', 'Yes')->get();
        
        $alerts = 0;
    
        foreach($message as $thread){
            $alerts += Message::where('thread_id', '=', $thread->id)
                           ->where('user_id', '!=', Auth::id())
                           ->where('last_seen', '=', null)->count();
        }

        header('Content-Type: application/json');
        return response()->json(['actions' => sizeof($actions),
            'alerts' => $alerts]);
    }
}
