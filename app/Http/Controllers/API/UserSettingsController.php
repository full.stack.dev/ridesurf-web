<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use App\Http\Traits\LocationHelper;
use App\User;
use App\RideRequest;
use Carbon\Carbon;
use Storage, DB, Response, File;
use Hash, Validator;

class UserSettingsController extends Controller
{

        use LocationHelper;
        
        public function changePassword(Request $request){

        $user = Auth::user();

        $password = $request->get('password');
        $confirm = $request->get('confirmPassword');

        if (strcmp($password, $confirm) !== 0){
            //Not Equal
            $response = "Passwords do not macht";
            header('Content-Type: application/json');
            return response()->json($response);
        }else{
            //Equal
            $user->password = Hash::make($password);
            $user->update();

            $response = array(
               "message" =>"Successfully changed password");
            header('Content-Type: application/json');
            return response()->json($response);
        }

    }

    public function updatePaypalAccount(Request $request){

        $user = Auth::user();
        $user->paypal_email = $request->get('email');
        $user->update();

        $response = array(
           "message" => "Successfully updated Paypal account",
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function saveRideReminder(Request $request){
        $from = $request->get('from');
        $to = $request->get('to');
        $date = $request->get('date');
        $state = $this->getState($from);

        $ride_request = new RideRequest;
        $ride_request->user_id = Auth::user()->id;
        $ride_request->city_from = $from;
        $ride_request->city_to = $to;
        $ride_request->state = $state;
        $ride_request->date = $date;
        $ride_request->save();

        $response = array(
            'status' => 200,
            'message' => "Successfully stored request alert");

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function getRideReminders(Request $request){
        $user = Auth::user();

        $reminders = RideRequest::where('user_id', '=', $user->id)->get();
        if(count($reminders) > 0 ){
            $message = array(
                'empty' => 0,
                'reminders' => $reminders);
        }else{
            $message = array(
                'empty' => 1,
                'reminders' => $reminders);
        }
        
        header('Content-Type: application/json');
        return response()->json($message);
    }

    public function deleteRideReminder(Request $request){

        $reminder = RideRequest::find($request->get('id'));

        $reminder->delete();

        return Response::json([
            'message' => 'Successfully deleted alert',
        ], 200); 
    }
}
