<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\PreferencesTrait;
use Illuminate\Support\Facades\Auth; 
use App\Preferences;
use App\User;
use App\UserPreferences;
use Carbon\Carbon;

class PreferencesController extends Controller
{

	use PreferencesTrait;

	public function __contruct()
    {
        $this->middleware('auth');
        
    }

    public function getTheUserPreferences(){

    	$user = Auth::user();
    	$user_preference = $this->getUserPreferences($user);

    	foreach($user_preference as $uPref){
    		$uPref->preference = $uPref->preference;
    	}

    	header('Content-Type: application/json');
        return response()->json($user_preference);

    }

    public function getPreferenceOptions(){

    	$preferences = Preferences::all();
    	header('Content-Type: application/json');
        return response()->json($preferences);
    }
    public function updatePreferences(Request $request){

    	$user = Auth::user();
    	$input = $request->get('data');

      	$user_preference = $this->getUserPreferences($user);

	    foreach($user_preference as $uPrefe){

	    	$preference = Preferences::where('description','=', $input[$uPrefe->preference->category])
	    				 			  ->get()->first();
	    	
	    	$uPrefe->preference_id = $preference->id;
	        $uPrefe->update();
	     }

	    $response = array(
	    	'status' => 200,
	    	'message' => 'Successfully stored' );
	    
	    header('Content-Type: application/json');
        return response()->json($response);

    }

    public function getOtherUserPreferences($id){

        $user = User::find($id);
        $user_preference = $this->getUserPreferences($user);

        foreach($user_preference as $uPref){
            $uPref->preference = $uPref->preference;
        }

        header('Content-Type: application/json');
        return response()->json($user_preference);

    }


}
