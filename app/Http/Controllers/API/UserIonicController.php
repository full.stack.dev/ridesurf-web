<?php

namespace App\Http\Controllers\API;

use App\DeleteUsers;
use App\Driver;
use App\Http\Controllers\Controller;
use App\Http\Traits\DestroyRides;
use App\Http\Traits\MailHelper;
use App\Http\Traits\Notifications;
use App\Http\Traits\UploadImage;
use App\Http\Traits\Formating;
use App\Jobs\SendVerificationEmail;
use App\Mail\EmailVerification;
use App\ProfileImages;
use App\Rides;
use App\RidesStop;
use App\User;
use App\UserPreferences;
use App\UserRides;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Response;
use Validator;
use OneSignal;

class UserIonicController extends Controller
{

    use MailHelper, Notifications, UploadImage, DestroyRides,Formating;

    public $successStatus = 200;

    private $url;
    private $adminEmails;
    private $admin;

    public function __construct()
    {
        $this->url = "http://127.0.0.1:8000";

        $this->adminEmails = explode(',', env('ADMIN_EMAILS'));

        $this->admin = User::where('first_name', '=', 'admin')->get()->first();

    }

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = Auth::user();

            if (count($user->driver) > 0 ) {
                $driver = true;
            } else {
                $driver = false;
            }
            if ($user->avatar == null) {
                $image = false;
            } else {
                $image = true;
            } 

            $response = array(
                'action' => 1,
                'user' => $user,
                'driver' => $driver,
                'role' => $user->getRoleNames(),
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'user_id' => $user->id,
                'email' => $user->email,
                'verified' => $user->verified,
                'avatar' => $user->avatar,
                'paypal' => $user->paypal_email,
                'token' => $user->createToken('MyApp')->accessToken,
                'image' => $image,
            );
            return response()->json($response);
        } else {
            $response = array(
                'action' => 2,
                'message' => 'Your email or password are incorrect.',
                'email' => $request->get('email'),
                'password' => $request->get('password'),
            );
            header('Content-Type: application/json');
            return response()->json($response);
        }
    }

    public function getUserInformation(Request $request){
        $user = Auth::user();

        if (count($user->driver) > 0 ) {
            $driver = true;
        } else {
            $driver = false;
        }
        if ($user->avatar == null) {
            $image = false;
        } else {
            $image = true;
        } 

        $response = array(
            'action' => 1,
            'user' => $user,
            'driver' => $driver,
            'role' => $user->getRoleNames(),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'user_id' => $user->id,
            'email' => $user->email,
            'verified' => $user->verified,
            'avatar' => $user->avatar,
            'paypal' => $user->paypal_email,
            'token' => $user->createToken('MyApp')->accessToken,
            'image' => $image,
        );

        return response()->json($response);
    }

    public function makeLogout(Request $request)
    {

        if (Auth::check()) {
            Auth::logout();
            $response = array(
                'action' => 1,
                'message' => 'Logout Successful',
            );
        } else {
            $response = array(
                'action' => 2,
                'message' => 'Error, please try again.',
            );
        }
        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function registerUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'repassword' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = $request->all();

        $check = User::where('email', '=', $data['email'])->get()->count();

        if ($check > 0) {
            $message = array(
                'action' => 3,
                'status' => 'Error',
                'message' => 'Email already in use');

            header('Content-Type: application/json');
            return response()->json($message);
        }

        $user = new User;
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->email_token = str_random(40);

        $user->save();

        $prefenreces = DB::table('preferences')->where('eStatus', '=', 'Active')->get();

        // Add Preferemces
        foreach ($prefenreces as $pref) {
            DB::table('user_preferences')->insert(
                ['user_id' => $user->id, 'preferences_id' => $pref->iPreferencesId, 'option' => $pref->vNO_Lable_EN,
                    'created_at' => Carbon::now()]
            );
        }
        $user->assignRole('User');

        $to_name = $user->first_name;
        $to_email = $user->email;
        $info = array('name' => $to_name, "body" => "New User");

        Mail::send('email.newuser', $info, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('New User');
            $message->from('no-reply@ridesurf.io', 'Ridesurf');
        });

        dispatch(new SendVerificationEmail($user));

        $response = array(
            'action' => 1,
            'user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'gender' => $user->gender,
            'vLanguageCode' => 'EN',
            'token' => $user->createToken('MyApp')->accessToken,
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function saveProfileData(Request $request)
    {

        if (Auth::check()) {
            $user = Auth::user();
        }

        if (empty($user)) {

            $response = array(
                'status' => 'Error',
                'message' => 'User is not logged in, please login',
            );
            header('Content-Type: application/json');
            return response()->json($response);
        }

        $info = $request->get('data');
        $temp = $info['phone_number'];
        $phone = $this->FormatPhone($temp);
        if(!ctype_digit($phone)){
            $response = array(
                'status' => 'Error',
                'message' => 'Phone Number must be only number',
            );
            header('Content-Type: application/json');
            return response()->json([$response],201);
            Log::info($phone);
        }

        if(!empty($info['area_code'])){
            $temp_area = explode('+',$info['area_code']);
            $area = $temp_area[1];
            Log::info($temp_area);
        }else{
            $area = null;
        }
        $user->fill($info);
        $user->phone_number = $phone;
        $user->area_code = $area;
        $user->address = $info['address'];
        $user->city = $info['city'];
        $user->country = $info['country'];
        $user->state = $info['state'];
        $user->update();

        if ($user->avatar == null) {
            $user->avatar = '/images/avatar.jpg';
        }

        if (count($user->driver) > 0 ) {
            $driver = true;
        } else {
            $driver = false;
        }

        if ($user->avatar == null) {
            $image = false;
        } else {
            $image = true;
        }
        $newUser = array(
            'action' => 1,
            'user' => $user,
            'driver' => $driver,
            'role' => $user->getRoleNames(),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'user_id' => $user->id,
            'email' => $user->email,
            'verified' => $user->verified,
            'avatar' => $user->avatar,
            'paypal' => $user->paypal_email,
            'image' => $image,
        );

        $response = array(
            'action' => 1,
            'message' => 'Successfully edited profile',
            'user' => $newUser,
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }
    public function uploadImageProfile(Request $request)
    {
        ini_set('memory_limit', '-1');
        header('Content-Type: application/json');

        $res = $request->input('vImage');
        if (isset($res) && !empty($res)) {
            $image = $res;
            $user = Auth::user();

            $inner_path = public_path('/images/user_information/' . $user->id);
            $encryp = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 10);
            $image_name = $encryp . ".jpg";

            if (!File::exists($inner_path)) {
                File::makeDirectory($inner_path, $mode = 0777, true, true);
            }
            try {
                if(File::exists($user->avatar)){
                    unlink($user->avatar);
                }

                $image = str_replace('data:image/jpeg;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($inner_path . '/' . $image_name, base64_decode($image));
                $user->avatar = '/images/user_information/' . $user->id . '/' . $image_name;
                $user->update();

       
                $response = array(
                    'status' => '1',
                    'message' => 'Successfully uploaded profile image',
                );

                header('Content-Type: application/json');
                return response()->json($response);
            } catch (\Exception $e) {
                $response = array(
                    'status' => '2',
                    'message' => 'Error Has Occurred',
                    'Error' => $e,
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
        } else {
            $response = array(
                'status' => '2',
                'message' => 'Error Has Occurred',
            );

            header('Content-Type: application/json');
            return response()->json($response);
        }

    }
    public function getProfilePic(Request $request)
     {
        $user = Auth::user();

        $response = array(
            'profile_pic'=>$user->avatar,
            'status' => '1',
            'message' => 'Profile Image Fetch',
        );

        header('Content-Type: application/json');
        return response()->json($response);
     }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */

    public function checkRideOfferForm(Request $request)
    {

        $from = $request->get('from');
        $to = $request->get('to');
        $date = $request->get('date');
        $stopOverAddress = $request->get('stopOverAddress');
        $message = "";

        if ($from == null || $from == "") {

            $message = $message . "Input From is empty, ";
            $response = array(
                'action' => 2,
                'message' => $message,
            );

        }
        if ($to == null || $to == "") {

            $message = $message . "Input To is empty, ";
            $response = array(
                'action' => 2,
                'message' => $message,
            );
        }
        if ($date == null || $date == "") {
            $message = $message . "Date is empty, ";
            $response = array(
                'action' => 2,
                'message' => $message,
            );
        }
        if ($stopOverAddress == null || $stopOverAddress == "") {

            $message = $message . "Input Stop Over is empty.";
            $response = array(
                'action' => 2,
                'message' => $message,
            );
        }
        if ($from != null && $to != null && $date != null &&
            $stopOverAddress != null) {
            $response = array(
                'action' => 1,
                'message' => 'Success',
            );
        }

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function EditViewOfferRideForm(Request $request)
    {
        $user = Auth::user();
        $ride_id = $request->get('ride_id');

        $ride = Rides::find($ride_id);

        if ($ride == null) {
            $response = array(
                'status' => 'error',
                'message' => 'Error trying to get information. Please try again.',
            );

            header('Content-Type: application/json');
            return response()->json($response);
        }

        $make = DB::table('make')->where('iMakeId', '=', $user->driver[0]->make_id)->get()->first()->vMake;
        $model = DB::table('model')->where('iModelId', '=', $user->driver[0]->model_id)->get()->first()->vTitle;
        $color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
            ->get()->first()->vColour_EN;
        $type = DB::table('car_type')->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;

        $hour = date('g:i a', strtotime($ride->startHour));
        $date = date('d-m-Y', strtotime($ride->dateFrom));

        $response = array(
            'make' => $make,
            'model' => $model,
            'carYear' => $user->driver[0]->year,
            'color' => $color,
            'seats' => $ride->seats,
            'luggage_size' => $ride->luggage_size,
            'luggage_amount' => $ride->luggage_amount,
            'hour' => $hour,
            'date' => $date,
            'ladiesOnly' => $ride->ladies_only,
            'price_principal' => $ride->price,
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }

    public function acceptPassenger(Request $request)
    {

        $ride = Rides::find($request->get('ride_id'));

        //Make sure there is enough seats
        if ($ride->seats_available <= 0) {

            $response = array(
                'status' => '1',
                'message' => "Sorry, you can't accept more passengers than seats. Line 796",
            );

            header('Content-Type: application/json');
            return response()->json($response);
        }

        $user = User::find($request->get('user_id'));

        $user_ride = UserRides::where('user_id', '=', $user->id)->where('rides_id', '=', $ride->id)->get()->first();
        $seats = $user_ride->seats;

        if ($user_ride->isStop == true) {
            //Check that the ride book is a stop over
            $ride_stop = RidesStop::find($ride->ride_stop[0]->id);

            //Check if the ride is From Point A to Stop Over
            if ($user_ride->city_from == $ride->location_from) {
                $ride_stop->seats_from = $ride_stop->seats_from - $seats;
                if ($ride_stop->seats_from < 0) {
                    $response = array(
                        'status' => '1',
                        'message' => "Sorry, you can't accept more passengers than seats.",
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }

            } else {
                //The ride is from Stop Over to Point B
                $ride_stop->seats_to = $ride_stop->seats_to - $seats;
                if ($ride_stop->seats_to < 0) {
                    $response = array(
                        'status' => '1',
                        'message' => "Sorry, you can't accept more passengers than seats.",
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }

            $ride_stop->update();
        } else {
            //This is a normal ride
            $ride->seats_available = $ride->seats_available - $user_ride->seats;
            if ($user_ride->isStop == true) {
                $ride_stop = RidesStop::find($ride->ride_stop[0]->id);
                $ride_stop->seats_to = $ride_stop->seats_to - $seats;
                $ride_stop->seats_from = $ride_stop->seats_from - $seats;
                if ($ride_stop->seats_to < 0 || $ride_stop->seats_from < 0) {
                    $response = array(
                        'status' => '1',
                        'message' => "Sorry, you can't accept more passengers than seats.",
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
                $ride_stop->update();
            }
            if ($ride->seats_available < 0) {
                $response = array(
                    'status' => '1',
                    'message' => "Sorry, you can't accept more passengers than seats.",
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
            $ride->update();
        }

        $user_ride->status = 'Approved';

        $user_ride->update();
        $ride->update();
        $message = "Congratulations, you are going to " . $user_ride->ride->location_to . "! The driver has approved your request to join the ride. Please go to your booking page and pay ASAP.";

        $thread = Thread::create([
            'subject' => 'Approved for ride to ' . $ride->location_to,
            'rides_id' => $ride->id,
            'isNotification' => 'Yes'
        ]);
        $thread->isNotification = 'Yes';
        $thread->update();
       
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $user->id,
            'last_read' => new Carbon,
        ]);
        //App Notification to sender
        OneSignal::sendNotificationUsingTags(
            $message,
            array(
                ["field" => "tag","key"=>"userid", "relation" => "=", "value" => $user->id],
            ),
            $url = null,
            $data = null,
            $buttons = null,
            $schedule = null
        );
        $driver_name = $user->first_name;
        $email = $user->email;

        $info = array("user" => $user->first_name, "rideTo" => $ride->location_to, "driver" => $driver_name, "ride_id" => $ride->id);

        Mail::send('email.acceptPassenger', $info, function ($message) use ($email) {
            $message->to($email)->subject('Ride Request Update');
        });

        $response = array(
            'status' => '2',
            'message' => 'You have accepted the passenger',
            'user_ride' => $user_ride,
            'passenger' => $user_ride->user,
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function denyPassenger(Request $request)
    {

        $ride = Rides::find($request->get('ride_id'));

        $user = User::find($request->get('user_id'));

        $user_ride = UserRides::where('user_id', '=', $user->id)->where('rides_id', '=', $ride->id)->get()->first();

        $user_ride->status = 'Denied';

        $user_ride->update();

        $ride->seats_available = $ride->seats_available + $user_ride->seats;

        //Send Message to Driver
        $message = "Sorry, the driver has denied your request to join the trip. We encourge you to look for another one!";

        $thread = Thread::create([
            'subject' => 'New passenger request for ' . $ride->location_to,
            'rides_id' => $ride->id,
            'isNotification' => 'Yes',
        ]);
        $thread->isNotification = 'Yes';
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $user->id,
            'last_read' => new Carbon,
        ]);
         //App Notification to sender
         OneSignal::sendNotificationUsingTags(
            $message,
            array(
                ["field" => "tag","key"=>"userid", "relation" => "=", "value" => $user->id],
            ),
            $url = null,
            $data = null,
            $buttons = null,
            $schedule = null
        );
        $driver_name = $user->first_name;
        $email = $user->email;
        $info = array("user" => $user->first_name, "rideTo" => $ride->location_to);

        Mail::send('email.denyPassenger', $info, function ($message) use ($email) {
            $message->to($email)->subject('Ride Request Update');
        });

        $response = array(
            'status' => '1',
            'message' => 'You have denied the passenger',
            'user_ride' => $user_ride,
            'passenger' => $user,
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function sendVerification()
    {

        $user = Auth::user();

        $email = new EmailVerification($user);

        Mail::to($user->email)->send($email);

        $response = array(
            'status' => 200,
            'message' => 'Successfully sent email');

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function deleteAccount()
    {

        $user = Auth::user();

        $id = Auth::id();
        $rides = Rides::where('user_id', '=', $id)->get();
        $reason = 'Delete Account';

        $booked = UserRides::where('user_id', '=', $id)->get();

        foreach ($booked as $book) {
            if ($book->booker_payment_paid == 'Yes') {
                $response = array(
                    'status' => 400,
                    'message' => 'Error deleting account,
                                  you have paid booked rides,
                                  please delete them and try again.',
                );
                header('Content-Type: application/json');
                return response()->json($response);
            }

            $book->delete();
        }

        foreach ($rides as $ride) {
            $this->destroySimpleRideDriver($ride->id, $id, $reason);
        }

        if(count($user->driver) > 0){
            $driver = Driver::where('user_id', '=', $user->id)->first();
            $driver->delete();
        }

        $preferences = UserPreferences::where('user_id', '=', $id)->get();
        foreach ($preferences as $pref) {
            $pref->delete();
        }

        $threads = Thread::forUser(Auth::id())->get();
        foreach ($threads as $t) {
            //Todo Save them in another place
            foreach ($t->messages as $message) {
                $message->delete();
            }
            $t->delete();
        }
        
        $dir = public_path('/images/user_information/' . $user->id);
        $this->rrmdir($dir);

        $check = DeleteUsers::where('user_id', '=', $id)->get()->first();

        if (count($check) > 0) {
            $response = array(
                'status' => 400,
                'message' => 'We are having problems deleting your account, please contact us.',
            );
            header('Content-Type: application/json');
            return response()->json($response);
        } else {
            $DeleteUser = new DeleteUsers;
            $DeleteUser->name = $user->first_name;
            $DeleteUser->email = $user->email;
            $DeleteUser->user_id = $user->id;
            $DeleteUser->delete_date = Carbon::now();

            $DeleteUser->save();
            DB::table('model_has_roles')->where('model_id', $id)->delete();
            $user->delete();

            $response = array(
                'status' => 200,
                'message' => 'Successfully deleted account',
            );
            header('Content-Type: application/json');
            return response()->json($response);

        }
    }

    //Delete files and folders 
    public function rrmdir($dir) {
       if (is_dir($dir)) {
         $objects = scandir($dir);
         foreach ($objects as $object) {
           if ($object != "." && $object != "..") {
              if (filetype($dir."/".$object) == "dir") 
                $this->rrmdir($dir."/".$object); 
              else 
                unlink($dir."/".$object);
           }
         }
         reset($objects);
         rmdir($dir);
       }
    }

}
