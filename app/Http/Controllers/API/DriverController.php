<?php

namespace App\Http\Controllers\API;

use App\Driver;
use App\Http\Controllers\Controller;
use App\Http\Traits\MailAdmins;
use App\Http\Traits\MailHelper;
use App\Http\Traits\UploadImage;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class DriverController extends Controller
{
    use MailHelper, UploadImage, MailAdmins;

    private $adminEmails;

    public function __construct(Request $request)
    {
        $this->adminEmails = explode(',', env('ADMIN_EMAILS'));
    }

    public function gettingDriverInfo()
    {

        $user = Auth::user();

        if (count($user->driver) > 0) {
            $driver = $user->driver[0];
        } else {
            $driver = null;
        }

        $response = array(
            'user' => $user,
            'driver' => $driver);

        header('Content-Type: application/json');
        return response()->json($response);


    }

    public function saveCarImage(Request $request)
    {
        //This Image saves both the Driver License and Car Picture
        ini_set('memory_limit', '-1');
        header('Content-Type: application/json');
        $car = $request->input('carImage');
        $license = $request->input('license');

        $user = Auth::user();
        $roles = $user->getRoleNames();
        $inner_path = public_path('/images/user_information/' . $user->id . '/car_details');
        $car_name = 'carImage.jpg';
        $lic_name = 'license.jpg';

        if (!File::exists($inner_path)) {
            File::makeDirectory($inner_path, $mode = 0777, true, true);
        }

        if ($roles[0] == 'Driver') {
            //Normal change of car
            try {
                $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                if (!empty($car)) {
                    if (file_exists(public_path() . $driver->path_image)) {
                        unlink(public_path() . $driver->path_image);
                    }
                    $car = str_replace('data:image/jpeg;base64,', '', $car);
                    $car = str_replace(' ', '+', $car);
                    \File::put($inner_path . '/' . $car_name, base64_decode($car));
                    $driver->path_image = '/images/user_information/' . $user->id . '/car_details/' . $car_name;
                }

                if (!empty($license)) {
                    if (file_exists(public_path() . $driver->driver_license)) {
                        unlink(public_path() . $driver->driver_license);
                    }
                    $license = str_replace('data:image/jpeg;base64,', '', $license);
                    $license = str_replace(' ', '+', $license);
                    \File::put($inner_path . '/' . $lic_name, base64_decode($license));
                    $driver->driver_license = '/images/user_information/' . $user->id . '/car_details/' . $lic_name;
                }
                $driver->status = 1;
                $driver->update();
            } catch (\Exception $e) {
                $response = array(
                    'status' => '2',
                    'message' => 'Error Has Occured',
                    'Error' => $e
                );
                header('Content-Type: application/json');
                return response()->json($response);
            }
        } else {
            //Driver Applied but was denied
            if (count($user->driver) > 0) {
                try {
                    $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                    if (!empty($car)) {
                        if (file_exists(public_path() . $driver->path_image)) {
                            unlink(public_path() . $driver->path_image);
                        }
                        $car = str_replace('data:image/jpeg;base64,', '', $car);
                        $car = str_replace(' ', '+', $car);
                        \File::put($inner_path . '/' . $car_name, base64_decode($car));
                        $driver->path_image = '/images/user_information/' . $user->id . '/car_details/' . $car_name;
                    }

                    if (!empty($license)) {
                        if (file_exists(public_path() . $driver->driver_license)) {
                            unlink(public_path() . $driver->driver_license);
                        }
                        $license = str_replace('data:image/jpeg;base64,', '', $license);
                        $license = str_replace(' ', '+', $license);
                        \File::put($inner_path . '/' . $lic_name, base64_decode($license));
                        $driver->driver_license = '/images/user_information/' . $user->id . '/car_details/' . $lic_name;
                    }
                    $driver->status = 1;
                    $driver->update();
                } catch (\Exception $e) {
                    $response = array(
                        'status' => '2',
                        'message' => 'Error Has Occured',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            } //New Driver
            else {
                try {
                    $driver = new Driver;
                    $driver->user_id = $user->id;
                    $driver->status = 1;

                    $car = str_replace('data:image/jpeg;base64,', '', $car);
                    $car = str_replace(' ', '+', $car);
                    \File::put($inner_path . '/' . $car_name, base64_decode($car));

                    $license = str_replace('data:image/jpeg;base64,', '', $license);
                    $license = str_replace(' ', '+', $license);
                    \File::put($inner_path . '/' . $lic_name, base64_decode($license));

                    $driver->path_image = '/images/user_information/' . $user->id . '/car_details/' . $car_name;
                    $driver->driver_license = '/images/user_information/' . $user->id . '/car_details/' . $lic_name;

                    $driver->save();
                } catch (\Exception $e) {
                    $response = array(
                        'status' => '2',
                        'message' => 'Error Has Occured',
                        'Error' => $e
                    );
                    Log::info($e);
                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
        }
        $data = '';
        if (!empty($car)) {
            $data = $driver->path_image;
        }
        if (!empty($license)) {
            $data = $driver->driver_license;
        }
        try {
            $reason = "New driver request";
            $reply = "User: " . $user->first_name . " has requested to be a driver and/or change car";
            $this->mailCurrentAdminds($this->adminEmails, $reply, $reason);
        } catch (\Exception $e) {
            Log::info($e);
        }

        $response = array(
            'status' => '1',
            'data' => $data,
            'message' => 'Successfully uploaded car photo',
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }

    public function saveInsurance(Request $request)
    {
        ini_set('memory_limit', '-1');
        header('Content-Type: application/json');
        $insurance = $request->input('insurance');
        $user = Auth::user();
        $roles = $user->getRoleNames();
        $inner_path = public_path('/images/user_information/' . $user->id . '/car_details');
        $ins_name = 'insurance.jpg';
        if (!File::exists($inner_path)) {
            File::makeDirectory($inner_path, $mode = 0777, true, true);
        }

        if ($roles[0] == 'Driver') {
            try {
                $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                /*if(file_exists(public_path().$driver->insurance_path) ){
                    unlink(public_path().$driver->insurance_path);
                }*/
                if (!empty($insurance)) {
                    $insurance = str_replace('data:image/jpeg;base64,', '', $insurance);
                    $insurance = str_replace(' ', '+', $insurance);
                    \File::put($inner_path . '/' . $ins_name, base64_decode($insurance));
                    $driver->insurance_path = '/images/user_information/' . $user->id . '/car_details/' . $ins_name;
                }
                $driver->update();
            } catch (\Exception $e) {
                Log::info($e);
                $response = array(
                    'status' => '2',
                    'message' => 'Error Has Occured',
                    'Error' => $e
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
        } else {
            //Driver Applied but was denied
            if (count($user->driver) > 0) {
                try {
                    $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                    /*if(file_exists(public_path().$driver->insurance_path) ){
                        unlink(public_path().$driver->insurance_path);
                    }*/
                    if (!empty($insurance)) {
                        $insurance = str_replace('data:image/jpeg;base64,', '', $insurance);
                        $insurance = str_replace(' ', '+', $insurance);
                        \File::put($inner_path . '/' . $ins_name, base64_decode($insurance));
                        $driver->insurance_path = '/images/user_information/' . $user->id . '/car_details/' . $ins_name;
                    }
                    Log::info('We store a Insurance');
                    $driver->update();
                } catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => '2',
                        'message' => 'Error Has Occured',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            } //New Driver
            else {
                try {
                    $driver = new Driver;
                    $driver->user_id = $user->id;
                    $driver->status = 1;

                    $insurance = str_replace('data:image/jpeg;base64,', '', $insurance);
                    $insurance = str_replace(' ', '+', $insurance);
                    \File::put($inner_path . '/' . $ins_name, base64_decode($insurance));
                    $driver->insurance_path = '/images/user_information/' . $user->id . '/car_details/' . $ins_name;

                    $driver->status_message = "The user has requested to change cars, please check the info and approve or deny request.";
                    $driver->save();
                } catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => '2',
                        'message' => 'Error Has Occured',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
        }
        Log::info('Saved Insurance Image  ' . $user->first_name);
        $response = array(
            'status' => '1',
            'data' => $driver->insurance_path,
            'message' => 'Successful insurance upload',
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }

    public function saveRegistration(Request $request)
    {
        ini_set('memory_limit', '-1');
        header('Content-Type: application/json');
        // Log::info('$target_path  '.$request);

        $registration = $request->input('registration');
        $user = Auth::user();
        $roles = $user->getRoleNames();
        $inner_path = public_path('/images/user_information/' . $user->id . '/car_details');
        $resg = 'registration.jpg';

        if (!File::exists($inner_path)) {
            File::makeDirectory($inner_path, $mode = 0777, true, true);
        }

        if ($roles[0] == 'Driver') {
            //Normal change of car
            try {
                $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                /*if(file_exists(public_path().$driver->registration_path) ){
                    unlink(public_path().$driver->registration_path);
                }*/
                if (!empty($registration)) {
                    $registration = str_replace('data:image/jpeg;base64,', '', $registration);
                    $registration = str_replace(' ', '+', $registration);
                    \File::put($inner_path . '/' . $resg, base64_decode($registration));
                    $driver->registration_path = '/images/user_information/' . $user->id . '/car_details/' . $resg;
                    $driver->update();
                }
            } catch (\Exception $e) {
                Log::info($e);
                $response = array(
                    'status' => '2',
                    'message' => 'Error Has Occured',
                    'Error' => $e
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
        } else {

            //Driver Applied but was denied
            if (count($user->driver) > 0) {
                try {
                    $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                    /*if(file_exists(public_path().$driver->registration_path) ){
                         unlink(public_path().$driver->registration_path);
                     }*/
                    $registration = str_replace('data:image/jpeg;base64,', '', $registration);
                    $registration = str_replace(' ', '+', $registration);
                    \File::put($inner_path . '/' . $resg, base64_decode($registration));
                    $driver->registration_path = '/images/user_information/' . $user->id . '/car_details/' . $resg;

                    $driver->update();
                    Log::info('We store a registration');
                } catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => '2',
                        'message' => 'Error Has Occured',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            } //New Driver
            else {
                try {
                    $driver = new Driver;
                    $driver->user_id = $user->id;
                    $driver->status = 1;
                    $registration = str_replace('data:image/jpeg;base64,', '', $registration);
                    $registration = str_replace(' ', '+', $registration);
                    \File::put($inner_path . '/' . $resg, base64_decode($registration));
                    $driver->registration_path = '/images/user_information/' . $user->id . '/car_details/' . $resg;
                    $driver->status_message = "The user has requested to change cars, please check the info and approve or deny request.";
                    $driver->save();
                } catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => '2',
                        'message' => 'Error Has Occured',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
        }



        $response = array(
            'status' => '1',
            'data' => $driver->registration_path,
            'message' => 'Successful registration upload',
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }

    public function getDriverInfo()
    {

        $user = Auth::user();

        if (count($user->driver) > 0) {
            $driver = $user->driver[0];
            $driver->active = true;
        } else {
            $driver = $user;
            $driver = null;
        }
        header('Content-Type: application/json');
        return response()->json($driver);

    }
}
