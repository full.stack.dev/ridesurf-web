<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use App\Jobs\SendVerificationEmail;
use Illuminate\Support\Facades\Mail;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use App\Http\Traits\MailHelper; 
use App\Http\Traits\Notifications; 
use App\Http\Traits\MessageTrait;

use App\User;
use App\Rides;
use App\UserRides;
use Carbon\Carbon;
use DB;

class MessagesController extends Controller
{
    use Notifications, MessageTrait;
    
    public function getMessCount(Request $request){

        $count = $this->messageCount();

        $response = array(
            'status' => '1',
            'count' => $count,
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function getAllMessages(Request $request){

        $threads = Thread::forUser(Auth::id())->latest('updated_at')
                  ->where('isNotification','=', 'No')->get();

                 
        foreach($threads as $thread){
            $user_id = $thread->participants()
                       ->where('user_id','!=', Auth::id() )->first()->user_id;

            $otherParticipant = User::find($user_id);
            $thread->creator = $otherParticipant;
            $thread->userImage = $otherParticipant->avatar;

            if($thread->creator->avatar == null){
                $thread->userImage = '/images/ridesurf.png';
            }
            $thread->messages = Message::where('thread_id', '=', $thread->id)
                                ->orderBy('created_at', 'desc')->first();

            $thread->unReads = Message::where('thread_id', '=', $thread->id)
                               ->where('user_id', '!=', Auth::id())
                               ->where('last_seen', '=', null )->count();
        }


        header('Content-Type: application/json');
        return response()->json($threads);

    }

    public function getSingleMessage($id){

        try {
            $thread = Thread::findOrFail($id);
            $user = Auth::user();
            $thread->markAsRead($user->id);
            $thread->messages = $thread->messages;

            $thread->reciver = $thread->participants()
                                ->where('user_id','!=', $user->id)->get()->first()->user_id;
            $thread->reciver_name = User::where('id', '=', $thread->reciver)->get()->first()->first_name;
            $thread->reciver_img = User::where('id', '=', $thread->reciver)->get()->first()->avatar;

            foreach($thread->messages as $message){
                if( $message->user_id != $user->id){
                    $message->last_seen = Carbon::now();
                    $message->update();
                }
                $message->user = $message->user;
            }

        } catch (ModelNotFoundException $e) {
            $thread = $e;
        }


        header('Content-Type: application/json');
        return response()->json($thread);
    }

    public function sendMessage(Request $request, $id, $reciver_id){

        try {

            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            header('Content-Type: application/json');
            return response()->json($e);
        }

        if(Auth::id() == $reciver_id){
            $message = array(
                'message'=>'There was a problem storing; receivier same as sender.');
            header('Content-Type: application/json');
            return response()->json($message);
        }

        $thread->activateAllParticipants();

        $body = $request->get('body');

        // Message
        $mes = Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $body,
        ]);

        $mes->last_seen = null;
        $mes->update();

        /* TODO 
        *  Make a console command a way to send a message if last seen is been more than an hour
        $sender = Auth::user();
        $reciver = User::find($reciver_id);
        $email = $reciver->email;
        $subject_to = "New Message from " . $sender->first_name . ". ";

        $info_user = array(
            'reciver' => $reciver,
            'reply' => $body,
            'sender' => $sender,
            'thread_id' => $thread->id,
        );

        Mail::send('email.sendMessage', $info_user, function($message) use ($email, $subject_to) {
            $message->to($email)->subject($subject_to);
        }); */

        $thread->messages = $thread->messages;
            foreach($thread->messages as $message){
                $message->user = $message->user->avatar;
        }

        header('Content-Type: application/json');
        return response()->json($thread);
    }

    public function deleteSingleMessage($id){

        $data = Thread::find($id);

        foreach($data->messages as $message){
            $message->delete();
        }
        
        $data->delete();

        $response = array(
            'status' => 200,
            'message' => 'Successfully deleted message'
        );
        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function contactDriver($user_id){

        $thread = Thread::forUser($user_id);

        header('Content-Type: application/json');
        return response()->json($thread);

    }

    /* Will pull the date from single message but
    *  Using the user id from the driver and  
    *  using the user id from the passenger
    */
    public function messageDriver($id, $ride_id){

        $thread_id = Thread::where('rides_id','=', $ride_id)
                        ->where('deleted_at', '=', null)->get()->first();
        if(empty($thread_id)){
            $thread_id = 0;
            header('Content-Type: application/json');
            return response()->json($thread_id);
        }
        try {
            
            $thread = Thread::find($thread_id->id);
            $user = Auth::user();
            $thread->markAsRead($user->id);
            $thread->messages = $thread->messages;

            $thread->reciver = $thread->participants()
                                ->where('user_id','!=', $user->id)->get()->first()->user_id;
            $reciver = User::where('id', '=', $thread->reciver)->get()->first();                     
            $thread->reciver_name = $reciver->first_name;
            $thread->reciver_img = $reciver->avatar;                    

            foreach($thread->messages as $message){
                if($message->last_seen ==  null & $message->user_id != $user->id){
                    $message->last_seen = Carbon::now();
                    $message->update();
                }
                $message->user = $message->user;
            }

        } catch (ModelNotFoundException $e) {
            $thread = $e;
        }

        header('Content-Type: application/json');
        return response()->json($thread);
    }

    /* This function will provide the current user the list
    *  of people that they can contact
    */
    public function getUserToContact(Request $request){

        $user = Auth::user();

        $user_ride = UserRides::where('user_id', '=', $user->id)
                    ->whereHas('ride', function ($q){
                        $q->where('dateFrom','>=', Carbon::now()->format('Y-m-d'));
                        $q->groupBy('user_id');
                    })
                    ->where('status','=', 'Approved')
                    // ->where('booker_payment_paid', '=', 'Yes')
                    ->get();

        //If the user is a passenger and had booked a ride           
        foreach($user_ride as $uRide){
                $uRide->ride_info = $uRide->ride->location_to;
                $uRide->user_info = $uRide->ride->user->first_name;
                $uRide->otherUser_id = $uRide->ride->user->id;
                $uRide->ride_id = $uRide->ride->id;
                $uRide->date = date('j M Y', strtotime($uRide->ride->dateFrom));
        }

        $rides = Rides::where('user_id','=',$user->id)
                        ->where('dateFrom','>=', Carbon::now()->format('Y-m-d'))
                        ->get();

        //if the user is a driver and has people booked
        foreach($rides as $ride){
            if(count($ride->user_ride) > 0){
                foreach($ride->user_ride as $uRide){
      
                    $uRide->ride_info= $ride->location_to;
                    $uRide->user_info = $uRide->user->first_name;
                    $uRide->otherUser_id = $uRide->user->id;
                    $uRide->date = date('j M Y', strtotime($uRide->ride->dateFrom));
                    $user_ride->add($uRide);
                }
                
            }
        }
        $user_ride->merge($rides);

        if(empty($user_ride)){
            $user_ride = 0;
        }

        header('Content-Type: application/json');
        return response()->json($user_ride);

    }

    public function getSingleUserContact(Request $request){

        $rider = $request->get('rider');

        if($rider){
            $user = Auth::user();
            $user_ride = UserRides::where('user_id', '=', $user->id)
                    ->where('rides_id', '=', $request->get('rides_id'))
                    ->whereHas('ride', function ($q){
                        $q->where('dateFrom','>=', Carbon::now()->format('Y-m-d'));
                        $q->groupBy('user_id');
                    })
                    ->where('status','=', 'Approved')
                    // ->where('booker_payment_paid', '=', 'Yes')
                    ->first();
            $user_ride->ride_info = $user_ride->ride->location_to;
            $user_ride->user_info = $user_ride->ride->user->first_name;
            $user_ride->otherUser_id = $user_ride->ride->user->id;
            $user_ride->ride_id = $user_ride->ride->id;     
            $user_ride->date = date('j M Y', strtotime($user_ride->ride->dateFrom));

        }else{

            $user_ride = UserRides::where('user_id', '=', $request->get('passenger_id'))
                    ->where('rides_id', '=', $request->get('rides_id'))
                    ->whereHas('ride', function ($q){
                        $q->where('dateFrom','>=', Carbon::now()->format('Y-m-d'));
                        $q->groupBy('user_id');
                    })
                    ->where('status','=', 'Approved')
                    // ->where('booker_payment_paid', '=', 'Yes')
                    ->first();
            $user_ride->ride_info = $user_ride->ride->location_to;
            $user_ride->user_info = $user_ride->ride->user->first_name;
            $user_ride->otherUser_id = $user_ride->ride->user->id;
            $user_ride->ride_id = $user_ride->ride->id;
            $user_ride->date = date('j M Y', strtotime($user_ride->ride->dateFrom));
        }

        return response()->json($user_ride);

    }

    public function storeMessageNew(Request $request){

        $info = $request->get('ride');


        $pieces = explode(",", $info);
        $ride_id = $pieces[0];
        $otherUser_id = $pieces[1];
        $body = $request->get('body');
                
        //Trait from MessageTrait
        $this->StoreNewMessage( $ride_id, $otherUser_id, $body, $request->get('before_pay'));            

        $response = array(
            'message' => 'Successfully sent message',
            'status' => 1);

        header('Content-Type: application/json');
        return response()->json($response);
    }
}