<?php

namespace App\Http\Controllers\API;

use App\TripPurpose;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\LocationHelper;
use App\Http\Traits\MailHelper; 
use App\Http\Traits\Notifications; 
use App\Http\Traits\MailAdmins; 
use App\Http\Traits\DestroyRides; 
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth; 
use App\Jobs\SendVerificationEmail;
use Illuminate\Support\Facades\Mail;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Input;
use Illuminate\Mail\Mailable;
use App\User;
use App\Vehicle;
use App\UserRides;
use App\Driver;
use App\Reviews;
use App\Rides;
use App\RidesStop;
use App\Report;
use App\SearchLog;
use App\RideRequest;
use Carbon\Carbon;
use Validator;
use Storage, DB, Response, File;
use Session,Redirect, ArrayObject;
use Illuminate\Support\Facades\Log;
class RidesController extends Controller
{
    use MailHelper, Notifications, LocationHelper, MailAdmins, DestroyRides;

    private $url;
    private $adminEmails;
    private $admin;


    public function __construct()
    {
        $this->url = "http://127.0.0.1:8000";

        $this->adminEmails = explode(',', env('ADMIN_EMAILS'));

        $this->admin = User::where('first_name', '=', 'admin')->get()->first();

    }

    public function getCurrentRides(Request $request, $page){
        
        $user = Auth::user();
        $now = Carbon::now();
        $today = Carbon::today()->format('Y-m-d');
        $time_today = strtotime(Carbon::now());

        if(count($user->ride)){
            $rides = Rides::where('user_id', '=', $user->id )
                    ->orderBy('dateFrom', 'asc')
                    ->where(function ($query) use ($now){
                        $query->whereRaw('CONCAT(dateFrom," ",startHour) >= NOW()');
                        // $query->where('dateFrom', '>=', $now->format('Y-m-d'));
                        // $query->where('startHour', '>=', $now->format('H:i:s'));
                    })
                    ->paginate(5,['*'], 'page', $page);

            foreach($rides as $ride){

                if( count($ride->ride_stop ) > 0){
                    $ride->stop = $ride->ride_stop;
                    
                }
                if(!empty($ride->airport_details)){
                    $ride->airport = $ride->airport_details;
                }
                if(count($ride->user_ride) > 0){
                    $ride->passengerCount = 0;
                    foreach($ride->user_ride as $uRide){
                            $ride->passengerCount = $ride->passengerCount + 1;
                            $uRide->user = $uRide->user;
                    }
                }else{
                     $ride->passengerCount = 0;
                }
                $ride->date = date('j M Y', strtotime($ride->dateFrom));
                $ride->start = date("g:i a", strtotime($ride->startHour));
                $time =  strtotime($ride->startHour);
                $ride->end = date("h:i a",($time + $ride->duration));
            }        
            // $rides= $rides->setCollection($rides->getCollection()->filter(

            //     function ($item) use ($rides,$time_today) {
            //         $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
            //         if($time_ride >= $time_today){
            //             return $item;
            //         }

            //     }
            // ));
            
        }else{
            $rides = null;
        }

        header('Content-Type: application/json');
        return response()->json($rides);
 
    }

    public function getPastRides(Request $request, $page){
        $user = Auth::user();

        $today = Carbon::today()->format('Y-m-d');
        $time_today = strtotime(Carbon::now());
        $now = Carbon::now();

        if(count($user->ride)){
            $rides =  Rides::where('user_id', '=', $user->id )
                        ->orderBy('dateFrom', 'desc')
                        ->where(function ($query) use ($now){
                            $query->whereRaw('CONCAT(dateFrom," ",startHour) <= NOW()');
                            // $query->where('dateFrom', '>=', $now->format('Y-m-d'));
                            // $query->where('startHour', '>=', $now->format('H:i:s'));
                        })
                        ->paginate(5,['*'], 'page', $page);

            foreach($rides as $ride){
                if(count($ride->user_ride) > 0){
                    $ride->passengerCount = 0;
                    foreach($ride->user_ride as $uRide){
                            $ride->passengerCount = $ride->passengerCount + 1;
                            $uRide->user = $uRide->user;
                    }
                }else{
                     $ride->passengerCount = 0;
                }
                $ride->date = date('j M Y', strtotime($ride->dateFrom));
                $ride->start = date("g:i a", strtotime($ride->startHour));
                $time =  strtotime($ride->startHour);
                $ride->end = date("h:i a",($time + $ride->duration));
            }        
            
        }else{
            $rides = null;
        }

        header('Content-Type: application/json');
        return response()->json($rides);
    }

    public function getRideData(Request $request, $id){

        $ride = Rides::find($id);

        if(count($ride->user_ride) > 0){
            foreach($ride->user_ride as $indexKey =>$uRide ){
                $passengers[$indexKey] = $uRide->user;
                $passengers[$indexKey]->city_to = $uRide->city_to;
            }
            $ride->passengers = $passengers;
        }else{
            $ride->passengers = null;
        }


        header('Content-Type: application/json');
        return response()->json($ride);
    }

    public function updateRide(Request $request, $id){
        $ride = Rides::find($id);

        $data = $request->get('data');
        $ride->update($data);

        if( !empty($data['stops']) ){
            $stops = $data['stops'];
            foreach($stops as $stop){
                $ride_stop = RidesStop::where('id','=', $stop['id'])->get()->first();
                $ride_stop->price_from = $stop['price_from'];
                $ride_stop->price_to = $stop['price_to'];
                $ride_stop->update();
            }
        }

        if(!empty($data['aiport'])){
            $airport_details = $data['airport'];
            $airport = AirportDetails::find($airport_details['id']);
            $airport->fill($airport_details);
            $airport->update();
        }

        $response = array(
            'status' => 200,
            'message' => 'Successfully edited ride'
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }

    public function requestJoin(Request $request){

        $description = $request->get('description');
        $user = Auth::user();

        $ride_id = $request->get('rideDetail');
        $ride = Rides::find($ride_id);
        $stop = $request->get('isStop');

        $seats = $request->input('seats');
        
        $user_ride = new UserRides;

        $user_ride->user_id = $user->id;
        $user_ride->rides_id = $ride->id;
        $user_ride->seats = $seats[0];
        $user_ride->description = $description;
        $user_ride->status = "Requested";
        $user_ride->ride_cost = $ride->price;
        $user_ride->city_from = $ride->city_from;
        $user_ride->city_to = $ride->city_to;
        $user_ride->isStop = $stop;

        $user_ride->save();
    
        $this->mailRequest_ToDriver($user, $ride);
        
        $response = array(
                'action' => 1,
                'message' => "Successfully requested to become a driver",
                'ride' => $ride);

        header('Content-Type: application/json');
        return response()->json($response);
    }
    
    public function createRide(Request $request){
        
        $response = $request->get('data');

        //Parse Locations
        $arrayFrom = explode(',', $response['location_from']);
        $arrayTo = explode(',', $response['location_to']);
        
        $waypoints = json_decode($response['waypoints']);

        if($response['description'] == null){
            return response()->json(['message'=>'Please add a description'], 401);
        }
        if($response['luggage_size'] == null){
            $response['luggage_size'] = "Small";
        }
        if($response['ladies_only'] == null){
            $response['ladies_only'] = 'No';
        }

        if($response['seats'] == null || $response['seats'] == 0){
            $response['seats'] = 1;
        }

        $city_from = $this->parseLocation($arrayFrom, $response['location_from'] );

        $city_name = $this->getCity($city_from);
        $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);


        if($geocodeObject['status'] = "OK"){
            $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
            $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
            $city_from = $this->parseLocation($temp,  $response['location_from']);
        }

        $city_to = $this->parseLocation($arrayTo, $response['location_to']);
        $city_name = $this->getCity($city_to);
        $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);


        if($geocodeObject['status'] = "OK"){
            $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
            $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
            $city_to = $this->parseLocation($temp, $response['location_to']);
        }

        $state = $this->getState($city_from);
        $state_to = $this->getState($city_to);

        if(strlen($state) > 2){
            $temp_from = $this->parseState($state);

            $city = $this->getCity($city_from);
            $city_from = $city ."," . $temp_from;
        }
        if( strlen($state_to) > 2){
            $temp_to = $this->parseState($state_to);
            $city = $this->getCity($city_to);
            $city_to = $city ."," . $temp_to;
        }

        //create the instance of rides
        $ride = new Rides;
        $ride->fill($response); //Fill data from the Request
        $ride->city_from = $city_from;
        $ride->city_to = $city_to;
        $ride->user_id = Auth::id();
        $ride->completed = 'Ongoing';
        $ride->seats_available = $response['seats'];
        $ride->save();

        if($ride->seats_available == null){
            $ride->seats_available = 1;
            $ride->save();
        }

        if(count($waypoints)>0){
            foreach($waypoints as $waypoint){
                $waypoint_array = explode(',',$waypoint->city_address);
                $ridestop = new RidesStop;
                $ridestop->price_from = $waypoint->price_from;
                $ridestop->price_to = $waypoint->price_to;
                $ridestop->rides_id = $ride->id;
                $ridestop->user_id = Auth::id();
                $ridestop->address = $waypoint->city_address;
                $ridestop->seats_from = $ride->seats;
                $ridestop->seats_to = $ride->seats;
                $ridestop->city_name = $this->parseLocation($waypoint_array, $waypoint);
                $ridestop->save();
            }
        }


        $response = array(
            'status' => 200,
            'message' => 'Successfully created a new ride'
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function createRideRountrip(Request $request){

        $response = $request->get('data');

        //Parse Locations
        $arrayFrom = explode(',', $response['location_from']);
        $arrayTo = explode(',', $response['location_to']);
        $waypoints = json_decode($response['waypoints']);

        if($response['description'] == null){
            return response()->json(['message'=>'Please add a description'], 401);
        }
        if($response['luggage_size'] == null){
            $response['luggage_size'] = "Small";
        }
        
        if($response['ladies_only'] == null){
            $response['ladies_only'] = 'No';
        }

        if($response['seats'] == null || $response['seats'] == 0){
            $response['seats'] = 1;
        }

        $city_from = $this->parseLocation($arrayFrom, $response['location_from'] );
        $city_to = $this->parseLocation($arrayTo, $response['location_to']);
        
        $city_name = $this->getCity($city_from);
        $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);


        if($geocodeObject['status'] = "OK"){
            $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
            $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
            $city_from = $this->parseLocation($temp,  $response['location_from']);
        }

        $city_to = $this->parseLocation($arrayTo, $response['location_to']);
        $city_name = $this->getCity($city_to);
        $geocodeObject = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$city_name.',US+&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M'), true);


        if($geocodeObject['status'] = "OK"){
            $city_name = $geocodeObject['results'][0]['address_components'][0]['long_name'];
            $temp = explode(',',$geocodeObject['results'][0]['formatted_address']);
            $city_to = $this->parseLocation($temp, $response['location_to']);
        }

        //create the instance of ride Start
        $ride = new Rides;
        $ride->fill($response); //Fill data from the Request
        $ride->dateFrom = $response['dateBeginning'];
        $ride->startHour = $response['timeStart'];
        $ride->city_from = $city_from;
        $ride->city_to = $city_to;
        $ride->user_id = Auth::id();
        $ride->completed = 'Ongoing';
        $ride->seats_available = $response['seats'];
        $ride->save();

        if($ride->seats_available == null){
            $ride->seats_available = 1;
        }
        if(count($waypoints)>0){
            foreach($waypoints as $waypoint){
                $waypoint_array = explode(',',$waypoint->city_address);
                $ridestop = new RidesStop;
                $ridestop->price_from = $waypoint->price_from;
                $ridestop->price_to = $waypoint->price_to;
                $ridestop->rides_id = $ride->id;
                $ridestop->user_id = Auth::id();
                $ridestop->address = $waypoint->city_address;
                $ridestop->seats_from = $ride->seats;
                $ridestop->seats_to = $ride->seats;
                $ridestop->city_name = $this->parseLocation($waypoint_array, $waypoint);
                $ridestop->save();
                
            }
        }
        //create the instance of ride Return
        $return = new Rides;
        $return->fill($response); //Fill data from the Request
        $return->dateFrom = $response['dateReturn'];
        $return->startHour = $response['timeReturn'];
        $return->location_from = $ride->location_to;
        $return->location_to = $ride->location_from;
        $return->city_from = $city_to; // It should be the other way around since is a return
        $return->city_to = $city_from;
        $return->user_id = Auth::id();
        $return->completed = 'Ongoing';
        $return->seats_available = $response['seats'];
        $return->save();
        if(count($waypoints)>0){
            foreach($waypoints as $waypoint){

                $waypoint_array = explode(',',$waypoint->city_address);
                $ridestop2 = new RidesStop;
                $ridestop2->price_from = $waypoint->price_from;
                $ridestop2->price_to = $waypoint->price_to;
                $ridestop2->rides_id = $return->id;
                $ridestop2->user_id = Auth::id();
                $ridestop2->address = $waypoint->city_address;
                $ridestop2->seats_from = $return->seats;
                $ridestop2->seats_to = $return->seats;
                $ridestop2->city_name = $this->parseLocation($waypoint_array, $waypoint);
                $ridestop2->save();
            }
        }


        $response = array(
            'status' => 200,
            'message' => 'Successfully created a new ride'
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function reportProblem(Request $request){

        $report = new Report;
        $report->rides_id = $request->get('rides_id');
        $report->passenger_id = Auth::id();
        $report->reason = $request->get('reason');
        $report->description = $request->get('body');
        $report->completed = false;

        $report->save();

        $response = array(
            'status' => 200,
            'message' => 'We have recieved your problem. We will respond within 24 hours.'
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }

    //Delete Ride from Driver
    public function deleteRide(Request $request){

        $ride = Rides::find($request->get('ride'));
        $user = Auth::user();
        $reason = $request->get('bodyMessage');
        $response = $this->destroySimpleRideDriver($ride->id, $user->id, $reason);

        if($response == true){
            $response = array(
                'status' => 1,
                'message' => "We have deleted your ride and sent notifications to all passengers that the ride has been cancelled.");
            header('Content-Type: application/json');
            return response()->json($response);
        }else{
            $response = array(
                'status' => 0,
                'message' => "Error deleting ride, please contact us for more details.");
            header('Content-Type: application/json');
            return response()->json($response);
        }
    }

    public function repeatRide(Request $request){

        $temp = $request->get('ride');

        $ride = Rides::find($temp['id']);

        $newRide = $ride->replicate();
        $newRide->dateFrom = Carbon::now()->addDays(1)->format('Y-m-d');

        $newRide->save();

        header('Content-Type: application/json');
        return response()->json($newRide);

    }

    
}
