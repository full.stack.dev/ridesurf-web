<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\LocationHelper; 

use App\Airports;
use App\Rides;
use App\AirportDetails;
use Illuminate\Support\Facades\Log;
 
use Auth;

class AirportRideController extends Controller
{
	use LocationHelper;

    public function getAirportList(){

    	$airports = Airports::with('meetings')->get();
    	return response()->json($airports);
    }

    public function createAirportRide(Request $request){
    	$ride = new Rides;

        try{

            $ride->fill($request->get('ride'));
            $arrayFrom = explode(',', $ride->location_from);
            $ride->city_from = $this->parseLocation($arrayFrom, $ride->location_from);
            $arrayTo = explode(',', $ride->location_to);
            $ride->city_to = $this->parseLocation($arrayTo, $ride->location_to);
            $ride->user_id = Auth::id();
            $ride->ride_type = "airport";
            $ride->seats_available = $ride->seats;
            $ride->trip_purpose_id = 7;
            $ride->save();

        }catch(\Exception $e){
            Log::error($e);
            Log::error('ERROR AR0001 Could not create ride');
            return response()->json(['message'=>'There was an error creating the ride, please try again'], 500);
        }

        $air = $request->get('airportDetail');
        // print_r($air);
        // Commented to a newer version
    	$airportDetail = new AirportDetails;
        $airportDetail->type = 'Parked'; 
        $airportDetail->airport_id = $air['id'];
     	$airportDetail->ride_id = $ride->id;
    	$airportDetail->save();

    	return response()->json(['status' => 'success', 'message' => 'successfully created ride']);

    }
}
