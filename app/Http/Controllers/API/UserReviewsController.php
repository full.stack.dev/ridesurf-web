<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use App\User;
use App\UserRides;
use App\Rides;
use App\Reviews;
use Carbon\Carbon;

class UserReviewsController extends Controller
{
    public function getUserReviews($id){

    	$reviews = Reviews::where('user_rate_id', '=', $id)
    			   ->with('reviewFrom')->with('reviewTo')->get();
    	if(!empty($reviews) ){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

    	$message = array(
    		'reviews' => $reviews,
    		'status' => $status
        );
    	
    	header('Content-Type: application/json');
        return response()->json($message);
    }
}
