<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Feedback;
use App\IpRequest;
use App\Rides;
use Carbon\Carbon;
use App\User;
use Auth, DB, Session, Mail;


class HomeController extends Controller
{

    /**
     * Show the application dashboarduser.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $today = Carbon::today()->format('Y-m-d');
        return view('home', compact('today'));
    }

    public function fetch(Request $request)
    {
        
         if($request->get('query'))
         {

          $query = $request->get('query');
          
          $data = DB::table('locations')
            ->where('city', 'LIKE', "%{$query}%")->take(10)
            ->get();
          $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
                    
          foreach($data as $row)
          {
           $output .= '
           <li><a href="#" value='.$row->id.'>'.$row->name. '</a></li>
           ';
          }
          $output .= '</ul>';
          echo $output;
         }
    }

    public function contact_update(Request $request){
      $ip = new IpRequest;
      $ip->ip = request()->ip();
      $ip->url_visted = "contact-form";
      $ip->save();

      $contact = new Contact;

      $contact->name = $request->get('name');
      $contact->email = $request->get('email');
      $contact->subject = $request->get('subject');
      $contact->description = $request->get('description');

      $contact->save();

      $data = [
        'name' => $contact->name,
        'subj' => $contact->subject,
        'desc' => $contact->description,
        'senderEmail' => $contact->email
      ];
      $email = 'support@ridesurf.io';
      Mail::send('email.contactForm', $data, function($message) use ($email) {
          $message->to('support@ridesurf.io')->subject("Contact Form");
      });

      Session::flash('message', "Your message has been sent. Please allow us 24 hours to respond.");
       
      return redirect()->back();

    }

    public function send_report(Request $request){


      $url = $request->get('url');

      if(Auth::check()){
        $user_id = Auth::user()->id;
      }else{
        $user_id = null;
      }


      return view('errors.bug_report_form', compact('url', 'user_id'));
    }

    public function post_report(Request $request){


      Session::flash('message', "Thanks for your input we will review and fix the problem as soon as possible");

      return redirect()->back();
    }

    public function feedback(Request $request){

      return view('company.feedback');
    }

    public function post_feedback(Request $request){

      $feedback = new Feedback();
      $feedback->name = $request->get('name');
      $feedback->source = $request->get('source');
      $feedback->description = $request->get('description');
      $feedback->save();

      Session::flash('message', 'Thanks for your Feedback');
      return redirect('/');
    }

    public function about()
    {
      return view('company.about');
    }

    public function contact(Request $request)
    {
      return view('company.contact');
    }

    public function faq()
    {
      return view('company.faq');
    }

    public function privacy()
    {
      return view('company.privacy');
    }

    public function codeofconduct()
    {
      return view('company.code-of-conduct');
    }

    public function terms()
    {
      return view('company.terms');
    }

    public function press()
    {
      return view('company.press');
    }

    public function opportunities()
    {
      return view('company.opportunities');
    }

    public function howtoridesurf(){
      return view('company.howtoridesurf');
    }

    public function homebass(){
      $now = date('Y-m-d');

      $rides = Rides::where('dateFrom', '>', $now)
               ->where('trip_purpose_id', '=', 8)
               ->get();
      
     $today = strtotime(Carbon::now());

      $rides = $rides->filter(function ($item) use ($rides, $today) {
        $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
        // First Make sure that the date is after today hour and date
                if($time_ride >= $today){
                    if( $item->stop_id != null){

                        if($item->total == null && ($item->seat_from >= 1 || $item->seat_to >= 1) ){
                            return $item;
                        }
                    }else{

                        if($item->total == null ){
                            return $item;
                        }
                    }
                    if($item->total != 0){
                        $total = floor($item->total / 2);
                    }else{

                        $total = 0;
                    }
                    if( ( $total - $item->seats_available ) >= 1){
                        return $item->total > 0;
                    }
                }

      })->values()->all();

          foreach($rides as $ride){
              $user = User::find($ride->user_id);
              if(count($user->driver) > 0){
                  $ride->make = DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake;
                  $ride->model = DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle;
                  $ride->color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                              ->get()->first()->vColour_EN;
                  $ride->type = DB::table('car_type')
                  ->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;
                  $ride->year = $user->driver[0]->year;
              }
          }
        

      $control = false;
      $price = null;
      $date = null;
      $women = null;
      return view('company.homebass', compact('rides', 'control', 'price', 'date', 'women'));
    }

    public function searchHomeBase(Request $request){
      $now = date('Y-m-d');

      $rides = Rides::where('dateFrom', '>', $now)
               ->where('trip_purpose_id', '=', 8);
      $date = $request->date;
      $price = $request->get('max_price');
      $women = $request->get('ladies_only');

      if($date){
        $today = strtotime($request->date);
      }else{
        $today = strtotime(Carbon::now());
      }
      if($women){
        $rides->where('ladies_only', '=', 'Yes');
      }
      if($price){
          if($request->max_price != null)
              $rides->where('price', '<=', $price );
      }


      $rides = $rides->get();

      $rides = $rides->filter(function ($item) use ($rides, $today) {
        $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
        // First Make sure that the date is after today hour and date
                if($time_ride >= $today){
                    if( $item->stop_id != null){

                        if($item->total == null && ($item->seat_from >= 1 || $item->seat_to >= 1) ){
                            return $item;
                        }
                    }else{

                        if($item->total == null ){
                            return $item;
                        }
                    }
                    if($item->total != 0){
                        $total = floor($item->total / 2);
                    }else{

                        $total = 0;
                    }
                    if( ( $total - $item->seats_available ) >= 1){
                        return $item->total > 0;
                    }
                }

          })->values()->all();

          foreach($rides as $ride){
              $user = User::find($ride->user_id);
              if(count($user->driver) > 0){
                  $ride->make = DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake;
                  $ride->model = DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle;
                  $ride->color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                              ->get()->first()->vColour_EN;
                  $ride->type = DB::table('car_type')
                  ->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;
                  $ride->year = $user->driver[0]->year;
              }
          }
      $control = true;

        return view('company.homebass', compact('rides', 'control', 'price', 'date', 'women'));
    }

    public function searchSynapseRides(Request $request){

        $body = $request->body;

        if($body['date']){
            $now = substr($body['date'], 0, 10);
            $today = strtotime($request->date);
        }
        else{
            $now = date('Y-m-d');
            $today = strtotime($now);
        } 
        

        $rides =  Rides::where('dateFrom', '>=', $now)
                  ->whereHas('trip_purpose', function ($query){
                    $query->where('type', '=', 'synapse');
                  })->get();

           

        $rides = $rides->filter(function ($item) use ($rides, $today) {
            $time_ride = strtotime($item->dateFrom  ." ".$item->startHour);
            // First Make sure that the date is after today hour and date
            if($time_ride >= $today){
                if( $item->stop_id != null){

                    if($item->total == null && ($item->seat_from >= 1 || $item->seat_to >= 1) ){
                        return $item;
                    }
                }else{

                    if($item->total == null ){
                        return $item;
                    }
                }
                if($item->total != 0){
                    $total = floor($item->total / 2);
                }else{

                    $total = 0;
                }
                if( ( $total - $item->seats_available ) >= 1){
                    return $item->total > 0;
                }
            }

        })->values()->all();

        foreach($rides as $ride){
            $user = User::find($ride->user_id);
            if(count($user->driver) > 0){

                $ride->rating = $user->rating;
                $ride->avatar = $user->avatar;
                $ride->first_name = $user->first_name;
                $ride->last_name = $user->last_name;
                $ride->path_image = $user->driver[0]->path_image;
                $ride->car_image = $user->driver[0]->path_image;
                $ride->make = DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake;
                $ride->model = DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle;
                $ride->color = DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                    ->get()->first()->vColour_EN;
                $ride->type = DB::table('car_type')
                    ->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN;
                $ride->year = $user->driver[0]->year;

            }
        }
        
        if(count($rides) > 0)
          return response()->json(['action' => 1, 'rides' => $rides, 'openRides' => 0]);
        else
          return response()->json(['action' => 0, 'rides' => 0, 'openRides' => 0]);

    }
    public function searchSynapse(Request $request){


        $date = $request->date;
        $price = $request->get('max_price');
        $women = $request->get('ladies_only');

        if($date){
            $today = strtotime($request->date);
        }else{
            $today = strtotime(Carbon::now());
        }
        if($women){
            $rides->where('ladies_only', '=', 'Yes');
        }
        if($price){
            if($request->max_price != null)
                $rides->where('price', '<=', $price );
        }

        $control = true;

        $states = DB::table('states')->get();

        return view('company.Synapse', compact('rides', 'control', 'price', 'date', 'women', 'states'));
    }

}
