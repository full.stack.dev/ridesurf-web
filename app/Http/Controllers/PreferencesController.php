<?php

namespace App\Http\Controllers;

use App\Preferences;
use App\User;
use App\UserPreferences;
use Illuminate\Http\Request;
use Session, Redirect;

class PreferencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __contruct() 
    {
        $this->middleware('permission:Administer roles & permissions');
        $this->middleware('permission:role-list');
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $preferences = Preferences::paginate(15);

        return view('dashboardadmin.preferences.index', compact('preferences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboardadmin.preferences.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $preference = new Preferences;

        $preference->fill($data);
        $preference->save();

        Session::flash('message','Successfully created a new preference');
        return redirect('/dashboardadmin/preference');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Preferences  $preferences
     * @return \Illuminate\Http\Response
     */
    public function show(Preferences $preferences)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Preferences  $preferences
     * @return \Illuminate\Http\Response
     */
    public function edit(Preferences $preference)
    {   
        return view('dashboardadmin.preferences.edit', compact('preference'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Preferences  $preferences
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Preferences $preference)
    {
        $data = $request->all();
        $preference->fill($data);
        $preference->update();

        Session::flash('message','Successfully edited the preference');
        return view('dashboardadmin.preferences.edit', compact('preference'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Preferences  $preferences
     * @return \Illuminate\Http\Response
     */
    public function destroy(Preferences $preference)
    {
        $preference->delete();

        Session::flash('message','Successfully deleted the preference');
        return redirect('/dashboardadmin/preference');
    }

    public function assignPreferneces(){


        $users = User::all();
        $preferences = Preferences::all();

        foreach($users as $user){
            foreach($preferences as $preference){
                if($preference->id % 2 == 0){
                    $user_preference = new UserPreferences;
                    $user_preference->user_id = $user->id;
                    $user_preference->preference_id = $preference->id;
                    $user_preference->save();
                }
            }
        }

        Session::flash('message','Successfully updated users');
        return redirect('/dashboardadmin/preference');
    }
}
