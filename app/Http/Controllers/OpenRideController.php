<?php

namespace App\Http\Controllers;

use App\OpenRide;
use App\User;
use App\TripPurpose;
use App\OpenRideHours;
use App\UserOpenRide;
use App\UserRides;
use App\Rides;
use ArrayObject;
use Carbon\CarbonPeriod;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use JavaScript, DB, Mail, Session;

class OpenRideController extends Controller
{
    private $admin;

    public function __construct()
    {
        $this->admin = User::where('first_name', '=', 'admin')->get()->first();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return view('rides.open-ride.create', [
            'user' => $user,
        ]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->get('body');

        $from = strtotime($input['open_date_from']);
        $to = strtotime($input['open_date_to']);
        
        if($to <= $from){
            $message = array(
                'message' => 'Your end date cannot be before your start date ',
                'status' => 'Error'
            );
        header('Content-Type: application/json');
        return response()->json($message, 500);
        }
        $open_ride = new OpenRide;
        $open_ride->fill($input);
        $open_ride->user_id = Auth::id();
        $open_ride->trip_purpose_id = $input['trip_purpose_id'];
        $open_ride->ladies_only = 'No';
        $open_ride->save();

        foreach($input['hours'] as $hour){

            $oph = new OpenRideHours;
            $oph->open_rides_id = $open_ride->id;
            $carbon = new Carbon($hour['time']);
            $oph->hour = $carbon->format('H:i:s');
            $oph->save();

        }

        $message = array(
            'message' => 'Successfully Created Ride',
            'open_ride' => $open_ride
        );
        header('Content-Type: application/json');
        return response()->json($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('rides.open-ride.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->get('body');
        $hours = $input['hours'];
        $from = strtotime($input['ride']['open_date_from']);
        $to = strtotime($input['ride']['open_date_to']);
        
        if($to <= $from){
            $message = array(
                'message' => 'Your end date cannot be before your start date ',
                'status' => 'error'
            );
            header('Content-Type: application/json');
            return response()->json($message);
        }
        $openRide = OpenRide::find($id);
        $openRide->fill($input['ride']);
        $openRide->update();

        if(count($hours) > 0){
            $openRide->hours()->delete();
            foreach($hours as $hour){

                $oph = new OpenRideHours;
                $oph->open_rides_id = $openRide->id;
                $carbon = new Carbon($hour['time']);
                $oph->hour = $carbon->format('H:i:s');
                $oph->save();

            }
        }
        $response = array(
                'message' => 'Your open ride has been updated',
                'status' => 'success'
            );
        header('Content-Type: application/json');
        return response()->json($response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $openRide = OpenRide::find($id);
        $hours = OpenRideHours::where('open_rides_id',$id)->get();
        foreach($hours as $hour){
            $hour->delete();
        }
        $openRide->delete();

        $response = array(
            'message' => "Open Ride successfully deleted",
            'status' => 'success',
        );
        return \Response::json($response);

    }

    public function fetchPurposes(){
        $purpose = TripPurpose::orderBy('type','asc')->get();

        
        header('Content-Type: application/json');
        return response()->json($purpose);
    }

    public function join(Request $request, $id){

        $check = UserOpenRide::where('user_id', '=', Auth::id())
                ->where('open_ride_id','=',$id)->count();

        if($check > 0){
            header('Content-Type: application/json');
            return response()->json([
                'message' => 'You already requested this ride',
                'status' => 'error'
            ]);    
        }
        $openRide = OpenRide::find($id);
        if($openRide->trip_purpose){
            $openRide->purpose = TripPurpose::where('id',$openRide->trip_purpose_id)->first()->type;
        }
        $currentU = Auth::user();
        $asked = UserOpenRide::where('user_id', '=', $currentU)->count();

        if($currentU->id == $openRide->user_id || $currentU->avatar == null ||
            $currentU->phone_number == null || $currentU->gender == null ||
            $asked > 0)  {
            $canJoin = false;
        }else{
            $canJoin = true;
        }
        $hours = OpenRideHours::where('open_rides_id', $id)
                 ->orderBy('hour')->get();

        $now  = Carbon::now();
        $end = Carbon::parse($openRide->open_date_to)->addDay(1);

        //Check the times that
        if($now->format('Y-m-d') <= $openRide->open_date_from){
            if($openRide->open_date_from == $now->format('Y-m-d')){
                $period = CarbonPeriod::between($openRide->open_date_from, $end);
            }else{
                $period = CarbonPeriod::between($openRide->open_date_from, $end);
            }
        }
        else{
            $now  = Carbon::now()->addDay(1);
            $period = CarbonPeriod::between($now, $end);
        }
        $dates = [];

        // Iterate over the period
        foreach ($period as $date) {
            array_push($dates, $date->format('m/d/y'));
        }

        // $dates = json_encode($dates);
        $user = User::find($openRide->user_id);

        $vehicle = array(
            'make' => DB::table('make')->where('iMakeId','=', $user->driver[0]->make_id)->get()->first()->vMake,
            'model' => DB::table('model')->where('iModelId','=', $user->driver[0]->model_id)->get()->first()->vTitle,
            'color' => DB::table('car_colour')->Where('iColourId', '=', $user->driver[0]->color_id)
                ->get()->first()->vColour_EN,
            'type' => DB::table('car_type')->where('iCarTypeId', '=', $user->driver[0]->type_id)->get()->first()->vTitle_EN,
            'path_image' => $user->driver[0]->path_image,
            'year' => $user->driver[0]->year,
        );

        // $vehicle = json_encode($vehicle);

        $user->joined = date("j M Y",strtotime($user->created_at));


        header('Content-Type: application/json');
        return response()->json([
            'openRide' => $openRide, 
            'hours' => $hours,
            'user' => $user,
            'dates' => $dates,
            'vehicle' => $vehicle,
            'canJoin' => $canJoin,
            'status' => 'Success'
        ]);

        // return view('rides.open-ride.join', [
        //     'openRide' => $openRide, 
        //     'hours' => $hours,
        //     'user' => $user,
        //     'dates' => $dates,
        //     'vehicle' => $vehicle,
        //     'canJoin' => $canJoin
        // ]);
    }
    public function postJoin(Request $request, $id){

        $input = $request->get('body');
        $hour = date("G:i", strtotime($input['hour']));
        $date =date("Y-m-d", strtotime($input['date']));

        $userRide = new UserOpenRide;
        $userRide->open_ride_id = $id;
        $userRide->user_id = Auth::id();
        $userRide->status = 'Requested';
        $userRide->hour = $hour;
        $userRide->date = $date;
        $userRide->seats = $input['seats'];
        $userRide->save();
        $openRide = OpenRide::find($id);

        $message = "You have a new request to join your open ride!";

        $thread = Thread::create([
            'subject' => 'Passenger request for the open ride to ' . $openRide->location_to ,
            'rides_id' => $openRide->id,
            'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $openRide->user->id,
            'last_read' => new Carbon,
        ]);

        $email = $openRide->user->email;
        $info = array(
            'openRide' => $openRide,
            'userRide' => $userRide,
            'first_name' => $openRide->user->first_name,
        );

        Mail::send('email.openRideRequest', $info, function($Newmessage) use ($email) {
            $Newmessage->to($email)->subject('Passenger request for an Open Ride');
        });

        $response = 'Successfully requested ';
        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function acceptPassenger(Request $request, $id){

        $userRide = UserOpenRide::find($id);
        $openRide = OpenRide::find($userRide->open_ride_id);
        $array = $openRide->toArray();

        //Switch open ride to ride
        $ride = new Rides;
        $ride->fill($array);
        $ride->user_id = $openRide->user_id;
        $ride->seats_available = $openRide->seats - $userRide->seats;
        $ride->city_from = $openRide->city_from;
        $ride->city_to = $openRide->city_to;
        $ride->dateFrom = $userRide->date;
        $ride->startHour = $userRide->hour;
        $ride->ladies_only = 'No';
        $ride->save();

        //Create new user ride to the normal ride
        $new_user_ride = new UserRides;
        $new_user_ride->rides_id = $ride->id;
        $new_user_ride->description = $userRide->description;
        $new_user_ride->user_id = $userRide->user_id;
        $new_user_ride->status = 'Approved';
        $new_user_ride->city_from = $ride->city_from;
        $new_user_ride->city_to = $ride->city_to;
        $new_user_ride->booker_payment_paid = 'No';
        $new_user_ride->ride_cost = $openRide->price;
        $new_user_ride->seats = $userRide->seats;
        $new_user_ride->save();

        $message = "Congratulations, you are going to ".$new_user_ride->ride->location_to."! The driver has approved your request to join the ride. Please go to your booking page and pay asap.";

        $thread = Thread::create([
            'subject' => 'Approved for ride to ' .$ride->location_to,
            'rides_id' => $ride->id,
            'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $userRide->user_id,
            'last_read' => new Carbon,
        ]);

        $driver_name = $ride->user->first_name;
        $email = $userRide->user->email;

        $info = array("user" => $userRide->user->first_name, "rideTo" => $ride->location_to, "driver"    => $driver_name, "ride_id" => $ride->id);

        Mail::send('email.acceptPassenger', $info, function($message) use ($email) {
            $message->to($email)->subject('Ride Request Update');
        });

        $openRide->active = false;
        $openRide->update();
        $userRide->status = 'Approved';
        $userRide->update();

        Session::flash('message', "Ride request has been updated to accepted");

        return redirect()->back();

    }

    public function denyPassenger(Request $requet, $id){

        $userRide = UserOpenRide::find($id);
        $userRide->status = "Denied";
        $userRide->update();

        $message = "Sorry, the driver has denied your request to join the trip. We encourge you to look for another one!";

        $thread = Thread::create([
            'subject' => 'New passenger request for ' .$userRide->open_ride->location_to,
            'rides_id' => $userRide->open_ride->id,
            'isNotification' => 'Yes',
        ]);
        $thread->isNotification = "Yes";
        $thread->update();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $this->admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $userRide->user->id,
            'last_read' => new Carbon,
        ]);

        $email = $userRide->user->email;
        $info = array("user" => $userRide->user->first_name, 
                "rideTo" => $userRide->open_ride->location_to);

        Mail::send('email.denyPassenger', $info, function($message) use ($email) {
            $message->to($email)->subject('Ride Request Update');
        });

        Session::flash('message', "Ride request has been updated to denied");
        return redirect()->back();

    }

    public function statusChange($id){
        $openRide = OpenRide::find($id);

        if($openRide->active == true){
            $openRide->active = false;
            $openRide->update();
            $response = array(
                'message' => 'Your Open Ride is now inactive',
                'status' => 'success'
            );
            header('Content-Type: application/json');
            return response()->json($response);
        }else{
            $openRide->active = true;
            $openRide->update();
            $response = array(
                'message' => 'Your Open Ride is now active',
                'status' => 'success'
            );
            
            header('Content-Type: application/json');
            return response()->json($response);
        }

        $response = array(
            'message' => 'There was an error trying to update the Open Ride, please try again.',
            'status' => 'error'
        );
        header('Content-Type: application/json');
        return response()->json($response, 400);
    }
}
