<?php

namespace App\Http\Controllers;

use App\CancelUserRides;
use Illuminate\Http\Request;

class CancelUserRidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CancelUserRides  $cancelUserRides
     * @return \Illuminate\Http\Response
     */
    public function show(CancelUserRides $cancelUserRides)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CancelUserRides  $cancelUserRides
     * @return \Illuminate\Http\Response
     */
    public function edit(CancelUserRides $cancelUserRides)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CancelUserRides  $cancelUserRides
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CancelUserRides $cancelUserRides)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CancelUserRides  $cancelUserRides
     * @return \Illuminate\Http\Response
     */
    public function destroy(CancelUserRides $cancelUserRides)
    {
        //
    }
}
