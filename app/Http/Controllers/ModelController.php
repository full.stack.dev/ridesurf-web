<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Rides;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Input;
use Auth, Storage, DB, Response, File, Mail;
use Session, Redirect;

class ModelController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:Administer roles & permissions');
        $this->middleware('permission:role-list');
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = DB::table('model')
                    ->join('make' ,'model.iMakeId', '=', 'make.iMakeId')
                    ->orderBy('vTitle', 'ASC')->paginate(25);

        return view('dashboardadmin.model.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $makes = DB::table('make')->orderBy('vMake', 'ASC')->get();

        return view('dashboardadmin.model.create', compact('makes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->except('_token');

        DB::table('model')->insert([
            'iMakeId' => $input['iMakeId'], 
            'vTitle' => $input['vTitle'], 
            'eStatus' => $input['eStatus']
        ]);

        Session::flash('message', 'Succesfully created model' . $input['vTitle']);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = DB::table('model')
                    ->join('make' ,'model.iMakeId', '=', 'make.iMakeId')
                    ->where('model.iModelId', '=', $id)
                    ->orderBy('vTitle', 'ASC')->get()->first();
        
        $makes = DB::table('make')->orderBy('vMake', 'ASC')->get();

        return view('dashboardadmin.model.edit', compact('model', 'makes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token');

        DB::table('model')->where('iModelId', '=', $id)
        ->update([
            'iMakeId' => $input['iMakeId'], 
            'vTitle' => $input['vTitle'], 
            'eStatus' => $input['eStatus']
        ]);

        Session::flash('message', 'Successfully edited model' . $input['vTitle']);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('model')->where('iModelId', '=', $id)->delete();

        Session::flash('message', 'Successfully deleted model');
        return redirect()->back();
    }

    public function filterModel(Request $request){
        if($request->get('vTitle'))
            $model = DB::table('model')->join('make' ,'model.iMakeId', '=', 'make.iMakeId')
            ->where('vTitle', '=' , $request->get('vTitle'))->paginate(5);
        else
            $model = DB::table('model')
                    ->join('make' ,'model.iMakeId', '=', 'make.iMakeId')
                    ->orderBy('vTitle', 'ASC')->paginate(25);

      return view('dashboardadmin.model.index', compact('model'));
    }
}
