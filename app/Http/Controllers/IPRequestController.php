<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IpRequest;

class IPRequestController extends Controller
{

	function __construct()
    {
        $this->middleware('permission:Administer roles & permissions');
        $this->middleware('permission:role-list');
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }
    
    public function index(Request $request){

    	$allRequest = IpRequest::orderBy('created_at', 'ASC')->paginate(25);

    	return view('dashboardadmin.request.list', compact('allRequest'));
    }
}
