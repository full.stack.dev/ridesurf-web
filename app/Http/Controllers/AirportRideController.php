<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airports;
use Illuminate\Support\Facades\Auth;
use Session, Redirect, Hash, DB;

class AirportRideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $airports = Airports::orderBy('name', 'desc')->get();
        $user = Auth::user();

        return view('rides.airport-ride.create', compact('airports', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ride = new Rides;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getList(Request $request){
        $airports = Airports::all();

        return view('dashboardadmin.airports.index', compact('airports'));
    }

    public function storeNewAirport(Request $request){
        $airport = new Airports;

        $input = $request->all();
        $airport->fill($input);
        $airport->save();

        Session::flash('message', 'Aiport Added');

        return redirect()->back();
    }

    public function createNewAirport(Request $request){

        $state = DB::table('states')->get();

        return view('dashboardadmin.airports.create', compact('state'));
    }

    public function editSingleAirport(Request $request, $id){
        $air = Airports::find($id);
        $state = DB::table('states')->get(); 
        return view('dashboardadmin.airports.edit', compact('air', 'state'));
    }

    public function updateAirport(Request $request, $id){
        $airport = Airports::find($id);

        $input = $request->all();
        $airport->fill($input);
        $airport->update();

        Session::flash('message', 'Aiport edited');

        return redirect()->back();
    }
}
