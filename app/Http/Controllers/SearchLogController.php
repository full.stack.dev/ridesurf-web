<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SearchLog;

class SearchLogController extends Controller
{
    public function index(){

    	$logs = SearchLog::paginate(15);
    	$city_from = SearchLog::distinct('city_from')
                     ->groupBy('city_from')
                     ->orderBy('city_from', 'ASC')
                     ->get();
    	foreach($city_from as $city){
    		$city->count = SearchLog::where('city_from', '=', $city->city_from)->count();
    	}
    	
        $city_from = $city_from->sortByDesc('count')->take(15);


    	$city_to = SearchLog::distinct('city_to')
                   ->groupBy('city_to')
                   ->orderBy('city_to')
                   ->get();

    	foreach($city_to as $city){
    		$city->count = SearchLog::where('city_to', '=', $city->city_to)->count();
    	}

        $city_to = $city_to->sortByDesc('count')->take(15);

        
       $frequency = SearchLog::groupBy('city_from')->groupBy('city_to')->get();

       foreach($frequency as $freq){
            $temp = SearchLog::where('city_from', '=', $freq->city_from)
                             ->where('city_to', '=', $freq->city_to)->count();
            $freq->count = $temp;
       }

       $frequency = $frequency->sortByDesc('count')->take(20);

    	$i=0; $j=0;

    	return view('dashboardadmin.search-logs.index', 
            compact('frequency','logs', 'city_from', 'city_to', 'i', 'j'));
    }
}
