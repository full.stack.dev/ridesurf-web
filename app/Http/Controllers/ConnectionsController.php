<?php

namespace App\Http\Controllers;

use App\Connections;
use Illuminate\Http\Request;
use DB;

class ConnectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __contruct() 
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data = Connections::all();
        return view('connections.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('connections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $input = $request->all();

        $conection = new Connections;
        
        $conection->location_id_one = $request->input('location_id_one');
        $conection->location_id_two = $request->input('location_id_two');
        $conection->name = $conection->location_id_one . ' - ' .  $conection->location_id_two ;
        $conection->price = $request->input('price');
        $conection->duration = $request->input('duration');
        $conection->miles = $request->input('miles');

        $conection->save();

        $find = Connections::where('location_id_one', '=', $request->input('location_id_two'))
                           ->where('location_id_two', '=', $request->input('location_id_one'))
                           ->get()->count();

        if( $find == 0 ){
            $back = new Connections;
            $back->location_id_one = $request->input('location_id_two');
            $back->location_id_two = $request->input('location_id_one');
            $back->name = $conection->location_id_two . ' - ' .  $conection->location_id_one ;
            $back->price = $request->input('price');
            $back->duration = $request->input('duration');
            $back->miles = $request->input('miles');

            $back->save();
        }

        return redirect()->route('connections.index')
                        ->with('success','Connection ' . $conection->name .' created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Connections  $connections
     * @return \Illuminate\Http\Response
     */
    public function show(Connections $connections)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Connections  $connections
     * @return \Illuminate\Http\Response
     */
    public function edit(Connections $connections, $id)
    {
        $data = Connections::find($id);

        return view('connections.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Connections  $connections
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Connections $connections, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $input = $request->all();

        $conection = Connections::find($id);
        $conection->name = $request->input('name');
        $conection->location_id_one = $request->input('location_id_one');
        $conection->location_id_two = $request->input('location_id_two');
        $conection->price = $request->input('price');
        $conection->duration = $request->input('duration');
        $conection->miles = $request->input('miles');

        $conection->update();

        return redirect()->route('connections.index')
                        ->with('success','Connection ' . $conection->name .' created successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Connections  $connections
     * @return \Illuminate\Http\Response
     */
    public function destroy(Connections $connections)
    {
        $data = Connections::find($connections->id);
        $data->delete();

        return view('connections.index')->with('success', 'Connections successfully deleted');
    }

    /**
     *
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {
        

         if($request->get('query'))
         {

          $query = $request->get('query');
          
          $data = DB::table('locations')
            ->where('name', 'LIKE', "%{$query}%")->take(20)
            ->get();
          $output = '<ul class="dropdown-menu" id="list-location" style="display:block; position:relative">';
          foreach($data as $row)
          {
           $output .= '
           <li><a href="#">'.$row->name. '</a></li>
           ';
          }
          $output .= '</ul>';
          echo $output;
         }
    }
}
