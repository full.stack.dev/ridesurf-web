<?php

namespace App\Http\Controllers;

use App\TripPurpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TripPurposeController extends Controller
{
    /**
     * @return array
     */
    public function index()
    {
        $types = TripPurpose::paginate(25);
        $i=0;

        return view('dashboardadmin.trip-purpose.index', compact('types','i'));
    }

    public function create(){
        return view('dashboardadmin.trip-purpose.create');
    }
    public function edit($id){

        $type = TripPurpose::find($id);
        return view('dashboardadmin.trip-purpose.edit', compact('type'));
    }

    public function store(Request $request){

        $input = $request->all();
        $type = new TripPurpose();
        $type->fill($input);
        $type->save();

        return redirect()->to('dashboardadmin/trip-purposes');
    }

    public function update(Request $request, $id){
        $type = TripPurpose::find($id);
        $input = $request->all();
        $type->fill($input);
        $type->update();

        Session::flash('success', 'Edited Trip Purpose');
        return redirect()->to('dashboardadmin/trip-purposes');
    }

    public function delete($id){

        $type = TripPurpose::find($id);
        $type->delete();

        return redirect()->to('dashboardadmin/trip-purposes');
    }
}
