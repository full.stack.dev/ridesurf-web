<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Http\Traits\MailAdmins;
use Illuminate\Http\Request;
use App\Rides;
use App\User;
use Carbon\Carbon;
use App\Exports\DriverExport;
use Maatwebsite\Excel\Facades\Excel;

use Auth, Session, File, Mail;
use Illuminate\Support\Facades\Log;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use MailAdmins;

    private $admin, $adminEmails;

    public function __contruct() 
    {
        $this->middleware('auth');
        $this->middleware('permission:driver-list');
        $this->middleware('permission:driver-create', ['only' => ['create','store']]);
        $this->middleware('permission:driver-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:driver-delete', ['only' => ['destroy']]);
        $this->admin = User::where('first_name', '=', 'admin')->get()->first();
        $this->adminEmails = explode(',', env('ADMIN_EMAILS'));

    }


    public function index()
    {
        $user = Auth::user();
        $ridesCount = Rides::where('user_id', '=', $user->id)->count();
        $rides = Rides::where('user_id', '=', $user->id)->orderBy('dateFrom', 'ASC')->get();
        return view('drivers/index', compact('user','rides', 'ridesCount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $input = $request->get('body');
        $driver = new Driver;
        $driver->fill($input['vehicle']);
        $driver->user_id = $user->id;
        $driver->status= "Requested";
        $driver->status_message = "The user has requested to be a driver please check the info and approve or deny request.";
        $driver->save();

        $response = array(
            'status' => '1',
            'message' => 'Successful Created User',
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->get('body');

        $user = Auth::user();
        $driver = Driver::where('user_id', '=', $user->id)->first();
        $driver->fill($input['vehicle']);
        $driver->status = 'Requested';
        $driver->update();

        //    $reason = "New driver request";
//            $reply = "User: " . $user->first_name . " has requested to be a driver and/or change car";
//
//            $this->mailCurrentAdminds($this->adminEmails, $reply, $reason);


        $response = array(
            'status' => 'success',
            'message' => 'Successfully updated your car',
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        //
    }

    public function reApproveDriver(Request $request, $id){
        $driver = Driver::find($id);

        if(!empty($driver)){
            $driver->status = 'Approved';
            $driver->update();
        }else{
            Session::flash('error', "There is no driver reference of user");
        return redirect()->back();
        }

        Session::flash('message', "User was successfully set as driver again");
        return redirect()->back(); 
    }

    public function autocomplete(){

        $term = Input::get('term');
        
        $results = array();
        
        $queries = DB::table('locations')
            ->where('first_name', 'LIKE', '%'.$term.'%')
            ->orWhere('last_name', 'LIKE', '%'.$term.'%')
            ->take(5)->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => $query->first_name.' '.$query->last_name ];
        }
    return Response::json($results);
    }


    public function driverlist(Request $request){

        $drivers = Driver::orderBy('created_at')->get();

        return view('dashboardadmin.driver.index', compact('drivers'));
    }


    public function emptyDriver(Request $request, $id){

        $driver = Driver::find($id);

        //Check that driver does not have passenger that paid in future rides
        if( count($driver->user->rides) > 0 ){
            foreach($driver->user->rides as $ride){
                if( count($ride->user_ride) > 0) {
                    foreach($ride->user_ride as $uRide){
                        if($uRide->booker_payment_paid == 'Yes' && $ride->dateFrom >= Carbon::now()->format('Y-m-d') ){
                            Session::flash('error', "This driver has ride scheduled it with passenger with them, please resolve them first, 
                                before reseting the driver");
                            return redirect()->back();
                        }
                    }
                }
            }
        }

        $user = User::find($driver->user_id);
        $dir = public_path('/images/user_information/' . $user->id . '/car_details');
        $this->rrmdir($dir);

        $driver->delete();


        Session::flash('message', "User was successfully set as passenger again");
        return redirect()->back();
    }

    public function rrmdir($dir) {
       if (is_dir($dir)) {
         $objects = scandir($dir);
         foreach ($objects as $object) {
           if ($object != "." && $object != "..") {
              if (filetype($dir."/".$object) == "dir") 
                $this->rrmdir($dir."/".$object); 
              else 
                unlink($dir."/".$object);
           }
         }
         reset($objects);
         rmdir($dir);
       }
    }

    public function saveInsurance(Request $request){

        ini_set('memory_limit', '-1');
        header('Content-Type: application/json');

        $input = $request->get('body');
        $insurance = $input['image'];

        $user = \Illuminate\Support\Facades\Auth::user();
        $roles = $user->getRoleNames();
        $inner_path = public_path('/images/user_information/'.$user->id.'/car_details');
        $ins_name= 'insurance.jpg';
        if(!File::exists($inner_path)) {
            File::makeDirectory($inner_path, $mode = 0777, true, true);
        }

        if(strpos($insurance, 'jpeg')){
            $insurance = str_replace('data:image/jpeg;base64,', '', $insurance);
        }else{
            $insurance = str_replace('data:image/png;base64,', '', $insurance);        
        }

        if($roles[0] == 'Driver'){
            //Normal change of car
            try{
                $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                // if(file_exists(public_path().$driver->insurance_path) ){
                //     unlink(public_path().$driver->insurance_path);
                // }
                if(!empty($insurance)){
                    $insurance = str_replace(' ', '+', $insurance);
                    \File::put($inner_path. '/' . $ins_name, base64_decode($insurance));
                    $driver->insurance_path = '/images/user_information/'.$user->id.'/car_details/'.$ins_name;
                }
                $driver->update();
            }
            catch (\Exception $e) {
                Log::info($e);
                $response = array(
                    'status' => '2',
                    'message' => 'Error Has occurred storing insurance',
                    'Error' => $e
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
        }
        else{

            //Driver Applied but was denied
            if(count($user->driver) > 0){
                try{
                    $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                    // if(file_exists(public_path().$driver->insurance_path) ){
                    //     unlink(public_path().$driver->insurance_path);
                    // }
                    if(!empty($insurance)){
                        $insurance = str_replace(' ', '+', $insurance);
                        \File::put($inner_path. '/' . $ins_name, base64_decode($insurance));
                        $driver->insurance_path = '/images/user_information/'.$user->id.'/car_details/'.$ins_name;
                    }

                    $driver->update();
                }
                catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => '2',
                        'message' => 'Error Has Occured',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
            //New Driver
            else{
                try{
                    $driver = new Driver;
                    $driver->user_id = $user->id;
                    $driver->status = 1;

                    $insurance = str_replace(' ', '+', $insurance);
                    \File::put($inner_path. '/' . $ins_name, base64_decode($insurance));
                    $driver->insurance_path = '/images/user_information/'.$user->id.'/car_details/'.$ins_name;

                    $driver->status_message = "The user has requested to change cars, please check the info and approve or deny request.";
                    $driver->save();
                }
                catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => '2',
                        'message' => 'Error Has Occurred storing insurance',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
        }
        Log::info('Saved Insurance Image  '.$user->first_name);
        $response = array(
            'status' => '1',
            'message' => 'Successful insurance upload',
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }

    public function saveRegistration(Request $request){

        ini_set('memory_limit', '-1');
        header('Content-Type: application/json');

        $input = $request->get('body');
        $registration = $input['image'];

        $user = \Illuminate\Support\Facades\Auth::user();
        $roles = $user->getRoleNames();
        $inner_path = public_path('/images/user_information/'.$user->id.'/car_details');
        $resg= 'registration.jpg';

        if(strpos($registration, 'jpeg')){
            $registration = str_replace('data:image/jpeg;base64,', '', $registration);
        }else{
            $registration = str_replace('data:image/png;base64,', '', $registration);        
        }

        if(!File::exists($inner_path)) {
            File::makeDirectory($inner_path, $mode = 0777, true, true);
        }

        if($roles[0] == 'Driver'){
            //Normal change of car
            try{
                $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                // if(file_exists(public_path().$driver->registration_path) ){
                //     unlink(public_path().$driver->registration_path);
                // }
                if(!empty($registration)){
                    $registration = str_replace(' ', '+', $registration);
                    \File::put($inner_path. '/' . $resg, base64_decode($registration));
                    $driver->registration_path = '/images/user_information/'.$user->id.'/car_details/'.$resg;
                    $driver->update();
                }
            }
            catch (\Exception $e) {
                Log::info($e);
                $response = array(
                    'status' => 'error',
                    'message' => 'Error Has Occurred storing registration',
                    'Error' => $e
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
        }
        else{

            //Driver Applied but was denied
            if(count($user->driver) > 0){
                try{
                    $driver = Driver::where('user_id', '=', $user->id)->get()->first();
                    // if(file_exists(public_path().$driver->registration_path) ){
                    //      unlink(public_path().$driver->registration_path);
                    //  }

                    $registration = str_replace(' ', '+', $registration);
                    \File::put($inner_path. '/' . $resg, base64_decode($registration));
                    $driver->registration_path = '/images/user_information/'.$user->id.'/car_details/'.$resg;

                    $driver->update();
                    Log::info('We store a registration');
                }
                catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => 'error',
                        'message' => 'Error Has Occurred storing registration',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
            //New Driver
            else{
                try{
                    $driver = new Driver;
                    $driver->user_id = $user->id;
                    $driver->status = 1;
                    $registration = str_replace(' ', '+', $registration);
                    \File::put($inner_path. '/' . $resg, base64_decode($registration));
                    $driver->registration_path = '/images/user_information/'.$user->id.'/car_details/'.$resg;

                }
                catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => 'error',
                        'message' => 'Error Has Occurred storing registration',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
        }

        $response = array(
            'status' => '1',
            'message' => 'Successful registration upload',
        );

        header('Content-Type: application/json');
        return response()->json($response);

    }

    public function saveLicense(request $request){
        ini_set('memory_limit', '-1');
        header('Content-Type: application/json');

        $input = $request->get('body');
        $license = $input['image'];

        $user = Auth::user();
        $roles = $user->getRoleNames();

        $inner_path = public_path('/images/user_information/'.$user->id.'/car_details');
        $lic_name = 'license.jpg';

        if(strpos($license, 'jpeg')){
            $license = str_replace('data:image/jpeg;base64,', '', $license);
        }else{
            $license = str_replace('data:image/png;base64,', '', $license);        
        }
        

        if(!File::exists($inner_path)) {
            File::makeDirectory($inner_path, $mode = 0777, true, true);
        }

        if($roles[0] == 'Driver'){
            //Normal change of car
            try{
                $driver = Driver::where('user_id', '=', $user->id)->get()->first();



                if(!empty($license)){
                   // if(file_exists(public_path().$driver->driver_license) ){
                   //     unlink(public_path().$driver->driver_license);
                   // }
                    $license = str_replace(' ', '+', $license);

                    \File::put($inner_path. '/' . $lic_name, base64_decode($license));
                    $driver->driver_license = '/images/user_information/'.$user->id.'/car_details/'.$lic_name;
                }

                $driver->status = 1;
                $driver->update();


            }
            catch (\Exception $e) {
                $response = array(
                    'status' => 'error',
                    'message' => 'Error Has Occurred storing driver license picture',
                    'Error' => $e
                );

                header('Content-Type: application/json');
                return response()->json($response);
            }
        }
        else{

            //Driver Applied but was denied
            if(count($user->driver) > 0){
                try{
                    $driver = Driver::where('user_id', '=', $user->id)->get()->first();

                    if(!empty($license)){

                       // if(file_exists(public_path().$driver->driver_license) ){
                       //     unlink(public_path().$driver->driver_license);
                       // }
                        $license = str_replace(' ', '+', $license);

                        \File::put($inner_path. '/' . $lic_name, base64_decode($license));
                        $driver->driver_license = '/images/user_information/'.$user->id.'/car_details/'.$lic_name;
                    }

                    $driver->status = 1;
                    $driver->update();
                }
                catch (\Exception $e) {
                    Log::info($e);
                    $response = array(
                        'status' => 'error',
                        'message' => 'Error Has Occurred storing driver license picture',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }

            //New Driver
            else{
                try{
                    $driver = new Driver;
                    $driver->user_id = $user->id;
                    $driver->status = 1;

                    $license = str_replace(' ', '+', $license);
                    \File::put($inner_path. '/' . $lic_name, base64_decode($license));

                    $driver->driver_license = '/images/user_information/'.$user->id.'/car_details/'.$lic_name;
                    $driver->status_message = "The user has requested to change cars, please check the info and approve or deny request.";
                    $driver->save();

                }
                catch (\Exception $e) {
                    $response = array(
                        'status' => 'error',
                        'message' => 'Error Has Occurred storing driver license picture',
                        'Error' => $e
                    );
                    Log::info($e);
                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
        }
        $response = array(
            'status' => '1',
            'message' => 'Successfully uploaded lincense photo',
        );

        header('Content-Type: application/json');
        return response()->json($response);
    }

    public function saveCarImage(Request $request){
        //This Image saves both the Driver License and Car Picture

        ini_set('memory_limit', '-1');
        header('Content-Type: application/json');

        $input = $request->get('body');
        $car = $input['image'];

        $user = Auth::user();
        $roles = $user->getRoleNames();

        $inner_path = public_path('/images/user_information/'.$user->id.'/car_details');
        $car_name= 'carImage.jpg';

        if(!File::exists($inner_path)) {
          File::makeDirectory($inner_path, $mode = 0777, true, true);
        }

        if(strpos($car, 'jpeg')){
            $car = str_replace('data:image/jpeg;base64,', '', $car);
        }else{
            $car = str_replace('data:image/png;base64,', '', $car);        
        }

        if($roles[0] == 'Driver'){
            //Normal change of car
                try{
                    $driver = Driver::where('user_id', '=', $user->id)->get()->first();


                    if(!empty($car)){
                        // if(file_exists(public_path().$driver->path_image) ){
                        //     unlink(public_path().$driver->path_image);
                        // }
                        $car = str_replace(' ', '+', $car);
                        \File::put($inner_path. '/' . $car_name, base64_decode($car));
                        $driver->path_image = '/images/user_information/'.$user->id.'/car_details/'.$car_name;

                    }

                    $driver->status = 1;
                    $driver->update();


                }
                catch (\Exception $e) {
                    $response = array(
                        'status' => 'error',
                        'message' => 'Error Has Occurred storing Car Picture',
                        'Error' => $e
                    );

                    header('Content-Type: application/json');
                    return response()->json($response);
                }
            }
            else{

                if(count($user->driver) > 0){
                    try{
                        $driver = Driver::where('user_id', '=', $user->id)->get()->first();

                        if(!empty($car)){

                           // if(file_exists(public_path().$driver->path_image) ){
                           //     unlink(public_path().$driver->path_image);
                           // }
                            $car = str_replace(' ', '+', $car);
                            \File::put($inner_path. '/' . $car_name, base64_decode($car));
                            $driver->path_image = '/images/user_information/'.$user->id.'/car_details/'.$car_name;

                        }

                        $driver->status = 1;
                        $driver->update();
                    }
                    catch (\Exception $e) {
                        Log::info($e);
                        $response = array(
                            'status' => 'error',
                            'message' => 'Error Has Occurred storing Car Picture',
                            'Error' => $e
                        );

                        header('Content-Type: application/json');
                        return response()->json($response);
                    }
                }
                //New Driver
                else{
                    try{
                        $driver = new Driver;
                        $driver->user_id = $user->id;
                        $driver->status = 1;

                        $car = str_replace(' ', '+', $car);
                        \File::put($inner_path. '/' . $car_name, base64_decode($car));
                        $driver->path_image = '/images/user_information/'.$user->id.'/car_details/'.$car_name;
                        $driver->status = 1;
                        $driver->update();

                    }
                    catch (\Exception $e) {
                        $response = array(
                            'status' => 'error',
                            'message' => 'Error Has Occurred storing Car Picture',
                            'Error' => $e
                        );
                        Log::info($e);
                        header('Content-Type: application/json');
                        return response()->json($response);
                    }
                }
            }
//
//            $reason = "New driver request";
//            $reply = "User: " . $user->first_name . " has requested to be a driver and/or change car";
//
//            $this->mailCurrentAdminds($this->adminEmails, $reply, $reason);

            $response = array(
                'status' => '1',
                'message' => 'Successfully uploaded car photo',
            );

            header('Content-Type: application/json');
            return response()->json($response);

    }
}
