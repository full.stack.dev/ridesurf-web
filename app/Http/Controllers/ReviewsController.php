<?php

namespace App\Http\Controllers;

use App\Reviews;
use App\User;
use App\Rides;
use App\UserRides;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Auth, Session;

class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reviews  $reviews
     * @return \Illuminate\Http\Response
     */
    public function show(Reviews $reviews, $id)
    {
        $reviews = Reviews::with('reviewFrom')->where('user_rate_id', '=', $id)->get();

        return response()->json($reviews);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reviews  $reviews
     * @return \Illuminate\Http\Response
     */
    public function edit(Reviews $reviews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reviews  $reviews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reviews $reviews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reviews  $reviews
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reviews $reviews)
    {
        //
    }

    public function rate(Request $request, $user, $ride){

        $loggedUser = Auth::user();

        $completeRide = Rides::find($ride);

        $driver = User::find($completeRide->user->id);

        $uRide = UserRides::where('rides_id', '=' , $ride)
                 ->where('user_id', '=', $loggedUser->id)->get()->first();

        $ride = Rides::find($ride);

        //if is Driver
        if($ride->user_id == Auth::id() ){
            if($uRide->driver_rate != null){
                Session::flash('message', "You already reviewed this ride, thank you for your review!");

                return redirect()->route('dashboarduser.index');
            }
        }

        //If is Passenger
        if($ride->user_id != Auth::id()){
            if($uRide->reviewed != null){
                Session::flash('message', "You already reviewed this ride, thank you for your review!");

                return redirect()->route('dashboarduser.index');
            }
        }
        if($loggedUser == $driver){
            $loggedUser = User::find($user);
        }else{
            $loggedUser = $driver;
        }

        $user_ride = UserRides::where('rides_id', '=', $ride)
                              ->where('user_id', '=', $loggedUser)
                              ->get()->first();



        return view('reviews.rate', compact('driver', 'loggedUser', 'ride', 'ride'));

    }

    public function storeRate(Request $request){

        

        $user_id = $request->get('user');

        $ride = $request->get('ride');
        
        
        $rideC = Rides::find($ride);

        $driver_id = $rideC->user->id;

        if(Auth::user()->id == $driver_id){

            //Driver Making Review
            $review = new Reviews;

            $review->user_rating_id = $driver_id;
            $review->user_rate_id = $user_id;
            $review->rate = $request->get('rating');
            $review->description = $request->get('description');

            $review->save();

            $count = Reviews::where('user_rate_id' , '=', $user_id)->get();

            $sumRate = 0;

            foreach($count as $user){
                $sumRate = $sumRate + $user->rate;
            }

            $userReviewed = User::find($user_id);

            if(count($count) > 0){
                $userReviewed->rating = $sumRate / count($count);
            }else{
                $userReviewed->rating = $user->rate;
            }
            $userReviewed->update();

            $user_ride = UserRides::where('rides_id', '=', $ride)
                    ->where('user_id', '=', $user_id)->get()->first();

            $user_ride->driver_rate = true;
            $user_ride->update();
            

        }else{
            //Passenger Making Review
            $userReviewed = User::find($driver_id);

            $review = new Reviews;

            $review->user_rating_id = $user_id;
            $review->user_rate_id = $driver_id;
            $review->rate = $request->get('rating');
            $review->description = $request->get('description');

            $review->save();

            $count = Reviews::where('user_rate_id' , '=', $driver_id)->get();

            $sumRate = 0;

            foreach($count as $review){
                $sumRate = $sumRate + $review->rate;
            }

            if(count($count) > 0){
                $userReviewed->rating = $sumRate / count($count);
            }else{
                $userReviewed->rating = $review->rate;
            }
            $userReviewed->update();

            $loggedUser = Auth::user();
            $user_ride = UserRides::where('rides_id', '=', $ride)
                        ->where('user_id', '=', $loggedUser->id)->get()->first();


            $user_ride->reviewed = true;
            $user_ride->update();
        
        }
        

        Session::flash('message', "Successfully reviewed user");

        return redirect()->route('dashboarduser.index');

    }
}
