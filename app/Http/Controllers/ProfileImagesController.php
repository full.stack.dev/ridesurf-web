<?php

namespace App\Http\Controllers;

use App\ProfileImages;
use Illuminate\Http\Request;

class ProfileImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProfileImages  $profileImages
     * @return \Illuminate\Http\Response
     */
    public function show(ProfileImages $profileImages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfileImages  $profileImages
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfileImages $profileImages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProfileImages  $profileImages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfileImages $profileImages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfileImages  $profileImages
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfileImages $profileImages)
    {
        //
    }
}
