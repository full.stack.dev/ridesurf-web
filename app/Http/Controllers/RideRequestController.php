<?php

namespace App\Http\Controllers;

use App\Http\Traits\LocationHelper;
use App\Http\Traits\Notifications;
use App\RideRequest;
use App\User;
use Carbon\Carbon;
use DB, Auth, Mail, View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class RideRequestController extends Controller
{
    use LocationHelper, Notifications;

    private $user, $now, $messageCount;

     public function __construct(Request $request)
    {

        $this->middleware(function ($request, $next) {
            
            $this->user = Auth::user();
            $this->now = Carbon::now();
            $this->messageCount = $this->messageCount();
            
            View::share( 'now', $this->now );
            View::share( 'user', $this->user );
            View::share('messageCount', $this->messageCount);
            return $next($request);
        });
        

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $ride_requests = RideRequest::where('user_id', '=', $user->id)
                        ->orderBy('created_at', 'ASC')->get();

        return view('dashboarduser.rideRequest', compact('user', 'ride_requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $city_from = $request->get('city_from');
        $city_to = $request->get('city_to');
        $user = Auth::user();
        $date = $request->get('date');

        $state = $this->getState($city_from);
        
        return view('rides.request', compact('user', 'city_from', 'city_to', 'state', 'date'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from = $request->get('cityFrom');
        $to = $request->get('cityTo');
        $date = $request->get('date');
        $return = $request->get('round-trip');

        
        $ride_request = new RideRequest;
        $ride_request->user_id = Auth::user()->id;
        $ride_request->city_from = $from;
        $ride_request->city_to = $to;
        $ride_request->state = $request->get('state');
        $ride_request->date = $date;
        $ride_request->save();

        if($return == 'Yes'){
            $ride_request = new RideRequest;
            $ride_request->user_id = Auth::user()->id;
            $ride_request->city_from = $to;
            $ride_request->city_to = $from;
            $ride_request->state = $request->get('state');
            $date_return = $request->get('date_return');
            $ride_request->date = $date_return;
            $ride_request->save();

            return view('rides.successrequest', compact('from', 'to', 'date', 'return', 'date_return'));

        }


        
        return view('rides.successrequest', compact('from', 'to', 'date', 'return'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RideRequest  $rideRequest
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RideRequest  $rideRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(RideRequest $rideRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RideRequest  $rideRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RideRequest $rideRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RideRequest  $rideRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rideR = RideRequest::find($id);

        $rideR->delete();

        return redirect()->back();
    }

    public function showAll(Request $request){
        $rideR = RideRequest::orderBy('created_at', 'ASC')->get();

        return view('dashboardadmin.ride-request.index', compact('rideR'));
    }

    public function showRequest(Request $request, $id){
        $rideR = RideRequest::find($id);

        return view('dashboardadmin.ride-request.show', compact('rideR'));
    }
}
