<?php

namespace App\Http\Controllers;

use App\User;
use App\Vehicle;
use App\ProfileImages;
use App\UserRides;
use App\Driver;
use App\Reviews;
use App\Rides;
use Carbon\Carbon;
use App\Providers\HelperServiceProvider;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Input;
use Auth, Storage, DB, Response, File, Mail;
use Session,Redirect, ArrayObject;

class IonicAuthController extends Controller
{

	public function __contruct() 
    {
        $this->middleware('auth');
    }

}