<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Rides;
use App\SearchLog;
use App\User;
use App\UserRides;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    public function weekly(Request $request){

        $now = Carbon::now();
        $startWeek = $now->startOfWeek()->format('Y-m-d H:i');
        $endWeek = $now->endOfWeek()->format('Y-m-d H:i');

        $usersThisWeek = User::whereBetween('created_at', [$startWeek, $endWeek])->count();
        $drivers = Driver::whereBetween('created_at', [$startWeek, $endWeek])->count();
        $ridesThisWeek = Rides::whereBetween('created_at', [$startWeek, $endWeek])->count();
        $ridesBooked = UserRides::whereBetween('created_at', [$startWeek, $endWeek])->count();
        $searches = SearchLog::whereBetween('created_at', [$startWeek, $endWeek])->count();

        $popularThisWeek = SearchLog::whereBetween('created_at', [$startWeek, $endWeek])
                                    ->groupBy('city_from')->groupBy('city_to')->get();
        foreach($popularThisWeek as $freq){
            $temp = SearchLog::whereBetween('created_at', [$startWeek, $endWeek])->where('city_from', '=', $freq->city_from)
                ->where('city_to', '=', $freq->city_to)->count();
            $freq->count = $temp;
        }

        $popularThisWeek = $popularThisWeek->sortByDesc('count')->take(10);

        return view('dashboardadmin.settings.stats', compact('usersThisWeek', 'drivers', 'popularThisWeek',
                                                                    'ridesThisWeek', 'ridesBooked', 'searches', 'startWeek', 'endWeek'));

    }
}
