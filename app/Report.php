<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function ride(){
        return $this->belongsTo('App\Rides', 'rides_id');
    
    }
    public function user_passenger(){
        return $this->belongsTo('App\User', 'passenger_id');
    
    }

}
