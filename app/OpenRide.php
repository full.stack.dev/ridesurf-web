<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenRide extends Model
{
    protected $fillable = ['price', 'open_date_from', 'open_date_to', 'seats', 'luggage_size', 'description', 'seats_available', 'luggage_amount', 'location_from', 'location_to', 'duration', 'ladies_only', 'city_from', 'city_to', 'type_purpose_id'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function trip_purpose(){
        return $this->hasMany('App\TripPurpose', 'id');
    }

    public function hours(){
        return $this->hasMany('App\OpenRideHours', 'open_rides_id');
    }

    public function user_ride(){
        return $this->hasMany('App\UserOpenRide', 'open_ride_id');
    }
}

