<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRides extends Model
{
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    
    }

    public function ride(){
        return $this->belongsTo('App\Rides', 'rides_id');
    
    }
}
