<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use HasApiTokens;
    
    protected $privilege = ['superuser', 'Admin', 'moderator'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'password', 'email_token', 'phone_number', 'last_name',
        'gender', 'birthday', 'state', 'country', 'city', 'address', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isPrivileged()
    {
        foreach ($this->privilege as $role) {
            if ($this->hasRole($role)) {
                return true;
            }
        }
        return false;
    }

    public function profileImages(){
        return $this->hasMany('App\ProfileImages', 'user_id');
    
    }

    public function ride(){
        return $this->hasMany('App\Rides', 'user_id');
    
    }

    public function driver(){
        return $this->hasMany('App\Driver', 'user_id');
    
    }

    public function UserRide(){
        return $this->hasMany('App\UserRides', 'user_id');
    
    }

    public function userPreferences(){
        return $this->hasMany('App\UserPreferences', 'user_id');
    
    }

    public function reviewFrom(){
        return $this->hasMany('App\Reviews', 'user_rating_id');
    
    }

    public function reviewTo(){
        return $this->hasMany('App\Reviews', 'user_rate_id');
    
    }

    public function report(){
        return $this->hasMany('App\Report', 'passenger_id');
    }

    public function rideRequested(){
        return $this->hasMany('App\RideRequest', 'user_id');
    
    }

    public function openRide(){
        return $this->hasMany('App\OpenRide', 'user_id');

    }
    public function user_open_ride(){
        return $this->hasMany('App\UserOpenRide', 'user_id');

    }

    public function hasFourEmpty(){
        $i=0;
        if($this->zipcode == null) $i++;
        if($this->state == null) $i++;
        if($this->city == null) $i++;
        if($this->address == null) $i++;

        if($i > 3){
            return true;
        }else{
            return false;
        }

    }
}
