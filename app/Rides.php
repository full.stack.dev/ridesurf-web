<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class Rides extends Model
{
    protected $fillable = ['price', 'dateFrom', 'seats', 'luggage_size', 'description', 'trip_purpose_id','luggage_amount', 'startHour', 'location_from', 'location_to', 'seats_available', 'ride_type', 
        'duration', 'ladies_only', 'city_from', 'city_to'];

	use Notifiable;
    use HasRoles;
    use HasApiTokens;

    public function connections(){
        return $this->belongsTo('App\Connections', 'connection');
    
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function user_ride(){
        return $this->hasMany('App\UserRides', 'rides_id');
    
    }
    public function ride_stop(){
        return $this->hasMany('App\RidesStop', 'rides_id');
    
    }
    public function reported(){
        return $this->hasMany('App\Report', 'rides_id');
    }

    public function cancel_ride(){
        return $this->hasMany('App\CancelUserRides', 'rides_id');
    }

    public function trip_purpose(){
        return $this->hasMany('App\TripPurpose', 'id', 'trip_purpose_id');
    }
    public function airport_details(){
        return $this->hasOne('App\AirportDetails', 'ride_id', 'id');
    }
}
