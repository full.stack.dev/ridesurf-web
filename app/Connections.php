<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Connections extends Model
{
    public function rides(){
        return $this->hasMany('App\Rides');
    
    }
}
