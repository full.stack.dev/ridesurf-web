<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancelUserRides extends Model
{
    public function ride(){
        return $this->belongsTo('App\Rides', 'rides_id');
    }
}
