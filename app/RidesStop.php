<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RidesStop extends Model
{
	protected $fillable = ['address', 'city_name', 'duration', 'price_from', 
						   'seats_from', 'price_to', 'seats_to'];

    public function ride_stop(){
    	
        return $this->belongsTo('App\RidesStop', 'rides_id');
    
    }
}
