/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


window.Vue = require('vue');
require('./bootstrap');
require('vue-flash-message/dist/vue-flash-message.min.css');
import 'vue-loading-overlay/dist/vue-loading.css';


import VueRouter from 'vue-router';
import App from './components/App';
import CreateRides from "./components/CreateRides";
import RideApp from './components/RideApp';
import moment from 'moment';

import VueRouterMultiView from 'vue-router-multi-view';
import Popover  from 'vue-js-popover'
import VueFlashMessage from 'vue-flash-message';
import { Cropper } from 'vue-advanced-cropper';
import 'cropperjs/dist/cropper.css';

import Loading from 'vue-loading-overlay';
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';


import Rides from './components/dashboarduser/rides/rides.vue';
import EditOpenRide from './components/dashboarduser/rides/editopenride.vue';
import JoinRide from './components/open-ride/JoinRide.vue';
import JoinNormalRide from './components/rides/JoinNormalRide.vue';
import ImageUploader from 'vue-image-upload-resize'
import Ridelist from './components/rides/ridelist.vue';
import AirportRideCreate from "./components/rides/airport/AirportRideCreate";
import OneWay from "./components/rides/normal/one-way";
import RoundTrip from "./components/rides/normal/round-trip";
import CreateOpenRides from './components/open-ride/CreateOpenRide.vue';
import CreateAirportRide from './components/rides/airport/AirportRideCreate';
import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(ImageUploader);
Vue.use(VueFlashMessage);
Vue.use(Popover);
Vue.use(VueRouter);
Vue.use(VueRouterMultiView);
Vue.component(Cropper);
Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
Vue.use(Loading);
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M',
        libraries: 'places',//// If you need to use place input
    }
});

let router = new VueRouter({
    routes:[
        {
            path: '/create-open-ride',
            component: require('./components/open-ride/CreateOpenRide.vue')
        },
        {
            path:'/success-open-ride',
            name: 'SuccessCreation',
            component: require('./components/open-ride/SuccessCreation.vue')
        },
        {
            path:'/success-open-ride-request',
            name: 'SuccessRequest',
            component: require('./components/open-ride/SuccessRequest.vue')
        },
        {
            path:'/',
            name:'Rides',
            component:Rides,

        },
        {
            path:'/open-rides/edit',
            name:'EditOpenRide',
            component:EditOpenRide,
        },
        {
            path:'/profile',
            name:'profile',
        },

    ]

});



let createRouter = new VueRouter({
    routes:[
        {
            path:'/',
            name:'oneway',
            component: OneWay,
            props: { default: true}
        },
        {
            path:'/round-trip',
            name:'round-trip',
            component: RoundTrip,
            props: { default: true}
        },
        {
            path:'/open-rides',
            name:'openrides',
            component: CreateOpenRides,
            props: { default: true}
        },
        {
            path:'/airport-rides',
            name:'createAirportRide',
            component: CreateAirportRide,
            props: { default: true}
        },
    ]
});

let searchRoutes = new VueRouter({
    routes:[
        {
            path:'/',
            name:'Ridelist',
            component: Ridelist,
            props: { default: true}
        },
        {
            path:'/open-rides-join',
            name:'JoinOpenRide',
            component: JoinRide,
             props: true
        },
        {
            path:'/rides-join',
            name:'joinnormalride',
            component: JoinNormalRide,
            props: true
        }
        // {
        //     path:'/airport-ride-create',
        //     name:'airportridecreate',
        //     component: AirportRideCreate,
        //      props: true
        // }
    ]
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('reviews', require('./components/dashboarduser/reviews/reviews.vue').default);
Vue.component('JoinRide', require('./components/open-ride/JoinRide.vue').default);
Vue.component('editopenride', require('./components/dashboarduser/rides/editopenride.vue').default);
Vue.component('profile', require('./components/dashboarduser/profile/profile.vue').default);
Vue.component('ridescard', require('./components/rides/RidesCard.vue'));
Vue.component('newdriver', require('./components/dashboarduser/driver/NewDriver.vue').default);
Vue.component('changecar', require('./components/dashboarduser/driver/ChangeCar.vue').default);
Vue.component('openridelist', require('./components/rides/OpenRideList.vue'));
Vue.component('cropperUpload', require('./components/dashboarduser/driver/cropperUpload.vue').default);
Vue.component('reviews', require('./components/dashboarduser/reviews/reviews.vue').default);
Vue.component('carUploader', require('./components/dashboarduser/driver/carUploader.vue').default);
Vue.component('RouteMenu', require('./components/rides/RouteMenu.vue').default);

// Vue.component('airportride', require('./components/rides/airport/AirportRideCreate').default);
// Vue.component('searchbar', require('./components/rides/SearchBar.vue').default);
// Vue.component('RideList', require('./components/rides/RideList.vue').default);
// Vue.component('create-open-ride', require('./components/open-ride/CreateOpenRide.vue').default);
//Vue.component('Rides', require('./components/dashboard/rides/rides.vue'));


Vue.filter('formatTime', function(value) {
    if (value) {
        return moment(String(value), "h:mm:ss ").format('hh:mm a')
    } else {
        return "unknown"
    }
});

Vue.filter('formatDate', function(value) {
    if (value) {
        console.log(value);
        return moment(String(value)).format('MM/DD/YYYY')
    } else {
        return "unknown"
    }
});

Vue.filter('getCity', function(city) {
    var temp = city.split(",");

      if(temp.length == 3){
        return temp[0];
      }
      if(temp.length == 4){
        return temp[1];
      }
      else{
        return city;
      }
});

const app = new Vue({
    el: '#app',
    components: { App },
    router: router,
});

const rides = new Vue({
    el: '#search',
    components: { RideApp },
    router: searchRoutes,
});

const create = new Vue({
    el: '#create',
    components: { CreateRides },
    router: createRouter,
});


