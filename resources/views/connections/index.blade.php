@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Connection Management</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('connections.create') }}"> Create New Connection</a>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
 <tr>
   <th>Connection Name</th>
   <th>Location (FROM)</th>
   <th>Location (TO) </th>
   <th>Price(Suggested)</th>
   <th>Duration</th>
   <th width="280px">Action</th>
 </tr>
 @foreach ($data as $key => $connection)
  <tr>
    <td>{{ $connection->first_name }}</td>
    <td>{{ $connection->location_id_one }}</td>
    <td>{{ $connection->location_id_two }}</td>
    <td>${{ $connection->price }}</td>
    <td>{{ $connection->duration }}</td>
    <td>
       <a class="btn btn-info" href="{{ route('connections.show',$connection->id) }}">Show</a>
       <a class="btn btn-primary" href="{{ route('connections.edit',$connection->id) }}">Edit</a>

 
        {!! Form::open(['method' => 'DELETE','route' => ['connections.destroy', $connection->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </td>
  </tr>
 @endforeach
</table>

@endsection