<!-- Create Locations -->
@extends('layouts.app')


@section('content')

	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Edit New Connection</h2>
	        </div>
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('connections.index') }}"> Back</a>
	        </div>
	    </div>
	</div>

	@if (count($errors) > 0)
	  <div class="alert alert-danger">
	    <strong>Whoops!</strong> There were some problems with your input.<br><br>
	    <ul>
	       @foreach ($errors->all() as $error)
	         <li>{{ $error }}</li>
	       @endforeach
	    </ul>
	  </div>
	@endif


	{!! Form::model($data, ['method' => 'PUT','route' => ['connections.update', $data->id]]) !!}
	
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Name:</strong>
	            {!! Form::text('name', $data->first_name, array('placeholder' => 'Name','class' => 'form-control')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group" id="from">
	            <strong>From</strong>
	            {!! Form::text('location_id_one', $data->location_id_one, array('placeholder' => 'FROM','class' => 'form-control', 'id' => 'location_from')) !!}
	            <div id="location_from_list">
    			</div>
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group" id="to">
	            <strong>To</strong>
	            {!! Form::text('location_id_two', $data->location_id_two, array('placeholder' => 'TO','class' => 'form-control', 'id' => 'location_to')) !!}
	            <div id="location_to_list">
    			</div>
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Price</strong>
	            {!! Form::text('price', $data->price, array('placeholder' => 'Price','class' => 'form-control', 'id' => 'prices')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Duration</strong>
	            {!! Form::number('duration', $data->duration, array('placeholder' => 'duration','class' => 'form-control', 'id' => 'prices')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Miles</strong>
	            {!! Form::number('miles', $data->miles, array('placeholder' => 'Miles','class' => 'form-control', 'id' => 'miles')) !!}
	        </div>
	    </div>
	    
	    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
	        <button type="submit" class="btn btn-primary">Submit</button>
	    </div>
	</div>
	{!! Form::close() !!}

	<script>
		$(document).ready(function(){

		 $('#location_from').keyup(function(){ 
		        var query = $(this).val();
		        if(query != '')
		        {
		         var _token = $('input[name="_token"]').val();
		         $.ajax({
			          url:"{{ route('connections.fetch') }}",
			          method:"POST",
			          data:{query:query, _token:_token},
			          success:function(data){
			           $('#location_from_list').fadeIn();  
			                    $('#location_from_list').html(data);
			          }
		         });
		        }
		    });

		    $(document).on('click', '#from li', function(){  
		        $('#location_from').val($(this).text());  
		        $('#location_from_list').fadeOut();  
		    }); 

		    $(document).on('click', '#to li', function(){  
		        $('#location_to').val($(this).text());  
		        $('#location_to_list').fadeOut();  
		    }); 

		    $('#location_to').keyup(function(){ 
		        var query = $(this).val();
		        if(query != '')
		        {
		         var _token = $('input[name="_token"]').val();
		         $.ajax({
			          url:"{{ route('connections.fetch') }}",
			          method:"POST",
			          data:{query:query, _token:_token},
			          success:function(data){
			           $('#location_to_list').fadeIn();  
			                    $('#location_to_list').html(data);
			          }
		         });
		        }
		    }); 

		});
	</script>

@endsection 