@extends('layouts.app')
@section('content')

<div class="container">
	<div class="container-fluid offer-ride ">
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="profile-detail-container bordered-container">
					<div style="text-align: center;">
						@if( $user->avatar)
							<img src="{{ $user->avatar }}"  width="150px"
						@endif
			             	height="150px" id="public_image" class="img-circle">
			            <h3 class="profile-name">{{$user->first_name}} {{$user->last_name}}</h3>
			            <p>{{$user->city}}, {{$user->state}}, {{$user->country}}</p>
			            <div class="stars">
			    		    <span data-rating="1" class="fa fa-star "></span>
			    		    <span data-rating="2" class="fa fa-star "></span>
			    		    <span data-rating="3" class="fa fa-star "></span>
			    		    <span data-rating="4" class="fa fa-star "></span>
			    		    <span data-rating="5" class="fa fa-star"></span>
		    		  	</div>
	    		  	</div>
			    </div>

				<div class="profile-detail-container bordered-container">
		          <span class="title">Verified Info</span>
		          <ul class="verified-info">
		            @if($user->verified == 0)
		              <li class="uncheck-list">Email not verified</li>
		              <a href="{{route('dashboarduser.sendverification')}}" class="btn btn-success">
		                Send Verification
		              </a>
		            @else
		              <li class="check-list">Email Address</li>
		            @endif
		            @if( $user->avatar == null)
		              <li class="uncheck-list"> <strong> No profile image</strong></li>
		            @else
		              <li class="check-list">Profile Image</li>
		            @endif
		            @if($user->phone_number == null)
		              <li class="uncheck-list"> <strong> No phone number</strong></li>
		            @else
		              <li class="check-list">Phone Number</li>
		            @endif
		            @if($user->address == null)
		              <li class="uncheck-list">Address not provided</li>
		            @else
		              <li class="check-list">Address Provided</li>
		            @endif
		          </ul>
		        </div>

		        <div class="profile-detail-container bordered-container">
		          @include('dashboarduser.layout.preferences')
		          <!-- This button is here so the layout preferences can be use in other places -->
		          
		          <div class="container"></div>
		        </div>
		        
		        <div class="profile-detail-container bordered-container">
		            <span class="title">Member Since</span>
		            <span class="activity">{{ date('m-d-Y', strtotime($user->created_at))  }}</span>
		        </div>
			</div>

			<div class="col-md-8">
				<div class="full-margin-25">
					<div class="heading-underline">
						<h3 class="bottom-pad"><i class="fa fa-user"></i> About Me</h3>
						<p class="bottom-pad">{{$user->description}}</p>
					</div>	

					<div style="margin-top: 50px;">
				        <h3 class="bottom-pad"><i class="fa fa-pencil-square-o"></i> Reviews</h3> 
				      
				        @if(count($reviews) > 0)

				          @foreach($reviews as $review)
				          <div class="col-md-12 reviews-container">
				            
				            <div class="col-md-3">
					          	@if($review->reviewFrom->avatar != null)
				              	<img src="{{ $review->reviewFrom->avatar_original }}"  width="80px" 
				             	height="80px" id="public_image" class="img-circle">
				            	@endif

					            <p>{{ $review->reviewFrom->first_name }} </p>
					            <p class="small-light"> {{ $review->created_at->diffForHumans(null, true).' ago' }}</p>
				        	</div>
				        	<div class="col-md-9">
					            <p> {{ $review->description }} </p>
					            <div id="stars-{{ $review->id }}">
								    <span data-rating="1" class="fa fa-star"></span>
								    <span data-rating="2" class="fa fa-star"></span>
								    <span data-rating="3" class="fa fa-star"></span>
								    <span data-rating="4" class="fa fa-star "></span>
								    <span data-rating="5" class="fa fa-star"></span>
								</div>

								<script type="text/javascript">
								$( document ).ready(function() {

									var stars = $('#stars-' + {{ $review->id }});

									@if($review->rate == null){
										var value = 0;
									}@else{
										var value = ( {{ $review->rate }} );
									}
								   	@endif

									// Remove class for selected stars
									stars.find('.checked').removeClass(' checked');
									// Add class to the selected star and all before
									for (i = 1; i <= value; i++) {
										stars.find('[data-rating="' + i + '"]').addClass('checked');
									}
								});
								</script>
				        	</div>
				          </div>
				          @endforeach

				        @else
				          <p>User has no reviews yet.</p>
				        @endif
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

			var stars = $('.stars');
	    var star  = stars.find('.fa-star');
      @if($user->rating != null)
	     var value = {{ $user->rating }};
      @else
        var value = 0;
      @endif

			// Remove class for selected stars
			  stars.find('.checked').removeClass(' checked');

			  // Add class to the selected star and all before
			  for (i = 1; i <= value; i++) {
			    stars.find('[data-rating="' + i + '"]').addClass('checked');
			  }
		});
</script>
@endsection
