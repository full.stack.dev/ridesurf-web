@extends('layouts.app')
@section('content')
<style>
   /* Set the size of the div element that contains the map */
  #map {
    height: 400px;  /* The height is 400 pixels */
    width: 100%;  /* The width is the width of the web page */
   }
</style>

<div class="container">
  <div class="container-fluid offer-ride">

    @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
    @endif
  		
    <!-- Left Side -->  
    <div class="col-md-8 col-lg-8">

      <h3 class="bottom-pad"> Ride Details </h3>
										
      <div class="row bottom-margin">  
        <div class="col-md-4" id="starts">
          <p class="small-light"> From </p>
          <p id="from-fetch"> {{ $ride->location_from }}</p> <!-- Important ID Do not delete -->
        </div>
        
        <div class="col-md-4" id="ends">
          <p class="small-light"> To </p>
          <p id="to-fetch" >{{ $ride->location_to }}</p> <!-- Important ID Do not delete -->
               
          <div id="stop_list">
            @if(count($ride->ride_stop))
              <p class="small-light"> Stop Over </p>
              <p id="stopOver">{{ $ride->ride_stop[0]->address }}</p> <!-- Important ID Do not delete -->
            @endif
          </div>
        </div>      
      </div>

      <div class="row bottom-margin">
        <div class="col-md-4">
          <p class="small-light"> Date </p>
          <p>{{ $ride->dateFrom }}</p>
        </div>
        <div class="col-md-4">
          <p class="small-light"> Price Per Seat</p>
            @if($ride->price != null)
              <p>  ${{ ($ride->price) }}</p>
            @endif
        </div>
        <div class="col-md-4">
          <p class="small-light">Seats Available</p>
          <p>{{ $ride->seats }}</p>
        </div>
      </div>  

      <div class="row bottom-margin">
        <div class="col-md-4">
          <p class="small-light"> Luggage Size</p>
          <p>{{ $ride->luggage_size }}</p>
        </div>
        <div class="col-md-4">
          <p class="small-light"> Luggage Amount</p>
          <p>{{ $ride->luggage_amount }}</p>
        </div>
        <div class="col-md-4">
          <p class="small-light"> Departure </p>
          <p>{{ date("g:i a", strtotime($ride->startHour)) }}</p>
        </div>
      </div>
            
      <div class="row bottom-margin">
        <div class="col-md-12">
          <p class="small-light"> Description </p>
          <p>{{ $ride->description }}</p>
        </div>
      </div>

      <div class="widget" style="height: 370px">
        @include('layouts.rides.rideSchedule', [$ride, $usersRiding, $end])
      </div>

    </div>
				
    <!-- Right Side / Route Map -->    
		<div class="col-md-4 col-lg-4">
			<div class="widget" id="ChangeMap">
				<h3> Route </h3>
				<div id="map"></div>
			</div>
		</div>

  </div>
</div>



<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4xgat2b-KuVCq2YxRLtzodrjiAuhMojc&libraries=places&callback=initMap">
    </script>
    <script>
          var map, infoWindow;
          function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: -37.0902, lng: 95.7129},
              zoom: 6
            });

            // Create the search box and link it to the UI element.
            var inputFrom = document.getElementById('from-fetch');
            var searchBoxFrom = new google.maps.places.SearchBox(inputFrom);

            var inputTo= document.getElementById('to-fetch');
            var searchBoxTo = new google.maps.places.SearchBox(inputTo);

            
            var inputStop = document.getElementById('stopOver');
            if(inputStop != null){
              var searchBoxStop = new google.maps.places.SearchBox(inputStop);
            }
            
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                  var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                  };
                  
                  map.setCenter(pos);
                }); 
            } else {
              // Browser doesn't support Geolocation
              handleLocationError(false, infoWindow, map.getCenter());
            }

            var geocoder = new google.maps.Geocoder();
            var address1 = "{{ $locationFrom}}";
            var address2 = "{{ $locationTo}}";

            var latitudefrom,longitudefrom;
            var latitudeto,longitudeto;
            var latitudeStop, longitudeStop;
            

            var directionsDisplay = new google.maps.DirectionsRenderer;
            var directionsService = new google.maps.DirectionsService;
            var StopOver = jQuery('#stopOver').val();
            var end = document.getElementById('to-fetch').value;

            geocoder.geocode( { 'address': address1}, function(results, status) {

              if (status == google.maps.GeocoderStatus.OK) {
                latitudefrom = results[0].geometry.location.lat();
                longitudefrom = results[0].geometry.location.lng();
                console.log(latitudefrom + " " + longitudefrom);
              } 
            });
            
            geocoder.geocode( { 'address': address2}, function(results, status) {

              if (status == google.maps.GeocoderStatus.OK) {
                latitudeto = results[0].geometry.location.lat();
                longitudeto = results[0].geometry.location.lng();
                
                    var waypts=[];
                    var from = new google.maps.LatLng(latitudefrom, longitudefrom);
                    var fromName = "{{ $locationFrom }}";
                    var dest = new google.maps.LatLng(latitudeto, longitudeto);
                    var destName = "{{ $locationTo }}";

                    if(StopOver != null){
                      geocoder.geocode( { 'address': StopOver }, function(results, status) {

                      if (status == google.maps.GeocoderStatus.OK) {
                        latitudeStop = results[0].geometry.location.lat();
                        longitudeStop = results[0].geometry.location.lng();

                        waypts.push({
                          location: {lat: latitudeStop, lng: longitudeStop},
                          stopover: true,
                        });

                        setTimeout(function() {
                        calculateAndDisplayRoute(directionsService, directionsDisplay,latitudefrom, longitudefrom,
                        latitudeto, longitudeto, waypts)}, 2000);

                        directionsDisplay.setMap(map);
                        console.log("Waypoints");
                        console.log(waypts);

                        console.log('We enter in the stop over is not null');
                        var service = new google.maps.DistanceMatrixService();

                        service.getDistanceMatrix({
                            origins: [from, fromName],
                            destinations: [destName, dest],
                            
                            travelMode: 'DRIVING'
                        }, callback);

                      function callback(response, status) {
                        if (status == 'OK') {
                          
                          
                        }
                      }
                      } 
                    });
                    }else{

                    var service = new google.maps.DistanceMatrixService();
                    service.getDistanceMatrix({
                            origins: [from, fromName],
                            destinations: [destName, dest],
                            travelMode: 'DRIVING'
                    }, callback);
                    var waypts = null;
                    
                    setTimeout(function() {
                    calculateAndDisplayRoute(directionsService, directionsDisplay,latitudefrom, longitudefrom,
                    latitudeto, longitudeto, waypts)}, 2000);

                    directionsDisplay.setMap(map);
                    console.log('Enter in stop over null');
                    function callback(response, status) {
                        if (status == 'OK') {
                          
                          
                        }
                      }
                    }
              } 
            });

            infoWindow = new google.maps.InfoWindow;
          }

          function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                  'Error: The Geolocation service failed.' :
                                  'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
          }


              function calculateAndDisplayRoute(directionsService, directionsDisplay,latitudefrom, longitudefrom,
                  latitudeto, longitudeto, waypts) {

                console.log("From " + latitudefrom + " " + longitudefrom);
                console.log('To ' + latitudeto + " " + longitudeto);
                directionsService.route({
                  origin: {lat: latitudefrom, lng: longitudefrom},  // Haight.
                  destination: {lat: latitudeto, lng: longitudeto},  // Ocean Beach.
                  waypoints: waypts, // stop Over
                  // Note that Javascript allows us to access the constant
                  // using square brackets and a string value as its
                  // "property."

                  travelMode: google.maps.TravelMode['DRIVING']
                }, function(response, status) {
                  if (status == 'OK') {
                    console.log(response);
                    directionsDisplay.setDirections(response);
                    var start = response.routes[0].legs[0].start_address;
                    if(waypts != null){
                      var stopOver = response.routes[0].legs[0].end_address;
                      var end = response.routes[0].legs[1].end_address;
                    }else{
                      var end = response.routes[0].legs[0].end_address;
                    }

                    //If is a Stop Over
                    if(waypts != null){
                      var distance_text =response.routes[0].legs[1].distance.value;
                      console.log(distance_text);
                      var distance = distance_text * 0.000621371192; // Transform Meters to Miles

                      var parseFrom = start.split(',');
                      if(parseFrom.length == 5){
                        var cityFrom = parseFrom[2] + ", "+ parseFrom[3].match(/[a-zA-Z]+/g);
                      }
                      if(parseFrom.length == 4){  
                        var cityFrom = parseFrom[1] + ", "+ parseFrom[2].match(/[a-zA-Z]+/g);
                      }
                      if(parseFrom.length == 3){
                        var cityFrom = parseFrom[0] + ", "+ parseFrom[1].match(/[a-zA-Z]+/g);
                      }

                      var parseFrom = end.split(',');
                      if(parseFrom.length == 5){
                        var cityTo = parseFrom[2] + ", "+ parseFrom[3].match(/[a-zA-Z]+/g);
                      }
                      if(parseFrom.length == 4){  
                        var cityTo = parseFrom[1] + ", "+ parseFrom[2].match(/[a-zA-Z]+/g);
                      }
                      if(parseFrom.length == 3){
                        var cityTo = parseFrom[0] + ", "+ parseFrom[1].match(/[a-zA-Z]+/g);
                      }

                      if(waypts != null){
                      var parseFrom = stopOver.split(',');
                        if(parseFrom.length == 5){
                          var cityStopOver = parseFrom[2] + ", "+ parseFrom[3].match(/[a-zA-Z]+/g);
                        }
                        if(parseFrom.length == 4){  
                          var cityStopOver = parseFrom[1] + ", "+ parseFrom[2].match(/[a-zA-Z]+/g);
                        }
                        if(parseFrom.length == 3){
                          var cityStopOver = parseFrom[0] + ", "+ parseFrom[1].match(/[a-zA-Z]+/g);
                        }
                      }
                      if(waypts != null){
                        var totoalTime = response.routes[0].legs[1].duration.value + 
                        response.routes[0].legs[0].duration.value;
                        var totalDistance = response.routes[0].legs[1].distance.value + 
                        response.routes[0].legs[0].distance.value;
                        var stop_over_duration = response.routes[0].legs[0].duration.value;
                        console.log(stop_over_duration);

                      }else{

                        return false;
                      }

                  }
                  //It is not a Stop Over 
                  else{

                    var distance_text =response.routes[0].legs[0].distance.value;
                      console.log(distance_text);

                      var distance = distance_text * 0.000621371192; // Transform Meters to Miles

                      var parseFrom = start.split(',');
                      if(parseFrom.length == 5){
                        var cityFrom = parseFrom[2] + ", "+ parseFrom[3].match(/[a-zA-Z]+/g);
                      }
                      if(parseFrom.length == 4){  
                        var cityFrom = parseFrom[1] + ", "+ parseFrom[2].match(/[a-zA-Z]+/g);
                      }
                      if(parseFrom.length == 3){
                        var cityFrom = parseFrom[0] + ", "+ parseFrom[1].match(/[a-zA-Z]+/g);
                      }

                      var parseFrom = end.split(',');
                      if(parseFrom.length == 5){
                        var cityTo = parseFrom[2] + ", "+ parseFrom[3].match(/[a-zA-Z]+/g);
                      }
                      if(parseFrom.length == 4){  
                        var cityTo = parseFrom[1] + ", "+ parseFrom[2].match(/[a-zA-Z]+/g);
                      }
                      if(parseFrom.length == 3){
                        var cityTo = parseFrom[0] + ", "+ parseFrom[1].match(/[a-zA-Z]+/g);
                      }

                  }
                    
                  } else {
                    window.alert('Directions request failed due to ' + status);
                  }
                });
              }


          //Add Marker of Stop Over
          function codeAddress(geocoder, map, address) {

              geocoder.geocode({'address': address}, function(results, status) {
                if (status === 'OK') {
                  console.log(map);
                  var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    var latlng = new google.maps.LatLng(latitude, longitude);

                  var marker = new google.maps.Marker({

                         map: map,
                         position: latlng,
                         title: 'Stop Over'

                     });


                } else {
                  console.log('Geocode was not successful for the following reason: ' + status);
                }
              });
            }


          function calcRoute(start, end) {

            var directionsService = new google.maps.DirectionsService();
            var request = {
              origin:start,
              destination:end,
              travelMode: google.maps.TravelMode.DRIVING
            };

            directionsService.route(request, function(result, status) {
              if (status == google.maps.DirectionsStatus.OK) {
                var distance_text =result.routes[0].legs[0].distance.value;
              console.log(distance_text);
              var distance = distance_text * 0.000621371192; // Transform Meters to Miles

              var parseFrom = start.split(',');
                  if(parseFrom.length == 5){
                    var cityStopOver = parseFrom[2] + ", "+ parseFrom[3].match(/[a-zA-Z]+/g);
                  }
                  if(parseFrom.length == 4){  
                    var cityStopOver = parseFrom[1] + ", "+ parseFrom[2].match(/[a-zA-Z]+/g);
                  }
                  if(parseFrom.length == 3){
                    var cityStopOver = parseFrom[0] + ", "+ parseFrom[1].match(/[a-zA-Z]+/g);
                  }


                jQuery('#ChangeMap').append(inputs);
                
              }
            });

          }

    </script>

 @endsection