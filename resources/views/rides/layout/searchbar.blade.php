@if($control == true)
	<div style="margin-bottom: 30px;">
		<!-- If user has searched for a ride -->
		<div class="container-fluid search-ride-form no-mobile">
			<div class="widget user-dashboard-profile form-group ">
				{!! Form::open(array('route'=>'rides.search','method'=>'GET', 'class'=>'online-form')) !!}

				<div class="row">
					<!-- DO NOT REMOVE ID's OF FORM -->
					<div class="col-md-3" id="starts">

						{!! Form::text('start', null, array('placeholder' => 'From',
						'class' => 'form-control', 'id' => 'from-fetch')) !!}

					</div>
					<div class="col-md-3" id="ends">

                  		{!! Form::text('stop', null, array('placeholder' => 'To','class' => 'form-control', 'id' => 'to-fetch')) !!}
              		</div>
					<div class="col-md-3" >
						<input type="date" value="{{$date}}" name="date" onfocus="(this.type='date')" id="dateFrom"
						placeholder="Date"  class="form-control">
						<input type="number" value="{{ $number }}" style="display: none;">
					</div>
					<div class="col-md-3">
						<button type="submit" class="form-control ridesearch" id="findButton strLoader">Search</button>
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-12">
						<style type="text/css">
							/* This is style is not appering in the live production */
							.search_filters{
							    color: #555;
							    float: left;
							    padding: 4px 20px 4px 0;
							}
							.search_filters label{
							    font-weight: normal;
							    padding: 4px 10px 4px 10px;

							}
						</style>
						<div class="search_filters">
							<label><strong>Filter Results:</strong></label>
						</div>
						<div class="search_filters">
							<input  type="checkbox" name="ladies_only"><label>Women Only</label>
						</div>
						<div class="search_filters">
							<label>Max Price</label>
							<span class="glyphicon glyphicon-minus control_price" id="minus_max"></span>
							<input name="max_price" type="number" class="price_input" id="max_price">
							<span class="glyphicon glyphicon-plus control_price" id="max_max"></span>
						</div>
					</div>

				</div>
				{!! Form::close() !!}
		</div>
	</div>

@else

	<!-- User needs to search for Rides -->
	<div class="container-fluid search-ride-form">

		<div class="widget user-dashboard-profile form-group">

			{!! Form::open(array('route'=>'rides.search','method'=>'GET', 'class'=>'online-form')) !!}

				<div class="row">
					<!-- DO NOT REMOVE ID's OF FORM -->
					<div class="col-md-3" id="starts">

						{!! Form::text('start', null, array('placeholder' => 'From',
						'class' => 'form-control', 'id' => 'from-fetch')) !!}

					</div>
					<div class="col-md-3" id="ends">

                  		{!! Form::text('stop', null, array('placeholder' => 'To','class' => 'form-control', 'id' => 'to-fetch')) !!}
              		</div>
					<div class="col-md-3" >
						<input type="date" name="date" onfocus="(this.type='date')" id="dateFrom"
						placeholder="Date"  class="form-control">

					</div>
					<div class="col-md-3">
						<button type="submit" class="form-control ridesearch"
						id="strLoader">Search</button>
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-12">
						<style type="text/css">
							/* This is style is not appering in the live production */
							.search_filters{
							    color: #555;
							    float: left;
							    padding: 4px 20px 4px 0;
							}
							.search_filters label{
							    font-weight: normal;
							    padding: 4px 10px 4px 10px;

							}
						</style>
						<div class="search_filters">
							<label><strong>Filter Results:</strong></label>
						</div>
						<div class="search_filters">
							<input  type="checkbox" name="ladies_only"><label>Women Only</label>
						</div>
						<div class="search_filters">
							<label>Max Price</label>
							<span class="glyphicon glyphicon-minus control_price" id="minus_max"></span>
							<input name="max_price" type="number" class="price_input" id="max_price">
							<span class="glyphicon glyphicon-plus control_price" id="max_max"></span>
						</div>
					</div>

				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endif
