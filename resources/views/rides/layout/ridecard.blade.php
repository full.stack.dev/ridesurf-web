<div class="row rides-list ride-list" id="rides_list">
	<div class="col-md-2">
		{!! Form::open(array('route' => ['rides.join', $ride->id, $ride->location_from, $ride->location_to,     $ride->price] ,'method'=>'GET')) !!}
		<h4 class="title">Driver</h4>
		<div class="profile-pic-sm" style="text-align: center;">
			<img src="{{ $ride->user->avatar }}" >
			<p align="center">{{ $ride->first_name }} {{ $ride->last_name }}</p>
			@include('layouts.rating', [$ride->rating, $ride->id] )
			<a href="{{ route('rides.viewUserProfile', $ride->user_id) }}"
			class="btn btn-info x-slim-button">View Profile</a>
		</div>
	</div>

	<div class="col-md-2 no-mobile">
		<h4 class="title">Vehicle</h4>
		<div class="profile-pic-sm" style="text-align: center;">
			<img src="{{ $ride->user->driver[0]->path_image }}" width="50px" class="img-circle"><br />
			<span class="add-id"> {{$ride->make}}</span><br/>
			<span class="add-id">  {{$ride->model}}</span> <br/>
			<span class="add-id"> {{$ride->year}}</span> <br/>
		</div>
	</div>

	<div class="col-md-6">
		<h4 class="title">Drive Info</h4>
		<div class="margin-left-20">
			<p><i class="fa fa-map-marker ride-card-icon"></i><strong class="bold-light">From:</strong> {{ $ride->location_from }}</p>
			<p><i class="fa fa-map-marker ride-card-icon"></i><strong class="bold-light">To:</strong> {{ $ride->location_to }} </p> 
			<p><i class="fa fa-calendar ride-card-icon"></i> {{ date('m/d/Y', strtotime($ride->dateFrom)) }}</p>
			<p><i class="fa fa-clock-o ride-card-icon"></i> {{date("g:i a", strtotime($ride->startHour)) }}
			</p>
			<p class="no-mobile"><i class="fa fa-map-o ride-card-icon"></i><strong class="bold-light">Details:</strong> {{ $ride->description }}</p>
			@if($ride->ladies_only == 'Yes')
				<p class="add-in ladies-pink"><i class="fa fa-female ride-card-icon"></i> <strong>WOMEN ONLY</strong> {{ $ride->ladies_only }}</p>
			@endif
		</div>
	</div>

	<div class="col-md-2">
		<h4 class="title">Booking</h4>
		<div style="text-align: center;">
			<h2 class="rideprice"> ${{ $ride->price }}</h2>
			
			@if($ride->stop == true)
				<input type="text" name="isStop" value="{{ $ride->stop  }}" style="display: none;">
			@endif
			<div style="margin-top:14px;">
			<button class="btn btn-primary">Join</button>
			{!! Form::close() !!}	
			</div>
		</div>
	</div>
</div>