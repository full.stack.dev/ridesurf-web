<div class="row rides-list ride-list" id="rides_list">
	
        <div class="col-md-12" id="form-sidebar">
            <label class="badge badge-success pull-right">Open Ride</label>
        </div>

	<div class="col-md-2">
		{!! Form::open(array('route' => ['rides.openRides', $ride->id] ,'method'=>'GET')) !!}
		<h4 class="title">Driver</h4>
		<div class="profile-pic-sm" style="text-align: center;">
			<img src="{{ $ride->user->avatar }}" >
			<p align="center">{{ $ride->first_name }} {{ $ride->last_name }}</p>
			@include('layouts.rating', [$ride->rating, $ride->id] )
			<a href="{{ route('rides.viewUserProfile', $ride->user_id) }}"
			class="btn btn-info x-slim-button">View Profile</a>
		</div>
	</div>

	<div class="col-md-2 no-mobile">
		<h4 class="title">Vehicle</h4>
		<div class="profile-pic-sm" style="text-align: center;">
			<img src="{{ $ride->user->driver[0]->path_image }}" width="50px" class="img-circle"><br />
			<span class="add-id"> {{$ride->make}}</span><br/>
			<span class="add-id">  {{$ride->model}}</span> <br/>
			<span class="add-id"> {{$ride->year}}</span> <br/>
		</div>
	</div>

	<div class="col-md-6">
		<h4 class="title">Drive Info</h4>
		<div class="margin-left-20">
			<p class="add-id">
				<i class="fa fa-map-marker ride-card-icon"></i>
				<strong class="bold-light">From:</strong> {{ $ride->location_from }} 
			</p> 
			<p class="add-id">
				<i class="fa fa-map-marker ride-card-icon" ></i>
				<strong class="bold-light">To:</strong> {{ $ride->location_to }} 
			</p> 
			<p class="add-id">
				<i class="fa fa-calendar ride-card-icon"></i>
				<strong class="bold-light">Open dates to ride:</strong> 
				{{ date('m/d/Y', strtotime($ride->open_date_from)) }} 
				to {{ date('m/d/Y', strtotime($ride->open_date_to)) }} 
			</p>

			<p class="add-in">
				<i class="fa fa-clock-o ride-card-icon"></i>
				<strong class="bold-light">Possible Times: </strong>

				@foreach($ride->hours as $hour)
				<label class="badge badge-success"> {{date("g:i a", strtotime($hour->hour)) }} </label> 
				@endforeach
			</p>

			<p class="add-in no-mobile">
				<i class="fa fa-map-o theme-color width-20"></i>
				<strong class="bold-light">Description:</strong> {{ $ride->description }}
			</p> 
		</div>
	</div>

	<div class="col-md-2">
		<h4 class="title">Booking</h4>
		<div style="text-align: center;">
			<h2 class="rideprice"> ${{ $ride->price }}</h2>
			<div style="margin-top:14px;">
				<button class="btn btn-primary">Join</button>
					
			</div>
		</div>
	{!! Form::close() !!}
	</div>
</div>