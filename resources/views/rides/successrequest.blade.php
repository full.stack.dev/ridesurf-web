@extends('layouts.app')
@section('content')

<div class="container">
	<div class="offer-ride">
		@if($return == 'No')
			<h2> Thanks for your request! </h2>

			<p>We'll notify you when a ride from {{ $from }} to {{ $to }} on or after 
			{{ date('m/d/Y', strtotime($date)) }} has been created.</p>
		@else
			<h2> Thanks for your request! </h2>

			<p>We'll notify you when a ride from {{ $from }} to {{ $to }} on or after 
			{{ date('m/d/Y', strtotime($date)) }} has been created.</p>

			<p> Return ride from {{ $to }} to {{ $from }} on or after 
			{{ date('m/d/Y', strtotime($date_return)) }} has been created.</p>
		@endif

		<p> You can review your requests <a href="{{ route('ride_request.index') }}"> here
	</div>
</div>
@endsection