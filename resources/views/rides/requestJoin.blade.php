@extends('layouts.app')
@section('content')

<div class="container">
	<div class="container-fluid offer-ride">
		<div class="container">
			<div class="col-md-9">
				<h2>Success!</h2>
				<p><br /><br />Your ride request has been submitted! We have sent your request to the driver. Drivers will approve requests as quickly as they can, but please be patient with them. Most drivers reply within 24 hours.</p>

				<a href="{{ route('dashboarduser.index')}}" class="btn btn-info mobile"> Return to Dashboard </a>
			</div>
		</div>
	</div>
</div>
@endsection