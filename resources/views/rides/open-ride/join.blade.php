@extends('layouts.app')
@section('content')
    <script>
        //VueJS conflicts with dropdown
        jQuery('.dropdown-toggle').dropdown();
        
    </script>

  <div class="container">
    <div id="app">
      
        <!-- Passing the value from controller to vue --> 
        <joinride currentuser="{{ $user }}"
                  openride="{{ $openRide }}"
                  hour="{{ $hours }}"
                  dates="{{ $dates }}"
                  auto="{{ $vehicle }}"
                  join="{{ $canJoin }}">
        </joinride>
    </div>
    </div>
  <script src="{{asset('js/app.js')}}"></script>

@endsection