@extends('layouts.app')
@section('content')
    <script>
        //VueJS conflicts with dropdown
        jQuery('.dropdown-toggle').dropdown();
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M&libraries=places&language=en"></script>

<div class="container">
    @hasrole('Driver|Admin')
    @if( (  $user->avatar == null)
    || $user->phone_number == null || count($user->driver) == 0 || $user->driver[0]->status == 'Requested'
    || $user->driver[0]->status == 'Denied' || $user->address == null )

        <div class="offer-ride">
            <div class="col-md-12" id="form-sidebar">
                <h2>Create A Ride</h2>
            </div>
            <div class="col-md-12 text-center simple-page-pad">
                <h4> Sorry, you can't create a ride until you complete some missing information.</h4>
                @if( $user->avatar != null)
                    <p class="margin-top-50">Profile image missing</p>
                @endif
                @if($user->phone_number == null)
                    <p class="margin-top-50">Phone number missing</p>
                @endif
                @if($user->status == 'Requested')
                    <p>Please wait for the admins to approve your request</p>
                @endif
                @if($user->address == null)
                    <p>Address is missing</p>
                @endif
                <div class="margin-top-50">
                    <a class="btn btn-primary" href="{{ route('dashboarduser.profile') }}">Edit your profile</a>
                </div>
                    
            </div>
        </div>

    @else
        <div id="app">
            <create-open-ride user="{{ $user }}"></create-open-ride>
        </div>

        <script src="{{asset('js/app.js')}}"></script>

    @endif

    @endhasrole
</div>
@endsection