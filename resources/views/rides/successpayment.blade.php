@extends('layouts.app')
@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

<div class="container">
	<div class="container-fluid offer-ride">
		<div class="container">
		<div class="row">
		
			<div class="col-md-8 form-group">
				<h2>Success!</h2>
				<p>You are going to {{ $ride->location_to }}! Your driver has been sent your information.</p>
				<p>If you have any questions about the ride please contact your driver for the logistics, timing and any questions you may have.</p>

				<p>Happy travels!</p>
			</div>
			<div class="col-md-4 form-group">

			</div>
		</div>
	</div>
</div>
@endsection