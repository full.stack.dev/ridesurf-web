@extends('layouts.app')
@section('content')

<div class="container">

@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

<div class="container-fluid offer-ride">
	<div class="container">
		<div class="row">

			<h2> Join Ride</h2> 

			<div class="col-md-7 form-group">
				<!--<div class="widget" style="height: 470px">-->
				<div>

					<h3>{{ $from }} &#8594; {{ $to }}</h3>

					<span class="add-id">On</span><span><strong> {{date('m/d/Y', strtotime($ride->dateFrom)) }}</strong></span> <span class="add-id">at <strong> {{ $start }} </strong></span> <br />
					@if($ride->ladies_only == 'Yes')
						<span class="add-in ladies-pink">
							<i class="fa fa-female"></i> <strong>WOMEN ONLY</strong> {{ $ride->ladies_only }}
						</span>
					@endif
					@if(count($ride->ride_stop) )
						<p> {{ $ride->city_from }} <i class="fa fa-arrow-right" aria-hidden="true"></i> </p> 
						@foreach($ride->ride_stop as $stop)
							<p> <span class="add-id"><strong>Stop Over:</strong> {{ $stop->city_name }} </span>
								<i class="fa fa-arrow-right" aria-hidden="true"></i>
							</p> 
						@endforeach
						<p> {{ $ride->city_to }} </p>
					@endif
					<br />
				
					<div class="row">
						<div class="user-dashboard-profile">
						<!--<li class="list-group-item" style="background-color: #eee;">-->
							<div>
								<div class="col-md-5 bordered-box right-margin">
									<h3 class="heading-underline">Driver</h3>
									<div class="profile-thumb profile-thumb-joinride">
							            <img src="{{ $ride->user->avatar }}" width="85px" height="85px" class="img-circle" alt="ridesurf driver">
									</div>
									
									<!-- User Name -->
									<p>{{ $ride->user->first_name }}
										{{ $ride->user->last_name }}</p>
									
									<div class="stars">
									    <span data-rating="1" class="fa fa-star checked"></span>
									    <span data-rating="2" class="fa fa-star checked"></span>
									    <span data-rating="3" class="fa fa-star checked"></span>
									    <span data-rating="4" class="fa fa-star "></span>
									    <span data-rating="5" class="fa fa-star"></span>
									</div>
									
									<p><strong>Active Since:</strong> {{  date('m/d/Y', strtotime($ride->user->created_at)) }}<br />
									<strong>Gender:</strong> {{ $ride->user->gender }}</p>
									<a href="{{ route('rides.viewUserProfile', $ride->user->id) }}"
										class="btn btn-info">View Public Profile</a>
								</div>
							</div>
							<div class="col-md-5 bordered-box">
									
								<h3 class="heading-underline">Vehicle</h3>
									
								<img src="{{ $ride->user->driver[0]->path_image }}" width="100px" height="100px" class="img-circle bottom-margin">
									
								<p>
								<strong>Make:</strong> {{ $vehicle->make }}<br />
							 	<strong>Model:</strong> {{ $vehicle->model }} <br />
								<strong>Color:</strong> {{ $vehicle->color }} <br />
								<strong>Year:</strong> {{ $vehicle->year }} <br />
								<strong>Type:</strong> {{$vehicle->type}} 
								</p>

							</div>
						</div>
					</div>

					<div class="row top-margin-15 bottom-margin">
						<span class="add-in"><strong>Ride Description: </strong> {{ $ride->description }}</span> <br/>
					</div>

					<div class="widget" style="height: auto; border-top: 1px solid #ccc;" >
					@if($usersRiding != null)
						@include('layouts.rides.rideSchedule', [$ride, $usersRiding, $end])
					@endif
					</div>
				</div>
			</div>
			
			<div class="col-md-5 text-center">
				{!! Form::open(['route' => ['rides.request', $ride->id, $price], 'method'=>'POST']) !!}
				<div class="widget" style="height: auto;">
					
					<div class="row" style="border-bottom: solid 1px rgba(0, 0, 0, 0.1); padding-bottom: 10px">
						<div class="col-md-4 mobile-split-50" style="border-right: solid 1px  rgba(0, 0, 0, 0.1)">
							<h4 class="add-id"><strong>Price </strong></h4>
							<span style="color:green;font-size: 3em"> ${{ $price }}</span>
						</div>
						<div class="col-md-8 mobile-split-50" style="">
							<h4> Seats Available </h4>
							<span style="font-size: 3em">{{ $ride->seats_available }}</span>
						</div>
					</div>
				</div>

				
		 		<!--<div class="container-fluid profile-header">-->
		 		<div class="container-fluid">
					<div class="row" style="margin-bottom: 25px;">
						<div class="col-xs-12 col-sm-12 col-md-12 text-center">
							<h3 style="margin-top: 25px"> Seats Requested: </h3>
							
							<select name="seats" class="form-control" style="margin-bottom: 15px;">

								@if($isStop) <!-- Check that is a stop --> 
									
									<!-- Check that is From Point A to Stop over -->
									@if($ride->location_from == $from) 
										@if($ride->ride_stop[0]->seats_from > 0)

											<option selected value="1"> 1 Seat </option>
											@for($i=1; $i < $ride->ride_stop[0]->seats_from; $i++)
												<option value="{{ $i+1 }}"> {{ $i+1 }} Seat </option>
											@endfor
										@else
											<option selected disabled > 0 Seats Avilable </option>
										@endif
									
									@else

										<!-- Is from Stop Over to Point B -->
										@if($ride->ride_stop[0]->seats_to > 0)

											<option selected value="1"> 1 Seat </option>
											@for($i=1; $i < $ride->ride_stop[0]->seats_to; $i++)
												<option value="{{ $i+1 }}"> {{ $i+1 }} Seat </option>
											@endfor
										@else
											<option selected disabled > 0 Seats Avilable </option>
										@endif
									@endif

								@else <!-- Is not a stop over --> 

										@if($ride->seats_available > 0)

											<option selected value="1"> 1 Seat </option>
											@for($i=1; $i < $ride->seats_available; $i++)
												<option value="{{ $i+1 }}"> {{ $i+1 }} Seat </option>
											@endfor
										@else
											<option selected disabled > 0 Seats Avilable </option>
										@endif

								@endif

							</select>
							<h4>Please introduce yourself:</h4>
							<textarea class="form-control" name="description" rows="7" class="form-control" required></textarea>
							<input type="text" value="{{$from}}" name="city_from" style="display: none;">
							<input type="text" value="{{$to}}" name="city_to" style="display: none;">
						</div>
					</div>
					
					<div class="col-md-12 text-center" id="buttonsChange">

						@if($ride->seats_available < 0 || Auth::user()->verified == 0 ||
						( $ride->ladies_only == 'Yes' && Auth::user()->gender = 'Male') || 
						Auth::user()->id == $ride->user->id || $exisits > 0 )

							<button type="submit" class="btn btn-primary" disabled>Request To Join</button>
						@else
							<button type="submit" class="btn btn-primary" id="strLoader">Request To Join</button>
						@endif
					</div>

					@if($isStop)
						<input value="1" type="number" name="isStop" style="display: none;">
					@else
						<input value="0" type="number" name="isStop" style="display: none;">
					@endif
					{!! Form::close() !!}
					<br/> <br/>
					@if($ride->user->id != Auth::id())

					<div class="wrapper bottom-margin" style="margin-top: 25px;">
						<p>Have a pre-booking question?</p>
						<a href="{{ route('messages.contact', $ride->id) }}" class="btn btn-success"> Contact Driver</a>
					</div>
					@endif
					
				</div>
			</div>	
		</div>
	</div>
</div>

<script>
$(document).ready(function(){

	var stars = $('.stars');
	var star  = stars.find('.fa-star');
	var value = {{ $ride->user->rating }}

	// Remove class for selected stars
	  stars.find('.checked').removeClass(' checked');

	  // Add class to the selected star and all before
	  for (i = 1; i <= value; i++) {
	    stars.find('[data-rating="' + i + '"]').addClass('checked');
	  }
});
</script>

@endsection

</div>