@extends('layouts.app')
@section('content')

<div class="container">
	<div class="offer-ride col-md-10 offset-1">
		
		<h2> Ride Reminder </h2>
			<div class="form-style" id="request_form">
			{!! Form::open(array('route' => ['ride_request.store'], 'Mehtod' => 'POST', 'class' => 'form-group')) !!}

				<div class="form-group float-label-control">
					<label>City From</label>
					<input value="{{$city_from}}" name="cityFrom" class="form-control" type="text" id="from-fetch" required>
				</div>
				<div class="form-group float-label-control">
					<label>City To</label>
					<input value="{{$city_to}}" name="cityTo" class="form-control" type="text" id="to-fetch" required>
				</div>
				<div class="form-group float-label-control">
					<label>State <span class="glyphicon glyphicon-info-sign" id="infoState">
						<span class="tooltiptext">Add State with two letters only</span>
					</span></label>
					
					<input name="state" value="{{$state}}" max="2500" class="form-control" type="text" maxlength="2" required>
				</div>
				<div class="form-group float-label-control">
					<label>Date</label>
					<input value="{{$date}}" name="date" class="form-control" type="date" id="dateFrom" required>
				</div>
				<div class="form-group float-label-control" id="control_round_trip">
					<label>Round Trip? </label>
					<select name="round-trip" class="form-control" id="round-trip">
						<option value="No" selected >No</option>
						<option value="Yes">Yes</option>
					</select>
				</div>
				<div id="control_round_trip"></div>
				<!-- To Cofirm 
				<div class="form-group float-label-control">
					<label>Max-Price</label>
					<input name="max_price" value="" min="0.01" step="0.01" max="2500" class="form-control" placeholder="$20.00" type="number">
				</div> --> 
				
				<button class="btn btn-info">Send</button>
			{!! Form::close() !!}
		</div>

	</div>

<div id="map"></div>
</div>
<script> 
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: 'roadmap'
  });

  // Create the search box and link it to the UI element.
  var inputFrom = document.getElementById('from-fetch');
  var options = {
    types: ['(cities)']
   };
  var searchBox = new google.maps.places.Autocomplete(inputFrom,options);

  var inputTo= document.getElementById('to-fetch');
  var searchBoxTo = new google.maps.places.Autocomplete(inputTo,options);
}

$(document).ready(function(){
	$('#round-trip').on('change', function(e){
		var value = $('#round-trip').val();
		if(value == 'Yes'){
			var info = '<div class="form-group float-label-control">'+
						'<label>Date Return </label>'+
						'<input name="date_return" class="form-control" type="date" id="dateFrom" required>'+
						'</div>';

			$('#control_round_trip').append(info).fadeIn(800);
		}
	});
});
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M&libraries=places&region=us&callback=initMap">
</script>
@endsection