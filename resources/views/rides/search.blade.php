@extends('layouts.app')
@section('content')

<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.5.0/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.5.0/mapbox-gl.css' rel='stylesheet' />

<style>
#map { position:absolute; top:0; bottom:0; width:0%; }
</style>

<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css' type='text/css' />
<!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

<div>
	@if ($message = Session::get('success'))
		<div class="alert alert-success" id="alert-success">
		  <p>{{ $message }}</p>
		</div>
	@endif

	<div class="offer-ride container">
		<div class="col-md-12">

			<!-- Search Bar -->
			<!-- Replace w VUEJS -->
			 <div id="search">
				<div>
			<!-- Passing the value from controller to vue -->
	{{-- 	 	<searchbar states="{{ $states }}"></searchbar>  --}}
					<router-multi-view
						class="wrapper"
						states="{{ $states }}"
						name="fade"
					/>
	{{--  	          	<RideList normalRides="{{ $rides }}" open="{{$openRides}}"> </RideList>
	 --}}{{-- 				<router-view />
	 --}}
				</div>
			</div>
			<div id="app" style="display: none;"></div>
			<script src="{{asset('js/app.js')}}"></script>

		</div>
	</div>
</div>

<!-- <div id="map"></div> -->
<!-- <script>
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: 'roadmap'
  });

  // Create the search box and link it to the UI element.
  var inputFrom = document.getElementById('from-fetch');

  var searchBox = new google.maps.places.Autocomplete(inputFrom);

  var inputTo= document.getElementById('to-fetch');

  var searchBoxTo = new google.maps.places.Autocomplete(inputTo);
}

$( document ).ready(function() {

	$('#minus_max').on('click', function(){
		var value = $('#max_price').val();
		value--;
		$('#max_price').val(value);
	});

	$('#max_max').on('click', function(){
		var value = $('#max_price').val();
		value++;
		$('#max_price').val(value);
	});

	var numItems = $('.ride_list').length;
	$('#count_rides').text(numItems);
	$('#from-fetch').attr("required", "true");
	$('#to-fetch').attr("required", "true");
	$('#dateFrom').attr("required", "true");

    $('#findButton').click(function(){
    	var from = $('#from-fetch').val();
    	var to = $('#to-fetch').val();
    	var date = $('#dateFrom').val();
    	console.log(from + to + date);

    	if(from == ""){
    		alert('from is null');
    		$('#from-fetch').addClass("errorClass");
    		return false;
    	}if(to == ""){
    		alert('to is null');
    		$('#to-fetch').addClass("errorClass");
    		return false;
    	}if(date == ""){
    		alert('Please add a date in the search ');
    		$('#dateFrom').addClass("errorClass")
    		return false;
    	}if(from != null && to != null && date != null){
    		return true;
    	}
    });
});
</script> -->

<!-- <script async defer
    src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js">
</script> -->
@endsection
