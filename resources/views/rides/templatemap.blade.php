<script>
// Initialize and add the map
	function initMap() {

		var geocoder = new google.maps.Geocoder();
		var address1 = "{{ $ucf->address }}";
		var address2 = "{{ $usf->address }}";
		var latitudefrom;
		var longitudefrom;
		var latitudeto;
		var longitudeto;

		geocoder.geocode( { 'address': address1}, function(results, status) {

			if (status == google.maps.GeocoderStatus.OK) {
				latitudefrom = results[0].geometry.location.lat();
				longitudefrom = results[0].geometry.location.lng();
				console.log(latitudefrom + " " + longitudefrom);
			} 
		});
		geocoder.geocode( { 'address': address2}, function(results, status) {

			if (status == google.maps.GeocoderStatus.OK) {
				latitudeto = results[0].geometry.location.lat();
				longitudeto = results[0].geometry.location.lng();
				calculateAndDisplayRoute(directionsService, directionsDisplay,latitudefrom, longitudefrom,
        		latitudeto, longitudeto);
			} 
		});
		console.log(latitudefrom + 'Checking');
		var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;

        var start = new google.maps.LatLng(latitudefrom, longitudefrom);

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: start
        });
        directionsDisplay.setMap(map);

        
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay,latitudefrom, longitudefrom,
        	latitudeto, longitudeto) {
        console.log(latitudefrom + " " + longitudefrom);
        directionsService.route({
          origin: {lat: latitudefrom, lng: longitudefrom},  // Haight.
          destination: {lat: latitudeto, lng: longitudeto},  // Ocean Beach.
          // Note that Javascript allows us to access the constant
          // using square brackets and a string value as its
          // "property."
          travelMode: google.maps.TravelMode['DRIVING']
        }, function(response, status) {
          if (status == 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
	
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M&callback=initMap">
    </script>