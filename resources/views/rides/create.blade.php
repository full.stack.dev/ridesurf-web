@extends('layouts.app')
@section('content')
<script>
    //VueJS conflicts with dropdown
    jQuery('.dropdown-toggle').dropdown();
	var gender = "{{Auth::user()->gender}}";
</script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M&libraries=places&language=en"></script>
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.css' >

<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css' type='text/css' />
<!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

<div class="container">
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.js"></script>
<link
rel="stylesheet"
href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.css"
type="text/css"
/>

	@hasrole('Driver|Admin')
	@if( ( count($user->profileImages) == 0 && $user->avatar == null) 
	|| $user->phone_number == null || count($user->driver) == 0 || $user->driver[0]->status == 'Requested'
	|| $user->driver[0]->status == 'Denied')

		<div class="offer-ride">
			<div class="col-md-12" id="form-sidebar">
			       <h2>Create A Ride</h2>
			</div> 
			<div class="col-md-12 text-center simple-page-pad">
				<h4> Sorry, you can't create a ride until you complete some missing information:</h4>
				@if($user->profileImage[0] != null && $user->avatar != null)
	              
	            <h4 class="margin-top-50">Profile image</h4>
                @endif
                @if($user->phone_number == null)
                    <h4 class="margin-top-50">Phone number</h4>
                @endif
                <div class="margin-top-50">
                    <a class="btn btn-primary" href="{{ route('dashboarduser.profile') }}">Edit your profile</a>
                </div>
			</div>
		</div>


	@else
		<div id="create">
			<div>
				<router-multi-view
					class="wrapper"
					name="fade"
				/>
			</div>
		</div>

		<div id="app" style="display: none;"></div>
		<div id="search" style="display: none;"></div>
		<script src="{{asset('js/app.js')}}"></script>

    </script>
    @endif

    @endhasrole

    @hasrole('User')
    	<div class="offer-ride">
		    <div class="col-md-12">
		       <h2>Create A Ride</h2>
		    </div>

		    @if(count($user->driver) > 0)
		   	 	<h3>You are not an approved driver yet.</h3>
		    	<p> Your Account has been desactivated because of missing information </p> 
		    @else
				<div class="col-md-12 col-lg-12 text-center" style="margin: 80px 0 100px 0;">
					<h3>You are not an approved driver yet.</h3>
					<p>Please apply to be a driver on your profile.<br/><br/></p>
					<a class="btn btn-primary" href="{{ route('dashboarduser.becomeDriver') }}">Apply To Drive</a>

				</div>
			@endif
		</div>

    @endhasrole
	</div>
@endsection 

