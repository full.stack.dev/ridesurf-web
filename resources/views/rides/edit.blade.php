@extends('layouts.app')
@section('content')

<div class="container">

    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>


  @hasrole('Driver|Admin')  
  <div class="container-fluid offer-ride">
  <div class="container">
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
    @endif
    <!-- Row Start -->
    <div class="row">
      <div class="col-md-8 col-lg-8">
        <div class="sidebar" id="form-sidebar">
          <!-- User Widget -->
          <div class="widget user-dashboard-profile" id="dashboard">
            <h2 style="margin-bottom: 15px;"> Edit Your Ride </h2> 


            
            {!! Form::model($ride, ['method' => 'PUT','route' => ['rides.update', $ride->id] ]) !!}
            
            <div class="row" id="editRide">
              
              <div class="col-md-6" id="starts">
                <p> From </p>
                <input type="text" value="{{ $ride->location_from }}" id="from-fetch" name="from-fetch" disabled="true" class="form-control">
              </div>
              <div class="col-md-6" id="ends">
                <p> To </p>
                 <input type="text" value="{{ $ride->location_to }}" 
                 id="to-fetch" name="to-fetch" class="form-control" disabled="true">
              </div>
              @hasrole('Admin')
              <div class="col-md-6">
                <input type="text" value="{{ $ride->city_from }}" 
                 id="to-fetch" name="to-fetch" class="form-control" >
              </div>
              <div class="col-md-6">
                <input type="text" value="{{ $ride->city_to }}" 
                 id="to-fetch" name="to-fetch" class="form-control" >
              </div>
              @endhasrole
                @if(count($ride->ride_stop)>0)

                @foreach($ride->ride_stop as $key => $stop)
                  <div class="col-md-12" id="stop">
                    <p> Stop Over</p>
                     <input type="text" value="{{ $stop->address }}" 
                     id="address-{{ $key }}" name="stopOver-{{ $key }}" class="form-control" disabled="true">
                     <p>Price For Stop From {{ $ride->city_from }} To {{ $stop->city_name }}</p>
                     <input type="text" value="{{ $stop->price_from }}" 
                     id="stopOver" name="StopPrice_From" class="form-control">
                     <p>Price For Stop From {{$stop->city_name}} To {{$ride->city_to}}</p>
                     <input type="text" value="{{ $stop->price_to }}" 
                     id="stopOver" name="StopPrice_To-{{ $key }}" class="form-control">
                  </div>
                @endforeach

                @endif
                <div class="col-md-12">
                  <p> Date </p>
                  @hasrole('Driver')
                  <input type="date" name="date" class="form-control" value="{{ $ride->dateFrom }}" placeholder="{{ $ride->dateFrom }}" id="dateFrom" readonly>
                  @endhasrole
                  @hasrole('Admin')
                  <input type="date" name="date" class="form-control" value="{{ $ride->dateFrom }}" placeholder="{{ $ride->dateFrom }}" id="dateFrom" >
                  @endhasrole
                </div>

                <div class="form-group">
                  <div class="col-md-6">
                    <p> Price Per Seat </p>
                    <input type="number" value="{{ $ride->price }}" name="price" class="form-control">

                  </div>
                  <div class="col-md-6">
                    <p> Seats Available</p>
                    <input type="number" value="{{ $ride->seats }}" name="seats" min="0" class="form-control">
                  </div>
                  <div class="col-md-6">

                    <p> Luggage Size </p>
                    <select class="form-control" name="luggage_size">
                      <option value="{{ $ride->luggage_size }}" selected="">{{ $ride->luggage_size }}</option>
                      <option value="Small">Small</option>
                      <option value="Medium">Medium</option>
                      <option value="Large">Large</option>
                    </select>

                  </div>
                  <div class="col-md-6">

                    <p> Luggage Amount </p>
                    <input type="number" 
                    value="{{ $ride->luggage_amount }}" name="luggage_amount" class="form-control">

                  </div>
                  <div class="col-md-6">
                    <p> Start Hour </p>
                    <input type="time" value="{{ $ride->startHour }}" name="startHour" class="form-control">

                  </div>
                  
                  <div class="col-md-6">
                    @if(Auth::user()->gender == 'Female')
                    <p> Women Only </p>
                    <select name="ladies_only" class="form-control" id="ladies_only">
                      <option value="{{ $ride->ladies_only }}" selected hidden>{{ $ride->ladies_only }}</option>
                      <option value="Yes"> Yes </option>
                      <option value="No"> No </option>
                    </select>
                    @endif
                  </div>
                  <div class="col-md-12">
                    <p> Description </p>
                    <textarea name="description" class="form-control" rows="6">{{ $ride->description }}</textarea>
                  </div>
              </div>

            </div>
            

            <div class="col-xs-12 col-sm-12 col-md-12 text-center" id="buttonsChange">
              <button type="submit" class="btn btn-primary">Submit Changes</button>
              
            </div>
           {!! Form::close() !!}
          </div>
        </div>

        <div class="widget" style="height: 370px">
          @if($usersRiding != null)
            @include('layouts.rides.rideSchedule', [$ride, $usersRiding, $end])
          @endif
        </div>
      </div>
      <div class="col-md-4 col-lg-4">
        <a href="{{ url()->previous() }}" class="btn btn-success">Back</a>
        <div class="widget " id="ChangeMap">
          <p> Route </p>
          <div id="map" style="height: 550px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>

    <script>
          var map, infoWindow;

          function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: -37.0902, lng: 95.7129},
              zoom: 6
            });

            // Create the search box and link it to the UI element.
            var inputFrom = document.getElementById('from-fetch');
            var searchBoxFrom = new google.maps.places.SearchBox(inputFrom);

            var inputTo= document.getElementById('to-fetch');
            var searchBoxTo = new google.maps.places.SearchBox(inputTo);

            @foreach($ride->ride_stop as $key=>$stop)
              var inputStop = document.getElementById('stopOver-{{ $key }}');
              if(inputStop != null){
                var searchBoxStop{{ $key }} = new google.maps.places.SearchBox(inputStop);
              }
            @endforeach
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                  var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                  };
                  
                  map.setCenter(pos);
                }); 
            } else {
              // Browser doesn't support Geolocation
              handleLocationError(false, infoWindow, map.getCenter());
            }

            var geocoder = new google.maps.Geocoder();
            var address1 = "{{ $locationFrom}}";
            var address2 = "{{ $locationTo}}";

            var latitudefrom,longitudefrom;
            var latitudeto,longitudeto;
            var latitudeStop, longitudeStop;
            

            var directionsDisplay = new google.maps.DirectionsRenderer;
            var directionsService = new google.maps.DirectionsService;
            var end = document.getElementById('to-fetch').value;

            geocoder.geocode( { 'address': address1}, function(results, status) {

              if (status == google.maps.GeocoderStatus.OK) {
                latitudefrom = results[0].geometry.location.lat();
                longitudefrom = results[0].geometry.location.lng();
                console.log(latitudefrom + " " + longitudefrom);
              } 
            });
            
            geocoder.geocode( { 'address': address2}, function(results, status) {

              if (status == google.maps.GeocoderStatus.OK) {
                latitudeto = results[0].geometry.location.lat();
                longitudeto = results[0].geometry.location.lng();
                
                    var waypts=[];
                    var from = new google.maps.LatLng(latitudefrom, longitudefrom);
                    var fromName = "{{ $locationFrom }}";
                    var dest = new google.maps.LatLng(latitudeto, longitudeto);
                    var destName = "{{ $locationTo }}";
                    @foreach($ride->ride_stop as $stop)
                      waypts.push({
                        location: "{{ $stop->address }}",
                        stopover: true,
                      });
                    @endforeach

                    setTimeout(function() {
                      calculateAndDisplayRoute(directionsService, directionsDisplay,latitudefrom, longitudefrom,latitudeto, longitudeto, waypts) }, 
                      5000);

                    directionsDisplay.setMap(map);

                    var service = new google.maps.DistanceMatrixService();

                    service.getDistanceMatrix({
                        origins: [from, fromName],
                        destinations: [destName, dest],
                        
                        travelMode: 'DRIVING'
                    }, callback);

                    function callback(response, status) {
                      if (status == 'OK') {
                        
                        
                      }
                    }
                  
              } 
            });

            infoWindow = new google.maps.InfoWindow;
          }

          function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                  'Error: The Geolocation service failed.' :
                                  'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
          }


          function calculateAndDisplayRoute(directionsService, directionsDisplay,latitudefrom, longitudefrom,
                  latitudeto, longitudeto, waypts) {

                console.log("From " + latitudefrom + " " + longitudefrom);
                console.log('To ' + latitudeto + " " + longitudeto);
                directionsService.route({
                  origin: {lat: latitudefrom, lng: longitudefrom},  // Haight.
                  destination: {lat: latitudeto, lng: longitudeto},  // Ocean Beach.
                  waypoints: waypts, // stop Over
                  // Note that Javascript allows us to access the constant
                  // using square brackets and a string value as its
                  // "property."

                  travelMode: google.maps.TravelMode['DRIVING']
                }, function(response, status) {
                  if (status == 'OK') {
                    console.log(response);
                    directionsDisplay.setDirections(response);
                    var start = response.routes[0].legs[0].start_address;
                    if(waypts != null){
                      var stopOver = response.routes[0].legs[0].end_address;
                      var end = response.routes[0].legs[1].end_address;
                    }else{
                      var end = response.routes[0].legs[0].end_address;
                    }

                    //If is a Stop Over
                    if(waypts != null){
                      var distance_text =response.routes[0].legs[1].distance.value;
                      console.log(distance_text);
                      var distance = distance_text * 0.000621371192; // Transform Meters to Miles

                      var cityFrom = parseLocation(start);
                      var cityTo = parseLocation(end);

                      if(waypts != null){
                        var stopOvers = [];
                        for(var i = 0; i < waypts.length; i++){
                          stopOver[i] = {
                            'city' : parseLocation(waypts[i].location),
                            'duration' : response.routes[0].legs[0].duration.value,
                          }
                        }
                      }
                      if(waypts != null){
                        var totoalTime = response.routes[0].legs[1].duration.value + 
                        response.routes[0].legs[0].duration.value;
                        var totalDistance = response.routes[0].legs[1].distance.value + 
                        response.routes[0].legs[0].distance.value;

                      }else{

                        return false;
                      }
                      console.log("Duration stop " + response.routes[0].legs[0].duration.text);

                      var forms = '<input type="text" value="'+cityTo+'" name="city_to" style="visibility:hidden;">'+
                                '<input type="text" value="'+cityFrom+
                                '" name="city_from" style="visibility:hidden;">';
                                if(waypts != null){
                                 forms = forms +  '<input type="text" value="'+cityStopOver+
                                  '" name="city_stop" style="visibility:hidden;">'+
                                  '<input type="text" value="'+stop_over_duration+'" name="stopOverDuration" style="visibility:hidden;">';
                                }
                                forms = forms + '<input type="number" value="'+totoalTime+
                                '" name="duration" style="visibility:hidden;">';


                      jQuery('#editRide').append(forms);
                  }
                  //It is not a Stop Over 
                  else{

                    var distance_text =response.routes[0].legs[0].distance.value;
                      console.log(distance_text);

                      var distance = distance_text * 0.000621371192; // Transform Meters to Miles
                      var city_from  = parseLocation(start);
                      var cityTo = parseLocation(end);

                      //Here having problems ? 
                      var totoalTime = response.routes[0].legs[0].duration.value;
                      
                      var totalDistance = response.routes[0].legs[0].distance.value;

                      var forms = '<input type="text" value="'+cityTo+'" name="city_to" style="visibility:hidden;">'+
                                '<input type="text" value="'+cityFrom+
                                '" name="city_from" style="visibility:hidden;">'+
                                '<input type="number" value="'+totoalTime+
                                '" name="duration"style="visibility:hidden;">';

                    jQuery('#editRide').append(forms);

                  }
                    
                  } else {
                    window.alert('Directions request failed due to ' + status);
                  }
                });
              }


          //Add Marker of Stop Over
          function codeAddress(geocoder, map, address) {

              geocoder.geocode({'address': address}, function(results, status) {
                if (status === 'OK') {
                  console.log(map);
                  var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    var latlng = new google.maps.LatLng(latitude, longitude);

                  var marker = new google.maps.Marker({

                         map: map,
                         position: latlng,
                         title: 'Stop Over'

                     });


                } else {
                  console.log('Geocode was not successful for the following reason: ' + status);
                }
              });
            }


          function calcRoute(start, end) {

            var directionsService = new google.maps.DirectionsService();
            var request = {
              origin:start,
              destination:end,
              travelMode: google.maps.TravelMode.DRIVING
            };

            directionsService.route(request, function(result, status) {
              if (status == google.maps.DirectionsStatus.OK) {
                var distance_text =result.routes[0].legs[0].distance.value;
                console.log(distance_text);
                var distance = distance_text * 0.000621371192; // Transform Meters to Miles
                var cityStopOver = parseLocation(start);

                var inputs = "<div class='stopOver' style='display:none; visibility:hidden'>"+
                "<input type='number' value='"+result.routes[0].legs[0].duration.value+"' id='stopOverDuration' name='stoOverDuration'>"+
                "<input type='number' value='"+distance+"' id='stopOverDistance' name='stopOverDistance'>"+
                "<input type='text' name='cityStopOver' value='"+cityStopOver+"' id='cityStopOver'>"+ 
                "</div>"; 

                jQuery('#ChangeMap').append(inputs);
                
              }
            });

          }

          function parseLocation(city){
            var parseFrom = city.split(',');

              if(parseFrom.length == 5){
                var parse = parseFrom[2] + ", "+ parseFrom[3].match(/[a-zA-Z]+/g);
              }
              if(parseFrom.length == 4){  
                var parse = parseFrom[1] + ", "+ parseFrom[2].match(/[a-zA-Z]+/g);
              }
              if(parseFrom.length == 3){
                var parse = parseFrom[0] + ", "+ parseFrom[1].match(/[a-zA-Z]+/g);
              }else{
                var parse = city;
              }
            return parse;
          }
    </script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M&libraries=places&callback=initMap">
    </script>
@endhasrole
@endsection

</div>