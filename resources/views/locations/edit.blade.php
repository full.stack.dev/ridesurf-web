<!-- Create Locations -->
@extends('layouts.app')


@section('content')

	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Create New Location</h2>
	        </div>
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('locations.index') }}"> Back</a>
	        </div>
	    </div>
	</div>

	@if (count($errors) > 0)
	  <div class="alert alert-danger">
	    <strong>Whoops!</strong> There were some problems with your input.<br><br>
	    <ul>
	       @foreach ($errors->all() as $error)
	         <li>{{ $error }}</li>
	       @endforeach
	    </ul>
	  </div>
	@endif


	{!! Form::model($data, ['method' => 'PUT','route' => ['locations.update', $data->id] ]) !!}
	
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>First Name:</strong>
	            {!! Form::text('name', $data->first_name, array('placeholder' => 'Name','class' => 'form-control')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Last Name:</strong>
	            {!! Form::text('name', $data->last_name, array('placeholder' => 'Name','class' => 'form-control')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Address:</strong>
	            {!! Form::text('address', $data->address, array('placeholder' => 'Description','class' => 'form-control')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Description</strong>
	            {!! Form::text('description', $data->description, array('placeholder' => 'Description','class' => 'form-control')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>City</strong>
	            {!! Form::text('city', $data->city , array('placeholder' => 'City','class' => 'form-control', 'id' => 'city_name')) !!}
	            <div id="cityList">
    			</div>
	        </div>
	    </div>
	    
	    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
	        <button type="submit" class="btn btn-primary">Submit</button>
	    </div>
	</div>
	{!! Form::close() !!}

	<script>
		$(document).ready(function(){

		 $('#city_name').keyup(function(){ 
		        var query = $(this).val();
		        if(query != '')
		        {
		         var _token = $('input[name="_token"]').val();
		         $.ajax({
			          url:"{{ route('locations.fetch') }}",
			          method:"POST",
			          data:{query:query, _token:_token},
			          success:function(data){
			           $('#cityList').fadeIn();  
			                    $('#cityList').html(data);
			          }
		         });
		        }
		    });

		    $(document).on('click', 'li', function(){  
		        $('#city_name').val($(this).text());  
		        $('#cityList').fadeOut();  
		    });  

		});
	</script>

@endsection 