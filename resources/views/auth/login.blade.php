@extends('layouts.app')

@section('content')
<div class="container-fluid offer-ride">
<div>
    <div class="row">
        <div class="col-md-7 col-md-offset-2">
            <div class="panel panel-default" style="padding: 30px 30px 10px 30px;">
                
                <h2>Log In</h2>

                <div style="text-align: center;margin-top:30px;">
                      <a href="{{ url('/login/facebook') }}" class="btn azm-social azm-btn azm-border-bottom azm-facebook"><i class="fa fa-facebook"></i>Log in with Facebook</a>
                </div>

                <h6>or</h6>

                
                <div class="panel-body" style="margin-top: 20px">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12" style="text-align: center; margin-top:15px;">
                                <button type="submit" class="btn btn-primary">
                                    Log In
                                </button>

                                <p style="font-size: 14px; margin-top: 15px;"><a href="{{ route('password.request') }}">Forgot your password?</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
