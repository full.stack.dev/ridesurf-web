@extends('layouts.app')
@section('content')

@if ($message = Session::get('success'))
        <div class="alert alert-success">
          <p>{{ $message }}</p>
        </div>
@endif

<div class="container-fluid offer-ride">
<div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="padding: 30px">
                <h2>Sign Up</h2>

                <div class="panel-body">

                    <div style="text-align: center;margin-top:20px;">
                      <a href="{{ url('/login/facebook') }}" class="btn azm-social azm-btn azm-border-bottom azm-facebook"><i class="fa fa-facebook"></i>Sign Up with Facebook</a>
                    </div>

                    <h6>or</h6>

                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Zip Code</label>

                            <div class="col-md-6">
                                <input id="zipcode" type="number" class="form-control" name="zipcode" value="{{ old('zipcode') }}" required autofocus>

                                @if($errors->has('zipcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                        
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                            @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                            <?php $isDebug = env('APP_DEBUG'); ?>

{{--                            <div class="g-recaptcha" data-sitekey="6LcKUosUAAAAAKl3i_z6sqwJ2Rr2QtZjqSHUAAq8" data-callback="enableBtn"></div>--}}

                        </div>

                        <!-- Legal --> 
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                                <p style="font-size: 12px; font-weight: normal; color: #bbb;">
                                    <input name="acceptTerms" type="checkbox" value="1" onclick="enableBtn()" required> By signing up you accept our <a href="/terms-of-use"> Terms of Use</a>, <a href="/code-of-conduct">Code of Conduct</a> and <a href="/privacy">Privacy Policy</a>.</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12" style="text-align: center; margin-top:15px;">
                                <button type="submit" class="btn btn-primary" id="button1">
                                    Sign Up
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
    var isDissable = true;
    $('document').ready(function(){
        document.getElementById("button1").disabled = true;
    });
    function enableBtn(){
        if(isDissable){
            document.getElementById("button1").disabled = false;
            isDissable = false;
        }else{
            document.getElementById("button1").disabled = true;
            isDissable = true;
        }
    }

</script>
@endsection
