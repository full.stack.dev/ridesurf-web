@extends('layouts.app')
@section('content')
@if ($message = Session::get('success'))
	<div class="alert alert-success">
	  <p>{{ $message }}</p>
	</div>
@endif

<div class="container">
<div class="container-fluid offer-ride bottom-pad bottom-margin">

	@include('dashboarduser.layout.menu')

	@include('dashboarduser.layout.dashboard', [$user, $booked, $offer])
  
  <div class="non-mobile">
    @include('dashboarduser.layout.sidebar')
  </div>

  <div class="col-md-8">
    <div class="misc-container" id="notifications">
      <div class="misc-title">
        <span class="icon"><i class="fa fa-bell-o"></i></span>
        <span class="txt">{{ $notify_menu + $cMessages }} Notifications</span>
      </div>
      
      <div style="border-top: 1px solid #ccc; padding-top: 10px;">
        
        @include('dashboarduser.layout.notifications', [$notifications, $today])

        @include('messenger.partials.flash')

        @each('messenger.partials.thread', $messages, 'thread', 'messenger.partials.no-threads')
      </div>
    </div>
       
    <div class="misc-container">
      <div class="misc-title" id="messages">
        <span class="icon"> <i class="fa fa-envelope-o"></i>
        </span>
        <span class="txt">{{$messageCount}} New Messages</span>
      </div> 

      <div style="border-top: 1px solid #ccc; padding-top: 10px;">

          @include('messenger.partials.flash')

          @each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')

          <div class="text-center">
            <a href="{{ route('messages.create') }}"> <button type="submit" class="btn btn-primary">New Message </button></a>

          </div>
      </div>
    </div>
  </div>

  <div class="mobile">
    @include('dashboarduser.layout.sidebar')
  </div>

</div>
</div>

@endsection