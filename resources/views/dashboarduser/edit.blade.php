@extends('layouts.app')


@section('content')

<div class="container">
  	<div class="row">
      <div class="col-lg-12 margin-tb">
          <div class="pull-left">
              
          </div>
          <div class="pull-right">
              <a class="btn btn-primary" href="{{ route('dashboarduser.index') }}"> Back</a>
          </div>
      </div>
    </div>


@if (count($errors) > 0)
  <div class="alert alert-danger">
    <label>Whoops!</label> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif


{!! Form::model($user, ['method' => 'PATCH','route' => ['dashboarduser.update', $user->id], 'files' => true  ] ) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <h2>Edit Your Profile</h2>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label>First Name:</label>
                {!! Form::text('first_name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label>Last Name:</label>
                {!! Form::text('last_name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label>Email:</label>
                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label>Profile Image:</label>
                <input type="file" onchange="previewFile()" class="form-control" id="profile_image" name="profileimage">
                @if($image != null)
                <p id="ValidProfile"> Current Picture </p>
                <img src="{{ $image }}" width="100" alt="Image preview...">
                @else
                <p id="ValidProfile"></p>
                <img src="" width="100" alt="Image preview..." style="display: none">
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label>Password:</label>
                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label>Confirm Password:</label>
                {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6" >
        <div class="form-group">
            <h2> Car Details </h2>
            <div class="row" id="carInfo">
                @if($user->vehicle_id == null)
                <div class="col-xs-12 col-sm-12 col-md-12" id="car-details">
                    <div class="form-group" id="makeDiv">
                        <label> Make </label>
                        <input type="text" id="make" name="make" class="form-control">
                         <div id="make_list">
                         </div>
                    </div>

                </div>
                @else
                <div class="col-xs-12 col-sm-12 col-md-12" id="car-details">
                    <div class="form-group" id="makeDiv">
                        <label> Make </label>
                        <input type="text" id="make" readonly name="make" 
                        class="form-control" value="{{ $vehicle->first_name }}">
                         <div id="make_list">
                         </div>

                    </div>
                    <div class="form-group" id="modelDiv">
                        <input type="text" id="model" name="model" readonly
                        class="form-control" value="{{ $vehicle->model_name }}">
                        <div id="make_list">
                        </div>
                    </div>
                    <div class="form-group" id="yearDiv">
                        <input type="number" id="year" name="year" readonly
                        class="form-control" value="{{ $vehicle->year }}">
                    </div>
                    <label> Change Car </label>
                    <input type="checkbox" name="change" class="form-control" onclick="changecar();">
                </div>

                @endif
            </div>
        </div>
    </div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
</div>
{!! Form::close() !!}

<script type="text/javascript">
	function previewFile() {
	  var preview = document.querySelector('img');
	  var file    = document.querySelector('input[type=file]').files[0];
	  var reader  = new FileReader();
	  var text = document.getElementById('ValidProfile');
	  text.style.display="none";
	  preview.style.display = "inline";
	  reader.onloadend = function () {
	    preview.src = reader.result;
	  }

	  if (file) {
	    reader.readAsDataURL(file);
	  } else {
	    preview.src = "";
	  }
    }

    $(document).ready(function(){

       $(document).on('keyup', '#make', function(){ 
              var query = $(this).val();
              if(query != '')
              {
               $.ajax({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  type: "POST",
                  
                  url:"{{ route('dashboarduser.fetchVehicleMake') }}",
                  method:"POST",
                  data:{
                    query : query,
                   _token: $('meta[name="csrf-token"]').attr('content') 
                 },
                  success:function(data){
                   $('#modelDiv').remove();
                   $('#yearDiv').remove();
                   $('#make_list').fadeIn();  
                   $('#make_list').html(data);
                   

                  },
                  error: function (request, status, error) {
                    alert(status + " " + error + " "  + request.responseText);
                },
               });
              }
          });

       $(document).on('click', '#makeDiv li', function(){  
            $('#make').val($(this).text());  
            $('#make_list').fadeOut();  


            var model = '<div class="form-group" id="modelDiv">' +
                        '<label> Model </label>'+
                        '<input type="text" id="model" name="model" class="form-control">'+
                        '<div id="model_list">'+
                        '</div></div>';

            $('#car-details').append(model).hide().fadeIn(500);
        });

       $(document).on('keyup', '#model', function(){ 

              var model = $(this).val();

              var make = $('#make').val();

              if(model != '')
              {
               $.ajax({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  type: "POST",
                  
                  url:"{{ route('dashboarduser.fetchModel') }}",
                  method:"POST",
                  data:{
                    model : model,
                    make : make,
                   _token: $('meta[name="csrf-token"]').attr('content') 
                 },
                  success:function(data){
                   $('#yearDiv').remove();
                   $('#model_list').fadeIn();  
                   $('#model_list').html(data);
                   

                  },
                  error: function (request, status, error) {
                    alert(status + " " + error + " "  + request.responseText);
                },
               });
              }
          });
       

       $(document).on('click', '#modelDiv li', function(){
            $('#model').val($(this).text());  
            $('#model_list').fadeOut();  
            var model = $('#model').val();

            console.log(model);
            $.ajax({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  type: "POST",
                  
                  url:"{{ route('dashboarduser.fetchVehicleYear') }}",
                  method:"POST",
                  data:{
                    model : model,
                   _token: $('meta[name="csrf-token"]').attr('content') 
                 },
                  success:function(data){
                  console.log(data);
                   var year = '<div class="form-group" id="yearDiv">' +
                        '<label> Year </label>'+
                        '<select name="year" class="form-control id="year">';

                        for(var i=0; i < data.length; i++) {
                        
                          year = year + '<option value="'+data[i].id+'">'+data[i].year+'</option>';
                        }

                        year = year + '</select>'+
                        '</div>';

                  $('#car-details').append(year).hide().fadeIn(500);
                   

                  },
                  error: function (request, status, error) {
                    alert(status + " " + error + " "  + request.responseText);
                },
               });
            
        });


   });

  function changecar(){
    jQuery('#car-details').remove();

    var data = '<div class="col-xs-12 col-sm-12 col-md-12" id="car-details">'+
                    '<div class="form-group" id="makeDiv">'+
                        '<label> Make </label>'+
                        '<input type="text" id="make" name="make" class="form-control">'+
                         '<div id="make_list">'+
                         '</div>'+
                    '</div>'+
                '</div>';

    jQuery('#carInfo').append(data);
  }

</script>

@endsection