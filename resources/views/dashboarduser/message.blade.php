@extends('layouts.app')
@section('content')


<div class="container">
    <div class="container-fluid offer-ride">

    	@include('dashboarduser.layout.menu')
        
        <div class="col-md-offset-1 col-md-10 pages-bottom-pad">

            <div class="filter">
                <div class="dropdown">
        			<button class="btn btn-success dropdown-toggle message-filter-button" type="button" id="dropdownMenuButtonMessage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        			&#9663;
        			</button>
        			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        				<a class="dropdown-item" id="control-1" href="{{ route('messages.filter', '1' ) }}">All Messages</a>
        				<a class="dropdown-item" id="control-2" href="{{ route('messages.filter', '2' ) }}"> Notifications </a>
        				<a class="dropdown-item" id="control-3" href="{{ route('messages.filter', '3' ) }}">Unread Messages</a>
        			</div>
                </div>
            </div>
            <div class="container-fluid bordered-container pages-bottom-pad">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                      <p>{{ $message }}</p>
                    </div>
                @endif

                @include('messenger.partials.flash')

                <div class="row">
                	<div class="col-md-12">
                		<div class="widget">
                			@each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
                		</div>
                	</div>
            	</div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a href="{{ route('messages.create') }}"> <button type="submit" class="btn btn-primary">New Message</button></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var control = {{ $control }};
    if(control == 1 || control == 0){
        jQuery('#control-1').append('&#x2714;');
        jQuery('#dropdownMenuButtonMessage').append('All Messages');
    }else if(control == 2){
        jQuery('#control-2').append('&#x2714;');
        jQuery('#dropdownMenuButtonMessage').append('Notifications');
    }else{
        jQuery('#control-3').append('&#x2714;');
        jQuery('#dropdownMenuButtonMessage').append('Unread');
    }
</script>
	
@endsection