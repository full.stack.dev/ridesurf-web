@extends('layouts.app')

@section('content')

<div class="container">
  <div class="container-fluid offer-ride">

    <!-- Get Top Menu -->
  	@include('dashboarduser.layout.menu' )

    <div class="col-md-12">
      <h2 class="text-thin"><span class="icon"><i class="fa fa-pencil-square-o margin-right-15"></i></span>Reviews</h2>

      <div class="col-md-offset-1 col-md-10 pages-bottom-pad">
        <!-- Reviews Container -->
        <div class="misc-container">

                <div id="app">
                  <!-- Passing the value from controller to vue -->
                  <reviews :user='{{ $user }}'></reviews>
                </div>
                <div id="search"></div>
                <script src="{{asset('js/app.js')}}"></script>
          </div>
        </div>
      </div>
    </div>       
  </div>
</div>
@endsection