<div>

<h2 class="text-thin border-bottom-gray">Past Rides</h2>
	
	<?php $count = 0; ?>
	@if($userRide != null)
		@foreach($userRide as $join)

			@if( $join->date <= $now && $join->status == 'Approved' && $join->sale_id != null)

				<div class="bordered-box-2">
					<div class="row">
						<div class="col-md-3">
							<div class="profile-pic-sm">
								<img src="{{ $join->ride->user->avatar }}" alt="" class="img-circle">
							</div>
							<p> <strong>Driver:</strong> {{ $join->ride->user->first_name }} </p>
						</div>
						<div class="col-md-6">
							<p>{{ $join->ride->location_from }} &#8594; {{ $join->ride->location_to }}</p>
							<p><strong>Date:</strong> {{ date('m/d/Y', strtotime($join->ride->dateFrom)) }} </p>
							<p><strong>Time:</strong> {{date("h:i a", strtotime($join->ride->startHour))}}</p>
						</div>
						<div class="col-md-3" id="join-buttons">


							@if($join->ride->rated == null )
								<p><a class="btn btn-primary" href="{{ route('reviews.rate',[$join->ride->user->id,$join->ride->id]) }}">Review</a></p>
							@else
								<p>Reviewed</p>
							@endif
							@if(count($join->reported) == 0)
								<p><a class="btn btn-danger" href="{{ route('dashboarduser.reportRide',[$join->ride->id]) }}">
										<i class="fa fa-flag"></i> Report
									</a></p>
							@else
								@if($join->reported->passenger_id != $user->id)
									<p><a class="btn btn-danger" href="{{ route('dashboarduser.reportRide',[$join->ride->id]) }}">
											<i class="fa fa-flag"></i> Report</a></p>
								@endif
							@endif

						</div>
					</div>
				</div>
				<?php $count++; ?> 
			@endif
		@endforeach
	@endif

	@if($count == 0)
		<div class="widget text-center">
			<p>You have no past rides.</p>
		</div>
	@endif
</div>