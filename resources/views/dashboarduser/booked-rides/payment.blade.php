@if($userRide != null)

	<h2 class="text-thin border-bottom-gray">Pending Payment</h2>

	@foreach($userRide as $join)
		@if($join->status == 'Approved' && ($join->booker_payment_paid == 'No' ||  
			$join->booker_payment_paid == null) 
			&& $join->date >= $now)

			<div class="bordered-box-2">
			
				<div class="row">
					<div class="col-md-4 col-sm-12">
				    	<img src="{{ $join->ride->user->avatar }}" alt="profile image" class="img-circle image-width-60">
				    	<h4> {{ $join->ride->user->first_name }} 
				    		 {{ $join->ride->user->last_name }} 
				    	</h4>
				    	<p class="small-light"> Joined {{ date('j M Y', strtotime($join->ride->user->created_at)) }} </p>

				    	<div class="stars-{{ $join->ride->user->id }}">
			                <span data-rating="1" class="fa fa-star checked"></span>
			                <span data-rating="2" class="fa fa-star checked"></span>
			                <span data-rating="3" class="fa fa-star checked"></span>
			                <span data-rating="4" class="fa fa-star "></span>
			                <span data-rating="5" class="fa fa-star"></span>
		            	</div>
				    	
				    	<a href="{{ route('rides.viewUserProfile', $join->ride->user_id) }}"
						class="btn btn-info slim-button full-margin-10">View Profile</a>

		            	<script>
							$(document).ready(function(){

							    var stars = $('.stars-'+{{ $join->ride->user->id }});
							    var star  = stars.find('.fa-star');
							    var value = {{ $join->ride->user->rating }}

							    // Remove class for selected stars
							      stars.find('.checked').removeClass(' checked');

							      // Add class to the selected star and all before
							      for (i = 1; i <= value; i++) {
							        stars.find('[data-rating="' + i + '"]').addClass('checked');
							      }
							});
						</script>

					</div>
					<div class="col-md-4 col-sm-12">
						<p> <strong>From: </strong> {{ $join->ride->location_from }}</p>
						<p> <strong>To: </strong> {{ $join->ride->location_to }}</p>
						<p> <strong>Date: </strong> {{ date('m/d/Y', strtotime($join->ride->dateFrom)) }} </p>
						<p><strong>Time:</strong> {{ date("h:i a", strtotime($join->ride->startHour)) }}</p>
						<p><strong>Status:</strong> {{ $join->status }} <br> <strong>Payment Required</strong> </p>
					</div>
					<div class="col-md-4 col-sm-12" id="join-buttons">

						<a class="btn btn-grey bottom-margin-10" style="width: 120px !important" href="{{ route('rides.payment', $join->ride->id) }}">
							<i class="fa fa-credit-card padding-right-10"></i>Pay Ride</a>

						<a class="btn btn-success bottom-margin-10" style="width: 120px !important" href="{{ route('messages.contact', $join->ride->id) }}">
							<i class="fa fa-envelope-o padding-right-10"></i>Contact</a>

						{!! Form::open(['method' => 'DELETE','route' => ['rides.destroyUserRide', $join->ride->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Cancel', ['class' => 'btn btn-danger bottom-margin-10']) !!}
					        {!! Form::close() !!}
					</div>
				</div>
			</div>

		@endif
	@endforeach

@else

	<h2 class="text-thin border-bottom-gray">Pending Payment</h2>

	<div class="widget text-center">
		<p>You have no pending payments.</p>
	</div>


@endif