@extends('layouts.app')

@section('content')

<div class="container">
<div class="container-fluid offer-ride">

  <!-- Get Top Menu -->
	@include('dashboarduser.layout.menu' )

	<div class="wrapper">
		<h2> Rides Requested Notifications</h2>
		<table class="table table-striped">
			<thead>
			<tr>
				<th> From </th>
				<th> To </th>
				<th> State </th>
				<th> Requested </th>
				<th> From Date </th>
				<th> Actions </th>
			</tr>
			</thead>
			<tbody>
				@foreach($ride_requests as $requests)
				<tr>
					<td> {{$requests->city_from}}</td>
					<td> {{$requests->city_to}}</td>
					<td> {{$requests->state}}</td>
					<td> {{$requests->created_at->diffForHumans(null, true).' ago'}}</td>
					<td> {{ date('m/d/Y', strtotime($requests->date)) }}</td>
					<td>
						{!! Form::open(['method' => 'DELETE','route' => ['ride_request.destroy', $requests->id],' style'=>'display:inline']) !!}
				            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
				        {!! Form::close() !!}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

</div>
</div>

@endsection