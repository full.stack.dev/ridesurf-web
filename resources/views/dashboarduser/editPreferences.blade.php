@extends('layouts.app')
@section('content')

<div class="container">
	<div class="container-fluid offer-ride">
	<!-- Menu -->
		@include('dashboarduser.layout.menu')
		
	<!-- SideBar-->
		<div class="no-mobile">
	 		@include('dashboarduser.layout.sidebar')
	 	</div>
	 	<div class="col-md-7 col-xs-12">
	 		<div class="misc-container">
		 	{!! Form::open(['method' => 'PUT','route' => ['dashboarduser.updatePreferences'] ]) !!}
		 		@foreach($user_preference_single as $uPreference)
			 		<div class="form-group">
			 			<label>{{ $uPreference->preference->category }}</label>
			 			<select name="{{ $uPreference->preference->category  }}" class="form-control">
			 				<option hidden selected value="{{ $uPreference->preference->id }}">
			 					{{ $uPreference->preference->description }}
			 				</option>
			 				@foreach($preferences as $preference)
			 					@if($preference->category == $uPreference->preference->category)
			 						<option value="{{ $preference->id }}">
			 							{{ $preference->description }}</option>
			 					@endif
			 				@endforeach
			 			</select>
			 		</div>
		 		@endforeach
		 		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
	              <button type="submit" class="btn btn-primary">Save Changes</button>
	          </div>
		 	{!!  Form::close() !!}
		 	</div>
	 	</div>
	</div>
</div>
@endsection