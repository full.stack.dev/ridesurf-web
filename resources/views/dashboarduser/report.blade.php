@extends('layouts.app')
@section('content')
@if ($message = Session::get('success'))
	<div class="alert alert-success">
	  <p>{{ $message }}</p>
	</div>
@endif

<div class="container">
<div class="container-fluid offer-ride">
	@include('dashboarduser.layout.menu' )
	@include('dashboarduser.layout.dashboard', [$user, $booked, $offer])
	<!-- SideBar-->
	@include('dashboarduser.layout.sidebar', [$user])

	<div class="col-md-7 col-xs-12">
	    <div class="col-md-12 col-xs-12">
	    	<h3 class="text-center">Report Problem</h3>
	    	<p class="text-center"><strong>User:</strong> {{ $ride->user->first_name }} {{ substr($ride->user->last_name, 0,1) }}.</p>

	    	@if( count($ride->reported) == 0 )
		    	{!! Form::open(array('route' => ['dashboarduser.storeReport' , $ride->id],'method'=>'POST')) !!}
		    	<div class="form-group">
		    		<label>Reason for report:</label>
		    		<select name="reason" class="form-control"> 
		    			<option>Ride did not happen</option>
		    			<option>Driver was inappropriate</option>
		    			<option>Ride was not as expected</option>
		    		</select>
		    	</div>

		    	<div class="form-group">
		    		<label>Please Describe:</label>
		    		<textarea class="form-control" name="description"></textarea>
		    	</div>

		    	<button class="btn btn-primary">Submit</button>
		    	{!! Form::close() !!}
	    	@else
	    		<h3>Your problem has been reported. Please allow us up to 24 to respond.</h3>
	    	@endif
	    </div>
	</div>
</div>
</div>