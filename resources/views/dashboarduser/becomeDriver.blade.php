@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('node_modules/croppie/croppie.css')}}">

<div class="container">
  <div class="container-fluid offer-ride">
    @if (Session::has('message'))
       <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @include('dashboarduser.layout.menu')
    @include('dashboarduser.layout.dashboard', [$user, $booked, $offer])
    <!-- SideBar-->
    <div class="no-mobile">
      @include('dashboarduser.layout.sidebar', [$user])
    </div>
    <div class="col-md-7 col-xs-12" id="form-div">

      @if( $user->avatar == null || $user->phone_number == null || $user->verified == 0
        || $user->address == null || $user->state == null || $user->city == null || $user->country == null)


          <div class="col-md-12" style="margin-bottom: 20px;">
            <h2>Become A Driver</h2>
          </div>
          <div class="col-md-12">
            <h4>Sorry, you cannot become a driver until you complete the following items:</h4>
            <ul class="uncheck-list">
            @if($user->verified == 0)
              <li>Your email is not <strong> verified </strong></li>
            @endif
            @if($user->avatar == null )
              <li>Profile image <strong> not provided </strong></li>
            @endif
            @if($user->phone_number == null)
              <li>Phone number is <strong> not provided </strong></li>
            @endif
            @if($user->address == null)
              <li> Address is <strong>not provided</strong></li>
            @endif
            @if($user->city == null)
              <li> City is <strong>not provided</strong></li>
            @endif
            @if($user->state == null)
              <li> State is <strong>not provided</strong></li>
            @endif
            @if($user->country == null)
              <li> Country is <strong>not provided</strong></li>
            @endif
            </ul>

            <div class="text-center" style="margin-top: 50px;">
            <a class="btn btn-info" href="{{ route('dashboarduser.profile') }}">Edit your profile</a>
            </div>

          </div>
      @else

        <!-- If everything is working -->
        @if(count($user->driver) > 0 )
          <h2>Change Car</h2>
            @if($user->driver[0]->status == 'Requested')
              <div class="alert alert-warning">
                <p> We recieved your application and we are currently reviewing it.</p> 
              </div> 
            @elseif($user->driver[0]->status == 'Denied')
                <div class="alert alert-danger">
                  <p> {{ $user->driver[0]->status_message }} </p> 
                  <p> Please press update after you complete the necesary information:</p> 
                </div>
            @endif
            <div id="app">
                <changecar usercar="{{ $driver }}"
                            vehicles="{{ $vehicles }}"
                ></changecar>
            </div>
            <script src="{{asset('js/app.js')}}"></script>
        @else
          <h3>Become A Driver</h3>
            <div id="app">
                <newdriver vehicles="{{ $vehicles }}"></newdriver>
            </div>
            <script src="{{asset('js/app.js')}}"></script>
        @endif


       @endif
    </div>
  </div>
  </div>

@endsection
