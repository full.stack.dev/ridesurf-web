@extends('layouts.app')
@section('content')

<script>
    //VueJS conflicts with dropdown
    jQuery('.dropdown-toggle').dropdown();
</script>


<div class="container">
  <div class="container-fluid offer-ride">

    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
    
    @include('dashboarduser.layout.menu')

    <!-- SideBar-->
    <div class="no-mobile">
      @include('dashboarduser.layout.sidebar', [$user, $preferences, $settings])
    </div>
    <!-- End of Sidebar -->

    <div class="col-md-8 col-xs-12 bottom-pad mobile-pad">

      <h2 class="text-thin">Edit Profile</h2>

      <div class="misc-container">

        <div id="app">
          <profile states="{{$state}}" user="{{ $user }}" countries="{{$countries}}"></profile>
        </div>

        <script src="{{asset('js/app.js')}}"></script>

      </div>
    </div>
  </div>
</div>

<script>
    function updateAreaCode(code){
        var newCode = "+" + code;
        jQuery('#areaCode').val(newCode);
        $('#countries').modal('hide');
    }
</script>
@endsection
