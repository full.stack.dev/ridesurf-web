@extends('layouts.app')
@section('content')

<div class="container">
<div class="container-fluid offer-ride">

	@include('dashboarduser.layout.menu')

	<div class="col-md-12 bottom-pad">

		@if(count(Auth::user()->driver) > 0 )
			@if(Auth::user()->paypal_email == null)
				
				<div class="alert alert-danger" role="alert">
					<p> We use Paypal to send out driver earnings. Please update your Paypal e-mail on the profile tab.</p> 
				</div>
			@endif

		    	<table class="table table-striped">
				  <thead>
				    <tr>
				      <th scope="col">#RideID</th>
				      <th scope="col">Date</th>
				      <th scope="col">Passenger</th>
				      <th scope="col">Status</th>
				      <th scope="col">Total Pay</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@if(!empty($rides))
				  	@foreach($rides as $ride)

				    <tr>
				      <th scope="row">{{ $ride->id }}</th>
				      <td>{{ $ride->dateFrom }}</td>
				      <td>{{ $ride->passengers}}</td>
				      <td>
				      		@if($ride->paid_driver == 'Yes')
				      			Paid
				      		@else
				      			Not Paid Yet
				      		@endif
				      </td>
				      <td>${{ $ride->totalToPay }}</td>
				    </tr>

				    @endforeach
				    @endif
				  </tbody>
				</table>
		@else
			<p> You are not an approved driver yet. You may apply to be a driver by <a href="{{ route('dashboarduser.becomeDriver') }}"> clicking here</a>. </p> 
		@endif
	</div>
</div>
</div>
@endsection
