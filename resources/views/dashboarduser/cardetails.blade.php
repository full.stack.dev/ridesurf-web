@extends('layouts.app')
@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('node_modules/croppie/croppie.css')}}">

<div class="container">
  <div class="container-fluid offer-ride">
  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
  	@include('dashboarduser.layout.menu' )
  <!-- SideBar-->
    <div class="no-mobile">
    @include('dashboarduser.layout.sidebar', [$user, $preferences, $settings])
  </div>
  <!-- End of Sidebar -->
        <div class="col-md-8 col-xs-12">

          <div class="form-group">
              
              <div class="row" id="carInfo">
                <ul class="verified-info">
                  @if(count($user->driver) > 0)
                  <h2> Car Details </h2>
                  @if($user->driver[0]->status == 'Denied')
                    <li class="uncheck-list">
                   <strong>Status: </strong> 
                    Please add your vehicle's information again to become a driver.
                  </li>
                  @endif
                  @if($user->driver[0]->status == 'Requested')
                  <li class="list" style="list-style: none">
                      <strong>Status: </strong> Your information is being reviewed.
                  </li>
                  @endif
                  @if($user->driver[0]->status == 'Approved')
                  <li class="check-list">
                    <strong>Status: </strong> 
                    You are an approved driver.
                  </li>
                  @endif
                </ul>
                  <div class="col-xs-12 col-sm-12 col-md-12" id="car-details">
                    
                    <div class="col-md-offset-9 form-group">
                        <a href="{{ route('dashboarduser.becomeDriver') }}" class="btn btn-default form-control" 
                        style="z-index: 999;">Edit Info</a>
                    </div>
                    <div class="col-sm-12 col-md-12 form-group">
                      <label>Vehicle Image:</label><br/>
                      <img src="{{ $vehicle->path_image }}" class="circle-image-full">
                    </div>
                    <div class="col-sm-12 col-md-12 form-group">
                      <label>Registration:</label><br/>
                      <img src="{{ $vehicle->registration_path }}" width="250" >
                    </div>
                    <div class="col-sm-12 col-md-12 form-group">
                      <label>Insurance:</label><br/>
                      <img src="{{ $vehicle->insurance_path }}" width="250" >
                    </div>
                    <div class="col-sm-12 col-md-12 form-group">
                      <label>Driver's License:</label><br/>
                      <img src="{{ $vehicle->driver_license }}" width="250" >
                    </div>
                    
                    <div class="form-group" id="makeDiv">
                          <p><strong>Make:</strong> {{ $vehicle->make }}</p>
                           <div id="make_list">
                           </div>

                      </div>
                      <div class="form-group" id="modelDiv">
                        <p><strong>Model:</strong> {{ $vehicle->model }}</p>
                          <div id="make_list">
                          </div>
                      </div>
                      <div class="form-group" id="classDiv">
                        <p><strong>Type:</strong> {{ $vehicle->type }}</p>
                          <div id="make_list">
                          </div>
                      </div>
                      <div class="form-group" id="yearDiv">
                        <p><strong>Color:</strong> {{ $vehicle->color }}</p>
                      </div>
                      <div class="form-group" id="yearDiv">
                        <p><strong>Year:</strong> {{ $vehicle->year }}</p>
                      </div>
                      

                        <div class="form-group" id="tagDiv">
                          <p><strong>License Plate:</strong> {{ $vehicle->vehicle_tag }}</p>
                        </div>
                  </div>
                  @endif
              </div>
          </div>
        </div>
  </div>
</div>
@endsection