<button type="button" class="btn btn-info btn-lg padding" data-toggle="modal" data-target="#myModal"> <i class="fa fa-upload margin-right-15"></i> Upload Image</button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-upload"></i> Upload Picture</h4>
      </div>
      <div class="modal-body">
      	<div id="resize"></div>
        <input type="hidden" name="newImage" value="0" id="newImage">
        <input type="hidden" id="profile_image" name="profile_image">
        <input type="file" accept="image/*" onchange="previewFile()" class="form-control" id="profile_image_upload" 
        name="profile_image_upload">


      	<div id="controls"></div>
        <div id="imageCanvas">
          <img src="{{ $user->avatar }}"  alt="Image preview..." 
        	id="preview" style="max-width: 100%;">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="closeModel" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script src="{{asset('node_modules/croppie/croppie.js')}}"></script>
<script type="text/javascript">

  var $uploadCrop;

	function previewFile() {
    jQuery('.cropper-bg').remove();
	  var preview = document.getElementById('preview');
    jQuery('#profile_image_upload').hide();
	  var file    = document.querySelector('input[type=file]').files[0];
	  var reader  = new FileReader();

	  preview.style.display = "inline";
	  reader.onloadend = function () {
	    preview.src = reader.result;
	  }

	  if (file) {
	    reader.readAsDataURL(file);
	  } else {
	    preview.src = "";
	  }

    jQuery('#controls').html('<div class="alert alert-info" id="crop-text" > <p ><strong> Please crop your photo then click upload</strong> </p> </div>'
      +'<p class="btn btn-primary" id="makeCrop" onclick="makeCropper();"> Crop Image </p>');

  }

function rotateleft(data){
  $uploadCrop.croppie('rotate', -90);
}
function makeCropper(){
  jQuery('#uploadImage').remove();
  jQuery('#makeCrop').hide();
  jQuery('#preview').hide();
  jQuery('#controls').append('<p onclick="cropimage();" id="cropButton" class="btn btn-primary"> Crop </p>');
  jQuery('#controls').append('<p class="btn btn-primary" id="rotateLeft" onclick="rotateleft(-90);" data-deg="-90">Rotate Left</p>');
  $uploadCrop = jQuery('#preview').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square'

    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true,
  });
}

function cropimage(){

  jQuery('#cropButton').remove();
  jQuery('#rotateLeft').remove();
  var $uploadCrop = $('#preview');
  $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function (resp) {
      var preview = document.getElementById('preview');
      preview.src = resp;
      $('#profile_image').val(resp);
        var value = $('#profile_image').val();

        $('input[name=newImage]').val("1");
    });
  jQuery("#preview").show();
  jQuery('#crop-text').remove();
  jQuery("#preview").croppie('destroy');
  jQuery('#makeCrop').show();

  var upload = '<button id="uploadImage" onClick="uploadImage();" class="btn btn-warning padding">Upload*</button>'
  jQuery('#imageCanvas').append(upload);

}

function uploadImage(){

  var image = jQuery('#profile_image').val();

  $.ajax({
        type:"POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:"{{ route('dashboarduser.uploadProfileImage') }}",
        method:"POST",
        dataType: "json",
        data:{
          image:image,
          _token : $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data){
          jQuery('#profile_image_upload').val('');
          jQuery('#profile_image_upload').show();
          jQuery('#controls').html('');
          jQuery('#controls').append('<p> Current Picture </p>');
          var data = '<div class="alert alert-success" id="displayError" role="alert">'+
                  '<p> '+data['message']+'</p>'+
                '</div>';
          jQuery('#alertError').append(data).fadeIn(1000);
          jQuery('#displayError').fadeOut(4500);
          jQuery('#myModal').modal('hide');
          location.reload();

        },
        error: function (xhr, status, error) {
          console.log(xhr.responseText);
          var err = JSON.parse(xhr.responseText);
          var data = '<div class="alert alert-danger" id="displayError" role="alert">'+
                  '<p> '+err.message+'</p>'+
                '</div>';
              jQuery('#alertError').append(data).fadeIn(1000);
              jQuery('#displayError').fadeOut(4500);
          }

    });

}
$(document).ready(function(){

  $('#state_select').hide();

  $('#submitButton').on('click', function(e){
     $('#profile_image_upload').val('');
  })


  $('#countries').on('change', function(e){
    var country = $(this).val();
    if(country == "United States"){
      $('#state_input').hide();
      $('#state_input').prop("disabled",true);
      $('#state_select').show()
      $('#state_select').prop("disabled",false);
    }else if(country == ""){
      $('#state_select').hide();
      $('#state_select').prop("disabled",true);
      $('#state_input').show();

      $('#state_input').prop("disabled",false);
    }


  });

});

</script>