
<div class="col-md-12">
	@foreach($rides as $ride)
	<?php $today = strtotime(Carbon\Carbon::now()); 
		  $time_ride = strtotime($ride->dateFrom  ." ".$ride->startHour);
	?>
	
	<div class="row rides-list">

		<div class="col-md-2">
			<h4 class="title">Driver</h4>
				<div class="profile-pic-sm" style="text-align: center;">
					<img src="{{ $ride->user->avatar }}" 
						alt="profile_image" class="img-circle"><br />	
					<p align="center">{{ $ride->user->first_name }} {{ $ride->user->last_name }}</p>
					@include('layouts.rating', [$ride->rating, $ride->id] )
					<a href="{{ route('rides.viewUserProfile', $ride->user_id) }}"
					class="btn btn-info">View Profile</a>
				</div>
		</div>

		<div class="col-md-6">
			<h4 class="title">Trip Details</h4>

			<span class="add-id"><i class="fa fa-map-marker"></i><strong>  From:</strong> {{ $ride->location_from }} </span> <br/>
			<span class="add-id"><i class="fa fa-map-marker"></i><strong>  To:</strong> {{ $ride->location_to }} </span> <br/>
			<span class="add-id"><i class="fa fa-calendar"></i><strong>  Date:</strong> {{ date('m/d/Y', strtotime($ride->dateFrom)) }}</span><br/>
			<span class="add-in"><i class="fa fa-clock-o"></i><strong>  Time: </strong> {{date("g:i a", strtotime($ride->startHour)) }}
			</span> <br/>
			<span class="add-in no-mobile"><i class="fa fa-map-o"></i><strong>  Description: </strong> {{ $ride->description }}
			</span> <br/>
			@if($ride->ladies_only == 'Yes')
				<span class="add-in ladies-pink">
					<i class="fa fa-female"></i> <strong>WOMEN ONLY</strong> {{ $ride->ladies_only }}
				</span>
			@endif
		</div>

		
		<div class="col-md-2">
			<h4 class="title">Booking</h4>
			<div style="text-align: center;">
				<h2 class="rideprice"> ${{ $ride->price }}</h2>
				
				@if($ride->stop == true)
					<input type="text" name="isStop" value="{{ $ride->stop  }}" style="display: none;">
				@endif
			</div>
		</div>

		<div class="col-md-2 top-margin-15">

			@if($time_ride >= $today)
				
				<p>Seats Available: {{ $ride->seats_available }} </p>
				<p>Passengers: {{count($ride->user_ride)}}</p>
				<a class="btn btn-warning" href="{{ route('rides.edit',$ride->id) }}">Edit</a>
				
					{!! Form::open(['method' => 'DELETE','route' => ['rides.destroy', $ride->id],'style'=>'display:inline']) !!}
		            	{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
		        	{!! Form::close() !!}
		        
		        @else
		        	<a class="btn btn-success" href="{{ route('rides.show',$ride->id) }}">show</a>
		        	@if(count($ride->user_ride) > 0 )
		        		<div class="wrapper" style="padding-top: 25px">
		        			<p>Seats Available: {{ $ride->seats_available }} </p>
							<p>Passengers : {{count($ride->user_ride)}}
		        		@foreach($ride->user_ride as $user_ride)
		        			@if($user_ride->driver_rate == null && $user_ride->transaction_id != null)
								@if($user_ride->user != null )
									<p> Review Passenger: {{ $user_ride->user->first_name }}</p>
									<a class="btn btn-success" href="{{ route('reviews.rate',[$user_ride->user->id,$ride->id]) }}">Review</a>
						        @else
									<p> User deleted account </p>
								@endif
		        			@elseif($user_ride->driver_rate != null)
		        				@if(count($user_ride->user))
		        					<p> Thanks for your review to {{ $user_ride->user->first_name }}
		        				@endif
		        			@else
		        				<p> No passengers </p>
		        			@endif
		        		@endforeach
		        		</div>
		        	@endif
		        @endif
		</div>
	</div>
	@endforeach
	{{ $rides->links() }}
</div>
