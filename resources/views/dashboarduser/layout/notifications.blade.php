
@if( !empty($notifications)) <!-- Check that there are Notifications -->

  @foreach($notifications as $passenger)
  
    @if(!empty($passenger)) <!-- Check that the notification is not empty --> 
      @if($passenger->status == 'Requested')
          <div class="row notifications">

            <div class="col-md-3">
              <div class="profile-pic-sm">
                <img src="{{ $passenger->user->avatar }}">
              </div>

              <div class="stars">
                <span data-rating="1" class="fa fa-star"></span>
                <span data-rating="2" class="fa fa-star"></span>
                <span data-rating="3" class="fa fa-star"></span>
                <span data-rating="4" class="fa fa-star"></span>
                <span data-rating="5" class="fa fa-star"></span>
              </div>

              <a href="{{ route('rides.viewUserProfile', $passenger->user->id) }}"
                    class="btn btn-info">View Profile</a>
            </div>
            <div class="col-md-6">
                @if($passenger->ride) <!-- is a normal ride -->

                <p><strong>{{ $passenger->user->first_name }} has requested to join your ride:</strong></p>
                <p>{{ $passenger->city_from }} &#8594; {{ $passenger->city_to }} <br/>
                <p> <strong>Date : </strong>{{ date("m/d/Y", strtotime($passenger->ride->dateFrom)) }} <br/>
                <strong>Reviews:</strong> {{ count($passenger->user->reviewTo) }}<br/>
                <strong>Preferences:</strong> <?php $pref_user = DB::table('user_preferences')
                                    ->where('user_id', '=', $passenger->user->id)->get(); ?>
                </p>
                @else <!-- is an open ride -->
                    <p> <small> Accepting would switch your open ride to a normal ride with this date and hour.</small></p>
                    <p><strong>{{ $passenger->user->first_name }} has requested to set a date on your    <italic> Open Ride </italic> </strong></p>
                    <p>{{ $passenger->open_ride->city_from }} &#8594; {{ $passenger->open_ride->city_to }} <br/>
                    <p> <strong>Suggested Date : </strong>{{ date("m/d/Y", strtotime($passenger->date)) }} <br/>
                        <strong>Suggested hour:  </strong>{{ date("g:i a", strtotime($passenger->hour)) }} <br/>
                        <strong>Reviews:</strong> {{ count($passenger->user->reviewTo) }}<br/>
                        <strong>Preferences:</strong> <?php $pref_user = DB::table('user_preferences')
                        ->where('user_id', '=', $passenger->user->id)->get(); ?>
                @endif
                <p> 
                @include('dashboarduser.layout.preferences')
                </p>

            </div>
            <div class="col-md-2">

                <!-- Normal Ride --> 

                @if($passenger->ride) 
                    {!! Form::open(array('route' => ['rides.acceptPassenger',
                    $passenger->ride->id, $passenger->user->id] ,'method'=>'POST')) !!}<button type="submit" class="btn btn-success">Accept</button>{!! Form::close() !!}
                    {!! Form::open(array('route' => ['rides.denyPassenger',
                    $passenger->ride->id, $passenger->user->id] ,'method'=>'POST')) !!}<button type="submit" class="btn btn-danger" style="min-width: 89px !important;">Deny</button>{!! Form::close() !!}
                
                <!-- Open Ride --> 

                @else 
                    {!! Form::open(array('route' => ['rides.acceptOpenRide',
                    $passenger->id] ,'method'=>'POST')) !!}

                    <button type="submit" class="btn btn-success">Accept</button>

                    {!! Form::close() !!}

                    {!! Form::open(array('route' => ['rides.denyOpenRide',
                    $passenger->id ] ,'method'=>'POST')) !!}

                    <button type="submit" class="btn btn-danger" style="min-width: 89px !important;">Deny</button>

                    {!! Form::close() !!}
                @endif
            </div>
        </div>
        @endif
        
        @if($passenger->status == 'Approved' && ($passenger->booker_payment_paid == 'No' || $passenger->booker_payment_paid == null))
          <div class="row notifications">
            <h4>Congrats! Your ride has been approved.</h4>
            <p>Your trip to {{$passenger->ride->location_to}} has been approved by the driver, please pay now to confirm your ride.</p>
            <a href="{{route('rides.payment', $passenger->ride->id)}}" class="btn btn-success">Pay Driver </a>
          </div>
        @endif

        @if($passenger->status == 'Approved' && $passenger->booker_payment_paid == 'Yes' 
            && $passenger->time_ride <= $today)
          <div class="row notifications">
            <h4 class="greenish">How was your ride to {{ $passenger->ride->city_to }} on {{ date("m/d/Y", strtotime($passenger->ride->dateFrom)) }}?</h4>
            <p>Please review the ride/driver and if something went wrong, let us know!</p>
            <a class="btn btn-primary full-margin-10" 
            href="{{ route('reviews.rate',[$passenger->ride->user->id,$passenger->ride->id]) }}">Review Ride</a>
            <a class="btn btn-danger float-right full-margin-10" href="{{ route('dashboarduser.reportRide',[$passenger->ride->id]) }}">
                <i class="fa fa-flag"></i> Report Problem</a>

          </div>
        @endif
    @endif
  @endforeach <!-- End loop of each notification -->
@endif <!-- End if is Notifications are Empty -->
