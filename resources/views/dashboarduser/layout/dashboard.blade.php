
<div class="col-md-12">
  <div class="container-fluid profile-header">
    
    <div class="col-md-3">
      <div class="profile-details">
        <div class="profile-pic top-margin-15">
         
          @include('layouts.userProfile')
          
          <div class="stars top-margin-15">
    		    <span data-rating="1" class="fa fa-star checked"></span>
    		    <span data-rating="2" class="fa fa-star checked"></span>
    		    <span data-rating="3" class="fa fa-star checked"></span>
    		    <span data-rating="4" class="fa fa-star "></span>
    		    <span data-rating="5" class="fa fa-star"></span>
    		  </div>
  		  
          <a href="{{ route('rides.viewUserProfile', $user->id) }}"
                    class="btn btn-info slim-button">View Public Profile</a>
        </div>
      </div> <!-- End Profile Details -->
    </div>


    <div class="col-md-9">
      <div class="experience-container">
        <span class="experience-title">Hello, {{ $user->first_name }}!</span>
      </div>

      <div class="multi-step-container">    
          <ul class="list-group list-group-horizontal no-mobile">
            <li class="list-group-item">
                @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                        <div class="dash-readout-sm">{{ $v }}</div>
                    @endforeach
                @endif
                <p class="step-text dash-box-text">Status</p>
            </li>
            <li class="list-group-item">
              <span class="dash-readout">
                {{ $booked }}
              </span>
              <p class="step-text dash-box-text">Booked Rides</p></li>
            <li class="list-group-item">
              <span class="dash-readout">{{ $offer }}</span>
              <p class="step-text dash-box-text">Offered Rides</p>
            </li>
    		    <li class="list-group-item">
              <span class="dash-readout">{{ $messageCount }}</span>
              <p class="step-text dash-box-text">Messages</p>
            </li>
          </ul>
          
      </div> 
    </div>  
  </div>
</div>

 <script type="text/javascript">
	$(document).ready(function(){

			var stars = $('.stars');
	    var star  = stars.find('.fa-star');
      @if($user->rating != null)
	     var value = {{ $user->rating }};
      @else
        var value = 0;
      @endif

			// Remove class for selected stars
			  stars.find('.checked').removeClass(' checked');

			  // Add class to the selected star and all before
			  for (i = 1; i <= value; i++) {
			    stars.find('[data-rating="' + i + '"]').addClass('checked');
			  }
		});
</script>