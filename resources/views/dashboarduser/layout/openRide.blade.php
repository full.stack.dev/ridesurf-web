
@foreach($openRides as $ride)
	<div class="bordered-box-2">
		<div class="row">
			<div class="col-md-2">
				<h4 class="title">Driver</h4>
					<div class="profile-pic-sm" style="text-align: center;">
						<img src="{{ $ride->user->avatar }}" 
							alt="profile_image" class="img-circle"><br />	
						<p align="center">{{ $ride->user->first_name }} {{ $ride->user->last_name }}</p>
						@include('layouts.rating', [$ride->rating, $ride->id] )
						<a href="{{ route('rides.viewUserProfile', $ride->user_id) }}"
						class="btn btn-info">View Profile</a>
					</div>
			</div>
			<div class="col-md-6">
				<h4 class="title">Open Ride Information </h4>
				<p>From: {{ $ride->location_from }}</p> 
				<p>To: {{ $ride->location_to }}</p>
				<p> Dates: </p> 
				<p> From: {{ date('j M Y', strtotime($ride->open_date_from)) }} 
					to {{ date('j M Y', strtotime($ride->open_date_to)) }} </p>
				<p>
				<p> Purpose of Trip: {{ $ride->trip_purpose }}</p>
				@foreach($ride->hours as $hour)
					<label class="badge badge-success"> {{ date("g:i a", strtotime($hour->hour)) }}</label> 
				@endforeach 
				</p> 
			</div>
			<div class="col-md-4">
				
				<h4 class="title">Actions </h4>
				<div class="row">
					<input type="checkbox"  id="active-{{ $ride->id }}" name="my-checkbox">
				</div>
				<a class="btn btn-warning" href="{{ route('open-rides.edit', $ride->id) }}">Edit</a>
				{!! Form::open(['method' => 'DELETE','route' => ['open-rides.destroy', $ride->id],'style'=>'display:inline']) !!}
	            	{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
	        	{!! Form::close() !!}
			</div>
		</div>
	</div>	

<script type="text/javascript">


	$( document ).ready(function() {
		var active = {{ $ride->active }};
	    $("#active-{{ $ride->id }}").bootstrapSwitch({
	    	state:true,
	    });
	    

	});

</script>

@endforeach



