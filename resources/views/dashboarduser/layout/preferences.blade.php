

<!-- ******************** Mobile ******************** --> 
<div class="row preferences-mobile mobile" style="margin-bottom: 20px">
  @foreach($user_preference as $uPreference)

      <div class="col-md-3 col-sm-12">
        @if($uPreference->preference->category == 'Smoking')
        <a href="#" class=""><span class="img-icon fa-stack ">
          @if($uPreference->preference->action == 'No') <!-- No -->
            <i class="material-icons fa-stack-1x big-pref-icons">smoking_rooms</i>
            <i class="material-icons fa-stack-2x big-pref-icons-no">not_interested</i>
          @else <!-- Yes -->
            <i class="material-icons fa-stack-1x big-pref-icons">smoking_rooms</i>
          @endif
        </span>
        </a>
        @endif

        @if($uPreference->preference->category == 'Pets')
        <a href="#" class=""><span class="img-icon fa-stack ">
          @if($uPreference->preference->action == 'No') <!-- No -->
            <i class="fa fa-paw fa-stack-1x big-pref-icons"></i>
            <i class="material-icons fa-stack-2x big-pref-icons-no">not_interested</i>
          @else <!-- Yes -->
            <i class="fa fa-paw fa-stack-1x big-pref-icons"></i>
          @endif
        </span>
        </a>
        @endif

        @if($uPreference->preference->category == 'Chattiness')
        <a href="#" class=""><span class="img-icon fa-stack ">
          @if($uPreference->preference->action == 'No') <!-- No -->
            <i class="fa fa-comments fa-stack-1x big-pref-icons"></i>
            <i class="material-icons fa-stack-2x big-pref-icons-no">not_interested</i>
          @else <!-- Yes -->
            <i class="fa fa-comments fa-stack-1x big-pref-icons"></i>
          @endif
        </span>
        </a>
        @endif

        @if($uPreference->preference->category == 'Music')
        <a href="#" class=""><span class="img-icon fa-stack ">
          @if($uPreference->preference->action == 'No') <!-- No -->
            <i class="fa fa-music fa-stack-1x big-pref-icons"></i>
            <i class="material-icons fa-stack-2x big-pref-icons-no">not_interested</i>
          @else <!-- Yes -->
            <i class="fa fa-music fa-stack-1x big-pref-icons"></i>
          @endif
        </span>
        </a>
        @endif
      </div>

  @endforeach
</div>


<!-- ******************** Desktop ******************** --> 
<div class="no-mobile" style="margin-bottom: 80px">
  @foreach($user_preference as $uPreference)
  <div>
    <div class="pref-container">
    @if($uPreference->preference->category == 'Smoking')
      <a href="#" class="img-icon fa-stack">

        @if($uPreference->preference->action == 'No') <!-- No -->
          <i class="material-icons fa-stack-1x big-pref-icons">smoking_rooms</i>
          <i class="material-icons fa-stack-2x big-pref-icons-no">not_interested</i>
        @else <!-- Yes -->
          <i class="material-icons fa-stack-1x big-pref-icons">smoking_rooms</i>
        @endif

      </a>
    @endif
    </div>

    <div class="pref-container">
      @if($uPreference->preference->category == 'Pets')
      <a href="#" class="img-icon fa-stack">
        @if($uPreference->preference->action == 'No') <!-- No -->
          <i class="fa fa-paw fa-stack-1x big-pref-icons"></i>
          <i class="material-icons fa-stack-2x big-pref-icons-no">not_interested</i>
        @else <!-- Yes -->
          <i class="fa fa-paw fa-stack-1x big-pref-icons"></i>
        @endif

      </a>
      @endif
    </div>

    <div class="pref-container">
      @if($uPreference->preference->category == 'Chattiness')
      <a href="#" class="img-icon fa-stack">
        @if($uPreference->preference->action == 'No') <!-- No -->
          <i class="fa fa-comments fa-stack-1x big-pref-icons"></i>
          <i class="material-icons fa-stack-2x big-pref-icons-no">not_interested</i>
        @else <!-- Yes -->
          <i class="fa fa-comments fa-stack-1x big-pref-icons"></i>
        @endif
      </a>
      @endif
    </div>

    <div class="pref-container">
      @if($uPreference->preference->category == 'Music')
      <a href="#" class="img-icon fa-stack">
        @if($uPreference->preference->action == 'No') <!-- No -->
          <i class="fa fa-music fa-stack-1x big-pref-icons"></i>
          <i class="material-icons fa-stack-2x big-pref-icons-no">not_interested</i>
        @else <!-- Yes -->
          <i class="fa fa-music fa-stack-1x big-pref-icons"></i>
        @endif

      </a>
      @endif
    </div>
  </div>
  @endforeach
</div>
  