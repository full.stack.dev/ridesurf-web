<div class="bordered-box-2">
@if($userRide != null)
	<h3 class="border-bottom-gray"> Upcoming Rides </h3>
	@foreach($userRide as $join)
	<?php $date = $join->ride->dateFrom ." ".$join->ride->startHour;?>
		@if($join->status == 'Approved' && $join->booker_payment_paid == 'Yes' && 
		$date >= $now)
				
			<div class="row">
				<div class="col-md-8">
					<div class="profile-pic-sm">
						@if(count($join->ride->user->profileImages) > 0)
							<img src="{{ $join->ride->user->profileImages[0]->path }}" alt="" class="img-circle">
						@elseif($join->ride->user->avatar_original != null)
							<img src="{{ $join->ride->user->avatar_original }}" alt="" class="img-circle">
						@endif
					</div>
					<h3> Driver: {{ $join->ride->user->first_name }} </h3>
					<h4> <strong> From: </strong> {{ $join->ride->location_from }}</h4>
					<h4> <strong> To: </strong> {{ $join->ride->location_to }}</h4>
					<h4> <strong> Date: </strong> {{ date('m/d/Y', strtotime($join->ride->dateFrom)) }} </h4>
					<h4><strong>Time:</strong> {{ date("h:i a", strtotime($join->ride->startHour)) }}</h4>
				</div>
				<div class="col-md-4" id="join-buttons">
					<a class="btn btn-primary" href="{{ route('rides.show', $join->ride->id) }}">View Ride</a>
					<a class="btn btn-success" href="{{ route('messages.contact', $join->ride->id) }}">Contact Driver</a>
					<a class="btn btn-danger" href="{{ route('dashboarduser.cancelUserRide', 
					$join->ride->id) }}">Cancel Ride</a>
				</div>
			</div>
		
		@endif
	@endforeach

	@else
		
	<h3 class="border-bottom-gray">Upcoming Rides</h3>
	<div class="widget text-center">
		<p>You have no upcoming rides.</p>
	</div>
		
	@endif
</div>		

@if($userRide != null)
<div class="bordered-box-2">
	<h3 class="border-bottom-gray"> Pending Payment </h3>
	@foreach($userRide as $join)
		@if($join->status == 'Approved' && $join->booker_payment_paid == 'No' 
		&& $join->ride->dateFrom >= \Carbon\Carbon::today()->format('Y-m-d') )
			
			<div class="row">
				<div class="col-md-8">

					<div class="profile-pic-sm">
						@if(count($join->ride->user->profileImages) > 0)
							<img src="{{ $join->ride->user->profileImages[0]->path }}" alt="" class="img-circle">
						@elseif($join->ride->user->avatar_original != null)
							<img src="{{ $join->ride->user->avatar_original }}" alt="" class="img-circle">
						@endif
					</div>

					<p>{{ $join->ride->user->first_name }} </p>
					<p>{{ $join->ride->location_from }} to {{ $join->ride->location_to }}</p>
					<p> <strong> on </strong> {{ date('m/d/Y', strtotime($join->ride->dateFrom)) }} at {{date("h:i a", strtotime($join->ride->startHour)) }}.</p>
					
				</div>
				<div class="col-md-4" id="join-buttons">
					<a class="btn btn-primary" href="{{ route('rides.payment', $join->ride->id) }}">Pay Ride</a>
					<a class="btn btn-success" href="{{ route('messages.contact', $join->ride->id) }}">Contact Driver</a>
					{!! Form::open(['method' => 'DELETE','route' => ['rides.destroyUserRide', $join->ride->id],'style'=>'display:inline']) !!}
				            {!! Form::submit('Cancel Ride', ['class' => 'btn btn-danger']) !!}
				        {!! Form::close() !!}
				</div>
			</div>
		@endif
	@endforeach
</div>
@else
<div class="bordered-box-2">
	<h3 class="border-bottom-gray">Pending Payment</h3>
	<div class="widget text-center">
		<p>You have no rides pending payment.</p>
	</div>
</div>

@endif

@if($userRide != null)
<div class="bordered-box-2">
	<h3 class="border-bottom-gray">Pending Approval</h3>
	@foreach($userRide as $join)
		@if($join->status == 'Request' && $join->booker_payment_paid == 'No' 
		&& $join->ride->dateFrom >= \Carbon\Carbon::today()->format('Y-m-d'))

		
				<div class="row">
					<div class="col-md-8">
						<div class="profile-pic-sm">
						@if(count($join->ride->user->profileImages) > 0)
							<img src="{{ $join->ride->user->profileImages[0]->path }}" alt="" class="img-circle">
						@elseif($join->ride->user->avatar_original != null)
							<img src="{{ $join->ride->user->avatar_original }}" alt="" class="img-circle">
						@endif
					</div>
						<h3> Driver: {{ $join->ride->user->first_name }} </h3>
						<h4> <strong> From </strong> : {{ $join->ride->location_from }}</h4>
						<h4> <strong> To </strong> : {{ $join->ride->location_to }}</h4>
						<h4> <strong> Date </strong> : {{ date('m/d/Y', strtotime($join->ride->dateFrom)) }} </h4>
						<h4><strong>Time:</strong> {{date("h:i a", strtotime($join->ride->startHour)) }}</h4>
					</div>
					<div class="col-md-4" id="join-buttons">
						<a class="btn btn-primary" href="{{ route('rides.show', $join->ride->id) }}">View Ride</a>
						<a class="btn btn-success" href="{{ route('messages.contact', $join->ride->id) }}">Contact Driver</a>
						{!! Form::open(['method' => 'DELETE','route' => ['rides.destroyUserRide', $join->ride->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Cancel Ride', ['class' => 'btn btn-danger']) !!}
					        {!! Form::close() !!}
					</div>
				</div>	
		@endif <!-- End of Type Ride ( Request, Paid, or Not Paid) -->		
	@endforeach
</div>
			
@else
<div class="bordered-box-2">
	<h3 class="border-bottom-gray">Pending Approval</h3>
	<div class="widget text-center">
		<p>You have no pending rides.</p>
	</div>
</div>

@endif 

<div class="bordered-box-2">
	<h3 class="border-bottom-gray">Past Rides</h3>
	@if($userRide != null)
		@foreach($userRide as $join)
		    <?php $date = $join->ride->dateFrom ." ".$join->ride->startHour;?>
			@if( $date <= $now && $join->status == 'Approved' && $join->transaction_id != null)
				<div class="widget" style="height: auto">
					<div class="row">
					<div class="col-md-2">
						<div class="profile-pic-sm">
							@if(count($join->ride->user->profileImages) > 0)
								<img src="{{ $join->ride->user->profileImages[0]->path }}" alt="" class="img-circle">
							@elseif($join->ride->user->avatar_original != null)
								<img src="{{ $join->ride->user->avatar_original }}" alt="" class="img-circle">
							@endif
						</div>
					</div>
					<div class="col-md-6">
						<h4> <strong>Driver:</strong> {{ $join->ride->user->first_name }} </h4>
						<p>{{ $join->ride->location_from }} &#8594; {{ $join->ride->location_to }}</p>
						<p><strong>Date:</strong> {{ date('m/d/Y', strtotime($join->ride->dateFrom)) }} </p>
						<p><strong>Time:</strong> {{date("h:i a", strtotime($join->ride->startHour))}}</p>
					</div>
					<div class="col-md-3" id="join-buttons">
						
					
						@if($join->ride->rated == null )
						<a class="btn btn-primary" href="{{ route('reviews.rate',[$join->ride->user->id,$join->ride->id]) }}">Review Ride</a>
						@else
							<p>Thanks for reviewing</p>
						@endif
						@if(count($join->reported) == 0)
							<a class="btn btn-danger" href="{{ route('dashboarduser.reportRide',[$join->ride->id]) }}"><i class="fa fa-flag"></i> Report Problem</a>
						@else
							@if($join->reported->passenger_id != $user->id)
								<a class="btn btn-danger" href="{{ route('dashboarduser.reportRide',[$join->ride->id]) }}">
								<i class="fa fa-flag"></i> Report Problem</a>
							@endif
						@endif
					
					</div>
					</div>
				</div>
				
			@endif
		@endforeach

		@else
			<div class="widget text-center">
				<p>You have no past rides.</p>
			</div>
		@endif
</div>
