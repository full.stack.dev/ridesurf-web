@if ($message = Session::get('success'))
		<div class="alert alert-success">
		  <p>{{ $message }}</p>
		</div>
	@endif

<div class="col-md-12 no-mobile">
    <h2><i class="fa fa-tachometer"></i> Dashboard</h2>
</div>    

<div class="col-md-12 no-mobile">
	<ul class="secondary-nav">
  	<li id="dashboarduser"><a href="{{ route('dashboarduser.index') }}">Overview</a></li>
  	<li id="profile"><a href="{{ route('dashboarduser.profile') }}">Profile</a></li>
  	<li id="message"><a href="{{ route('dashboarduser.message') }}">Messages ({{$messageCount}})</a></li>
  	<li id="ridesCurrent"><a href="{{ route('dashboarduser.ridesCurrent') }}">Offered Rides</a></li>
  	<li id="booked"><a href="{{ route('dashboarduser.booked') }}">Booked Rides</a></li>
  	<li id="reviews"><a href="{{ route('dashboarduser.reviews') }}">Reviews</a></li>
  	<li id="payments"><a href="{{ route('dashboarduser.payments') }}">Payments</a></li>
	</ul>
</div>

<div class="row mobile dash-info-mobile">
  <div class="col-xs-2 dash-item">
    <a href="{{ route('dashboarduser.index') }}">
    @if(!empty($user->getRoleNames()))
          @foreach($user->getRoleNames() as $v)
              <p><i class="fa fa-tachometer"></i></p><span class="dash-box-text" >Overview</span>
          @endforeach
    @endif
    
    </a>
  </div>
  <div class="col-xs-2 dash-item">
    <a href="{{ route('dashboarduser.profile') }}">
    @if(!empty($user->getRoleNames()))
          @foreach($user->getRoleNames() as $v)
              <p><i class="fa fa-user"></i></p>
          @endforeach
    @endif
    <span class="dash-box-text" >Profile</span>
    </a>
  </div>
  <div class="col-xs-2 dash-item">
    <a href="{{ route('dashboarduser.booked') }}">
      <p class="dashmobile"><i class="fa fa-car"></i></p>
      <span class="step-text dash-box-text">Booked</span>
    </a>
  </div>
  <div class="col-xs-2 dash-item">
    <a href="{{ route('dashboarduser.ridesCurrent') }}">
      <p class="dashmobile"><i class="fa fa-road"></i></p>
      <span class="step-text dash-box-text">Offered</span>
    </a>
  </div>
  <div class="col-xs-2 dash-item">
    <a href="{{ route('dashboarduser.message') }}">
      <p class="dashmobile"><i class="fa fa-envelope-o"></i></p>
      <span class="step-text dash-box-text">Messages</span>
    </a>
  </div>
  <div class="col-xs-2 dash-item">
    <a href="{{ route('dashboarduser.payments') }}">
      <p><span class="glyphicon glyphicon-usd"></span></p>
      <span class="step-text dash-box-text">Payment</span>
    </a>
  </div>
</div>

<script type="text/javascript">
	var path = window.location.pathname;
	var page = path.split("/").pop();

	if(page != 'ridePast'){
		document.getElementById(page).classList.add('current');
	}
</script>

