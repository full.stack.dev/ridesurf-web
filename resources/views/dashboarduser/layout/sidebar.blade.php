<div class="col-md-4 ">
<div style="padding: 0;">

  <div class="profile-detail-container bordered-container">
    <span class="title">Verified Info</span>
    <ul class="verified-info">
      @if($user->verified == 0)
        <li class="uncheck-list">Email not verified</li>
        <a href="{{route('dashboarduser.sendverification')}}" class="btn btn-success">
          Send Verification
        </a>
      @else
        <li class="check-list">Email Address</li>
      @endif
      @if( count($user->profileImages) == 0 && $user->avatar == null)
        <li class="uncheck-list">No Profile Image</strong></li>
      @else
        <li class="check-list">Profile Image</li>
      @endif
      @if($user->phone_number == null)
        <li class="uncheck-list">No phone number</strong></li>
      @else
        <li class="check-list">Phone Number</li>
      @endif
      @if($user->address == null)
        <li class="uncheck-list">Address not provided</li>
      @else
        <li class="check-list">Address Provided</li>
      @endif
    </ul>
  </div>


  <div class="profile-detail-container bordered-container">

    <span class="title">Preferences</span>

    @include('dashboarduser.layout.preferences')
    <!-- This button is here so the layout preferences can be use in other places -->
    
    <p align="center"><a href="{{ route('dashboarduser.editPreferences') }}" 
      class="btn btn-info">Edit Preferences</a></p>
  </div>

  <div class="profile-detail-container bordered-container">

      <span class="title">Member Since</span>
      <span class="activity">{{ date('m-d-Y', strtotime($user->created_at))  }}</span>

  </div>
  <div class="profile-detail-container bordered-container" id="settings-sidebar">

      <span class="title">Settings</span>

      <div>

        <div style="display: flex; justify-content: center;">
            <a class="btn btn-info" href="{{ route('dashboarduser.updatepassword') }}">
              <i class="fa fa-lock right-margin text-small2"></i> Update Password
            </a>
        </div>
        
        @if(count($user->driver) == 0)
          <div class="wrapper" style="margin-top: 15px; display: flex; justify-content: center;">
               <a href="{{ route('dashboarduser.becomeDriver') }}" class="btn btn-info">
                  <i class="fa fa-car right-margin text-small2"></i> Become A Driver</a>
          </div>
        @else
          <div class="wrapper" style="margin-top: 15px; display: flex; justify-content: center;">
            <a href="{{ route('dashboarduser.cardetails') }}" class="btn btn-info">
            <i class="fa fa-car right-margin text-small2"></i> Car Details</a>
          </div>
        @endif
        <div class="wrapper"  style="margin-top: 15px; display: flex; justify-content: center;">
          <a href="{{ route('ride_request.index') }}" class="btn btn-info">
                <i class="fa fa-bell right-margin text-small2"></i> Ride Notifications
          </a>
        </div>
        <div class="deleteButton" style="margin-top: 25px; display: flex; justify-content: center;">
          {!! Form::model($user, ['method' => 'DELETE', 'id' => 'deleteUser', 'files' => 'true' ,
          'route' => ['dashboarduser.deleteuser', $user->id] ] ) !!}
              {{ csrf_field() }}
              <button class="btn btn-danger-linkstyle"
              onclick="return confirm('Are you sure you want to delete your account?')">
                  <i class="fa fa-trash right-margin text-small2"></i> Delete Account
              </button>
          {{ Form::close() }}
        </div>

      </div>
  </div>

</div>
</div>