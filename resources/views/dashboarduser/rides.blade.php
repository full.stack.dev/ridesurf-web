@extends('layouts.app')
@section('content')

<script>
    //VueJS conflicts with dropdown
    jQuery('.dropdown-toggle').dropdown();
</script>
@if ($message = Session::get('success'))
	<div class="alert alert-success">
	  <p>{{ $message }}</p>
	</div>
@endif

<div class="container">
<div class="container-fluid offer-ride bottom-margin">

	@include('dashboarduser.layout.menu')

	
    <div id="app">
      <div>
          <router-multi-view
            class="wrapper"
            name="fade"
          />
      </div>
    </div>
</div>
</div>
<script src="{{asset('js/app.js')}}"></script>

@endsection