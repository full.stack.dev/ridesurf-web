@extends('layouts.app')
@section('content')
@if ($message = Session::get('success'))
	<div class="alert alert-success">
	  <p>{{ $message }}</p>
	</div>
@endif

<div class="container">
<div class="container-fluid offer-ride">
	@include('dashboarduser.layout.menu' )
	<!-- SideBar-->
    <div class="no-mobile">
	   @include('dashboarduser.layout.sidebar', [$user])
    </div>

	<div class="col-md-7 col-xs-12">
    	<div class="col-md-12 col-xs-12">
            <h3> We understand things happen!</h3>
    		<p> Cancel Ride to {{ $ride->location_to }}</p>

    		@if($control == true)
    			<p> Canceling Rides 24 hours before the ride will consist in a 50% penaltiy plus service fees. We’ll also compensate the driver 50% because the passenger cancelled shortly before the ride.</p>
    			<p> Total return is <strong> ${{ money_format('%.2n', $return) }}</strong>
    		@else
    			<p> We will return full amount expect services fees. </p>
    			<p> Total Refound <strong> ${{ money_format('%.2n', $return) }}</strong>
    		@endif

    		{!! Form::open(['method' => 'DELETE','route' => ['rides.destroyUserRide', $ride->id],
    		'style'=>'display:inline']) !!}
                <input type="hidden" value="{{$return}}" name="refound">
    			<input value="{{ $control }}" name="control" style="display: none;">
    			<p> Please state the reason why you are canceling</p>

    			<div class="form-group">
    				<select name="cancel_reason" class="form-control">
    					<option value="I no longer need to go to this place"> I no longer need to go to this place </option>
    					<option value="Found a better Option"> Found a better Option</option>
    					<option value="Rider changed Route "> Rider changed Route </option>
    					<option value="Other">Other</option>
    				</select>
    			</div>
    		
	            {!! Form::submit('Cancel Ride', ['class' => 'btn btn-danger']) !!}
	        {!! Form::close() !!}
    	</div>
	</div>
</div>
</div>