@extends('layouts.app')
@section('content')
	
	@if ($message = Session::get('success'))
		<div class="alert alert-success">
		  <p>{{ $message }}</p>
		</div>
	@endif

<div class="container">
	<div class="container-fluid offer-ride">

		@include('dashboarduser.layout.menu')
		@include('dashboarduser.layout.dashboard', [$user, $booked, $offer])
		<div class="no-mobile">
	  		@include('dashboarduser.layout.sidebar', [$user,$preferences, $settings])
	  	</div>

	  	<div class="col-md-8">
	  		<h3> Update Password </h3>

	  		{!! Form::open(['method' => 'POST','route' => ['dashboarduser.patchpassword'] , 'class' => 'form-group passwordstyle', 'id'=>'submitForm']) !!}
	  			{{ Form::password('password',['class'=> 'form-control','placeholder'=>'Enter your Password', 'id'=> 'pass']) }}
				{{ Form::password('password_confirmation', 
				['class' => 'form-control','placeholder'=>'Re-enter your Password', 'id'=>'passConfirm']) }}

				<button class="btn btn-success">Change Password</button>
	  		{!! Form::close() !!}
	  	</div>
	</div>
</div>

<script type="text/javascript">
	$( document ).ready(function() {
    	$("#submitForm").on("submit", function(){
		 	if($('#pass').attr('value') != $('#passConfirm').attr('value')){
		 		alert('Password do not match');
		 		return false;
		 	}
		})
	});
</script>
@endsection