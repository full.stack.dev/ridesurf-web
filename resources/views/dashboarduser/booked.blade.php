@extends('layouts.app')
@section('content')

<div class="container">
  <div class="container-fluid offer-ride">

  	@include('dashboarduser.layout.menu')
    
    <div class="col-md-3">
    	<div class="tab">
  		  <button class="tablinks active" onclick="openCity(event, 'upcoming')">
  		  Upcoming Rides</button>
  		  <button class="tablinks" onclick="openCity(event, 'pending')">Pending Rides</button>
  		  <button class="tablinks" onclick="openCity(event, 'payment')">Payment Rides</button>
  		  <button class="tablinks" onclick="openCity(event, 'past')">Past Rides</button>
  		</div>

    </div>

    <div class="col-md-9">

    	 <div id="upcoming" class="tabcontent">
  	  @include('dashboarduser.booked-rides.upcoming', [$userRide, $user, $now])
  	</div>

  	<div id="pending" class="tabcontent inactive">
  	  @include('dashboarduser.booked-rides.pending', [$userRide, $user, $now])
  	</div>

  	<div id="payment" class="tabcontent inactive">
  	  @include('dashboarduser.booked-rides.payment', [$userRide, $user, $now])
  	</div>

  	<div id="past" class="tabcontent inactive">
  	  @include('dashboarduser.booked-rides.past', [$userRide, $user, $now])
  	</div>
      
    </div>
  </div>
</div>
<script type="text/javascript">
function openCity(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

@endsection