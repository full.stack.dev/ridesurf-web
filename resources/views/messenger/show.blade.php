@extends('layouts.app')
@section('content')

<!-- Messages between you and 1 other user -->

<div class="container">
	<div class="container-fluid offer-ride">
		<div class="row">

			<div class="col-md-4" style="border: 1px solid #ccc; text-align: center; padding-top: 30px;">
				<div class="widget">
					<h4>Conversation with</h4>
					<div class="user-dashboard-profile">
						<div class="profile-thumb">

								<img src="{{ $otherUser->avatar }}" alt="" class="img-circle" width="150px" height="150px">

						</div>
						<!-- User Name -->
						
						<h5 >{{ $otherUser->first_name }}</h5>
						<div class="stars">
						    <span data-rating="1" class="fa fa-star checked"></span>
						    <span data-rating="2" class="fa fa-star "></span>
						    <span data-rating="3" class="fa fa-star "></span>
						    <span data-rating="4" class="fa fa-star "></span>
						    <span data-rating="5" class="fa fa-star"></span>
						</div>
					</div>

					<div class="ride text-center top-margin-15 bottom-pad-10">
						
						@if($ride != null)
							<p><strong> From:</strong> {{ $ride->location_from }} </p>
							<p><strong> To:</strong> {{ $ride->location_to }} </p>
							<p><strong> Date:</strong> {{ date('m-d-Y', strtotime($ride->dateFrom)) }} </p>
						@endif
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div class="row widget">
					<div class="col-md-offset-1 col-md-11">

				    	@include('messenger.partials.form-message')

				    	<h6> Message History </h6>
				        @each('messenger.partials.messages', $thread->messages, 'message', $otherUser, 'otherUser')
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

			var stars = $('.stars');
	    	var star  = stars.find('.fa-star');
	    	var value = {{ $otherUser->rating}}

			// Remove class for selected stars
			  stars.find('.checked').removeClass(' checked');

			  // Add class to the selected star and all before
			  for (i = 1; i <= value; i++) {
			    stars.find('[data-rating="' + i + '"]').addClass('checked');
			  }
		});
</script>
@stop
