@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 150px;">
<div class="row">
<div class="col-md-9 col-md-offset-1">
<div class="panel panel-default" style="padding: 30px">
    <h2>Send Message</h2>
    <div class="panel-body" style="margin-top: 20px">

        <div class="col-md-4">
            <div>
                <div class="row">
                <div class="col-md-10">
                    <p> <strong> To: </strong></p>
                    <div class="wrappper">
                        <img src="{{ $ride->user->avatar }}" alt="" width="80" class="img-circle">

                        <h3>{{ $ride->user->first_name }} </h3>
                        <div class="stars">
                            <span data-rating="1" class="fa fa-star"></span>
                            <span data-rating="2" class="fa fa-star"></span>
                            <span data-rating="3" class="fa fa-star"></span>
                            <span data-rating="4" class="fa fa-star "></span>
                            <span data-rating="5" class="fa fa-star"></span>
                        </div>
                    </div>
                    <p> <strong> Trip: </strong>  {{ $ride->location_from }} &#8594; {{ $ride->location_to }} on  {{ $ride->dateFrom }}</p>
                </div>
                
                </div>
            </div>
        </div>

<div class="col-md-8">
    <form action="{{ route('messages.store') }}" method="post">
        {{ csrf_field() }}
        

            <!-- Message Form Input -->
            <div class="form-group">
                <label class="control-label">Message</label>
                <textarea name="message" class="form-control">{{ old('message') }}</textarea>
            </div>

            @if($users->count() > 0)
                <div class="form-group">
                    
                    <select name="recipients" class="form-control">
                        <option value="{{ $users->id }}">{{ $users->first_name }}'s trip from {{ $ride->location_from }}
                            to {{ $ride->location_to }}</option>
                    </select>
                    
                </div>
            @endif

            <input value="{{ $ride->id }}" hidden name="ride_id">
    
            <!-- Submit Form Input -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Submit</button>
            </div>
    </form>
    </div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

            var stars = $('.stars');
            var star  = stars.find('.fa-star');
            var value = {{ $ride->user->rating }}

            // Remove class for selected stars
              stars.find('.checked').removeClass(' checked');

              // Add class to the selected star and all before
              for (i = 1; i <= value; i++) {
                stars.find('[data-rating="' + i + '"]').addClass('checked');
              }
        });
</script>
@stop
