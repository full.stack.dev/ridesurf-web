@extends('layouts.app')

@section('content')

<div class="container">
    <div class="container-fluid offer-ride">
        <h3>Create a new message</h3>
        <form action="{{ route('messages.store') }}" method="post">
            {{ csrf_field() }}
            <div class="col-md-6">
                @if($users->count() > 0)
                <!-- Message Form Input -->
                <div class="form-group">
                    <label class="control-label">Messages</label>
                    <textarea name="message" class="form-control">{{ old('message') }}</textarea>
                </div>

                
                    <div class="form-group">
                        <select name="recipients" class="form-control">
                            @foreach($users as $user)
                            <option value="{{$user->rides_id}},{{ $user->contact_user }}">{{ $user->user->first_name }}'s trip <strong> from </strong> 
                                {{ $user->ride->location_from }}
                                <strong> to </strong> {{ $user->ride->location_to }}</option>
                            @endforeach
                        </select>
                        
                    </div>
                    <!-- Submit Form Input -->
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary form-control">Submit</button>
                    </div>
                @else
                    <h3>You currently have no users to message.</h3>
                    <p> If you wish to contact us, you can do it by 
                        <a href="{{ route('company.contact') }}" class="btn btn-success">here</a>
                    </p>
                @endif
        

            </div>
        </form>
    </div>
</div>
@stop
