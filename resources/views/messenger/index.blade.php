@extends('layouts.app')
@section('content')

<div class="container">
    <div class="col-md-12 pages-bottom-pad">
        <div class="container pages-nav-jump" style="margin-bottom: 20px;">
            <h2>Message Inbox</h2>
        </div>


        <div class="filter">
            <div class="dropdown">
              <button class="btn btn-icon dropdown-toggle" type="button" id="dropdownMenuButtonMessage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-inbox"></i></button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" id="control-1" href="{{ route('messages.filter', '1' ) }}">All Messages</a>
                <a class="dropdown-item" id="control-2" href="{{ route('messages.filter', '2' ) }}">Admin Messages</a>
                <a class="dropdown-item" id="control-3" href="{{ route('messages.filter', '3' ) }}">Unread Messages</a>
              </div>
            </div>
        </div>
        <div class="container-fluid bordered-container pages-bottom-pad">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                  <p>{{ $message }}</p>
                </div>
            @endif


            @include('messenger.partials.flash')

        	<div>
                
        		@each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
        	</div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <a href="{{ route('messages.create') }}"><button type="submit" class="btn btn-primary">New Message</button></a>
        </div>
    </div>
</div>

<script type="text/javascript">
    var control = {{ $control }};
    if(control == 1 || control == 0){
        jQuery('#control-1').append('&#x2714;');
        jQuery('#dropdownMenuButtonMessage').hmtl('All Messages');
    }else if(control == 2){
        jQuery('#control-2').append('&#x2714;');
        jQuery('#dropdownMenuButtonMessage').append('Admin Messages');
    }else{
        jQuery('#control-3').append('&#x2714;');
        jQuery('#dropdownMenuButtonMessage').append('Unread');
    }
</script>

@stop
