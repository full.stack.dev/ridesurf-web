@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Create a new message</h1>
    <form action="{{ route('messages.store') }}" method="post">
        {{ csrf_field() }}
        <div class="col-md-8">

            <!-- Message Form Input -->
            <div class="form-group">
                <label class="control-label">Messages</label>
                <textarea name="message" class="form-control">{{ old('message') }}</textarea>
            </div>

            @if($users->count() > 0)
                <div class="form-group">
                    
                    <select name="recipients" class="form-control">
                        <option value="{{ $users->id }}">{{ $users->first_name }}'s trip from {{ $ride->location_from }}
                            to {{ $ride->location_to }}</option>
                    </select>
                    
                </div>
            @endif

            <input value="{{ $ride->id }}" hidden name="ride_id">
    
            <!-- Submit Form Input -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Submit</button>
            </div>
        </div>

        <div class="col-md-4">
            <div class="widget" style="height: auto">
                <div class="row">
                <div class="col-md-10">
                    <div class="wrappper">
                        <img src="{{ $users->profileImages[0]->path }}" alt="" width="80" class="img-circle">
                        <h3> Driver: {{ $users->first_name }} </h3>
                        <div class="stars">
                            <span data-rating="1" class="fa fa-star"></span>
                            <span data-rating="2" class="fa fa-star"></span>
                            <span data-rating="3" class="fa fa-star"></span>
                            <span data-rating="4" class="fa fa-star "></span>
                            <span data-rating="5" class="fa fa-star"></span>
                            <p> {{ $users->rating }} / 5 </p>
                        </div>
                    </div>
                    <h4> <strong> From </strong> : {{ $ride->location_from }}</h4>
                    <h4> <strong> To </strong> : {{ $ride->location_to }}</h4>
                    <h4> <strong> Date </strong> : {{ $ride->dateFrom }} </h4>
                </div>
                
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){

            var stars = $('.stars');
            var star  = stars.find('.fa-star');
            var value = {{ $ride->user->rating }}

            // Remove class for selected stars
              stars.find('.checked').removeClass(' checked');

              // Add class to the selected star and all before
              for (i = 1; i <= value; i++) {
                stars.find('[data-rating="' + i + '"]').addClass('checked');
              }
        });
</script>
@stop
