@if($thread->creator()->id != 8)
<h4>New message:</h4>
<form action="{{ route('messages.update', $thread->id) }}" method="post">
    {{ method_field('put') }}
    {{ csrf_field() }}
        
    <!-- Message Form Input -->
    <div class="form-group">
        <textarea name="message" class="form-control">{{ old('message') }}</textarea>
    </div>

    @if($otherUser->count() > 0)
        <div class="checkbox">
                <input type="checkbox" name="recipients" value="{{ $otherUser->id }}" hidden>
        </div>
    @endif

    <!-- Submit Form Input -->
    <div class="form-group">
        <button type="submit" class="btn btn-primary form-control" style="padding-bottom:32px !important;">Send Message</button>
    </div>
</form>
@endif