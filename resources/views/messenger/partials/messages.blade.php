<div class="user" style="margin-bottom: 10px;">    
    <div class="row">
        @if($message->user_id == Auth::id())
        <div class="col-md-9">
            <div class="media-body form-group">
                
                @if($message->user_id == 8)
                    <?php echo $message->body ?>
                @else
                    <div class="chat-message-bubble">{{ $message->body }}</div>
                @endif
                <div class="text-muted">
                    <p class="small-light">Posted {{ $message->created_at->diffForHumans() }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div style="text-align: center;">
                
                <img src="{{ $message->user->avatar}}"
                     alt="{{ $message->user->first_name }}" width="50px" class="img-circle">
                
                <p style="text-align: center;">{{ $message->user->first_name }}</p>
            </div>
        </div>
        @else
            <div class="col-md-3">
                <div style="text-align: left;">

                    <img src="{{ $message->user->avatar}}"
                         alt="{{ $message->user->first_name }}" width="50px" class="img-circle">
                    
                    <p style="text-align: left;">{{ $message->user->first_name }}</p>
                </div>
            </div>

            <div class="col-md-7">
                <div class="media-body form-group" style="text-align: left;">
                    
                    @if($message->user_id == 8)
                        <?php echo $message->body ?>
                    @else
                        <div class="chat-message-bubble-blue">{{ $message->body }}</div>
                    @endif
                    <div class="text-muted">
                        <p class="small-light" style="margin-left: 20px;">Posted {{ $message->created_at->diffForHumans() }}</p>
                    </div>
                </div>
            </div>
        @endif
    </div>

</div>
