<?php $class = $thread->isUnread(Auth::id()) ? 'alert-info' : ''; 
      $user = Auth::id();
?>

@if($thread != null)

    <div class="row notifications">
   
        <!-- Non-Mobile Version -->
        <div class="no-mobile">
            <div class="col-md-2">
                <div>
                    @if($thread )
                        <img src="{{ $thread->otherUser->avatar }}" alt="ridesurf profile image" class="img-circle image-width-60">
                    @endif
                </div>
            </div>

            <div class="col-md-3">
                <div class="message-mobile-2" style="text-align:left;padding:0;margin:0;">
                    <!-- If unread, BOLD -->
                    @if($thread->isUnread(Auth::id()) == true)
                        <p class="message-sender-unread"><strong>
                    @else
                        <p class="message-sender">
                    @endif
                    {{ $thread->otherUser->first_name }}
                    @if($thread->isUnread(Auth::id()) == true)
                        </strong></p>
                    @else
                        </p>
                    @endif
                    <p class="message-date">{{ $thread->latestMessage->created_at->diffForHumans(null, true).' ago' }}</p>
                    <!-- Another Format Here : -->
                    <!--  date_format( $thread->latestMessage->created_at, "m/d/Y H:i:s") -->
                </div>
            </div>

            <div class="col-md-5">
                <!-- If unread, BOLD -->
                    @if($thread->isUnread(Auth::id()) == true)
                        <p class="message-preview"><strong>
                    @else
                        <p class="message-preview">
                    @endif
                    {{ $thread->latestMessage->body }}
                    @if($thread->isUnread(Auth::id()) == true)
                        </p> </strong></p>
                    @else
                        </p>
                    @endif
            </div>

            <div class="col-md-2">
                <div style="text-align: right; padding:0; margin:0;">
                    <a href="{{ route('messages.show', $thread->id) }}" class="btn btn-grey" 
                        style="width:40px; margin-bottom: 5px;"><i class="fa fa-reply"></i></a>

                    {!! Form::open(['method' => 'DELETE','route' => ['messages.delete', $thread->id],'style'=>'display:inline']) !!}
                        <button class="btn btn-danger btn-square" style="width:40px;"><i class="fa fa-trash-o"></i></button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        
     <!-- Mobile Version -->
        <div class="mobile">
            <div class="col-md-4">
                <div class="profile-thumb message-mobile text-center">
                    <img src="{{ $thread->creator()->avatar }}" alt="ridesurf profile image" class="img-circle">
                </div>

                <div class="message-mobile-2">
                    <!-- If unread, BOLD -->
                    @if($thread->isUnread(Auth::id()) == true)
                        <p class="message-sender-unread text-center"><strong>
                    @else
                        <p class="message-sender text-center">
                            @endif
                            {{ $thread->creator()->first_name }}
                            @if($thread->isUnread(Auth::id()) == true)
                        </p></strong></p>
                        @else
                        </p>
                    @endif
                    <p class="message-date text-center">{{ $thread->latestMessage->created_at->diffForHumans(null, true).' ago' }}</p>
                    <!-- Another Format Here : -->
                    <!--  date_format( $thread->latestMessage->created_at, "m/d/Y H:i:s") -->
                </div>
            </div>
            <div class="col-md-8">

                <!-- If unread, BOLD -->
                @if($thread->isUnread(Auth::id()) == true)
                    <p class="message-preview text-center"><strong>
                @else
                    <p class="message-preview text-center">
                        @endif
                        {{ $thread->latestMessage->body }}
                        @if($thread->isUnread(Auth::id()) == true)
                    </p> </strong></p>
                    @else
                    </p>
                @endif

                <div style="text-align: center; padding:0;margin:0;">
                    <a href="{{ route('messages.show', $thread->id) }}" class="btn btn-grey btn-square"><i class="fa fa-reply"></i></a>

                    {!! Form::open(['method' => 'DELETE','route' => ['messages.delete', $thread->id],'style'=>'display:inline']) !!}
                    <button class="btn btn-danger btn-square"><i class="fa fa-trash-o"></i></button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endif