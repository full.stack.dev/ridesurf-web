<!DOCTYPE html lang="{{ app()->getLocale() }}">
<html lang="en">
<head>

  <title>Ridesurf | Long Distance & Event Carpool App</title>
  @include('meta::manager', [
    'title'         => 'Ridesurf | Long Distance & Event Carpooling App',
    'description'   => 'Ridesurf is a long-distance & event carpooling app. It’s a safe and easy way for people who are going the same direction to share a ride with one another. Purchase or sell the extra seats in a car on long drives and lower your carbon footprint at the same time!',
    'image'         => 'https://ridesurf.io/images/ridesurf_social_sm.jpg',
  ])

  <!-- Open Graph -->
  <meta property="og:title" content="Ridesurf | Long Distance & Event Carpool App">
  <meta property="og:site_name" content="Ridesurf">
  <meta property="og:url" content="https://ridesurf.io">
  <meta property="og:description" content="Ridesurf is a long-distance & event carpooling app. It’s a safe and easy way for people who are going the same direction to share a ride with one another.">
  <meta property="og:image" content="https://ridesurf.io/images/ridesurf_social_sm.jpg">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  
  <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
  <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('css/style.css') }}?v=1.0">
  <link rel="stylesheet" href="{{ asset('css/form-styling.css') }}?v=1.0">
  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script type="text/javascript" src="{{ asset('js/starrr.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
  <script src="{{ asset('js/typed/typed.js') }}"></script>

  <!-- Emojis -->
  <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Palanquin+Dark" rel="stylesheet">

  <!-- Google Verification -->
  <meta name="google-site-verification" content="3EBBsV1grhgzoIhSmE2Q9k83Ti3CVpi0hc2aQCZ3ZAY" />
</head>
<body>

<!-- Nav Bar -->
<nav class="navbar navbar-default navbar-not-fixed nav-home">
    <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="{{ url('/') }}"><img src="images/ridesurf_white.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">

      <ul class="nav navbar-nav navbar-right home-nav">

      @hasrole('User')
        <li><a href="{{ route('dashboarduser.becomeDriver') }}">Become a Driver</a></li>
      @endhasrole
      @guest
        <li class="hov-line"><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
        <li><a class="nav-item outlinebutton home-signup-button" href="{{ route('register') }}">{{ __('Sign Up') }}</a></li>
      @else
      @hasrole('Admin')
        <li><a class="nav-item" href="{{ route('dashboardadmin.index') }}"> Admin </a>
      @endhasrole
        <li class="nav-item dropdown pull-right">
          <a class="nav-link dropdown-toggle avatar-cont" data-toggle="dropdown" href="#">
            <span>{{ Auth::user()->first_name }}</span>
            
            @include('layouts.userProfile')

            @if($notify_menu > 0)
              <div class="number-badge">{{ $notify_menu + $cMessages }}</div>
            @endif
            <span class="tog"><i class="fa fa-chevron-down"></i></span>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="{{ route('dashboarduser.index') }}">Dashboard 
              <span class="number-badge-lite">{{$notify_menu}}</span></a>
            <a class="dropdown-item" href="{{ route('dashboarduser.message') }}">Messages 
              <span class="number-badge-lite">{{$cMessages}}</span> </a>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();"> 
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> @csrf 
            </form>
          </div>
        </li>

        <!-- Mobile Menu, removes dropdown -->
        <li class="mobile-nav"><a href="{{ route('dashboarduser.index') }}">Dashboard ({{$notify_menu}})</a></li>
        <li class="mobile-nav"><a href="{{ route('messages') }}">Messages</a></li>
        <li class="mobile-nav"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> 
                {{ __('Log Out') }}</a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> 
                @csrf </form>
        </li>
        <!-- End Mobile Menu -->
       
       @endguest
    </ul>

    </div>
  </div>
</nav>  

<!-- Banner & Search --> 
<div class="container-fluid section-1">
  @if(session()->has('message'))
      <div class="alert alert-success" >
          {{ session()->get('message') }}
      </div>
  @endif
  @if(session()->has('error'))
      <div class="alert alert-danger" id="errorMessage">
          {{ session()->get('error') }}
      </div>
  @endif

  <div class="col-md-offset-1 col-md-10 col-xs-12">
      <h1>Carpool Your <span id="titleTyped"></span> </h1>

    <a class="nav-item outlinebutton2" href="{{ route('rides.search') }}">RIDE</a>
    <a class="nav-item outlinebutton2" href="{{ route('rides.create') }}">DRIVE</a>

  </div>
</div>

<!-- EDC
<div class="container-fluid section-2 text-center special-event">

  
    <h3 class="caps-bold white">Looking for Home Bass carpool info?</h3>

  <div class="top-margin-15">

      <a href="https://ridesurf.io/homebass" class="btn btn-info-white">Home Bass Carpool</a>

  </div>

</div>

</div> -->

<!-- Apps -->
<div class="container-fluid section-2" style="background-color: #f7f7f7;">
  <div class="col-md-offset-1 col-md-4 mobile-right-center top-margin-15">
    <h3 class="caps-bold theme-color">Download Ridesurf:</h3>
  </div>
  <div class="col-md-3 mobile-right-center">
    <div>
      <a href="https://apps.apple.com/us/app/ridesurf/id1473718241">
        <img src="images/apple.png">
      </a> 
    </div>
  </div>
  <div class="col-md-3 text-center">
    <div>
      <a href="https://play.google.com/store/apps/details?id=com.ridesurf.app&hl=en_US">
        <img src="images/google.png">
      </a>
    </div>
  </div>
</div>

<!-- Synapse 
<div>
  <div class="container-fluid section-2 text-center event-synapse-home">
    <h2 class="caps-bold white synapse-child">Looking for Synapse Summit carpool info?</h2>
    <p class="synapse-child"><a href="https://ridesurf.io/synapse" class="btn btn-info-white">Synapse Carpool</a></p>
  </div>
</div>
-->

<!-- Find a ride 
<div class="container-fluid section-ridesearch">
  <div class="col-md-offset-2 col-md-8 col-xs-12">
    <div class="text-center bottom-pad">
      <h2>Find A Ride</h2>
    </div>
  </div>
  <div class="col-md-offset-1 col-md-10 col-xs-12">
    <div class="home-info">


      <div class="offer-ride container">
        <div class="col-md-12">


          <div id="search"><div>

              <router-multi-view
                class="wrapper"
                {{--states="{{ $states }}"--}}
                name="fade"
              />
            </div>
          </div>
          <div id="app" style="display: none;"></div>
          {{--<script src="{{asset('js/app.js')}}"></script>--}}

        </div>
      </div>-->

{{-- OLD FORM

      {!! Form::open(array('route'=>'rides.search','method'=>'GET','class'=>'online-form')) !!}

             <div class="row homesearchbg">
                  <div class="col-md-3" id="starts">
                      {!! Form::text('start', null, array('placeholder' => 'From','class' => 'form-control', 'id' => 'from-fetch')) !!}

                  </div>
                  <div class="col-md-3" id="ends">
                      {!! Form::text('stop', null, array('placeholder' => 'To','class' => 'form-control', 'id' => 'to-fetch')) !!}
                  </div>

                  <div class="col-md-3">
                      <input type="date" name="date" onfocus="(this.type='date')" class="form-control" placeholder="Date" required="" id="dateFrom">
                  </div>

                  <div class="col-md-3">
                      <button type="submit" class="form-control ridesearch">Search</button>
                  </div>

             </div>

      {!! Form::close() !!} --}}
    </div>
  </div>
</div>


<!-- How to Ridesurf -->
<div class="container-fluid section-3">
  <div class="col-md-offset-2 col-md-8 col-xs-12">
        <div>
          <h2>How To Ridesurf</h2>
        </div>
  </div>
  <div class="col-md-offset-1 col-md-10 col-xs-12">
    <div class="col-md-4 col-xs-12">
        <div class="text-container">
        <img src="{{ asset('/images/suitcase.png')}}" width="145">
        <p class="text-heading"><strong>FIND A MATCH</strong></p>
        <p class="text-description">Find someone taking the same journey.</p>
     </div>
    </div>
    <div class="col-md-4 col-xs-12">
      <div class="text-container">
      <img src="{{ asset('/images/ride.png')}}" width="106">
      <p class="text-heading"><strong>BOOK & CONNECT</strong></p>
      <p class="text-description">The rider books the ride, the driver connects with the rider to confirm ride details.</p>
    </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <div class="text-container">
          <img src="{{ asset('/images/map.png')}}" width="108">
        <p class="text-heading"><strong>ARRIVAL</strong></p>
        <p class="text-description">End the ride by completing a rating a review.</p>
        </div>
    </div> 
  </div>
<div id="map"></div>
</div>

<!-- Going somewhere -->
<div class="container-fluid section-2" style="background-color: #d9ede3;">
  <div class="col-md-offset-1 col-md-5 col-xs-12" id="panelgreen">
   <img src="images/stock/roadtrip5.png" class="img-section-2" >
  </div>
  <div class="col-md-5 col-xs-12">
    <div style="padding-bottom: 15%;">
   <h2>Driving somewhere this winter?</h2>

   <p>Selling the extra seats in your car for long distance ride sharing makes your trip cheaper, more environmentally friendly, and more fun!</p>
  <a href="{{route('rides.create')}}" class="link-btn">Create a ride</a>
  </div>
  </div>
</div>

@include('layouts.footer')
<script> 

$("document").ready(function(){
  setTimeout(function() {
    $('#errorMessage').fadeOut('fast');
}, 5000); 
});
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: 'roadmap'
  });


  // Create the search box and link it to the UI element.
  var inputFrom = document.getElementById('from-fetch');

  var searchBox = new google.maps.places.Autocomplete(inputFrom);

  var inputTo= document.getElementById('to-fetch');
  var searchBoxTo = new google.maps.places.Autocomplete(inputTo);
}
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M&libraries=places&region=us&callback=initMap">
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132854726-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-132854726-1');

  var title = new Typed('#titleTyped', {
      strings: ['Adventure', 'Event', 'Concert'],
      typeSpeed: 35,
      backSpeed: 35,
      backDelay: 2000,
      cursorChar: '_',
      shuffle: false,
      smartBackspace: true,
      loop: true
  });
</script>

</body>
</html>