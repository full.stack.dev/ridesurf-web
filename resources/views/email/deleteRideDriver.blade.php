@include('email.layouts.header')
<h2> Hello {{ $name }},</h2>

<p> Unfortunately your driver had to cancel your trip to {{ $location_to }}. We're going to refund you for the cost of the drive. We're very sorry for any inconvience this may cause you.</p>

<p>The Ridesurf Team</p>

@include('email.layouts.footer')