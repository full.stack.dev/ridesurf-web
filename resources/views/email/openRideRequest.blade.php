@include('email.layouts.header')
<h2> Hi {{ $first_name }},</h2>

<p> A rider has requested a ride from you on {{$userRide->date}} at {{$userRide->hour}}
    for your ride from {{$openRide->location_from}} to {{$openRide->location_to}}.</p>
<p> Please login to your account to approve or deny this request at your earliest convenience.</p>
<p> Happy travels! <br />Team Ridesuf</p>
@include('email.layouts.footer')