@include('email.layouts.header')
<h3> Hi {{ $user_ride->user->first_name }},</h3>

<p> Your ride to {{ $ride->location_to }} was successfully deleted, and we have refunded you.</p>

<p> Here are the ride details: </p>

<p> <strong>To:</strong> {{ $ride->location_to }}</p>
<p> <strong>Date:</strong> {{ $ride->dateStart }} </p>
<p> <strong>Refund Amount:</strong> {{ $user_ride->price_paid }} </p>

	<p><br/>The Ridesurf Team
	<br />
	<br />
	</p>

@include('email.layouts.footer')