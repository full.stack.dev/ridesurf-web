@include('email.layouts.header')
<h4> Congratulations {{ $name }}, you have a new passenger!</h4>

<p>The user {{ $user }} just booked a seat for your ride from <strong>{{ $rideFrom }}</strong> to <strong> {{ $rideTo }}</strong> on {{ $rideDate }} at {{ $rideTime }}</p>

<p>Happy travels!<br/>Ridesurf
<br />
<br />
</p>
@include('email.layouts.footer')