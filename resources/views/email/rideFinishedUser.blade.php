@include('email.layouts.header')

<h3> Thanks for riding with {{ $driver }}!</h3>

<p> Please let us know how your ride was so we can pay the driver.</p>
<p> <strong>From:</strong> {{ $rideFrom }} <strong>To:</strong> {{ $rideTo }}</p>
<p> <strong>Date:</strong> {{ $date }} <br /><br /></p>
	
<p>
	<a class="button" href="{{ route('reviews.rate', [$driver_id, $rides_id]) }}"> Review Ride </a>
	<br />
	<br />
	Team Ridesurf
	<br />
	<br />
</p>

@include('email.layouts.footer')