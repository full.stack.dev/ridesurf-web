@extends('layouts.app')

@section('content')

<div class="container-fluid offer-ride">
	<div class="container">
		<div class=”row”>
		<div class=”col-md-8 col-md-offset-2">
			<div class=”panel panel-default”>
				<div class=”panel-heading”>Registration Confirmed</div>
					<div class=”panel-body”>
					Your email has been successfully verified. 
					@if(Auth::user())
						Complete your profile <a href="{{route('dashboarduser.profile')}}">here</a>.
					@else
					Click here to <a href="{{url('/login')}}">login</a>.
					@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection