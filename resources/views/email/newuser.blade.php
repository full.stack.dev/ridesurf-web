@include('email.layouts.header')

<p>Hi {{ $name }},</p>
 
<p>Welcome to Ridesurf! We hope you enjoy your travels and make lots of new friends. If you have any questions, please check the <a href="https://ridesurf.io/faq/">FAQ</a> on our website and if you don't find what you need feel free to drop us a line on the <a href="https://ridesurf.io/contact">contact page</a> and we'll respond within 24 hours. We also have an emergency phone line listed on our website (please use only for real emergencies).</p>

<p>Happy travels!<br />Team Ridesurf</p>

@include('email.layouts.footer')