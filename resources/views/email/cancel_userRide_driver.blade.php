@include('email.layouts.header')

	<h3>Hi {{ $name }},</h3>

	<p>Unfortunately {{$passenger_name}} had to cancel their booking on your ride to {{$location_to}}. We're very sorry for any inconvience this may cause you.</p>

	<p><br/>The Ridesurf Team
	<br />
	<br />
	</p>

@include('email.layouts.footer')