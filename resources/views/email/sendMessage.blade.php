@include('email.layouts.header')

	<h3> Hi {{ $reciver->first_name }}!</h3>

	<p>You have a new message from {{ $sender->first_name }}:</p>

	<p>{{ $reply }}</p>

	<p>
	<a href="{{ route('messages.show', $thread_id) }}" class="button">Reply</a>
	<br />
	<br />
	Team Ridesurf
	</p>


@include('email.layouts.footer')