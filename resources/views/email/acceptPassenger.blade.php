@include('email.layouts.header')

<h3> Congratulations {{ $user }}, </h3>

<p> The driver {{ $driver }} has approved your request to join them to {{ $rideTo }}!</p>
<p> You can pay for your ride now by clicking here:<br /><br />
 <a href="{{ url('rides/payment', $ride_id) }}" class="button">Purchase Ride</a>
 <br />
 <br/>
</p>


@include('email.layouts.footer')