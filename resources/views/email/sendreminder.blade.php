@include('email.layouts.header')

<h3> Hello, {{ $user_name }}! </h3>

<p> You are receiving this email because you signed up for an email alert.</p>

<p>
	<strong>From:</strong> {{$ride->location_from}} <br />
	<strong>To:</strong> {{$ride->location_to}} <br />
	<strong>Date</strong> {{ date('m/d/Y', strtotime($ride->dateFrom)) }} <br />
	<strong>Hour</strong> {{date("g:i a", strtotime($ride->startHour)) }} <br />
	<strong>Driver</strong> {{$ride->user->driver[0]->first_name}} <br />
	<strong>Actions</strong> <a href="{{ route('rides.join', [$ride->id, $ride->location_from, $ride->location_to, $ride->price]) }}"> Join </a>
</p>

<p>You can manage your ride reqeust <a href="{{ route('ride_request.index') }}" class="btn btn-info">here</a>.</p>

@include('email.layouts.footer')