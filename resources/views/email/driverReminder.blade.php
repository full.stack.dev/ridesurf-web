@include('email.layouts.header')

    <div class="container-fluid offer-ride">
        <div class="container">
           <h2> Hello {{$driver}},</h2>

            <p> The following items are expired: <strong>{{$expired}}</strong></p>
            <p> You won't be able to create any new rides until you upload a current {{$expired}} photo.</p>

            <p> You can upload the photo on our website here: <a href="https://ridesurf.io/dashboarduser/cardetails">Car Details</a> </p>
        </div>
    </div>

@include('email.layouts.footer')