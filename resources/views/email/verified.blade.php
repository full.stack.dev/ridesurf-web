@extends('layouts.app')
@section('content')
<div class="container-fluid offer-ride">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class=”panel panel-default”>
				<div class=”panel-heading”>Registration</div>
				<div class=”panel-body”>
				<p>You have successfully registered. An email has been sent to you for verification.
				<br />
				<br />
				Team Ridesurf
				</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection