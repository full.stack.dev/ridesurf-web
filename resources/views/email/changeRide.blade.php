@include('email.layouts.header')
<h2> Hello {{ $name }},</h2>

<p> Your ride to {{ $rideTo }} has been modified, here is the updated information.</p>

<p> From: {{ $rideFrom }}</p>
<p> To: {{ $rideTo }}</p>
<p> Driver: {{ $driver }} </p>
<p> Date: {{ $date }} </p>
<p> Time: {{ $hour	 }} </p>
<p> Description: {{ $description }} <p>

<a href="{{ url('/rides/show/', $ride_id) }}">

	<p><br/>The Ridesurf Team
	<br />
	<br />
	</p>
	
@include('email.layouts.footer')