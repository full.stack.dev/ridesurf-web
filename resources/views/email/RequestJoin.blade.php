@include('email.layouts.header')

<h3>Hi {{ $driver }},</h3>

<p>A rider has requested to join your ride. Please review their information and approve or deny their request as soon as possible.</p>

<p><strong> From:</strong> {{ $rideFrom }}</p>
<p><strong> To:</strong> {{ $rideTo }}</p>
<p><strong> Date: </strong>{{ $rideDate }} </p>
<p><strong> Name:</strong> {{ $user }}</p>
<p><strong> Reviews:</strong> {{ $userRaiting }} </p>
<p><strong> User Verified?</strong>
	 @if( $userVerify == null || $userVerify == 0) 
	 Not verified
	 @else
	 Yes
	 @endif
<br />
<br />
<a class="button" href="https://ridesurf.io/rides/rides/create"> Go to Dashboard </a>
<br />
</p>

@include('email.layouts.footer')