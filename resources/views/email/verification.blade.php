@include('email.layouts.header')

<h3>Please verify your email address to use Ridesurf.</h3>

<p>
	Click the following link to verify your email: <br/><br/>
	<a class="button" href="{{url('/verifyemail/'.$email_token)}}"> Verify Email </a>
	<br />
	<br />
	Happy travels!<br />
	Team Ridesurf<br/>
	<br/>
</p>

@include('email.layouts.footer')