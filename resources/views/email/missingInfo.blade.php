@include('email.layouts.header')
    <h3> Hi {{$name}},</h3>

    <p> Thank you for joining the Ridesurf community! <p>
    <p> It looks like your profile is not yet completed. In order to apply to be a driver or to join a ride you need to complete your profile. </p>

    <p>Happy travels!<br />
    Team Ridesurf</p>
@include('email.layouts.footer')