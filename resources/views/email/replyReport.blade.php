@include('email.layouts.header')

<h3>Hi {{ $first_name }},</h3>

<p>We are currently reviewing your case. You'll hear from us as soon with a resolution.</p>
<p>We're very sorry you had a less than amazing experience and we're going to do all we can to make things right.</p>

	<p><br/>The Ridesurf Team
	<br />
	<br />
	</p>

@include('email.layouts.footer')