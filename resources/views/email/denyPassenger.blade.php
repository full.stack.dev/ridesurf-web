@include('email.layouts.header')
<h4>Hi {{ $user }}, </h4>

<p>Unfortunately the driver for the trip to {{ $rideTo }} has denied your ride request.</p>

<p>This happens from time to time -- please don't be discouraged! We know you'll find a great ride if you keep looking.</p>

	<p><br/>The Ridesurf Team
	<br />
	<br />
	</p>
	
@include('email.layouts.footer')