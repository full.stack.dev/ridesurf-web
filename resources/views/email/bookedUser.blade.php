@include('email.layouts.header')
<h4>Hi {{ $user }}, </h4>
<p>Great news, you're going to {{ $rideTo }}!</p>
<p>You're all set for your road trip with {{ $driver }}.<p>
<table width="100%">
	<thead>
		<tr>
			<th> From </th>
			<th> To </th>
			<th> Date </th>
			<th> Time </th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{ $rideFrom }}</td>
			<td> {{ $rideTo }}</td>
			<td>{{ $rideDate }}</td>
			<td> {{ $rideTime }}</td>
		</tr>
	</tbody>
</table>
<p><strong>Your Booking Confirmation Code:</strong> {{$booking_confirm}}</p>

<p>Please be sure to arrange a public meeting location with your driver at least a day before your trip (think Starbucks!). Also be sure to review our <a href="https://ridesurf.io/code-of-conduct">Code of Conduct</a> and your driver's ride preferences.</p>

<p>Enjoy your trip!<br />
The Ridesurf Team
<br />
</p>

@include('email.layouts.footer')