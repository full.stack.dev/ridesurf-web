@include('email.layouts.header')
<h2> Hello {{ $first_name }},</h2>

<p> {{$reply}} </p>
<p> A total of <strong> ${{$total}} </strong> </p>

@include('email.layouts.footer')