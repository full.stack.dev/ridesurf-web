@include('email.layouts.header')
<h2> Hello {{ $name }},</h2>

<p>Congratulations, you have been approved as a driver!</p>
<p>Go create your first ride! 
	<br />
	<br />
	<br />
	<a class="button" href="https://ridesurf.io/rides/rides/create"> Create Ride </a>
	<br />
	<br />
</p>

@include('email.layouts.footer')