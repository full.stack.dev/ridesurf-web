@extends('layouts.app')


@section('content')
<div class="offer-ride">
	<div class="text-center">
		<div clas="row">
			<a class="center" href="{{ url('/') }}">
				<img src="{{asset('images/logo.png')}}" width="300">
			</a>
		</div>
		<div class="row" style="margin-top: 25px;">
			<h2> Whoops, Something went wrong </h2>
			<p> Error 500 </p>
		</div>

	</div>
</div>

<script type="text/javascript">
	var newURL = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search
	jQuery('#url_link').val(newURL);
	console.log(newURL);
</script>
@endsection