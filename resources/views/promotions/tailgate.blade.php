@extends('layouts.promotionLayout')

@section('content')
    <link href="{{ asset('css/landing-page.css') }}" rel="stylesheet">


    <section class="hero-banner">
        <div class="container text-center">
            <span class="hero-banner-icon"><i class="flaticon-sing"></i></span>
            <p>College Football is on!</p>
            <h1>Tailgating this Football Seasson? </h1>
            <p> Ride share your passion for sports!  </p>
        </div>
    </section>

    <section class="section-padding--small bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 align-self-center mb-5 mb-lg-0">
                    <div class="innovative-wrapper">
                        <h3 class="primary-text">College Football Season is on!  <br class="d-none d-xl-block"> Get ready and Worry less! </h3>
                        <p class="h4 primary-text2 mb-3"> Ridesurf can help you get to that game! </p>
                        <p>Ride sharing is a great way to get to an event! but sometimes is hard to find someone to ride with!
                         Ridesurf can help you connect with a Driver/Rider so everyone can enjoy what really matters! The Game!</p>
                    </div>
                </div>
                <div class="col-lg-6 pl-xl-5">
                    <img class="img-responsive" src="{{asset('images/stock/driver.jpg')}}">

                </div>
            </div>
        </div>
    </section>

@endsection

