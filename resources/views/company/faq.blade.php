@extends('layouts.app')
@section('content')
<script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
    });
</script>

<div class="container">
	<div id="pages-top">
		<h2>FAQ</h2>
	</div>

	<div id="pages-content">

		<h3>Account</h3>

    	<!-- Create An Account -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus"></span> How do I create an account?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>To sign up for Ridesurf, click the <a href="{{ route('register') }}">"Sign Up"</a> link in the top right corner of our website if you are using a desktop or laptop, or in the "hamburger menu" of our mobile website. Fill in your information manually or automatically by connecting with Facebook (you must already have a Facebook account). We will never sell your information. Please see our Terms and Conditions and Privacy Policy for further details.</p>
                </div>
            </div>
        </div>

        <!-- Delete Account -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-plus"></span> How do I delete my account?</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>To delete your account, scroll to the bottom of your account dashboard. On the side will be a red button under the Settings Panel in the sidebar that says "Delete Account". Click this button and you will be asked if you want to delete your account. Select yes. This will permanently delete your account and your information will be removed from our servers.</p>
                </div>
            </div>
        </div>

       	<!-- Profile photo -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-plus"></span>Uploading a profile photo</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>To display well on our website and help other users identify you, your profile picture must abide by the following guidelines:

                    	<ul>
                    		<li>Your photo must be a jpg/jpeg no larger than 15MB.</li>
                    		<li>You must be alone in your photo. This eliminates confusion and identiity fraud.</li>
                    		<li>You must not be wearing sunglasses or anything that obstructs part of your face, like sports gear.</li>
                    		<li>The photo must be an upper-torso or head shot (zoomed in or cropped on your upper torso and above or shoulders and above).</li>
                    		<li>Smiling photos are better than not smiling.</li>
                    		<li>We cannot accept blurry photos, dark photos, full body or far away photos, silly faces or costumed photos, digitally enhanced photos (no snapchat filters, please).</li>
                    		<li>The whole point of the photo is so you are easy to recognize when you are connecting in person for your ride. Your photo should be a very clear, current representation of yourself.</li>
                    	</ul>
                    </p>
                </div>
            </div>
        </div>


		<h3>Rides</h3>

		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-plus"></span> How do I create a ride?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>First, you must sign up for an account and apply to be a driver. Once you've been approved to drive, to offer a ride, click <a href="{{ route('rides.create') }}" >"Offer Ride"</a> in the menu on the homepage (upper right part of website) when logged in. You must be logged in to create a ride. Fill out the form with your ride details. A suggested price will automatically be generated. If you want to alter it, click the field and enter your own value. Be sure to enter any relevant details about the ride in the details box, plus an accurate selection for luggage size, leaving time, etc. Be ready to show your valid driver's license and insurance if a passenger asks (it is their right to see it and refusal is grounds for fair ride cancellation).</p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" 
                    href="#collapseThree"><span class="glyphicon glyphicon-plus"></span> 
                	How do I join a ride?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>In the menu click <a href="{{ route('rides.search') }}" >"Find Ride"</a>, then fill out the form with your from location, your desired destination, your desired date, and if you have a women only preference or maximum price preference. If there are any rides that match, they will be displayed. If not, you may create a email alert for matching rides. To join a ride, you must first put in a request to the driver. You may also message the driver beforehand. The driver will approve or deny your request. You will then be able to book the ride by paying upfront. Your payment will be held in escrow until your ride is completed and you have verified safe arrival. After booking you are encouraged to communicate with the driver to determine an exact pickup and dropoff location.</p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" 
                    href="#collapseFour"><span class="glyphicon glyphicon-plus"></span> As a driver, how do I get paid?</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>After the ride has been completed, both the passenger and the driver are required to leave a review and rating. Once your passenger has submitted your rating and review, the payment will be added to our automatic payout system which releases payouts every Friday.</p>
                </div>
            </div>
        </div>

	    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" 
                    href="#collapse-7"><span class="glyphicon glyphicon-plus"></span> Cancellations & No-shows</a>
                </h4>
            </div>
            <div id="collapse-7" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>If your passenger or driver cancels right before departure or is a no-show, please follow these guidelines: <br /></p>
                    	<ul>
                    		<li>24+ hours out: If the driver/rider cancels more than 24 hours out, the rider gets a full refund minus the credit card processing fee and the driver does not get paid. We must keep our $1.50 service transaction fee as this is for the credit card processing and payment reversal and the credit card companies take these funds no matter if the ride is completed or not. In the event it is the rider who cancels, their seat will be released to the available ride pool again.</li>
                    		<li>Less than 24 hours out (Driver): If the driver cancels less than 24 hours out, the rider gets a full refund and the driver gets a cancellation strike. If the driver cancels more than 3 rides 24 hours out, they are suspended from our service. They also get an automatic "driver cancelled less than 24 hours out" system review.</li>
                    		<li>Less than 24 hours out (Rider): If the rider cancels less than 24 hours out, the driver still receives partial payment for the trip.</li>
                    		<li>No-Show (Driver): If the driver is a no-show, please report them to us immediately. The rider will receive a full refund and the driver's account will be suspended until they prove they had a valid reason for standing up a passenger, if they did have a valid reason (personal or work emergengies). We will also do our best to connect riders with a similarly priced alternative travel arrangement.</li>
                    		<li>No-Show (Rider): If the rider is a no-show, please report to us asap. The driver will still be paid for the trip and the rider will have a "no show" review left on their profile by the system.</li>
                    	</ul>

				<p>When you report a no-show, we give the passenger/driver up to 7 days to agree or disagree with your claim, and process each case carefully.</p>
                </div>
            </div>
        </div>

	</div>
</div>
<script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
        	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
    });
</script>

@endsection