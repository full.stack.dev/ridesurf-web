@extends('layouts.app')
@section('content')
	
<div class="container-fluid text-center special-event margin-top-30" style="padding: 120px;">
  
    <h2 class="white" style="padding-top:40px;">Ridesurf x Home Bass</h2>

</div>

<div class="container">
	@if ($message = Session::get('success'))
		<div class="alert alert-success" id="alert-success">
		  <p>{{ $message }}</p>
		</div>
	@endif

	<div class="container-fluid text-center full-margin-100">
		<h5 style="color: #fe855b;">Ridesurf is proud to be the official carpool app for Home Bass!</h5> 

		<p class="full-margin-25">If you're driving to Home Bass, create a ride here to carpool with other attendees. You can make money, make friends and it's better for the environment to carpool!<br/><br/>Enter "The Grand Orlando Resort" as the dropoff location and be sure to add a stopover at the airport if you're ok with picking up attendees at the airport on your way to Home Bass.</p>

		<p class="full-margin-25"><a href="{{route('rides.create')}}" class="btn btn-primary">Create a ride</a></p>

		<p>Looking for a ride? Use the ride search or see all Home Bass / EDC listings below.</p>
	</div>

	<div class="offer-ride text-center">
		<h2>Home Bass Ride Search</h2>

		<div class="container-fluid search-ride-form">

			<div class="widget user-dashboard-profile form-group">
			
			{!! Form::open(array('route'=>'search.filterHomeBass','method'=>'GET', 'class'=>'online-form')) !!}
				
				<div class="row">
					<!-- DO NOT REMOVE ID's OF FORM --> 
					<div class="col-md-3" >
						<input type="date" name="date" value="{{$date}}" onfocus="(this.type='date')" id="dateFrom"
						placeholder="Date"  class="form-control">
					</div>
					
					<div class="search_filters">
							<label><strong>Filter Results:</strong></label>
						</div>
						<div class="search_filters">
							<input  type="checkbox" value="{{$women}}" name="ladies_only"><label>Women Only</label>
						</div>
						<div class="search_filters">
							<label>Max Price</label> 

							<input name="max_price" type="number" class="price_input" id="max_price" value="{{$price}}">

					</div>
					<div class="col-md-3">
						<button type="submit" class="form-control ridesearch" 
						id="strLoader">Search</button>
					</div>
				</div>
				<br>
				
				<div class="row">
					<div class="col-md-12">
						<style type="text/css">
							/* This is style is not appering in the live production */
							.search_filters{
							    color: #555;
							    float: left;
							    padding: 4px 20px 4px 0;
							}
							.search_filters label{
							    font-weight: normal;
							    padding: 4px 10px 4px 10px;

							}
						</style>
						
					</div>
					
				</div>
			</div>
				{!! Form::close() !!}
		</div>

		<div class="col-md-12">
			@if($control && empty($rides))
				<p> There are no matches with this criteria! </p>
			@endif

	 		@foreach($rides as $ride)	
					@include('rides.layout.ridecard', [$ride])

			@endforeach
		</div>
</div>
@endsection