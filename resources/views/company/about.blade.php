@extends('layouts.app')
@section('content')

<div class="container">
	
	<div id="pages-top">
		<h2>About Ridesurf</h2>
	</div>

	<div id="pages-content">

		<div class="florida-launch">
			<p><strong>Active Locations: </strong><i class="em em-tangerine"></i> Florida, <i class="em em-palm_tree"></i>California (Click <a href="http://eepurl.com/ggIG1b">here</a> to bring Ridesurf to your state!)</p>
		</div>

		<p>
			Ridesurf makes it safe and easy for people who are traveling the same direction to share a ride with one another. Share your road trip, drives to festivals and events, and any pre-planned trip by motor vehicle.
		</p>

		<h3>Driving?</h3>
		<ul>
			<li>Provide a few details about yourself and your car</li>
			<li>Drivers/passengers use the same app</li>
			<li>Post a ride in seconds</li>
			<li>Anyone looking for the same trip can now request to book your extra seat(s)!</li>
		</ul>

		<h3>Looking for a ride? </h3>
		<ul>
			<li>Provide a few details about yourself</li>
			<li>Search for rides and set alerts for future trips</li>
			<li>Request a ride in seconds</li>
		</ul>

		<h3>Once a ride is booked:</h3>
		<ul>
			<li>Passenger pays for the trip and funds are held until trip is complete</li>
			<li>Coordinate a pick-up somewhere safe </li>
			<li>Meet, show each other your valid IDs</li>
			<li>Enjoy your trip together</li>
			<li>Post a quick rating and review of the trip</li>
		</ul>

		<p>With our many safety features you can leave your worries behind. We verify that every driver has a valid photo ID and collect detailed car information, plus ride reviews and ratings are mandatory and cannot be altered. </p>

		<span style="color: rgb(192, 192, 192);"><em>Icons made by <a href="http://www.flaticon.com/authors/freepik">Freepik</a> from <a href="http://www.flaticon.com">www.flaticon.com</a> & Free Vectors via <a href="https://www.vecteezy.com">Vecteezy.com</a></em></span>

	</div>
</div>

@endsection

