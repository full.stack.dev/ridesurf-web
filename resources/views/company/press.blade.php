@extends('layouts.app')
@section('content')

<div class="container">
	<div id="pages-top">
		<h2>Press</h2>
	</div>
	<div id="pages-content">

		<h3 style="text-align: center"><a href="/presskit/Ridesurf_Press_Kit.pdf">Download Press Kit</a></h3>


		<h3>2019</h3>

		<p><br />
			<strong>Newswire Press Release: </strong> <a href="https://www.newswire.com/news/ridesurf-launches-long-distance-and-event-carpooling-app-in-california-20993140" target="_blank">Ridesurf Launches Long-Distance and Event Carpooling App in California</a>
			<br />
		</p>

		<p><br />

			<strong>TechStars: </strong> <a href="https://www.techstars.com/content/accelerators/launchpad-lift-spotlight-ridesurf/" target="_blank">TechStar's LaunchPad Lift Spotlight: Ridesurf </a>
			<br />
		</p>

		<p><br />
			<strong>South Florida Business Journal:</strong> <a href="/presskit/articles/south-fl-biz-journal.pdf" target="_blank">Ventures Roundup: 25 startups to compete at SUP X in Fort Lauderdale</a>
			<br />
		</p>

		<p><br />
			<strong>Blackstone Foundation:</strong> <a href="https://bxcfideas.com/leading-student-entrepreneurial-teams-begin-launchpad-lift-program-de90a89a1fc1" target="_blank">Leading Student Entrepreneurial Teams Begin LaunchPad Lift Program</a>
			<br />
		</p>

		<p><br />
			<strong>Orlando Business Journal:</strong> <a href="/presskit/articles/orlandobizjournal.jpg" target="_blank">Orlando firm brings new look to carpooling</a>
			<br />
		</p>

		<p><br />
			<strong>UCF Today:</strong> <a href="https://today.ucf.edu/4-student-teams-to-compete-for-75000-in-shark-tank-style-joust-competition/" target="_blank">4 Student Teams to Compete for $75,000 in ‘Shark Tank’-Style Joust Competition</a>
			<br />
		</p>

		<p><br />
			UCF College of Business:<a href="https://business.ucf.edu/4-student-teams-compete-75000-shark-tank-style-joust-competition/" target="_blank"> 4 Student Teams to Compete for $75,000 in ‘Shark Tank’-Style Joust Competition</a>
			<br /><br /><br />
		</p>

		<h3>2018</h3>
		<p><br />
			UCF Today: <a href="https://today.ucf.edu/healthy-eating-and-travel-apps-win-big-at-ucfs-2018-social-venture-competition/" target="_blank">Healthy Eating and Travel Apps Win Big at UCF’s 2018 Social Venture Competition</a></p>
		<p></p>
	</div>
</div>
@endsection

