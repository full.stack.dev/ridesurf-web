@extends('layouts.app')

@section('content')


	<style>
		/* FEATURES
----------------------*/

		.gradient-fill:before {
			color: #fc73b4;
			background: -moz-linear-gradient(top, #9477b4 0%, #08d5bf 100%);
			background: -webkit-linear-gradient(top, #9477b4 0%, #08d5bf 100%);
			background: linear-gradient(to bottom, #9477b4 0%, #fc73b4 100%);
			-webkit-background-clip: text;
			background-clip: initial;
			-webkit-text-fill-color: transparent;
		}

		.card.features {
			border: 0;
			border-radius: 3px;
			box-shadow: 0px 5px 7px 0px rgba(0, 0, 0, 0.04);
			transition: all 0.3s ease;
		}

		@media (max-width:991px) {
			.card.features {
				margin-bottom: 2rem;
			}
			[class^="col-"]:last-child .card.features {
				margin-bottom: 0;
			}
		}

		.card.features:before {
			content: "";
			position: absolute;
			width: 3px;
			color: #fc73b4;
			background: -moz-linear-gradient(top, #9477b4 0%, #fc73b4 100%);
			background: -webkit-linear-gradient(top, #9477b4 0%, #fc73b4 100%);
			background: linear-gradient(to bottom, #9477b4 0%, #fc73b4 100%);
			top: 0;
			bottom: 0;
			left: 0;
		}

		.card-text {
			font-size: 14px;
		}

		.card.features:hover {
			transform: translateY(-3px);
			-moz-box-shadow: 0px 5px 30px 0px rgba(0, 0, 0, 0.08);
			-webkit-box-shadow: 0px 5px 30px 0px rgba(0, 0, 0, 0.08);
			box-shadow: 0px 5px 30px 0px rgba(0, 0, 0, 0.08);
		}

		.box-icon {
			box-shadow: 0px 0px 43px 0px rgba(0, 0, 0, 0.14);
			padding: 10px;
			width: 70px;
			border-radius: 3px;
			margin-bottom: 1.5rem;
			background-color: #FFF;
		}

		.circle-icon {
			box-shadow: 0px 9px 9px 0px rgba(0, 0, 0, 0.06);
			width: 75px;
			height: 75px;
			border-radius: 50%;
			margin-bottom: 5px;
			background-color: #FFF;
			color: #08d5bf;
			font-size: 30px;
			text-align: center;
			line-height: 80px;
			font-weight: 300;
			transition: all 0.3s ease;
		}

		@media (max-width:992px) {
			.circle-icon {
				width: 70px;
				height: 70px;
				font-size: 28px;
				line-height: 50px;
			}
		}

		.ui-steps li:hover .circle-icon {
			background-image: -moz-linear-gradient( 122deg, #08d5bf 0%, #72fff0 100%);
			background-image: -webkit-linear-gradient( 122deg, #08d5bf 0%, #72fff0 100%);
			background-image: -ms-linear-gradient( 122deg, #08d5bf 0%, #72fff0 100%);
			background-image: linear-gradient( 122deg, #08d5bf 0%, #72fff0 100%);
			box-shadow: 0px 9px 32px 0px rgba(0, 0, 0, 0.09);
			color: #FFF;
		}

		.ui-steps li {
			padding: 10px 0;
		}

		.ui-steps li:not(:last-child) {
			border-bottom: 1px solid #08d5bf;
		}

		.perspective-phone {
			position: relative;
			z-index: -1;
		}

		@media (min-width:992px) {
			.perspective-phone {
				margin-top: -150px;
			}
		}


		.section {
			padding: 80px 0;
		}

		.section-title {
			text-align: center;
			margin-bottom: 3rem;
		}

		.section-title small {
			color: #998a9b;
		}

		@media (max-width:767px) {
			h1 {
				font-size: 40px;
			}
			h2 {
				font-size: 30px;
			}
		}
	</style>
	<div class="section light-bg">

		<div class="container">
			<div class="row">
				<div class="col-md-8 d-flex align-items-center">
					<ul class="list-unstyled ui-steps">
						<li class="media">
							<div class="circle-icon mr-4">1</div>
							<div class="media-body">
								<h5>Search for the ride you want!</h5>
								<ul>
									<li> Rides 24/7 365 rain or shine. </li>
									<li> Average price is $25 for a 250mi trip </li>
								</ul>
							</div>
						</li>
						<li class="media my-4">
							<div class="circle-icon mr-4">2</div>
							<div class="media-body">
								<h5>Book Your Ride</h5>
								<ul>
									<li>Find the perfect ride for you by filtering by price and ride preferences</li>
									<li>Review your driver's profile to make sure they are fully verified and have good reviews</li>
									<li>Tell your driver about yourself and your trip</li>
								</ul>
							</div>
						</li>
						<li class="media">
							<div class="circle-icon mr-4">3</div>
							<div class="media-body">
								<h5>Arrive early for your trip</h5>
								<ul>
									<li>Out of consideration for your driver, please arrive at least 10 minutes early to your pick up location</li>
									<li>Let them know immediately if you need to cancel or if you will be late</li>
									<li>Be courteous and understanding if they cannot wait for you if you're late</li>
									<li>Review your driver's license, license plate, VIN, and insurance. Do not travel if anything does not match their profile exactly.</li>
								</ul>
							</div>
						</li>
						<li class="media">
							<div class="circle-icon mr-4">4</div>
							<div class="media-body">
								<h5>Arrive at destination</h5>
								<ul>
									<li>When you arrive, please review the driver asap, as we do not pay them until you do.</li>
									<li>Be sure to tell your friends about your trip!</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
				<div class="col-md-4">
					<img src="images/mock-up.png" alt="iphone" class="img-fluid">
				</div>

			</div>

		</div>

	</div>
	<!-- // end .section -->

@endsection