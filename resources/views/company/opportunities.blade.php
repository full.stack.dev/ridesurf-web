@extends('layouts.app')
@section('content')

<div class="container">
	<div id="pages-top">
		<h2>Opportunities</h2>
	</div>
	<div id="pages-content">
		<p>At the moment we're seeking investors and interns.</p>
		<p>Please visit the contact page and send us an email for details!</p>
	</div>
</div>
@endsection

