@extends('layouts.app')
@section('content')

<div class="container">
	<div class="offer-ride">
		<h2> Feedback Form </h2>

		{!! Form::open(array('route' => 'company.post_feedback' , 'method' => 'POST' )) !!}

			<div class="form-group">
				<label>Your name</label>
				<input type="text" name="name" class="form-control float-label-control"
				placeholder="Your Name / Anonymous ">
			</div>
			<div class="form-group">
				<label>How did you heard about us?</label>
				<select class="form-control float-label-control" name="source">
					<option value="google-add">Google Add </option>
					<option value="faceboo-addk">Facebook Add</option>
					<option value="friend"> From A friend</option>
					<option value="word-to-mouth">Word To Mouth</option>
					<option value="other">Other</option>
					
				</select>
			</div>
			<div class="form-group">
				<label> Describe Your Experience </label>
				<textarea class="form-control float-label-control" name="description" rows="10"></textarea>
			</div>

			<button class="btn btn-success">Submit</button>

		{!! Form::close() !!}
	</div>
</div>
@endsection