@extends('layouts.app')
@section('content')

<div class="container">
	<div id="pages-top">
		<h2>Privacy Policy</h2>
	</div>
	<div id="pages-content">
		<p>1. General<br /><br />1.1 &ndash; Travlr Co. (&ldquo;we&rdquo; or &ldquo;us&rdquo;) take the privacy of your information very seriously. Our Privacy Policy is designed to tell you about our practices regarding the collection, use and disclosure of information that you may provide via the ridesurf.io website and other associated partner websites, microsites and sub-sites (the &ldquo;Site&rdquo;).<br /><br />1.2 &ndash; By using this Site or any services we offer, you are consenting to the collection, use, and disclosure of that information about you in accordance with, and are agreeing to be bound by, this Privacy Policy<br /><br />
		2. Ways that we collect information<br />
		<br />
		2.1 &ndash; We may collect and process the following personal information or data (information that can be uniquely identified with you) about you:<br />
		<br />
		2.1.1 &ndash; Certain information required to register with the Site or to access other services provided by us, including your name, address, date of birth and if you are a Driver certain information about your vehicle etc.;<br />
		<br />
		2.1.2 &ndash; Your e-mail address and a password;<br />
		<br />
		2.1.3 &ndash; A mobile phone number;<br />
		<br />
		2.1.4 &ndash; A record of any correspondence between you and us;<br />
		<br />
		2.1.5 &ndash; A record of any bookings you have made or advertisements you have placed with or through the Site;<br />
		<br />
		2.1.6 &ndash; Your replies to any surveys or questionnaires that we may use for research purposes;<br />
		<br />
		2.1.7 &ndash; Details of accounting or financial transactions including transactions carried out through the Site or otherwise. This may include information such as your credit card, debit card or bank account details, details of Trips or Legs (as described in our Conditions) you have booked or offered through the Site;<br />
		<br />
		2.1.8 &ndash; Details of your visits to the Site and the resources that you access;<br />
		<br />
		2.1.9 &ndash; Information we may require from you when you report a problem with the Site.<br />
		<br />
		2.1.10 &ndash; Passport, Driver's Licence, and such other documents as you may have accepted to provide us with<br />
		<br />
		2.2 &ndash; We only collect such information when you choose to supply it to us. You do not have to supply any personal information to us but you may not be able to take advantage of all the services we offer without doing so.<br />
		<br />
		2.3 &ndash; Information is also gathered without you actively providing it, through the use of various technologies and methods such as Internet Protocol (IP) addresses and cookies. These methods do not collect or store personal information.<br />
		<br />
		2.4 &ndash; An IP address is a number assigned to your computer by your Internet Service Provider (ISP), so you can access the Internet. It is generally considered to be non-personally identifiable information, because in most cases an IP address can only be traced back to your ISP or the large company or organisation that provides your internet access (such as your employer if you are at work).<br />
		<br />
		2.5 &ndash; We use your IP address to diagnose problems with our server, report aggregate information, and determine the fastest route for your computer to use in connecting to our site, and to administer and improve the site.<br />
		<br />
		3. Use<br />
		<br />
		3.1 &ndash; We may use this information to:<br />
		<br />
		3.1.1 &ndash; ensure that the content of the Site is presented in the most effective manner for you and for your computer and customise the Site to your preferences;<br />
		<br />
		3.1.2 &ndash; assist in making general improvements to the Site;<br />
		<br />
		3.1.3 &ndash; carry out and administer any obligations arising from any agreements entered into between you and us;<br />
		<br />
		3.1.4 &ndash; allow you to participate in features of the Site and other services;<br />
		<br />
		3.1.5 &ndash; contact you and notify you about changes to the Site or the services we offer (except where you have asked us not to do this);<br />
		<br />
		3.1.6 &ndash; collect payments from you;<br />
		<br />
		3.1.7 &ndash; analyse how users are making use of the Site and for internal marketing and research purposes;<br />
		<br />
		3.1.8 &ndash; ensure that you comply with the T&amp;Cs, Charter of Good Conduct and applicable law.<br />
		<br />
		3.1.9 &ndash; For the purpose of verification by a third party service provider of the information contained in the passport, Driver's Licence, and other identity documents that may be collected from you at the time of registration or at any other time during your use of the Site as may be required. Third party service providers will process data under Ridesurf&rsquo;s control and will be bound by the same degree of security and care as Ridesurf&rsquo;s under this Privacy Policy.<br />
		<br />
		3.2 &ndash; We will not resell your information to any third party nor use it for any third party marketing.<br />
		<br />
		4. Sharing your information<br />
		<br />
		4.1 &ndash; If we charge you any fees or collect any money from you in relation to any services on the Site, including any sponsorship money, credit or debit card payments will be collected by our payment processor.<br />
		<br />
		4.2 &ndash; In order for payments to be processed you may need to provide some necessary details to our payment processor. We will tell you about this at the point we collect that information.<br />
		<br />
		4.3 &ndash; We do not disclose any information you provide via the Site any third parties except:<br />
		<br />
		4.3.1 &ndash; As part of our booking process and in order to provide our services information may be passed to a Passenger (if you are a Driver) or to a Driver (if you are a Passenger).<br />
		<br />
		4.3.2 Where we involve a third party service provider, for the performance of any contract we enter into with you in order to facilitate or extend our services (e.g. to verify your identity)<br />
		<br />
		4.3.3 &ndash; If we are under a duty to disclose or share your personal data in order to comply with any legal obligation (for example, if required to do so by a court order or for the purposes of prevention of fraud or other crime);<br />
		<br />
		4.3.4 &ndash; in order to enforce any terms of use that apply to any of the Site, or to enforce any other terms and conditions or agreements for our Services that may apply;<br />
		<br />
		4.3.5 &ndash; we may share information with our international partner websites where you have made a booking through the Site with a user of our partner websites or where you have used the Site to interact with a third party;<br />
		<br />
		4.3.6 &ndash; we may transfer your personal information to a third party as part of a sale of some or all of our business and assets to any third party or as part of any business restructuring or reorganisation, but we will take steps with the aim of ensuring that your privacy rights continue to be protected;<br />
		<br />
		4.3.7 &ndash; to protect the rights, property, or safety of Travlr Co., the Site&rsquo;s users, or any other third parties. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.<br />
		<br />
		4.4 &ndash; Other than as set out above, we shall not disclose any of your personal information unless you give us permission to do so.<br />
		<br />
		5. Cookies<br />
		<br />
		5.1 &ndash; A cookie is a piece of data stored locally on your computer and contains information about your activities on the Internet. The information in a cookie does not contain any personally identifiable information you submit to our site.<br />
		<br />
		5.2 &ndash; On the Site, we use cookies to track users&rsquo; progress through the Site, allowing us to make improvements based on usage data. We also use cookies if you log in to one of our online services to enable you to remain logged in to that service. A cookie helps you get the best out of the Site and helps us to provide you with a more customized service.<br />
		<br />
		5.3 &ndash; Once you close your browser, our access to the cookie terminates. You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. To change your browser settings you should go to your advanced preferences.<br />
		<br />
		5.4 &ndash; We are required to obtain your consent to use cookies. [We have a clear cookies notice on the home page of the website.] If you continue to use the Site having seen the notice then we assume you are happy for us to use the cookies described above.<br />
		<br />
		5.5 &ndash; If you choose not to accept the cookies, this will not affect your access to the majority of information available on the Site. However, you will not be able to make full use of our online services.<br />
		<br />
		6. Access to and correction of personal information<br />
		<br />
		6.1 &ndash; We will take all reasonable steps in accordance with our legal obligations to update or correct personally identifiable information in our possession that you submit via this Site.<br />
		<br />
		6.2 &ndash; We take all appropriate steps to protect your personally identifiable information as you transmit your information from your computer to our Site and to protect such information for loss, misuse, and unauthorised access, disclosure, alteration, or destruction. We use leading technologies and encryption software to safeguard your data, and operate strict security standards to prevent any unauthorised access to it.<br />
		<br />
		6.3 &ndash; Where you use passwords, usernames, or other special access features on this site, you also have a responsibility to take reasonable steps to safeguard them.<br />
		<br />
		7. Other websites<br />
		<br />
		7.1 &ndash; This Site contains links and references to other websites. Please be aware that this Privacy Policy does not apply to those websites.<br />
		<br />
		7.2 &ndash; We cannot be responsible for the privacy policies and practices of sites that are not operated by us, even if you access them via the Site. We recommend that you check the policy of each site you visit and contact its owner or operator if you have any concerns or questions.<br />
		<br />
		7.3 &ndash; In addition, if you came to this Site via a third party site, we cannot be responsible for the privacy policies and practices of the owners or operators of that third party site and recommend that you check the policy of that third party site and contact its owner or operator if you have any concerns or questions.<br />
		<br />
		8. Transferring your information outside of Europe<br />
		<br />
		8.1 &ndash; As part of the services offered to you through the Site, the information you provide to us may be transferred to, and stored at, countries outside of the United States (&ldquo;USA&rdquo;). By way of example, this may happen if any of our servers are from time to time located in a country outside of the USA or one of our service providers is located in a country outside of the USA. We may also share information with other equivalent national bodies, which may be located in countries worldwide. These countries may not have similar data protection laws to the USA. If we transfer your information outside of the USA in this way, we will take steps with the aim of ensuring that your privacy rights continue to be protected as outlined in this privacy policy.<br />
		<br />
		8.2 &ndash; If you use the Site while you are outside the USA, your information may be transferred outside the USA in order to provide you with those services.<br />
		<br />
		8.3 &ndash; By submitting your personal information to us you agree to the transfer, storing or processing of your information outside the USA in the manner described above.<br />
		<br />
		9. Notification of changes to our Privacy Policy<br />
		<br />
		We will post details of any changes to our Privacy Policy on the Site to help ensure you are always aware of the information we collect, how we use it, and in what circumstances, if any, we share it with other parties.<br />
		<br />
		10. Contact us<br />
		<br />
		If at any time you would like to contact us with your views about our privacy practices, or with any enquiry relating to your personal information, you can do so by way of our contact page.</p>
	</div>
</div>
@endsection

