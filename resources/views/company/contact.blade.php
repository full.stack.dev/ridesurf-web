@extends('layouts.app')
@section('content')

<div class="container">	
	<div id="pages-top">
		<h2>Contact Ridesurf</h2>
	</div>

	<p class="bottom-margin bottom-pad">We're here to help. Please send us an e-mail by filling out the form below if you have non-urgent questions or issues with the service, and we'll get back to you within 24 hours. If it's urgent, call our 24hr customer service line listed below.</p>

	<div class="col-md-5">

		<h3>Basic question?</h3>
		<p class="bottom-pad">Please view our <a href="https://ridesurf.io/faq">FAQ</a> for answers to common questions.</p>

		<h3>Call Us:</h3>

		<p>Our 24/7 Emergency Help Line:</p>
		<p>+1 (321) 325-0157</p>

		

	</div>

	<div class="col-md-7">


			<h3 class="bottom-pad">Email us:</h3>

			{!! Form::open(array('route' => 'company.contact_update','method'=>'POST')) !!}
			<div class="form-group">
				<label> Your Name </label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label> Your Email </label>
				<input type="email" name="email" class="form-control">
			</div>
			<div class="form-group">
				<label> Subject </label>
				<input type="text" name="subject" class="form-control">
			</div>
			<div class="form-group">
				<label> Message </label>
				<textarea name="description" class="form-control"></textarea>
			</div>

			<div class="g-recaptcha" data-sitekey="6LcKUosUAAAAAKl3i_z6sqwJ2Rr2QtZjqSHUAAq8" 
		                            data-callback="enableBtn"></div>

			<button class="btn btn-primary" id="button1"> Submit </button>
			{!! Form::close() !!}

	</div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
	
    $('document').ready(function(){
        document.getElementById("button1").disabled = true;
    });
    function enableBtn(){
        document.getElementById("button1").disabled = false;
    }

</script>
@endsection

