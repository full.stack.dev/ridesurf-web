@extends('layouts.app')
@section('content')
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.5.0/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.5.0/mapbox-gl.css' rel='stylesheet' />

<style>
#map { position:absolute; top:0; bottom:0; width:0%; }
</style>

<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css' type='text/css' />
<!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
    <div class="container-fluid text-center event-synapse margin-top-30" style="padding: 180px;">

        <h1 class="white" style="padding-top:40px;">Ridesurf x Synapse</h1>

    </div>

    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success" id="alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="container-fluid text-center full-margin-100">

            <h5 style="color: #030285;">Ridesurf is proud to be the official carpool app for Synapse Summit 2020!</h5>

            <p class="full-margin-25">If you're driving to Synapse, create a ride to carpool with other attendees. You can make money, make friends and it's better for the environment to carpool!<br/><br/>Enter "Amalie Arena" as your destination and "Synapse" as your trip type! Pro tip: add a stopover at the airport if you're ok with picking up attendees there on your way to Synapse!</p>

            <p class="full-margin-25"><a href="{{route('rides.create')}}" class="btn btn-primary">Create a ride</a></p>

            <p>Looking for a ride? Use the ride search or see all Synapse rides listed below.</p>
        </div>

        <div class="offer-ride text-center">  
            <!--<script type="text/javascript">
                var synapse = true;
            </script> -->
            <div id="search">
                <div>
                    <router-multi-view
                        class="wrapper"
                        states="{{ $states }}"
                        name="fade"
                    />
                </div>
            </div>
            <div id="app" style="display: none;"></div>
            <script src="{{asset('js/app.js')}}"></script>

            <div class="col-md-12">

{{--        
            @if($control && empty($rides))
                    <p>There are no Synapse rides yet. Add your ride now!</p>
                @endif

                @foreach($rides as $ride)
                    @include('rides.layout.ridecard', [$ride])

            @endforeach 
--}}
            </div>
        </div>
@endsection