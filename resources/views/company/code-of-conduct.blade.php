@extends('layouts.app')
@section('content')

<div class="container">
	<div id="pages-top">
		<h2>Code of Conduct</h2>
	</div>

	<div id="pages-content">
		<p>
			<ol>
				<li>Provide true information about yourself.</li>
				<li>Create a profile with your legal name and a real photo.</li>
				<li>Only offer rides on trips you intend to take.</li>
				<li>Be reliable.</li>
				<li>Show up for your ride on time, stick to the details agreed, and leave the car clean and tidy.</li>
				<li>Stay safe. Use common sense.</li>
				<li>Follow the rules of the road, take care of your own safety (seat belt) and the safety of others (do not engage in behavior that could be distracting such as cell phone use).</li>
				<li>Be considerate and welcoming.</li>
				<li>Consider the preferences of the Ride Group and respect the car you're travelling in.</li>
				<li>Give fair ratings.</li>
			</ol>
		</p>
	</div>
</div>
@endsection

