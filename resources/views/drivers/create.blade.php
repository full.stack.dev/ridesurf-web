@extends('layouts.app')


@section('content')
	@hasrole('Driver|Admin')
	<h3> Offer A Ride</h3>
	<div class="row">
		<div class="widget col-lg-8 col-md-8 col-sm-12">

			{!! Form::open(array('route' => 'drivers.store','method'=>'POST')) !!}
				<label> From where are you departing? </label>
				{!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}


			</form>
		</div>

		{!! Form::open(array('route' => 'locations.store','method'=>'POST')) !!}
	
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Name:</strong>
	            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>Description</strong>
	            {!! Form::text('description', null, array('placeholder' => 'Description','class' => 'form-control')) !!}
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-12">
	        <div class="form-group">
	            <strong>City</strong>
	            {!! Form::text('city', null, array('placeholder' => 'City','class' => 'form-control', 'id' => 'city_name')) !!}
	            <div id="cityList">
    			</div>
	        </div>
	    </div>
	    
	    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
	        <button type="submit" class="btn btn-primary">Submit</button>
	    </div>
	</div>
	{!! Form::close() !!}
		<div class="widget col-lg-4 col-md-4 col-sm-12">
		</div>
	</div>
	@endhasrole
@endsection