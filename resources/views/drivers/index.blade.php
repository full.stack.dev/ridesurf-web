@extends('layouts.app')


@section('content')
	<div class="container">
	<section class="dashboard section">
		@if ($message = Session::get('success'))
		<div class="alert alert-success">
		  <p>{{ $message }}</p>
		</div>
		@endif
		
	<!-- Container Start -->
	<div class="container-fluid offer-ride">
	<div class="container">
		<!-- Row Start -->
		<div class="row">
			<div class="col-md-10 offset-md-1 col-lg-4 offset-lg-0">
				<div class="sidebar">
					<!-- User Widget -->
					<div class="widget user-dashboard-profile">
						<!-- User Image -->
						<div class="profile-thumb">
							@if(count($user->profileImages) > 0)
								<img src="{{ $user->profileImages[0]->path }}" alt="" class="img-circle">
							@endif
						</div>
						<!-- User Name -->
						<h5 class="text-center">{{ $user->first_name }}</h5>
						<p>{{ $user->create_at }}</p>
						<a href="user-profile.html" class="btn btn-main-sm">Edit Profile</a>
					</div>
					<!-- Dashboard Links -->
					<div class="widget user-dashboard-menu">
						<ul class="list-group">
							
							<li class="list-group-item" >
								<a href="{{ route('dashboarduser.index') }}"><i class="fa fa-file-archive-o"></i>My Profile </a>
							</li>
							<li class="list-group-item" >
								<a href=""><i class="fa fa-bolt"></i> Payments</a>
							</li>
							<li class="list-group-item" >
								<a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
								Logout</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
							</li>
							<li class="list-group-item" >
								<a href=""><i class="fa fa-power-off"></i>Delete Account</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Recently Favorited -->
				@hasrole('Driver|Admin')
				<div class="widget dashboard-container my-adslist">
					<h3 class="widget-header pull-left">My Rides</h3>
					<div class="pull-right">
			            <a class="btn btn-success" href="{{ route('rides.create') }}"> Create New Ride</a>
			        </div>

					<table class="table table-responsive product-dashboard-table">
						<thead>
							<tr>
								<th>Trips : <span> {{ $ridesCount }}</span></th>

								<th style="float:right;">Menu</th>
							</tr>
						</thead>
						<tbody>
							@foreach($rides as $ride)
							<tr>
								
								<td class="product-thumb">
									<img width="80px" height="auto" src="https://i.stack.imgur.com/ddX9U.png" alt="image description" class="img-circle"></td>
								<td class="product-details">
									<h3 class="title">Date:{{ $ride->dateFrom }}</h3>
									<span class="add-id"><strong>From:</strong> {{ $ride->location_from }} </span>
									<span class="add-id"><strong>To:</strong>{{ $ride->location_to }} </span> <br/>
									<span class="add-in"><strong>Hour: </strong> {{ $ride->startHour }}
									</span> <br/>
									<span class="add-in"><strong>Description: </strong> {{ $ride->description }}
									</span> <br/>
									<span><strong>Posted on: </strong><time>{{ $ride->created_at }}</time> </span> <br/>
								</td>
								<td class="action" data-title="Action">
									<span class="categories">Price</span><br/>
									
									<h2> ${{ $ride->price }}</h2>
									<span> Avilable Seats: </span>
									
									<p> {{ $ride->seats_available }} </p>
 								</td>
 								<td>
 									<a class="btn btn-warning" href="{{ route('rides.edit',$ride->id) }}">Edit</a>
 									{!! Form::open(['method' => 'DELETE','route' => ['rides.destroy', $ride->id],'style'=>'display:inline']) !!}
							            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
							        {!! Form::close() !!}
 								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					
				</div>
				@endhasrole
				@hasrole('User')

					<div class="widget dashboard-container my-adslist">
						<h3> You are currently not a driver but in just some easy steps you can become a driver ! 
						</h3>
						<p> Just Fill out some information about your car and insurance, we will review it and give it the good to go and you will start driving in no time ! </p>
					</div>

				@endhasrole
			</div>
		</div>
		<!-- Row End -->
	</div>
	<!-- Container End -->
</section>
</div> </div>

@endsection

