@extends('dashboardadmin.layout.dashboard')

<!-- List of Makes -->
@section('content')

<div class="wrapper">
	<h2> Create new Preference </h2>

	{!! Form::open(array('route' => 'preference.store','method'=>'POST')) !!}
	<div class="form-group">
		<label>Title:</label>
		<input type="name" class="form-control" name="title">
	</div>
	<div class="form-group">
		<label>Description</label>
		<textarea name="description" class="form-control"></textarea>
	</div>
	<div class="form-group">
		<label>Category</label>
		<select name="category" class="form-control">
			<option value="Smoking">Smoking</option>
			<option value="Pets"> Pets </option>
			<option value="Music"> Music </option>
			<option value="Chattiness"> Chattiness </option>
		</select>
	</div>
	<div class="form-group">
		<label>Action</label>
		<select name="action" class="form-control">
			<option value="Yes">Yes</option>
			<option value="No"> No </option>
		</select>
	</div>
	<div class="form-group">
		<label>Status</label>
		<select name="status" class="form-control">
			<option value="Active">Active</option>
			<option value="Inactive"> Inactive </option>
		</select>
	</div>
	<button type="submit" class="btn btn-primary">Create</button>
	{!! Form::close() !!}
</div>

@endsection