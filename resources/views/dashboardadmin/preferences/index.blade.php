@extends('dashboardadmin.layout.dashboard')

<!-- List of Makes -->
@section('content')

<table class="table table-striped">
<a href="{{ route('preference.create') }}" class="btn btn-success"> Add Preference </a>
<a href="{{ route('assignPreferneces') }}" class="btn btn-success float-left"> Update Users </a>
  <thead> 
    <tr>
      <th scope="col">#ID</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Category</th>
      <th scope="col">Action</th>
      <th scope="col">Status</th>
      <th scope="col"> Menu</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($preferences as $preference)
  		<tr>
  			<td>{{ $preference->id }}</td>
  			<td>{{ $preference->title }}</td>
  			<td>{{ $preference->description }}</td>
  			<td>{{ $preference->category }}</td>
  			<td>{{ $preference->action }}</td>
  			<td>{{ $preference->status }}</td>
  			<td>
  				<a class="btn btn-warning" 
  				href="{{ route('preference.edit', $preference) }}">Edit</a>
				{!! Form::open(['method' => 'DELETE','route' => ['preference.destroy', $preference],'style'=>'display:inline']) !!}
        		{!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onsubmit' => 'return ConfirmDelete()']) !!}
    			{!! Form::close() !!}
  			</td>
  		</tr>
  	@endforeach
  </tbody>
</table>
{{ $preferences->links() }}
@endsection