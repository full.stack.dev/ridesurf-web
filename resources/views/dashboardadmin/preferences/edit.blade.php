@extends('dashboardadmin.layout.dashboard')

<!-- List of Makes -->
@section('content')

<div class="wrapper">
	<h2> Edit new {{ $preference->title }} </h2>

	{!! Form::model($preference, ['method' => 'PUT',
		'route' => ['preference.update', $preference] ]) !!}
	<div class="form-group">
		<label>Title:</label>
		<input type="name" class="form-control" name="title" value="{{ $preference->title }}">
	</div>
	<div class="form-group">
		<label>Description</label>
		<textarea name="description" class="form-control">
			{{ $preference->description }}
		</textarea>
	</div>
	<div class="form-group">
		<label>Category</label>
		<select name="category" class="form-control">
			<option selected value="{{ $preference->category }}" hidden disabled>{{ $preference->category }}</option>
			<option value="Smoking">Smoking</option>
			<option value="Pets"> Pets </option>
			<option value="Music"> Music </option>
			<option value="Chattiness"> Chattiness </option>
		</select>
	</div>
	<div class="form-group">
		<label>Action</label>
		<select name="action" class="form-control">
			<option value="{{ $preference->action }}" 
				hidden selected disabled>{{ $preference->action }}
			</option>
			<option value="Yes">Yes</option>
			<option value="No"> No </option>
		</select>
	</div>
	<div class="form-group">
		<label>Status</label>
		<select name="status" class="form-control">
			<option value="{{ $preference->status }}" selected hidden>{{ $preference->status }}</option>
			<option value="Active">Active</option>
			<option value="Inactive"> Inactive </option>
		</select>
	</div>
	<button type="submit" class="btn btn-warning">edit</button>
	{!! Form::close() !!}
</div>

@endsection