@extends('dashboardadmin.layout.dashboard')
@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>

<div class="container-fluid mt--7">

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Trip Purpose Management') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{route('purpose.create')}}" class="btn btn-sm btn-primary">Create New Type</a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Type</th>
                                <th>Description</th>
                                <th width="280px">Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach ($types as $key => $type)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $type->type }}</td>
                                    <td>{{ $type->description }}</td>
                                    <td>
                                        <!-- @ can('role-edit') -->
                                        <a class="btn btn-primary" href="{{ route('purpose.edit',$type->id) }}">Edit</a>
                                        <!-- @ endcan -->
                                        <!-- @ can('role-delete') -->
                                    {!! Form::open(['method' => 'DELETE','route' => ['purpose.delete', $type->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                    <!-- @ endcan -->
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                     {!! $types->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection