@extends('dashboardadmin.layout.dashboard')

@section('content')

	<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
		<div class="container-fluid">
			<div class="header-body">
			</div>
		</div>
	</div>

	<div class="container-fluid mt--7">
		<div class="row">
			<div class="col">

				@if ($message = Session::get('message'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif

				<div class="card shadow">
					<div class="card-header border-0">
						<div class="row align-items-center">
							<div class="col-8">
								<h3 class="mb-0">{{ __('Airports') }}</h3>
							</div>
							<div class="col-4 text-right">
								<a href="{{ route('airport.createNewAirport') }}" class="btn btn-sm btn-primary">{{ __('Add new') }}</a>
							</div>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table align-items-center table-flush">
							<thead class="thead-light">
							<tr>
								<th scope="col">Name</th>
								<th scope="col">Address</th>
								<th scope="col">City</th>
								<th scope="col">State</th>
								<th scope="col">Zip</th>
								<th scope="col">Action</th>
							</tr>
							</thead>
							<tbody>
							@foreach($airports as $air)
								<tr>
								<td> {{ $air->name }} </td>
								<td> {{ $air->address }} </td>
								<td> {{ $air->city }} </td>
								<td> {{ $air->state }} </td>
								<td> {{ $air->zip }} </td>
								<td>
									<a href="{{route('airport.editSingle', $air->id)}}" class="btn btn-warning">Edit</a> </td>
								</tr>
							@endforeach
						<tbody>

						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
@endsection