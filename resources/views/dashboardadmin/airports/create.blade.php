@extends('dashboardadmin.layout.dashboard')
@section('content')

	<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
		<div class="container-fluid">
			<div class="header-body">
			</div>
		</div>
	</div>

	<div class="container-fluid mt--7">
		<div class="card bg-secondary shadow">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<h3 class="col-12 mb-0">{{ __('Create a new aiport') }}</h3>
					</div>
				</div>
				<div class="card-body">

						{!! Form::open(array('route' => 'airport.storeNewAirport','method'=>'POST')) !!}
						<div class="form-group">
							<label>Name:</label>
							<input type="text" class="form-control" name="name">
						</div>
						<div class="form-group">
							<label>Address</label>
							<input type="text" class="form-control" name="address">
						</div>
						<div class="form-group">
							<label>State</label>
							<select name="state" class="form-control">
								@foreach($state as $st)
									<option value="{{ $st->code }}">{{ $st->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>City</label>
							<input type="text" class="form-control" name="city">
						</div>
						<div class="form-group">
							<label>Zip Code</label>
							<input type="number" class="form-control" name="zip">
						</div>
						<button type="submit" class="btn btn-primary">Create</button>
						{!! Form::close() !!}

				</div>
			</div>
		</div>
	</div>
@endsection