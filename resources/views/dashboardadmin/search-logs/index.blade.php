@extends('dashboardadmin.layout.dashboard')

@section('content')

<div class="row">
	<div class="col-md-6 col-sm-12">
		<table class="table-responsive table-bordered" >
			<h2> Search Cities from Start </h2>
		  <div style="overflow-x:auto;">
		      <thead>
		         <tr>
		           <th>No</th>
		           <th>City Search</th>
		           <th>Numbers of time Searched</th>
		         </tr>
		        </thead>
		      <tbody>
		       @foreach ($city_from as $key => $city)
		        <tr>
		          <td>{{ ++$i }}</td>
		          <td> {{  $city->city_from }}</td>
		          <td> {{ $city->count  }}</td>
		        </tr>
		       @endforeach
		       </tbody>
		     </div>
		  </table>
		</div>

	<div class="col-md-6 col-sm-12">
		<table class="table-responsive table-bordered" >
			<h2> Search Cities from End </h2>
		  <div style="overflow-x:auto;">
		      <thead>
		         <tr>
		           <th>No</th>
		           <th>City Search</th>
		           <th>Numbers of time Searched</th>
		         </tr>
		        </thead>
		      <tbody>
		       @foreach ($city_to as $key => $city)
		        <tr>
		          <td>{{ ++$j }}</td>
		          <td> {{  $city->city_to }}</td>
		          <td> {{ $city->count  }}</td>
		        </tr>
		       @endforeach
		       </tbody>
		     </div>
		  </table>
	</div>
</div>

<div class="container-fluid margin-top">
	<table class="table-responsive table-bordered" >
	  <div style="overflow-x:auto;">
	      <thead>
	         <tr>
	           <th>No</th>
	           <th>Date Asked</th>
	           <th>From</th>
	           <th>To</th>
	           <th> Date Searched
	         </tr>
	        </thead>
	      <tbody>
	       @foreach ($logs as $key => $log)
	        <tr>
	          <td>{{ $key }}</td>
	          <td>{{ date('m/d/Y', strtotime($log->date_asked)) }}</td>
	          <td> {{$log->city_from}} </td>
	          <td>{{ $log->city_to }}</td>
	          <td> {{ date('m/d/Y', strtotime($log->created_at)) }}</td>
	        </tr>
	       @endforeach
	       </tbody>
	     </div>
	     {{$logs->links()}}
	  </table>

</div>


<div class="container-fluid margin-top">
	<h3> Most Frequent search </h3> 
	<table class="table-responsive table-bordered" >
	  <div style="overflow-x:auto;">
	      <thead>
	         <tr>
	           <th>Count</th>
	           <th>From</th>
	           <th>To</th>

	         </tr>
	        </thead>
	      <tbody>
	       @foreach ($frequency as $key => $log)
	        <tr>
	          <td>{{ $log->count }}</td>
	          <td> {{$log->city_from}} </td>
	          <td>{{ $log->city_to }}</td>
	        </tr>
	       @endforeach
	       </tbody>
	     </div>
	     {{$logs->links()}}
	  </table>
</div>

@endsection