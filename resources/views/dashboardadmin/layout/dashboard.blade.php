<!DOCTYPE html lang="{{ app()->getLocale() }}">
<html lang="en">
<head>
    <title>Ridesurf Dashboard Admin</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Icons -->
    <link href="{{ asset('nucleo/css/nucleo.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/all.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/style_dashboard.css') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
<!-- New Sidebar, need to put this in it's own layout file! -->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="">
            <img src="{{ asset('images/ridesurf.png')}}" class="navbar-brand-img" alt="Ridesurf Admin">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            @if(Auth::user()->avatar == null)
                                <img src="/images/avatar.jpg">
                            @else
                                <img id="avatar_small" src="{{ Auth::user()->avatar }}">
                            @endif
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class="dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Settings') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>{{ __('Activity') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>{{ __('Support') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="">
                            <img src="/img/brand/blue.png">hello
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('dashboardadmin.index') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Dashboard') }}
                    </a>
                </li>

                <li class="nav-item">
                    <?php $request = DB::table('drivers')->where('status', '=', 1)->count(); ?>

                    <a class="nav-link active" href="#navbar-users" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-users">
                        <i class="ni ni-circle-08" style="color: #08d5bf;"></i>
                        <span class="nav-link-text" style="color: #656565;"> Users ({{ $request }})</span>
                    </a>

                    <div class="collapse" id="navbar-users">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('users.index') }}">
                                    User List
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('dashboardadmin.requestDriversList') }}">
                                    Request Driver List ({{ $request }})
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('roles.index') }}">
                                    Roles
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                        <i class="fas fa-car" style="color: #08d5bf;"></i>
                        <span class="nav-link-text" style="color: #656565;">Rides</span>
                    </a>

                    <div class="collapse" id="navbar-examples">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('airport.list') }}">
                                    Airports
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('dashboardadmin.listRides') }}">
                                    Rides List
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('driver.list') }}">
                                    Drivers
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('dashboardadmin.payToDrivers') }}">
                                    Pay to Driver
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <?php $contacts = DB::table('contacts')->where('replied', '=', 0)->count(); ?>
                    <a class="nav-link active" href="#navbar-utility" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-utility">
                        <i class="fas fa-wrench" style="color: #08d5bf;"></i>
                        <span class="nav-link-text" style="color: #656565;"> Utility ({{$contacts}})</span>
                    </a>

                    <div class="collapse" id="navbar-utility">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('dashboardadmin.contactList')}}">
                                    Contact ({{$contacts}})
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('search.index') }}">
                                    Search Logs
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('purpose.index')}}">
                                    Trip Purposes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('model.index') }}">
                                    Car Model
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('dashboardadmin.listMake') }}">
                                    Car Make
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <?php $contacts = DB::table('contacts')->where('replied', '=', 0)->count(); ?>
                    <a class="nav-link active" href="#navbar-settings" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-settings">
                        <i class="ni ni-settings-gear-65" style="color: #08d5bf;"></i>
                        <span class="nav-link-text" style="color: #656565;"> Settings</span>
                    </a>

                    <div class="collapse" id="navbar-settings">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('preference.index') }}">
                                    Preferences
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('dashboardadmin.language-label') }}">
                                    Language Label
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('request.showAll') }}">
                                    Ride Requested
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('ip.index')}}">
                                    IP Request
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('stat.weekly')}}">
                                    Weekly Report
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <?php $reports = DB::table('reports')->where('replied', '=', null)->count(); ?>
                    <a class="nav-link" href="{{ route('reports.index') }}">
                        <i class="fas fa-flag" style="color: #08d5bf;"></i>
                        Report ({{ $reports }})
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/adminlogsreviewing') }}">
                        <i class="fas fa-clipboard-list" style="color: #08d5bf;"></i>
                        {{ __('Logs') }}
                    </a>
                </li>

            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Documentation</h6>
            <!-- Navigation -->
            <ul class="navbar-nav mb-md-3">
                <li class="nav-item">
                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">
                        <i class="ni ni-spaceship"></i> Getting started
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html">
                        <i class="ni ni-ui-04"></i> Components
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="main-content">
    <!-- Argon Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="">{{ __('Dashboard') }}</a>
            <!-- Form
            <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
                <div class="form-group mb-0">
                    <div class="input-group input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input class="form-control" placeholder="Search" type="text">
                    </div>
                </div>
            </form>-->
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="/img/theme/team-4-800x800.jpg">
                        </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm font-weight-bold">Admin Name</span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                        </div>
                        <a href="" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>{{ __('My profile') }}</span>
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ni ni-settings-gear-65"></i>
                            <span>{{ __('Settings') }}</span>
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ni ni-calendar-grid-58"></i>
                            <span>{{ __('Activity') }}</span>
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ni ni-support-16"></i>
                            <span>{{ __('Support') }}</span>
                        </a>
                        <div class="dropdown-divider"></div>

                        <a href="{{ route('logout') }}"  class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="ni ni-user-run"></i> {{ __('Logout') }}
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> @csrf </form>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

        @if(session()->has('message'))
            <div class="alerts" style="margin-bottom: 25px; text-align: center;">
                <div class="alert alert-success" >
                    {{ session()->get('message') }}
                </div>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alerts" style="margin-bottom: 25px; text-align: center;">
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            </div>
        @endif
    <div style="top: 50px;">    
        @yield('content')
    </div>     
    <!-- Footer, make separate file -->
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-12">
            <div class="text-right text-muted">
                <p style="font-size: 10px !important; font-weight: 500 !important; padding: 10px 80px 40px 0;"> &copy; {{ now()->year }} Travlr Co. dba Ridesurf</p>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .margin-tb {
        margin-top: 100px;
    }
</style>

<!-- Keep this here! Not part of footer -->

<!-- DO WE STILL NEED THIS? Moment.js library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

<!-- DO WE STILL NEED THIS? moment-duration-format plugin -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-duration-format/1.3.0/moment-duration-format.min.js"></script>

<!-- New Argon Theme Scripts -->
<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('vendor/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('vendor/chart.js/dist/Chart.extension.js') }}"></script>

<!-- Argon JS -->
<script src="{{ asset('js/argon.js') }}"></script>

</body>
</html>