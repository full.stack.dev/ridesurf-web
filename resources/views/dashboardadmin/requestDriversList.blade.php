@extends('dashboardadmin.layout.dashboard')

@section('content')

    <!-- Header Cards ... Make this own blade file -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Driver Requests') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <p>{{ $driverCount }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Request</th>
                                <th scope="col">Roles</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                             @foreach ($drivers as $key => $driver)
                              <tr>
                                <td>{{ $driver->user->id}}</td>
                                <td>{{ $driver->user->first_name }}</td>
                                <td>{{ $driver->user->email }}</td>
                                <td> {{ $driver->updated_at }}</td>
                                <td>
                                  @if(!empty($driver->user->getRoleNames()))
                                    @foreach($driver->user->getRoleNames() as $v)
                                       <label class="badge badge-success">{{ $v }}</label>
                                    @endforeach
                                  @endif
                                </td>
                                <td>
                                   <a class="btn btn-primary" href="{{ route('users.edit',$driver->user->id) }}">Edit</a>

                                    {!! Form::open(['method' => 'DELETE','route' => ['users.destroyFromAdmin', $driver->user->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                              </tr>
                             @endforeach
                        </table>

                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection