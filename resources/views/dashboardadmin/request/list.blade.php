@extends('dashboardadmin.layout.dashboard')

<!-- List of Rides -->
@section('content')
<nav aria-label="breadcrumb">
  	<ol class="breadcrumb">
	    <li class="breadcrumb-item active" aria-current="page">
	    	<div class="row">
	    		List IP Request
	    	</div>
	    </li>
	</ol>
</nav>

<div class="wrapper">
	<table class="table table-striped">
	  <thead>
	    <tr>
	      <th scope="col">#id</th>
	      <th scope="col">IP</th>
	      <th scope="col">From Where</th>
	      <th scope="col">Time</th>

	    </tr>
	  </thead>
	  
	  <tbody>
	  	
	  	@foreach($allRequest as $request)
	  	 
	    <tr>
	      <th scope="row">{{ $request->id }}</th>
	      <td>{{ $request->ip }}</td>
	      <td>{{ $request->url_visted }}</td>
	      <td>{{ $request->created_at }}</td>
	    </tr>
	     
	    @endforeach
	    {{ $allRequest->links() }}

	  </tbody>
	 
	</table>
</div>


@endsection