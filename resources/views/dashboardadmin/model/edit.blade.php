@extends('dashboardadmin.layout.dashboard')
@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
		<div class="container-fluid">
			<div class="header-body">
			</div>
		</div>
	</div>

	<div class="container-fluid mt--7">

		@if ($message = Session::get('success'))
			<div class="alert alert-success">
				<p>{{ $message }}</p>
			</div>
		@endif

		<div class="card bg-secondary shadow">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<h3 class="col-12 mb-0">Edit {{ $model->vTitle }}</h3>
					</div>
				</div>
				<div class="card-body">

					{!! Form::model($model, ['method' => 'PUT',
					'route' => ['model.update', $model->iModelId] ]) !!}
					<div class="form-group">
						<input class="form-control" value="{{ $model->vTitle }}" type="text" name="vTitle">

					</div>

					<div class="form-group">
						<label>Make</label>
						<select name="iMakeId" class="form-control">

							@foreach($makes as $make)
								@if($make->iMakeId == $model->iMakeId)
									<option value="{{ $make->iMakeId }}" selected> {{ $make->vMake }} </option>
								@endif
								<option value="{{ $make->iMakeId }}"> {{ $make->vMake }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<select class="form-control" name="eStatus">
							<option value="{{ $model->eStatus }}" selected hidden>{{ $model->eStatus }} </option>
							<option value="Active"> Active </option>
							<option value='Inactive'> Inactive </option>
						</select>
					</div>
					<button type="submit" class="btn btn-primary">Edit</button>
					<a href="{{ route('model.index') }}" class="btn btn-info" style="margin-left:50px;">Go Back</a>
					{!! Form::close() !!}

				</div>
			</div>
		</div>
	</div>
@endsection