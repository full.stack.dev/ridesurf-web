@extends('dashboardadmin.layout.dashboard')
@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div style="padding: 25px 45px;">
                        <div class="row">
                          {!! Form::open(array('route' => 'model.filterModel','method'=>'GET')) !!}
                            Search: <input type="text" name="vMake" placeholder="name"> 
                            <button class="btn btn-success">Search</button>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Car Model') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('model.create') }}" class="btn btn-sm btn-primary">{{ __('Add new') }}</a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                          <tr>
                            <th scope="col">#ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Status</th>
                            <th scope="col">Model</th>
                            <th scope="col"> Menu</th>
                          </tr>
                        </thead>
                        
                        <tbody>
                        	@foreach($models as $model)
                        	 
                          <tr>
                            <th scope="row">{{ $model->iModelId }}</th>
                            <td>{{ $model->vTitle }}</td>
                            <td> {{ $model->eStatus }}</td>
                            <td>{{ $model->vMake }}</td>
                            <td> 
                            	<a class="btn btn-warning" href="{{ route('model.edit',$model->iModelId) }}">Edit</a>
                      		{!! Form::open(['method' => 'DELETE','route' => ['model.destroy', $model->iModelId],'style'=>'display:inline']) !!}
                              	{!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onsubmit' => 'return ConfirmDelete()']) !!}
                          	{!! Form::close() !!}
                            </td>
                          </tr>
                           
                          @endforeach
                          
                        </tbody>
                    </table>

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            @include('pagination.admin', ['paginator' => $models])
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection