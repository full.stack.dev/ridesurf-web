@extends('dashboardadmin.layout.dashboard')

<!-- List of Makes -->
@section('content')
	<!-- Header Cards ... Make this own blade file -->
	<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
		<div class="container-fluid">
			<div class="header-body">
			</div>
		</div>
	</div>

	<div class="container-fluid mt--7">
		<div class="card bg-secondary shadow">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<h3 class="col-12 mb-0">Change {{ $make->vMake }}</h3>
					</div>
				</div>
				<div class="card-body">

					{!! Form::model($make, ['method' => 'PUT',
					'route' => ['dashboardadmin.updateMake', $make->iMakeId] ]) !!}
					<div class="form-group">
						<input class="from-control" value="{{ $make->vMake }}" type="text" name="vMake">

					</div>
					<div class="form-group">
						<select class="from-control" name="eStatus">
							<option value="{{ $make->eStatus }}" selected hidden>{{ $make->eStatus }} </option>
							<option value="Active"> Active </option>
							<option value='Inactive'> Inactive </option>
						</select>
					</div>
					<button type="submit" class="btn btn-primary">Edit</button>
					<a href="{{ route('dashboardadmin.listMake') }}" class="btn btn-info" style="margin-left:50px;">Go Back</a>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection