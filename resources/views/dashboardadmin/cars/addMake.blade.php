@extends('dashboardadmin.layout.dashboard')

<!-- List of Makes -->
@section('content')

	<!-- Header Cards ... Make this own blade file -->
	<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
		<div class="container-fluid">
			<div class="header-body">
			</div>
		</div>
	</div>

	<div class="container-fluid mt--7">
		<div class="card bg-secondary shadow">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<h3 class="col-12 mb-0">{{ __('Add Car Make') }}</h3>
					</div>
				</div>
				<div class="card-body">


					{!! Form::open(array('route' => 'dashboardadmin.createMake','method'=>'POST')) !!}
					<div class="form-group">
						<label>Name:</label>
						<input type="name" class="form-control" name="vMake">
					</div>
					<div class="form-group">
						<label>Status</label>
						<select name="eStatus" class="form-control">
							<option value="Active">Active</option>
							<option value="Inactive"> Inactive </option>
						</select>
					</div>
					<button type="submit" class="btn btn-primary">Create</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection