@extends('dashboardadmin.layout.dashboard')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Report ID {{ $report->id }}</h2>
        </div>
    </div>
</div>


@if ($message = Session::get('message'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif
  
<div class="wrapper">
  <div class="from-group">
    <label>Reason</label>
    <input readonly value="{{ $report->reason }}" class="form-control" type="text">
  </div>
  <div class="from-group">
    <label>Description</label>
    <textarea readonly class="form-control">
      {{ $report->description }}
    </textarea> 
  </div>
</div>
{!! Form::model($report, ['method' => 'POST','route' => ['reports.updateReport', $report->id]]) !!}
    <div class="from-group">
      <label>Reply to User: </label>
      <p> User: {{ $report->user_passenger->first_name }} </p>
      <p> Driver: {{ $report->ride->user->first_name }}
      <textarea name="reply" class="form-control"></textarea>
    </div>
    <button class="btn btn-success">Submit Response</button>
{!! Form::close() !!}
</div></div>


@endsection