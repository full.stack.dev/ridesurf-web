@extends('dashboardadmin.layout.dashboard')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page">
        {!! Form::open(array('route' => 'reports.filterReport','method'=>'GET')) !!}
          <label>User Name: </label><input type="text" name="first" placeholder="First Name"> 
          <label> Ride Id : </label> <input type="text" name="rides_id" placeholder="Ride ID"> 
          <button class="btn btn-success">Search</button>
        {!! Form::close() !!}
      </li>
    </ol>
</nav>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Report Management</h2>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
 <tr>
   <th>Passenger </th>
   <th>Driver </th>
   <th>Date Issue</th>
   <th>Ride Id </th>
   <th width="280px">Action</th>
 </tr>
  @foreach ($reports as $report)
  <tr>
    <td>{{ $report->user_passenger->first_name }}</td>
    <td>{{ $report->ride[0] }}</td>
    <td>{{ $report->created_at }}</td>
    <td></td>

    <td>
       <a class="btn btn-primary" href="{{ route('reports.reportReply',$report->id) }}">Edit</a>

    </td>
  </tr>
 @endforeach
</table>

@endsection