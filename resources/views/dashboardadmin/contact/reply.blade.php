@extends('dashboardadmin.layout.dashboard')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page">
        <p> Reply to {{$contact->name }} </p>
      </li>
    </ol>
</nav>

<div class="form-group">
	<label>Name : </label>
	<p>{{$contact->name}}</p>
	<label>Email : </label>
	<p>{{$contact->email}}</p>
	<label>Subject : </label>
	<p>{{$contact->subject}}</p>
	<label>Message : </label>
	<p>{{$contact->description}}</p>
</div>

{!! Form::open(array('route' => ['dashboardadmin.replyMessage', $contact->id],'method'=>'POST'))!!}

<div class="form-group offer-ride">
	<label> Reply Message </label>
	<textarea class="form-control" name="message"></textarea>
</div>

<button class="btn btn-success">Reply</button>
{!! Form::close() !!}


@endsection