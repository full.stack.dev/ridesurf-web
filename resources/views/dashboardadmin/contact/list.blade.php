@extends('dashboardadmin.layout.dashboard')
@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>

        @if ($message = Session::get('message'))
        <div class="alert alert-success">
          <p>{{ $message }}</p>
        </div>
        @endif

<div class="container-fluid mt--7">
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">{{ __('Contact Us Form Entries') }}</h3>
            </div>
            <div class="col-4 text-right"></div>
          </div>
        </div>

        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
             <tr>
               <th scope="col">Date</th>
               <th scope="col">Name</th>
               <th scope="col">Email</th>
               <th scope="col">Subject</th>
               <th scope="col">Replied</th>
               <th scope="col">Action</th>
             </tr>
            </thead>
            <tbody>
              @foreach($contacts as $contact)
              <tr>
                <td> {{$contact->created_at}}</td>
                <td>{{$contact->name}}</td>
                <td> {{$contact->email}}</td>
                <td> {{$contact->subject}}</td>
                <td> @if($contact->replied) Yes @else No @endif</td>
                <td> 
                  <a href="{{route('dashboardadmin.viewContactMessage', $contact->id)}}" class="btn btn-success">Reply</a>
                  {!! Form::open(['method' => 'DELETE', 'route' => ['contact.destroy', $contact->id],'style'=>'display:inline']) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onClick' => 'return ConfirmDelete()']) !!}
                  {!! Form::close() !!}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>

        <div class="card-footer py-4">
            <nav class="d-flex justify-content-end" aria-label="...">
                @include('pagination.admin', ['paginator' => $contacts])
            </nav>
        </div>

      </div>
    </div>
  </div>
</div>

@endsection