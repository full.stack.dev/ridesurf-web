@extends('dashboardadmin.layout.dashboard')

@section('content')
	<div class="form-group col-md-6">
		{!! Form::model($datas, ['method' => 'GET', 'id' => 'searchData',
            'route' => ['dashboardadmin.seachLabel'] ] ) !!}
		<label for="exampleInputEmail1">Search for Label</label>
		<input class="form-control" type="text" name="searchLabel">
		<button class="btn btn-info bg-info">Search</button>
		{!! Form::close() !!}
	</div>
	<div class="col-md-12 offset-xs-12">
		<a class="btn btn-primary" href="{{ route('dashboardadmin.addNewLabel') }}">Add New</a>
	</div>

	<div class="row">
		<div class="col-md-12 col-xs-12">
			<h3> Labels </h3>
			<table class="table table-bordered">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Name</th>
			      <th scope="col">Value</th>
			      <th scope="col">Lang</th>
			      <th scope="col">Menu</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@foreach($datas as $data)
			    <tr>
			      <th scope="row">{{ $data->vLabel }}</th>
			      <td>{{ $data->vValue }}</td>
			      <td>{{ $data->vCode }}</td>
			      <td width="140"><a class="btn btn-warning" href="{{ route('dashboardadmin.editLanguageLabel',$data->LanguageLabelId) }}">Edit</a>
			      	{!! Form::open(['method' => 'DELETE','route' => ['dashboardadmin.deleteLanguageLabel', 
			      		$data->LanguageLabelId],'style'=>'display:inline']) !!}
                    	<button class="btn btn-danger bg-danger" type="submit" onclick="return confirm('Are you sure you want to delete it?');">Delete</button>
                	{!! Form::close() !!}
			      </td>
			    </tr>
			    @endforeach
				</tbody>
				{{ $datas->links() }}
			</table>
		</div>
	</div>

@endsection