@extends('dashboardadmin.layout.dashboard')

@section('content')

{!! Form::open(array('route' => 'dashboardadmin.createNewLabel','method'=>'POST')) !!}

	<div class="col-md-6">
		<div class="form-group">
			<label> Label </label>
			<input class="form-control" type="text"  name="vLabel">
		</div>
		<div class="form-group">
			<label> Value </label>
			<textarea class="form-control" name="vValue"></textarea>
		</div>
		<div class="form-group">
			<label> Language </label>
			<select class="form-control" name="vCode">
				<option value="EN"> English </option>
				<option value="ES"> Spanish</option>
			</select>
		</div>
		<div class="text-center">
        <button type="submit" class="btn btn-primary">Create</button>
        <a href="{{ route('dashboardadmin.language-label') }}" class="btn btn-info" style="margin-left:50px;">Go Back</a>
    </div>
	</div>
	
{!! Form::close() !!}
@endsection