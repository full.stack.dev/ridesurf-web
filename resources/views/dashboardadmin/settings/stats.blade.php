@extends('dashboardadmin.layout.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="section-title">
            <h3> Statistics for this week : from {{date('m/d/Y', strtotime($startWeek)) }} to {{date('m/d/Y', strtotime($endWeek)) }} </h3>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <h3> User Statistics</h3>
                    <p> Users Registered this week : {{$usersThisWeek}}</p>
                    <p>Drivers Applied this week: {{$drivers}}</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <h3> Rides Statistics</h3>
                    <p> Rides Created this week: {{$ridesThisWeek}} </p>
                    <p> Rides Booked this week: {{$ridesBooked}}</p>

                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 25px">
            <div class="col-md-6">
                Top 10 Searches of the week
                <table class="table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>From</th>
                            <th>To</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($popularThisWeek as $key => $log)
                            <tr>
                                <td>{{ $log->count }}</td>
                                <td> {{$log->city_from}} </td>
                                <td>{{ $log->city_to }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-title">
                        <h3> Searches Statistic </h3>
                    </div>
                    <div class="card-body">
                        <p> Searches this week: {{$searches}} </p>
                    </div>
                </div>


            </div>
        </div>

    </div>
@endsection
