@extends('dashboardadmin.layout.dashboard')
@section('content')


<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
        </div>
    </div>
</div>

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">

            @if ($message = Session::get('message'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Role Management') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('roles.create') }}" class="btn btn-sm btn-primary">{{ __('Add new') }}</a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                          <tr>
                             <th>No</th>
                             <th>Name</th>
                             <th width="280px">Action</th>
                          </tr>
                        </thead>
                            @foreach ($roles as $key => $role)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $role->name }}</td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                                    <!-- @ can('role-edit') -->
                                    <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                                    <!-- @ endcan -->
                                    <!-- @ can('role-delete') -->
                                        {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    <!-- @ endcan -->
                                </td>
                            </tr>
                            @endforeach
                    </table>
                </div>
            </div>
        </div>
        {!! $roles->render() !!}
    </div>
</div>

@endsection