@extends('dashboardadmin.layout.dashboard')
@section('content')

@include('dashboardadmin.users.partials.header', [
    'title' => __('Hello,') . ' '. $user->first_name . ('!'),
    'description' => __('This is your profile page. You can see the rides you\'ve created or taken and manage your profile and ride preferences.'),
    'class' => 'col-lg-7'
])   

<div class="container-fluid mt--7">
  <div class="row">
    <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
        <div class="card card-profile shadow">
            <div class="row justify-content-center">
                <div class="col-lg-3 order-lg-2">
                    <div class="card-profile-image">
                        <a href="#">
                            <img src="{{ $user->avatar }}" class="rounded-circle">
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <div class="d-flex justify-content-between">
                    <!--<a href="#" class="btn btn-sm btn-info mr-4">{{ __('Connect') }}</a>-->
                    <a href="#" class="btn btn-sm btn-default float-right">{{ __('Message') }}</a>
                </div>
            </div>
            <div class="card-body pt-0 pt-md-4">
                <div class="row">
                    <div class="col">
                        <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                            <div>
                                <span class="heading">{{ count($user->UserRide) }}</span>
                                <span class="description">{{ __('Rides') }}</span>
                            </div>
                            <div>
                                <span class="heading">{{ $user->booked }}</span>
                                <span class="description">{{ __('Drives') }}</span>
                            </div>
                            <div>
                                <span class="heading">{{ $user->messageCount }}</span>
                                <span class="description">{{ __('Messages') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <h3>
                        {{ $user->first_name }} {{ $user->last_name }}<span class="font-weight-light">, AGE</span>
                    </h3>
                    <div class="h5 font-weight-300">
                        <i class="ni location_pin mr-2"></i>{{ __('City, State') }}
                    </div>
                    <div class="h5 mt-4">
                        @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $v)
                           <label class="badge badge-success">{{ $v }}</label>
                        @endforeach
                        @if(count($user->driver) > 0)
                          <p><strong>Status:</strong> {{ $user->driver[0]->status }}</p>
                        @endif
                      @endif
                      <div class="stars">
                          {{ $user->rating }}/5
                          <span data-rating="1" class="fa fa-star checked"></span>
                          <span data-rating="2" class="fa fa-star checked"></span>
                          <span data-rating="3" class="fa fa-star checked"></span>
                          <span data-rating="4" class="fa fa-star "></span>
                          <span data-rating="5" class="fa fa-star"></span>
                      </div>
                    </div>
                    <div>
                        <p><strong>Last Login:</strong> {{ date('j M Y', strtotime($user->last_login)) }} </p> 
                        <p><strong>Joined:</strong> {{ date('j M Y', strtotime($user->created_at)) }} </p>
                        <p><strong>Created From:</strong> {{$user->created_from}}</p>
                    </div>
                    <hr class="my-4" />
                    <a href="{{ route('rides.viewUserProfile', $user->id) }}">{{ __('View Public Profile') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-8 order-xl-1">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
            <div class="row align-items-center">
                <h3 class="col-12 mb-0">{{ __('User Control') }}</h3>
            </div>
        </div>
        <div class="card-body">

          <div class="btn-group" role="tablist">
              <div role="presentation" class="btn btn-secondary"><a href="#user_info" aria-controls="user_info" 
                  role="tab" data-toggle="tab">User Info</a></div>
              <div role="presentation" class="btn btn-secondary"><a href="#driver" aria-controls="driver" 
                  role="tab" data-toggle="tab">Driver</a></div>
              <div role="presentation" class="btn btn-secondary"><a href="#passenger" aria-controls="passenger" 
                  role="tab" data-toggle="tab">Passenger</a></div>
              <div role="presentation" class="btn btn-secondary"><a href="#reports" aria-controls="reports" 
                  role="tab" data-toggle="tab">Reports</a></div>
          </div>

           <!-- Tab panes -->
          <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="user_info">
                  @include('dashboardadmin.users.layout.user-info', [$user])
              </div>
              <div role="tabpanel" class="tab-pane" id="driver">
                  @if(count($user->driver) > 0)
                      @include('dashboardadmin.users.layout.user-driver', [$user, $vehicles])
                  @else
                      <h4 class="padding"> User is not a driver Yet </h4>
                  @endif
              </div>
              <div role="tabpanel" class="tab-pane" id="passenger">
                  @include('dashboardadmin.users.layout.user-passenger', [$user])
              </div>
              <div role="tabpanel" class="tab-pane" id="reports">
                  @include('dashboardadmin.users.layout.user-report', [$user])
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
  @include('dashboardadmin.layout.footers.auth')
</div>

<script>
$(document).ready(function() {
      
      $(document).on('change', '#make', function(e){

            var make = $(this).val();
            e.preventDefault();
            $('#model').prop('disabled', false);
            clearFields();
            $.ajax({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  type: "POST",

                  url:"{{ route('dashboarduser.fetchModel') }}",
                  method:"POST",
                  data:{
                    make : make,
                   _token: $('meta[name="csrf-token"]').attr('content')
                 },
                  success:function(data){
                    var select = $('#model');
                    select.append('<option selected disabled>Select A Model </option>');
                    for(var i=0; i < data.length; i++){

                      $('#model').append('<option value="'+data[i].iModelId+'">' + data[i].vTitle + '</option>');
                    }

                  },
                  error: function (request, status, error) {
                    alert(status + " " + error + " "  + request.responseText);
                },
              });
      });

      $(document).on('change', '#model', function(e){

            e.preventDefault();

            $('#type').prop('disabled', false);
            jQuery('#type').val('');
            jQuery('#type').text('');
            jQuery('#color').val('');
            jQuery('#color').text('');
            jQuery('#year').val('');
            jQuery('#year').text('');
            $.ajax({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  type: "POST",

                  url:"{{ route('dashboarduser.fetchType') }}",
                  method:"POST",
                  data:{
                   _token: $('meta[name="csrf-token"]').attr('content')
                 },
                  success:function(data){
                    var select = $('#type');
                    select.append('<option selected disabled>Select A Type </option>');
                    for(var i=0; i< data.length; i++){
                      select.append('<option value="'+data[i].iCarTypeId+'">' + data[i].vTitle_EN + '</option>');
                    }



                  },
                  error: function (request, status, error) {
                    alert(status + " " + error + " "  + request.responseText);
                },
               });

      });

      $(document).on('change', '#type', function(){

              $('#color').prop('disabled', false);
              jQuery('#color').val('');
              jQuery('#color').text('');
              jQuery('#year').val('');
              jQuery('#year').text('');
               $.ajax({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  type: "POST",

                  url:"{{ route('dashboarduser.fetchColor') }}",
                  method:"POST",
                  data:{

                   _token: $('meta[name="csrf-token"]').attr('content')
                 },
                  success:function(data){
                    var select = $('#color');
                    select.append('<option selected disabled>Select A Color </option>');
                    for(var i=0; i< data.length; i++){
                      select.append('<option value="'+data[i].iColourId+'">' + data[i].vColour_EN + '</option>');
                    }


                  },
                  error: function (request, status, error) {
                    alert(status + " " + error + " "  + request.responseText);
                },
               });

          });

    $(document).on('change', '#color', function(){
        $('#year').prop('disabled', false);
        jQuery('#year').val('');
        jQuery('#year').text('');
        var year = 1960
        $('#year').append('<option selected disabled>Select A Year </option>');
        for(var i=0; i < 60; i++){
          $('#year').append('<option value="'+(year+i) +'">' + (year+i) +' </option>');
        }
    });

    $('#updateInfo').on('click', function(e){
        $('#make').prop('disabled', false);
        $('#year').prop('disabled', false);
        $('#tag').prop('disabled', false);
        $('#registration').prop('disabled', false);
        $('#submitInfo').prop('disabled', false);
        $('#registration_expire').prop('disabled', false);
        $('#license_expire').prop('disabled', false);
        $('#insurance_expire').prop('disabled', false);
        
    });

    var stars = $('.stars');
    var star  = stars.find('.fa-star');
    var value = {{ $user->rating }}

    // Remove class for selected stars
      stars.find('.checked').removeClass(' checked');

      // Add class to the selected star and all before
      for (i = 1; i <= value; i++) {
        stars.find('[data-rating="' + i + '"]').addClass('checked');
      }

    
});

function clearFields(){
  jQuery('#model').val('');
  jQuery('#model').text('');
  jQuery('#type').val('');
  jQuery('#type').text('');
  jQuery('#color').val('');
  jQuery('#color').text('');
  jQuery('#year').val('');
  jQuery('#year').text('');
}
</script>

@endsection
