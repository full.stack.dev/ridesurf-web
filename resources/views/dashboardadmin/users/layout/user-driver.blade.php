<div>
    <div class="col-md-12 col-sm-12 vehicle-admin">

        <h2 class="pad-30">Car Informaton</h2>
        
        {!! Form::open(array('route' => ['users.updateDriverInformation', $user->id] ,'method'=>'POST')) !!}
        
        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Make: <strong> {{ $vehicle->make }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <select name="make_id" class="form-control form-control-alternative" id="make" disabled required>
                    <option selected value="{{ $user->driver[0]->make_id }}">
                         {{ $vehicle->make }}
                    </option>
                    @foreach($vehicles as $make)
                      <option value="{{ $make->iMakeId }}">{{ $make->vMake }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Model: <strong> {{ $vehicle->model }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <select name="model_id" class="form-control form-control-alternative" id="model" disabled required>
                    <option></option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Type: <strong> {{ $vehicle->type }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <select name="type_id" class="form-control form-control-alternative" id="type" disabled required>
                    <option></option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Color: <strong> {{ $vehicle->color }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <select name="color_id" class="form-control form-control-alternative" id="color" disabled required>
                    <option></option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Year: <strong> {{ $vehicle->year }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <input class="form-control form-control-alternative" id="year" type="number" disabled name="year">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Tag: <strong> {{ $user->driver[0]->vehicle_tag }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <input class="form-control form-control-alternative" id="tag" type="text" disabled name="vehicle_tag">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Registration : <strong> {{ $user->driver[0]->vehicle_registration }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <input class="form-control form-control-alternative" id="registration" type="text" disabled name="vehicle_registration">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Registration Expiration: <strong> {{ $user->driver[0]->registration_expire }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <input class="form-control form-control-alternative" id="registration_expire" type="date" disabled name="registration_expire">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> Insurance Expiration: <strong> {{ $user->driver[0]->insurance_expire }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <input class="form-control form-control-alternative" id="insurance_expire" type="date" disabled name="insurance_expire">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-6 control-label"> 
                <p> License Expiration: <strong> {{ $user->driver[0]->license_expire }} </strong> </p>
            </label>
            <div class="col-sm-6">
                <input class="form-control form-control-alternative" id="license_expire" type="date" disabled name="license_expire">
            </div>
        </div>
        <div class="row text-center">
            <div class="btn-group-toggle" data-toggle="buttons">
                <button class="btn btn-warning" id="updateInfo"> Update Info </button>
            </div>
                
            <button class="btn btn-primary" disabled type="submit" id="submitInfo"> 
            Submit Info </button>
        </div>
        {!! Form::close() !!}
    
        <hr>
        <!-- User Driver Pictures Information --> 
        <div class="row"> 
            <div class="col-md-6">
                <div class="form-group">
                    <label> Driver's License: </label> <br>
                    <img src="{{ $user->driver[0]->driver_license }}" width="400">
                </div>
                
                <button type="button" class="btn btn-info btn-lg padding" data-toggle="modal" 
                data-target="#driverLicense">Upload Image</button>
                
                <div id="driverLicense" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Upload Driver License</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('driver.updateImage', $user->id) }}" 
                                    method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="text" name="type" value="driver_license" style="display: none;">
                                    <input type="file" onchange="previewFile()" class="form-control" 
                                    id="profile_image_upload" 
                                    name="file_input">

                                    <img src=""  alt="Image preview..." width="500" 
                                    id="preview" style="max-width: 100%;">

                                    <button type="submit" class="btn btn-default">Upload</button>
                                </form>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" id="closeModel" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
        </div>

        <div class="row pad-30">
            <div class="col-md-6">
                
                <div class="form-group">
                    <label> Car Image: </label> <br> 
                    <img width="350" src="{{ asset($user->driver[0]->path_image) }}">
                </div>
                
                <button type="button" class="btn btn-info btn-lg padding" data-toggle="modal" data-target="#myModal">Upload Image</button>
                
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">  
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Upload Car Image</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('driver.updateImage', $user->id) }}" 
                                    method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="text" name="type" value="path_image" style="display: none;">
                                    <input type="file" onchange="previewFile()" class="form-control" id="profile_image_upload" 
                                    name="file_input">

                                    <img src=""  alt="Image preview..." width="500" 
                                    id="preview" style="max-width: 100%;">

                                    
                                    <button type="submit" class="btn btn-default">Upload</button>
                                </form>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" id="closeModel" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>

        <div class="row pad-30">
            <div class="col-md-6">    
                <div class="form-group">
                    <label> Registration:</label><br/>
                    <img src="{{ $user->driver[0]->registration_path }}" width="400" >
                </div>

                <button type="button" class="btn btn-info btn-lg padding" data-toggle="modal" 
                data-target="#regis">Upload Image</button>
                    
                <div id="regis" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Upload Registration Image </h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('driver.updateImage', $user->id) }}" 
                                    method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="text" name="type" value="registration_path" 
                                    style="display: none;">

                                    <input type="file" onchange="previewFile()" class="form-control" 
                                        id="profile_image_upload" 
                                    name="file_input">

                                    <img src=""  alt="Image preview..." width="500" 
                                    id="preview" style="max-width: 100%;">

                                    
                                    <button type="submit" class="btn btn-default">Upload</button>
                                </form>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" id="closeModel" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row pad-30">
            <div class="col-md-6">
                <div class="form-group">
                    <label> Insurance:</label><br/>
                    <img src="{{ $user->driver[0]->insurance_path }}" width="500" >
                </div>
                
                <button type="button" class="btn btn-info btn-lg padding" data-toggle="modal" 
                data-target="#insurance">Upload Image</button>
                    
                <div id="insurance" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Upload Insurance Image </h4>
                              </div>
                            <div class="modal-body">
                                <form action="{{ route('driver.updateImage', $user->id) }}" 
                                    method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="text" name="type" value="insurance_path" style="display: none;">
                                    <input type="file" onchange="previewFile()" class="form-control" 
                                    id="profile_image_upload" 
                                    name="file_input">

                                    <img src="{{ $user->driver[0]->insurance_path }}"  alt="Image preview..." width="500" 
                                    id="preview" style="max-width: 100%;">

                                    
                                    <button type="submit" class="btn btn-default">Upload</button>
                                </form>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" id="closeModel" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <h2> Actions </h2>

        <div class="text-center row pad-30">

            <a class="btn btn-danger" href="{{route('driver.emptyDriver', $user->driver[0]->id)}}"
                onclick="return confirm('Are you sure you want to delete data?');">
                Delete Driver Data</a>
        </div>
        

        @if($user->driver[0]->status == 'Denied')
            <div class="text-center row pad-30">

                <p> Approve driver again after denial due to expired document </p>

                <a class="btn btn-primary"
                   href="{{ route('driver.acceptDriver', $user->driver[0]->id) }}">
                    Approve driver again
                </a>
            </div>
        @endif
        
        
        @if($user->driver[0]->status == "Approved")
            <div class="text-center row pad-30">
                
                {!! Form::open(array('route' => ['users.denyDriver', $user->id] ,'method'=>'POST')) !!}
                        <button type="submit" class="btn btn-warning">Change back to User</button>
                {!! Form::close() !!}
            </div>
        @endif
        

        <div class="row pad-30">
            @if($user->driver[0]->status == "Requested")
                @include('dashboardadmin.users.layout.approve-denny', [$user])
            @else
        </div>

    @endif
    </div>
</div>


<script type="text/javascript">
function previewFile() {


      var preview = document.getElementById('preview');
      var file    = document.querySelector('input[type=file]').files[0];
      var reader  = new FileReader();

      preview.style.display = "inline";
      reader.onloadend = function () {
        preview.src = reader.result;
      }

      if (file) {
        reader.readAsDataURL(file);
      } else {
        preview.src = "";
      }
}
</script>
