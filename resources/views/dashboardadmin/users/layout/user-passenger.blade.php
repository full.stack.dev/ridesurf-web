<div class="col-md-12 col-sm-12">
    <h6 class="heading-small text-muted mb-4 pad-30">{{ __('Passenger Information') }}</h6>
	<div class="panel panel-default">
        <div class="panel-body">
            <div class="row text-center"> 
		      	<div class="col-md-6">
	            	<div class="card " style="width: 100%">
			          <div class="card-body">
			            <h5 class="card-title">User's Reviews: </h5>
			            <h6 class="card-subtitle mb-2 text-muted"> </h6>
			            <h5> {{ count($user->reviewTo) }}</h5>
			          </div>
			        </div>
		      	</div>
		      	<div class="col-md-6">
	            	<div class="card " style="width: 100%">
			          <div class="card-body">
			            <h5 class="card-title">User's Ratings</h5>
			            <h6 class="card-subtitle mb-2 text-muted"> </h6>
			            <h5> {{ count($user->reviewFrom) }}</h5>
			          </div>
			        </div>
		      	</div>
            </div>
        </div>

        <h3> Passenger Booked Rides </h3> 
        <div>
        	@foreach($user->UserRide as $uRide)
				@if($uRide->ride)
					@if( count( (array)$uRide->ride) > 0 )
						<div class="card">
							<div class="col-md-3"> From: <strong> {{ $uRide->city_from }} </strong></div>
							<div class="col-md-3"> To: <strong> {{ $uRide->city_to }}</strong> </div>
							<div class="col-md-3">
								Date <strong> {{ date('j M Y', strtotime($uRide->ride->dateFrom)) }} </strong>
							</div>
							<div class="col-md-3">
								<a class="btn btn-warning" href="{{ route('rides.edit',$uRide->ride->id) }}">Edit</a>
							</div>
						</div>
					@endif
				@endif
		  	@endforeach
		</div>
    </div>
</div>