<h2> Reports </h2>

<div class="panel panel-default">
    <div class="panel-body">    
		@foreach($user->report as $report)
			<div class="row"> 
                <div class="col-md-6">
                    <div class="form-group">
                        <label> Report Date: <strong> 
                        	{{ date('j M Y', strtotime($report->created_at)) }} </strong> 
                        </label> <br>
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" >
                        <label> Reson </label>
                        <p> {{ $report->reason }}</p> 

                        <label> Description </label>
                        <p> {{ $report->description }} </p> 
                    </div>
                </div>   
            </div>
		@endforeach
	</div> 
</div>