<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="card" style="width: 100%">
          <div class="card-body">
            
            {!! Form::open(array('route' => ['users.approveDriver', $user->id] ,'method'=>'POST')) !!}
            <button type="submit" class="btn btn-success">Approve Driver Request</button>

            {!! Form::close() !!}
          </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="card" style="width: 100%">
            <div class="card-body">
                <h5 class="card-title">Deny User</h5>

                {!! Form::open(array('route' => ['users.denyDriver', $user->id] ,'method'=>'POST', 'class' => 'form-group')) !!}
                <div class="form-group">
                    <label>Message:</label>
                    <textarea name="reply_message"  class="form-control"></textarea>
                </div>
                <label>Reasons:</label>
                <div class="form-control"> <label>Inappropriate Profile Picture</label><input type="checkbox" name="incorrect_picture" value="Incorrect Profile Picutre"></div>
                <div class="form-control"> <label>Incorrect Car Picture</label><input type="checkbox" name="incorrect_picture" value="Incorrect Car Picture"></div>
                <div class="form-control"> <label>Incorrect Insurance</label><input type="checkbox" name="incorrect_year" value="Incorrect Insurance"></div>
                <div class="form-control"> <label>Registration Year</label><input type="checkbox" name="incorrect_year" value="Incorrect Registration"></div>
                <div class="form-control"> <label>Incorrect Make</label><input type="checkbox" name="incorrect_make" value="Incorrect Make"></div>
                <div class="form-control"> <label>Incorrect Model</label><input type="checkbox" name="incorrect_model" value="Incorrect Model"></div>
                <div class="form-control"> <label>Incorrect Year</label><input type="checkbox" name="incorrect_year" value="Incorrect Year"></div>
                <div class="form-control"> <label>Incorrect Driver License</label><input type="checkbox" name="incorrect_year" value="Incorrect Driver License"></div>
                <button type="submit" class="btn btn-danger">Deny Driver Request</button>
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>