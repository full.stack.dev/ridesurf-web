
{!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}


<div class="col-md-12 col-sm-12">

    <h2 class="pad-30">{{ __('User Information') }}</h2>

    <div class="form-group">
        <label class="form-control-label">First Name</label>
        <div>
        {!! Form::text('first_name', null, array('placeholder' => 'First Name','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Last Name</label>
        <div>
        {!! Form::text('last_name', null, array('placeholder' => 'Last name','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Email</label>
        <div>
        {!! Form::text('email', null, 
        array('placeholder' => 'Email','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">PayPal Email</label>
        <div>
        {!! Form::text('paypal_email', null, array('placeholder' => 'PayPal','class' => 'form-control form-control-alternative')) !!}
        </div> 
    </div>

    <div class="form-group">
        <label class="form-control-label">Address</label>
        <div>
        {!! Form::text('address', null, array('placeholder' => 'adddress','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Address</label>
        <div>
        {!! Form::text('city', null, array('placeholder' => 'City','class' => 'form-control form-control-alternative')) !!}
        </div> 
    </div>

    <div class="form-group">
        <label class="form-control-label">State</label>
        <div>
        {!! Form::text('state', null, array('placeholder' => 'State 2 letters only','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Country</label>
        <div>
        {!! Form::text('country', null, array('placeholder' => 'Country','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Birthday</label>
        <div>
        {!! Form::text('birthday', null, array('placeholder' => 'Birthday','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Area code <small> Please do not add "+" or dashes </small> </label>
        <div>
        {!! Form::text('area_code', null, array('placeholder' => 'Area Code','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Phone Number</label>
        <div>
        {!! Form::text('phone_number', null, array('placeholder' => 'phone number','class' => 'form-control form-control-alternative', 'size' => 3)) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Password <small> *Leave empty unless changing</small></label>
        <div>
        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Confim Password</label>
        <div>
        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control form-control-alternative')) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="form-control-label">Role</label>
        <div>
        {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control form-control-alternative','multiple')) !!}
        </div>
    </div>

    <div class="text-center padding">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>

</div>
{!! Form::close() !!}