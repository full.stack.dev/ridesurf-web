@extends('dashboardadmin.layout.dashboard')
@section('content')

    <!-- Header Cards ... Make this own blade file -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="row" style="padding: 25px 45px;">

                            <div class="col-5">
                                {!! Form::open(array('route' => 'users.filterUser','method'=>'GET')) !!}

                                    <h3>User Search:</h3> <input type="text" name="first" placeholder="First Name">
                                <button class="btn btn-sm btn-primary">Search</button>

                                {!! Form::close() !!}

                            </div>
                            <div class="col-7">
                                {!! Form::open(array('route' => 'users.importUsers','method'=>'POST', 'files' => 'true')) !!}
                                <span style="font-size: 14px">
                                <h3>Import Users: </h3><input type="file" name="users_import" id="fileToUpload">
                                <button class="btn btn-sm btn-primary">Import</button>
                                </span>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid mt--7">

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Users') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('users.create') }}" class="btn btn-sm btn-primary">Add User</a>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">Img</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Email</th>
                            <th scope="col">Creation</th>
                            <th scope="col">Role</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $key => $user)
                            <tr>
                                <td>
                                    @if($user->avatar == null)
                                        <img src="/images/avatar.jpg" width="50px" class="img-circle" alt="user">
                                    @else
                                        <img src="{{ $user->avatar }}" width="50px" class="img-circle" alt="user"/>
                                    @endif
                                </td>
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->last_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td> {{ $user->created_at }}</td>
                                <td>
                                    @if(!empty($user->getRoleNames()))
                                        @foreach($user->getRoleNames() as $v)
                                            <label class="badge badge-success">{{ $v }}</label>
                                        @endforeach
                                    @endif
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">

                                            {!! Form::open(['method' => 'DELETE','route' => ['users.destroyFromAdmin', $user->id],'style'=>'display:inline']) !!}
                                            <button class="btn btn-danger" type="submit"
                                                    href="{{ route('users.edit',$user->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            {!! Form::close() !!}

                                            <a class="dropdown-item" href="{{ route('users.edit',$user->id) }}">{{ __('Edit') }}</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        @include('pagination.admin', ['paginator' => $data])
                    </nav>
                </div>

                

            </div>
        </div>
    </div>

</div>

@endsection