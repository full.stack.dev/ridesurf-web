@extends('dashboardadmin.layout.dashboard')
@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                	{!! Form::open(array('route' => 'dashboardadmin.filterRides','method'=>'GET')) !!}
                    <div style="padding: 25px 45px;">

                    	<div class="row">
                    		<h2>Filter</h2>
                    	</div>
                    	<div class="row">
					    	<div class="col-3"> <h3> Id: </h3> <input type="number" name="rides_id" > </div>
					    	<div class="col-3"> <h3> Date: </h3> <input type="date" name="date"> </div> 
					    	<div class="col-3"> <h3> Driver Name: </h3> <input type="text" name="name"> </div>
					    	<div class="col-3"> <button class="btn btn-primary">Filter</button> </div>
					    </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--7">

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Rides') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
						    <tr>
						      <th scope="col">#RideID</th>
						      <th scope="col">Driver Name</th>
						      <th scope="col">Passengers</th>
						      <th scope="col">Date of Ride</th>
						      <th scope="col">Cost Per Seat</th>
						      <th scope="col">Menu</th>
						    </tr>
						</thead>
						  
						<tbody>
						  	
						  	@foreach($rides as $ride)
						  	 
						    <tr>
						      <th scope="row">{{ $ride->id }}</th>
						      <td>{{ $ride->user->first_name }}</td>
						      <td>{{ count($ride->user_ride) }}</td>
						      <td>{{ $ride->dateFrom }}</td>
						      <td>${{ $ride->price }}</td>
						      <td> 
						      	<a class="btn btn-primary" href="{{ route('dashboardadmin.manageUsers',$ride->id) }}">Manage</a>
						      	<a class="btn btn-warning" href="{{ route('rides.edit',$ride->id) }}">Edit</a>
								{!! Form::open(['method' => 'DELETE','route' => ['rides.destroy', $ride->id],'style'=>'display:inline']) !!}
					            	{!! Form::submit('Delete', ['class' => 'btn btn-danger', 'id' => 'specialButton']) !!}
					        	{!! Form::close() !!}
						      </td>
						    </tr>
						     
						    @endforeach

						</tbody>
						 
					</table>

				</div>

                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        @include('pagination.admin', ['paginator' => $rides])
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        jQuery("#specialButton").click(function(){
            return confirm("Do you want to delete this ?");
        });
    </script>
@endsection