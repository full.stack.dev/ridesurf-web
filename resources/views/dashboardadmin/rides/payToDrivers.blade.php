@extends('dashboardadmin.layout.dashboard')

@section('content')
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item active" aria-current="page">
	    	 Export CSV 
	    </li>
	    <li class="breadcrumb-item active" aria-current="page">
	    	{!! Form::open(array('route' => 'dashboardadmin.exportPayUser','method'=>'POST')) !!}
		    	Range || From: <input type="date" name="dateFrom" > To : <input type="date" name="dateTo">
		    	<button class="btn btn-success">Export</button>
	    	{!! Form::close() !!}
	    </li>
	  </ol>
	</nav>
	<div class="wrapper">
		<table class="table table-striped">
		  <thead>
		    <tr>
		      <th scope="col">#RideID</th>
		      <th scope="col">Driver Name</th>
		      <th scope="col">Email</th>
		      <th scope="col">Date of Ride</th>
		      <th scope="col">Total Pay</th>
		      <th scope="col">Driver Paid</th>
		    </tr>
		  </thead>
		  {!! Form::open(array('route'=> ['dashboardadmin.driverPaid'] , 'method' => 'POST' )) !!}
		  <tbody>
		  	
		  	@foreach($totalPayout as $driver)
		  	 
		    <tr>
		      <th scope="row"> <a class="btn btn-warning" href="{{ route('rides.edit',$driver->rides_id) }}">{{ $driver->rides_id }}</a></th>
		      <td>{{ $driver->first_name }}</td>
		      <td>{{ $driver->email }}</td>
		      <td>{{ $driver->dateFrom }}</td>
		      <td> 
		      	<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" 
		      	data-target="#myModal-{{ $driver->rides_id }}">
				  ${{ $driver->price }}
				</button>
				<div class="modal fade" id="myModal-{{ $driver->rides_id }}" tabindex="-1" 
					role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">Transactions</h4>
				      </div>
				      <div class="modal-body">
				      	<div class="row">

				      		<div class="col-md-6">
				      			<p> Passenger Paid </p> 
				      			@foreach($driver->price_pay as  $paid)
		        					<p> ${{ $paid }} </p>
		        				@endforeach
				      			
						       
	        				</div>
	        				<div class="col-md-6">
	        					<p> Driver Payment </p> 
				      			@foreach($driver->transactions as $transactions)
		        					<p> ${{ $transactions }} </p>
		        				@endforeach
	        				</div>
        				</div>
				        
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div>

		      </td>
		      <td> 
		      	<input name="paidDriver-{{ $driver->rides_id }}" type="checkbox">
		      </td>
		    </tr>
		     
		    @endforeach
		    
		  </tbody>
		  <button class="btn btn-success"> Pay Drivers </button>
		 {!!  Form::close() !!}
		</table>
	</div>
@endsection