@extends('dashboardadmin.layout.dashboard')

<!-- List of Rides -->
@section('content')
<div class="row">
	<div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Manager Passenger of Ride {{ $ride->id }}</h2>
        </div>

    </div>

    <table class="table table-striped">
    	<thead>
    		<th scope="col">  Name </th>
    		<th scope="col">  Booking Number </th>
    		<th scope="col">  Booked Date</th>
    		<th scope="col">  Price Paid </th>
    		<th scope="col">  Paypayl Sales_Id </th>
    		<th scope="col">  Actions </th>

    	</thead>
    	<tbody>
    		@if(count($ride->user_ride) > 0)
	    		@foreach($ride->user_ride as $uRide)
	    			<td> {{ $uRide->user->first_name }} {{ $uRide->user->last_name }}</td>
	    			<td> {{ $uRide->booking_number }} </td>
	    			<td> {{ $uRide->updated_at }} </td>
	    			<td> {{ $uRide->price_paid }} </td>
	    			<td> {{ $uRide->sale_id }} </td>
	    			<td> 
	    				<a href="{{ route('rides.destroyUserRide', $uRide->ride->id) }}" 
	    					class="btn btn-primary" id="refund_user"> Refund User </a>
	    				<!--<a href="#" class="btn btn-danger" id="delelet_user"> Remove User</a> -->
	    			</td>
	    		@endforeach
    		@endif
    	</tbody>

    </table>
</div>

<script type="text/javascript">
	$( document ).ready(function() {
		jQuery('#refund_user').click(function() {
			if (confirm('Are you sure you want to refund user ')) {
            	return true;
        	}else{
        		return false;
        	}
		});
	});
</script>
@endsection