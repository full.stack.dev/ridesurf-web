@extends('dashboardadmin.layout.dashboard')

@section('content')


<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Report Management</h2>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
 <tr>
   <th>User </th>
   <th>From </th>
   <th>To</th>
   <th>Date Asked</th>
   <th>Created At </th> 
   <th width="280px">Action</th>
 </tr>
  @foreach ($rideR as $report)
  <tr>
    <td>
        @if(count($report->user) > 0 )
          {{ $report->user->first_name }}
        @else
          <p> User deleted Account </p> 
        @endif
    </td>
    <td>{{ $report->city_from }}</td>
    <td>{{ $report->city_to }}</td>
    <td>{{ $report->date }}</td>
    <td> {{ date('m/d/Y', strtotime($report->created_at)) }}</td>

    <td>
       <a class="btn btn-primary" href="{{ route('request.showsingle',$report->id) }}">show</a>
       {!! Form::open(['method' => 'DELETE','route' => ['ride_request.destroy', $report->id],' style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </td>
  </tr>
 @endforeach
</table>

@endsection