@extends('dashboardadmin.layout.dashboard')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show User</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('request.showAll') }}"> Back</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
    	<div class="row">
    		<div class="col-md-6">
    			<h3> User </h3> 
    		</div> 
    		<div class="col-md-3">
    			@if(count($rideR->user) > 0)
    				{{ $rideR->user->first_name }}
    			@else
    				<p> User deleted account </p>
    			@endif
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-6">
    			<h3> From </h3> 
    		</div> 
    		<div class="col-md-3">
    			{{ $rideR->city_from }}
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-6">
    			<h3> To </h3> 
    		</div> 
    		<div class="col-md-3">
    			{{ $rideR->city_to }}
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-6">
    			<h3> Date Asked </h3> 
    		</div> 
    		<div class="col-md-3">
    			{{ $rideR->date }}
    		</div>
    	</div>
    </div>
</div>
@endsection