@extends('dashboardadmin.layout.dashboard')
@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>

<div class="container-fluid mt--7">
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">{{ __('Drivers') }}</h3>
            </div>
            <div class="col-4 text-right">
              <a href="{{ route('driver.export') }}" class="btn btn-sm btn-primary">Export Drivers</a>
            </div>
          </div>
        </div>

        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">#id</th>
                <th scope="col">Driver Name</th>
                <th scope="col">Driver Since</th>
                <th scope="col">Rides</th>
                <th scope="col">Status</th>
                <th scope="col">Email</th>
                <th scope="col">Menu</th>
              </tr>
            </thead>
            
            <tbody>
              
              @foreach($drivers as $driver)
              @if( !empty($driver->user) )
                <tr>
                  <th scope="row">{{ $driver->id }}</th>
                  <td>{{ $driver->user->first_name }}</td>
                  <td>{{  date('m/d/Y', strtotime($driver->created_at)) }}</td>
                  <td>{{ count($driver->user->ride) }}</td>
                  <td>{{ $driver->status }}</td>
                  <td>{{ $driver->user->email }}</td>
                  <td> 
                    <a class="btn btn-primary" href="{{ route('users.edit', $driver->user_id) }}">View</a>
                  </td>
                </tr>
              @endif
              @endforeach
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection