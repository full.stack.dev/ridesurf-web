<div class="container-fluid footer-top">
  <div class="col-md-offset-1 col-md-10 col-xs-12">
   <div class="col-md-3 col-xs-12">
     
    <h2 class="footer">Ridesurf</h2>
     <ul class="footer-nav">
       <li><a href="/about">About</a></li>
       <li><a href="/contact">Contact</a></li>
       <li><a href="/faq">FAQ</a></li>
         <li><a href="/howtoridesurf">How to Ridesurf</a></li>
     </ul>
   </div>
   <div class="col-md-3 col-xs-12">
      <h2 class="footer">Legal</h2>
      <ul class="footer-nav">
        <li><a href="/privacy">Privacy Policy</a></li>
        <li><a href="/code-of-conduct">Code of Conduct</a></li>
        <li><a href="/terms-of-use">Terms of Use</a></li>
      </ul>
    </div>
    <div class="col-md-3 col-xs-12">
        <h2 class="footer">Updates</h2>
        <ul class="footer-nav">
          <li><a href="/press">Press</a></li>
          <li><a href="/opportunities">Opportunities</a></li>
          <li><a href="https://blog.ridesurf.io/">Blog</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-xs-12">
        <h2 class="footer">Social Media</h2>
       <ul class="social-icons">
         <li><a href="https://www.facebook.com/ridesurfapp"><i class="fa fa-facebook"></i></a></li>
         <li><a href="https://www.instagram.com/goridesurf/"><i class="fa fa-instagram"></i></a></li>
         <li><a href="https://www.twitter.com/goridesurf/"><i class="fa fa-twitter"></i></a></li>
       </ul>
      </div> 
  </div>
</div>
<div class="container-fluid footer-bottom">
    <div class="col-md-offset-1 col-md-10 col-xs-12 top-border bottom-pad">
      <span class="bottom-text">© Travlr Co. 2020. Ridesurf is a trademark of Travlr Co.</span>
</div>
</div>
<!-- Moment.js library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

<!-- moment-duration-format plugin -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-duration-format/1.3.0/moment-duration-format.min.js"></script>
