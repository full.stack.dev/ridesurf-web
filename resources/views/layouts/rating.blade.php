<div id="stars-{{ $ride->id }}">
    <span data-rating="1" class="fa fa-star"></span>
    <span data-rating="2" class="fa fa-star"></span>
    <span data-rating="3" class="fa fa-star"></span>
    <span data-rating="4" class="fa fa-star "></span>
    <span data-rating="5" class="fa fa-star"></span>
</div>
<script type="text/javascript">
$( document ).ready(function() {

	var stars = $('#stars-' + {{ $ride->id }});
	@if($ride->rating == null){
		var value = 0;
	}@else{
		var value = {{ $ride->rating }};
	}
   	@endif

	// Remove class for selected stars
	stars.find('.checked').removeClass(' checked');
	// Add class to the selected star and all before
	for (i = 1; i <= value; i++) {
		stars.find('[data-rating="' + i + '"]').addClass('checked');
	}
});
</script>