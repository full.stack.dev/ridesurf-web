<div class="row text-center padding-right-10">
  <h6> Passengers </h6>
  @if($usersRiding)
  @foreach($usersRiding as $userRide)
    <div class="col-md-4 col-sm-12 list-group-item">
      <div class="avatar full-margin-25">
        <img src="{{ $userRide->user->avatar }}" width="45px" alt="" class="img-circle">
        <p> {{ $userRide->user->first_name }} {{ $userRide->user->last_name }} </p>
        <p> From <strong> {{ $userRide->city_from }}</strong> </p>
        <p> To <strong> {{ $userRide->city_to }}</strong></p>
      </div>
    </div>
  @endforeach
  @endif
</div>
