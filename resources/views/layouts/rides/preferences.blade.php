<div class="col-md-6">
                
  <a href="#" class="float-right"><span class="img-icon fa-stack ">
    @if($ride->option_num == 3) <!-- No -->
      <i class="fa fa-fire fa-stack-1x"></i>
      <i class="fa fa-ban fa-stack-2x text-danger"></i>
    @elseif($ride->option_num == 2) <!-- Maybe -->
      <i class="fa fa-fire fa-stack-1x"></i>
      <i class="fa fa-minus fa-stack-2x text-danger"></i>
    @elseif($ride->option_num == 1) <!-- Yes -->
      <i class="fa fa-fire fa-stack-1x"></i>
    @endif
  </span>
  </a>
</div>