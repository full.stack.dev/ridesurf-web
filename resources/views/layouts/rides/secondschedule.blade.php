<?php $isStop = count($ride->ride_stop); ?>
<table class="table table-bordered text-center" id="timeline">

  <h3> Current Passengers </h3>
  <thead>
    <tr>
      <th>Location</th>
      <th><p> Driver </p></th>
      @for($i=0; $i < $ride->seats; $i++)
        <th> <img src="{{ url('/images/seat.png') }}" width="25px"> </th>
      @endfor
    </tr>
      
  </thead>
  <tbody>
    <!-- Start Destination -->
    <tr>
      <?php $startHour = strtotime($ride->startHour); ?>

      <td> {{ $ride->location_from }} {{date("h:i a", $startHour)  }} </td>
      <!-- Driver -->
      <td>
        @if(count($ride->user->profileImages) > 0 )
          <img src="{{ $ride->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
        @endif
        @if( !empty($ride->user->avatar))
          <img src="{{ $ride->user->avatar }}" width="45px" alt="" class="img-circle">
        @endif

        <p> {{ $ride->user->first_name }} {{ $ride->user->last_name }}</p>
      </td>

      <!-- Passenger -->
      @if($isStop)
        @for($i=0; $i < $ride->ride_stop[0]->seats_from; $i++)
        <td>
          <p> Open </p>
        </td>
        @endfor
      @else
        @for($i=0; $i < $ride->seats_available; $i++)
        <td>
          <p> Open </p>
        </td>
        @endfor
      @endif

      @for($j=0; $j < count($usersRiding) ; $j++)

        @if($usersRiding[$j]->status == 'Approved' &&  $usersRiding[$j]->city_from == $ride->location_from)
          <td> 
            @if(count($usersRiding[$j]->user->profileImages) > 0 )
              <img src="{{ $usersRiding[$j]->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
            @endif
            @if(!empty($usersRiding[$j]->user->avatar))
              <img src="{{ $usersRiding[$j]->user->avatar }}" width="45px" alt="" class="img-circle">
            @endif
            <p>

              {{ $usersRiding[$j]->user->first_name }}
              {{ $usersRiding[$j]->user->last_name }}
            </p>
          </td>
        @else

        @endif

      @endfor
      
    </tr>

    <!-- End of First Row -->


    <!-- If it has a stop --> 
    @if($isStop > 0)
    <?php $endTime = strtotime($ride->startHour);
          $sum = $ride->ride_stop[0]->duration + $endTime;
          $endStop = date("h:i a", $sum);
    ?>
    <tr>
           
      <td> {{ $ride->ride_stop[0]->address }} {{ $endStop }}</td>
      <!-- Driver -->
      <td>
        @if(count($ride->user->profileImages) > 0 )
          <img src="{{ $ride->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
        @endif
        @if( !empty($ride->user->avatar))
          <img src="{{ $ride->user->avatar }}" width="45px" alt="" class="img-circle">
        @endif
        <p> {{ $ride->user->first_name }} {{ $ride->user->last_name }}</p>
      </td>
      
      <!-- Passenger -->
      @if($isStop > 0)
        <!-- If none had book a ride Stop Over --> 
        @if($ride->ride_stop[0]->seats_from == $ride->seats_available &&
            $ride->ride_stop[0]->seats_to == $ride->seats)

              @for($i=0; $i < $ride->seats_available; $i++)
                <td>
                  <p> Open </p>
                </td>
              @endfor
        @endif
        <!-- If someone booked from Stop Over -->
        @if($ride->ride_stop[0]->seats_from < $ride->seats)
          @for($i=0; $i < $ride->ride_stop[0]->seats_from; $i++)
                <td>
                  <p> Open </p>
                </td>
              @endfor
        <!-- if Someone booked to Stop Over -->
        @elseif($ride->ride_stop[0]->seats_to < $ride->seats)
          @for($i=0; $i < $ride->ride_stop[0]->seats_to; $i++)
                <td>
                  <p> Open </p>
                </td>
              @endfor

        @endif
        
      @else
        @for($i=0; $i < $ride->seats_available; $i++)
        <td>
          <p> Open </p>
        </td>
        @endfor
      @endif

      
      @for($j=0; $j < count($usersRiding) ; $j++)

          @if($usersRiding[$j]->isStop == false)
            <td>
              @if(count($usersRiding[$j]->user->profileImages) > 0 )
                <img src="{{ $usersRiding[$j]->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
              @endif
              @if(!empty($usersRiding[$j]->user->avatar))
                <img src="{{ $usersRiding[$j]->user->avatar }}" width="45px" alt="" class="img-circle">
              @endif  
              <p>
                {{ $usersRiding[$j]->user->first_name }}
                {{ $usersRiding[$j]->user->last_name }}
              </p>
            </td>
          @endif

          @if($usersRiding[$j]->status == 'Approved' &&  
            $usersRiding[$j]->city_to == $ride->ride_stop[0]->address )
             <td>
              @if(count($usersRiding[$j]->user->profileImages) > 0 )
                <img src="{{ $usersRiding[$j]->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
              @endif
              @if(!empty($usersRiding[$j]->user->avatar))
                <img src="{{ $usersRiding[$j]->user->avatar }}" width="45px" alt="" class="img-circle">
              @endif  
              <p>
                
                {{ $usersRiding[$j]->user->first_name }}
                {{ $usersRiding[$j]->user->last_name }}
              </p>
            </td>
          @endif
          
          
        

      @endfor
      
    </tr>
    <!-- End of of Row --> 


    <!-- Destination To-->
    <tr>
      <td> {{ $ride->location_to }} {{ $end }}</td>
      <!-- Driver -->
      <td>
        @if(count($ride->user->profileImages) > 0 )
          <img src="{{ $ride->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
        @endif
        @if( !empty($ride->user->avatar))
          <img src="{{ $ride->user->avatar }}" width="45px" alt="" class="img-circle">
        @endif

        <p> {{ $ride->user->first_name }} {{ $ride->user->last_name }}</p>
      </td>
      
      <!-- Passenger -->
      @if($isStop == 0)
        @for($i=0; $i < $ride->seats_available; $i++)
          <td>
            <p> Open  </p>
          </td>
        @endfor
      @else
        @for($i=0; $i < $ride->ride_stop[0]->seats_to ; $i++)
          <td>
            <p> Open </p>
          </td>
        @endfor
      @endif

      @for($j=0; $j < count($usersRiding) ; $j++)

      @if($usersRiding[$j]->status == 'Approved' && $usersRiding[$j]->city_to == $ride->location_to )
        <td> 
          @if(count($usersRiding[$j]->user->profileImages) > 0 )
            <img src="{{ $usersRiding[$j]->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
          @endif
          @if(!empty($usersRiding[$j]->user->avatar))
            <img src="{{ $usersRiding[$j]->user->avatar }}" width="45px" alt="" class="img-circle">
          @endif
          <p>
            {{ $usersRiding[$j]->user->first_name }}
            {{ $usersRiding[$j]->user->last_name }}
          </p>
        </td>
      
      @endif
      @endfor

      
      
    </tr>
    <!-- End of Row --> 

    
    <!-- No Stop Overs --> 
    @else
      <tr>
      <td> {{ $ride->location_to }} {{ $end }}</td>
      <!-- Driver -->
      <td>
        @if(count($ride->user->profileImages) > 0 )
          <img src="{{ $ride->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
        @endif
        @if( !empty($ride->user->avatar))
          <img src="{{ $ride->user->avatar }}" width="45px" alt="" class="img-circle">
        @endif
        <p> {{ $ride->user->first_name }} {{ $ride->user->last_name }}</p>
      </td>
      
      <!-- Passenger -->
       @for($i=0; $i < $ride->seats_available; $i++)
        <td>
          <p> Open </p>
        </td>
        @endfor
      @for($j=0; $j < count($usersRiding) ; $j++)
      @if($usersRiding[$j]->status == 'Approved')
        <td> 
          @if(count($usersRiding[$j]->user->profileImages) > 0 )
            <img src="{{ $usersRiding[$j]->user->profileImages[0]->path }}" width="45px" alt="" class="img-circle">
          @endif
          @if(!empty($usersRiding[$j]->user->avatar))
            <img src="{{ $usersRiding[$j]->user->avatar }}" width="45px" alt="" class="img-circle">
          @endif
          <p>
            {{ $usersRiding[$j]->user->first_name }}
            {{ $usersRiding[$j]->user->last_name }}
          </p>
        </td>
      @endif
      @endfor

       
    </tr>
    @endif
  </tbody>
</table>