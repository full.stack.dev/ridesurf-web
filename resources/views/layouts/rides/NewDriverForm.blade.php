 {!! Form::open(array('route' => 'dashboarduser.createDriver' , 'method'=>'POST',
     'id' => 'formUser', 'files' => true)) !!}
     <div class="first_step">
        <div class="form-group float-label-control" id="makeDiv">

          <label>Car Make</label>
          <select name="make" class="form-control" id="make" required>
            <option selected disabled>Select a Car Make</option>
            @foreach($vehicles as $make)
              <option value="{{ $make->iMakeId }}">{{ $make->vMake }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group float-label-control" id="modelDiv">
          <label>Car Model</label>
          <select name="model" class="form-control" id="model" disabled required>
            <option></option>
          </select>
        </div>
        <div class="form-group float-label-control" id="modelDiv">
          <label>Car Type</label>
          <select name="type" id="type" class="form-control" disabled required>
            <option></option>
          </select>
        </div>
        <div class="form-group float-label-control" id="modelDiv">
          <label>Car Color</label>
          <select name="color" id="color" class="form-control" disabled required>
            <option></option>
          </select>
        </div>
        <div class="form-group float-label-control" id="modelDiv">
          <label>Car Year</label>
          <select name="year" class="form-control" id="year" disabled required>
            <option></option>
          </select>
        </div>

        <div class="form-group float-label-control" id="tagDiv">
            <label> License Plate </label>
            <input type="text" id="vehicle_tag" name="vehicle_tag" class="form-control"
            required>
        </div>

       </div>
       <div class="row no-display" id="driver_license_div">
          <div class="col-md-12 col-sm-12 form-group float-label-control">
               <div class="alert alert-info"> <h3><strong>Upload Driver License Picture</strong></h3>
              <p>Please make sure your name and expiration date are legible</p></div>
              <input type="file" id="driver_license" name="driver_license"
              onchange="previewFileNoCrop('preview_driver_license','driver_license');">

              <img src="" max-width="auto" alt="Image preview..." id="preview_driver_license" style="display: none">

          </div>
        </div>

        <div class="row no-display" id="registration_div">
          <div class="col-md-12 col-sm-12 form-group float-label-control">
               <div class="alert alert-info"> <h3><strong>Upload Car Registration Picture</strong></h3>
              <p>Please make sure your VIN number, Make, Model, your name and expiration date are legible</p></div>
              <input type="file" id="registration_image" name="registration_image"
              onchange="previewFileNoCrop('preview_registration','registration_image');">

              <img src="" max-width="auto" alt="Image preview..." id="preview_registration" style="display: none">

          </div>
        </div>


          <div class="row no-display" id="insurance_div">
             <div class="col-md-10 col-sm-12 form-group float-label-control">
                <div class="alert alert-info"> <h3> <strong>Upload your Insurance Picture: </strong> </h3> 
                  <p>Please make sure your Policy number, your name and expiration date are legible</p>
                </div>
                <input type="file" id="insurance_image" name="insurance_image"
                onchange="previewFileNoCrop('preview_insurance','insurance_image');">
                <img src="" max-width="auto" alt="Image preview..." id="preview_insurance" style="display: none">
            </div>
          </div>


          <div class="row no-display" id="car_div">
            <div class="col-md-10 col-sm-12 form-group float-label-control">
              <div class="alert alert-info"> <h3> <strong>Upload your Car Picture: </strong> <h3> </div>
              <input type="hidden" id="car_image" name="car_image">
              <div id="controls_car"></div>

              <input type="file" onchange="previewFile('uploader_car','preview_car', 'controls_car','car_image', 300, 300)"
              id="uploader_car"  name="uploader_car">
              <img src="" max-width="auto" alt="Image preview..." id="preview_car" style="display: none">
            </div>
          </div>



        <div class="col-xs-12 col-sm-12 col-md-12 text-center" id="buttonsChange">

          <button type="submit" class="btn btn-primary no-display" id="final_submit">Submit</button>
        </div>
     {!! Form::close() !!}
          <button type="submit" class="btn btn-primary" id="firsStep" >Continue</button>
          <button type="submit" class="btn btn-primary no-display" id="carImage">Continue</button>
          <button type="submit" class="btn btn-primary no-display" id="registration_continue">Continue</button>
          <button type="submit" class="btn btn-primary no-display" id="insurance_continue">Continue</button>

<script type="text/javascript">
  $(document).ready(function() {

      $('#firsStep').on('click', function(e){
          var isValid = false;
          $(".first_step :input").each(function() {
            var element = $(this);
            if (element.val() == "") {
                isValid = true;
                var info = '<div class="alert alert-danger" id="float-alert">'+
                           '<p> Please make sure to fillout all the information </p>' +
                           '</div>';
                element.css('border-color', 'red');
                console.log(element);
                $('.offer-ride').prepend(info);
                setTimeout(function(){
                  $('#float-alert').fadeOut();
                }, 2500);
            }
          });

          if(isValid == false){
            $('#firsStep').remove();
            $('.first_step').fadeOut(200);
            $('html, body').animate({ scrollTop: 175 }, 'slow');
            $('#driver_license_div').fadeIn(200);
            $('#carImage').fadeIn(350);
            $('#driverLincese').prop("disabled",true);
          }
      });

      $('#driver_license').on('change', function(e){
        $('#carImage').prop("disabled",false);
      });

      $('#carImage').on('click', function(e){

            $('#driver_license_div').fadeOut(200);
            $('#carImage').remove();
            $('html, body').animate({ scrollTop: 175 }, 'slow');
            $('#registration_div').fadeIn(200);
            $('#registration_continue').fadeIn(350);
            $('#registration_continue').prop("disabled",true);
          
      });

      $('#registration_image').on('change', function(e){
        $('#registration_continue').prop("disabled",false);
      });

      $('#registration_continue').on('click', function(e){
        $('#registration_div').fadeOut(200);
        $('#registration_continue').remove();
        $('html, body').animate({ scrollTop: 175 }, 'slow');
        $('#insurance_div').fadeIn(200);
        $('#insurance_continue').fadeIn(350);
        $('#insurance_continue').prop("disabled",false);
      });

      $('#insurance_continue').on('click', function(e){
        $('#insurance_div').fadeOut(200);
        $('#insurance_continue').remove();
        $('html, body').animate({ scrollTop: 175 }, 'slow');
        $('#car_div').fadeIn(380);
        $('#final_submit').fadeIn(400);
        $('#final_submit').prop("disabled",true);

      });

      $('#car_image').on('change', function(e){
          $('#final_submit').prop("disabled", false);
      });

      $('#vehicle_tag').on('change', function(e){
          $('#submitButton').prop("disabled",false);
      });
      $('#submitButton').prop("disabled",true);
});
</script>