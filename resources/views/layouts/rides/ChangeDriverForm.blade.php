 {!! Form::open(array('route' => 'dashboarduser.createDriver' , 'method'=>'POST',
     'id' => 'formUser', 'files' => true)) !!}
     <div class="first_step">
        <div class="form-group float-label-control" id="makeDiv">

          <label>Car Make</label>
          <select name="make" class="form-control" id="make" required>
            <option selected value="{{ $driver->make_id }}">{{ $car->make }}</option>
            @foreach($vehicles as $make)
              <option value="{{ $make->iMakeId }}">{{ $make->vMake }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group float-label-control" id="modelDiv">
          <label>Car Model</label>
          <select name="model" class="form-control" id="model" required>
            <option selected value="{{ $driver->model_id }}">{{ $car->model }}</option>
          </select>
        </div>
        <div class="form-group float-label-control" id="modelDiv">
          <label>Car Type</label>
          <select name="type" id="type" class="form-control" required>
            <option value={{ $driver->type_id }}>{{ $car->type }}</option>
          </select>
        </div>
        <div class="form-group float-label-control" id="modelDiv">
          <label>Car Color</label>
          <select name="color" id="color" class="form-control" required>
            <option value={{ $driver->color_id }}>{{ $car->color }}</option>
          </select>
        </div>
        <div class="form-group float-label-control" id="modelDiv">
          <label>Car Year</label>
          <select name="year" class="form-control" id="year" required>
            <option value={{ $driver->year }}> {{ $driver->year }}</option>
          </select>
        </div>

        <div class="form-group float-label-control" id="tagDiv">
            <label> License Plate </label>
            <input type="text" id="vehicle_tag" name="vehicle_tag" class="form-control"
             required value="{{ $driver->vehicle_tag }}"> 
        </div>

       </div>
       <div class="row no-display" id="driver_license_div">
          <div class="col-md-12 col-sm-12 form-group float-label-control">
            <div class="alert alert-info">
              <h3><strong>Upload Driver's License Photo</strong></h3>
              <p>Please make sure your name and expiration date are legible.</p>
            </div>

            <input type="file" id="driver_license" name="driver_license"
              onchange="previewFileNoCrop('preview_driver_license','driver_license');">

            <div class="top-margin-15">
              <img src="{{$driver->driver_license}}" alt="" id="preview_driver_license" style="display: none !important; max-width: 400px;">
            </div>

          </div>
        </div>
        <div class="row no-display" id="registration_div">
          <div class="col-md-12 col-sm-12 form-group float-label-control">
               <div class="alert alert-info"> <h3><strong>Upload Car Registration Photo</strong></h3>
              <p>Please make sure your VIN number, make, model, your name and expiration date are legible. Notice: Your application will be denied if your registration is expired.</p></div>
              <input type="file" id="registration_image" name="registration_image"
              onchange="previewFileNoCrop('preview_registration','registration_image');">

            <div class="top-margin-15">
              <img src="{{ $driver->registration_path }}" alt="" id="preview_registration" style="display: none !important; max-width: 400px;">
            </div>

          </div>
        </div>


          <div class="row no-display" id="insurance_div">
             <div class="col-md-10 col-sm-12 form-group float-label-control">
                <div class="alert alert-info"> <h3> <strong>Upload Insurance Photo</strong> </h3> 
                  <p>Please make sure your policy number, your name and expiration date are legible. Norice: your application will be denied if your insurance is expired.</p>
                </div>

                <input type="file" id="insurance_image" name="insurance_image"
                onchange="previewFileNoCrop('preview_insurance','insurance_image');">

              <div class="top-margin-15">
                <img src="{{ $driver->insurance_path }}" alt="" id="preview_insurance" style="display: none !important; max-width: 400px;">
              </div>
            </div>
          </div>


          <div class="row no-display" id="car_div">
            <div class="col-md-10 col-sm-12 form-group float-label-control">
              <div class="alert alert-info"> <h3> <strong>Upload Car Photo</strong> <h3> </div>
              <input type="hidden" id="car_image" name="car_image">
              <div id="controls_car"></div>

              <input type="file" onchange="previewFile('uploader_car','preview_car', 'controls_car','car_image', 300, 300)"
              id="uploader_car"  name="uploader_car">

              <div class="top-margin-15">
                <img src="{{ $driver->path_image }}" alt="" id="preview_car" style="display: none !important; max-width: 400px;">
              </div>
            </div>
          </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center" id="buttonsChange">

          <button type="submit" class="btn btn-primary no-display" id="final_submit">Submit</button>
        </div>
     {!! Form::close() !!}
          <button type="submit" class="btn btn-primary" id="firsStep" >Continue</button>
          <button type="submit" class="btn btn-primary no-display" id="license">Continue</button>
          <button type="submit" class="btn btn-primary no-display" id="registration_continue">Continue</button>
          <button type="submit" class="btn btn-primary no-display" id="insurance_continue">Continue</button>

<script type="text/javascript">
  $(document).ready(function() {
    $('#firsStep').on('click', function(e){

            $('#firsStep').remove();
            $('.first_step').fadeOut(200);
            $('html, body').animate({ scrollTop: 175 }, 'slow');
            $('#driver_license_div').fadeIn(200);
            $('#license').fadeIn(350);
            var preview4 = document.getElementById('preview_driver_license');
            var readerReg  = new FileReader();
            preview4.style.display = "inline";
            readerReg.onloadend = function () {
              preview4.src = readerReg.result;
            }
      });

      $('#license').on('click', function(e){
        var preview2 = document.getElementById('preview_registration');
        var readerReg  = new FileReader();
        preview2.style.display = "inline";
        readerReg.onloadend = function () {
          preview2.src = readerReg.result;
        }
        $('#driver_license_div').fadeOut(200);
        $('#license').remove();
        $('html, body').animate({ scrollTop: 175 }, 'slow');
        $('#registration_div').fadeIn(200);
        $('#registration_continue').fadeIn(350);
      });

      $('#registration_continue').on('click', function(e){
        var preview2 = document.getElementById('preview_insurance');
        var readerReg  = new FileReader();
        preview2.style.display = "inline";
        readerReg.onloadend = function () {
          preview2.src = readerReg.result;
        }
        $('#registration_div').fadeOut(200);
        $('#registration_continue').remove();
        $('html, body').animate({ scrollTop: 175 }, 'slow');
        $('#insurance_div').fadeIn(200);
        $('#insurance_continue').fadeIn(350);
      });

      $('#insurance_continue').on('click', function(e){
        var preview3 = document.getElementById('preview_car');
        var readerIns  = new FileReader();
        preview3.style.display = "inline";
        readerIns.onloadend = function () {
          preview3.src = readerIns.result;
        }
        $('#insurance_div').fadeOut(200);
        $('#insurance_continue').remove();
        $('html, body').animate({ scrollTop: 175 }, 'slow');
        $('#car_div').fadeIn(380);
        $('#final_submit').fadeIn(400);

      });

      $('#car_image').on('change', function(e){
          $('#final_submit').prop("disabled", false);
      });

      $('#vehicle_tag').on('change', function(e){
          $('#submitButton').prop("disabled",false);
      });


    $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "GET",

          url:"{{ route('dashboarduser.fetchAllData') }}",
          method:"GET",
          data:{
            make : "{{ $driver->make_id }}",
           _token: $('meta[name="csrf-token"]').attr('content')
         },
          success:function(data){
            console.log(data);
            var select = $('#model');
            for(var i=0; i < data.model.length; i++){
              $('#model').append('<option value="'+data.model[i].iModelId+'">' + data.model[i].vTitle + '</option>');
            }
            for(var i=0; i< data.modeltype.length; i++){
              $('#type').append('<option value="'+data.modeltype[i].iCarTypeId+'">' 
                + data.modeltype[i].vTitle_EN + '</option>');
            }
            for(var i=0; i< data.carcolor.length; i++){
              $('#color').append('<option value="'+data.carcolor[i].iColourId+'">' + data.carcolor[i].vColour_EN + '</option>');
            }
            var year = 1960;
            for(var i=0; i < 60; i++){
              $('#year').append('<option value="'+(year+i) +'">' + (year+i) +' </option>');
            }
            var preview = document.getElementById('preview_registration');
            var reader  = new FileReader();
            preview.style.display = "inline";
            reader.onloadend = function () {
              preview.src = reader.result;
            }
          },
          error: function (request, status, error) {
            alert(status + " " + error + " "  + request.responseText);
        },
      });
  });

</script>