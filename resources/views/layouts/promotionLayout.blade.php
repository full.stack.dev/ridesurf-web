<!DOCTYPE html lang="{{ app()->getLocale() }}">
<html lang="en">
<head>

    <title>Ridesurf | Long Distance Ride Sharing</title>
@include('meta::manager', [
  'title'         => 'Ridesurf | Long Distance Ride Sharing',
  'description'   => 'Ridesurf is a long distance ride sharing app. It’s a safe and easy way for people who are going the same direction to share a ride with one another. Sell the extra seats in your car for cash and help the environment at the same time by carpooling long drives.',
  'image'         => 'https://ridesurf.io/images/ridesurf_social_sm.jpg',
])
<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/form-styling.css') }}">


    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="{{ asset('js/starrr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

    <!-- Emojis -->
    <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Palanquin+Dark" rel="stylesheet">

    <!-- Google Verification -->
    <meta name="google-site-verification" content="3EBBsV1grhgzoIhSmE2Q9k83Ti3CVpi0hc2aQCZ3ZAY" />
</head>
<body>
<div id="loading">
    <p class="text-center"><img src="{{ asset('images/loader.gif')}}" /></p>
</div>
<!-- Nav Bar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{{  url('/') }}"><img src="{{ asset('images/ridesurf.png')}}"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">

                <li><a href="{{ route('rides.search') }}">Find Ride</a></li>
                <li><a href="{{ route('rides.create') }}">Offer Ride</a></li>
                @guest
                    <li><a href="{{ route('login') }}">{{ __('Log In') }}</a></li>
                    <li><a class="nav-item outlinebutton" href="{{ route('register') }}">{{ __('Sign Up') }}</a></li>
                @else
                    @hasrole('Admin')
                    <li><a class="nav-item" href="{{ route('dashboardadmin.index') }}">Admin </a></li>
                    @endhasrole
                    <li class="nav-item dropdown pull-right">
                        <a class="nav-link dropdown-toggle avatar-cont" data-toggle="dropdown" href="#"><span>{{ Auth::user()->first_name }}</span>

                            @include('layouts.userProfile')

                            @if($notify_menu > 0)
                                ({{ $notify_menu}})
                            @endif
                            <span class="tog"><i class="fa fa-chevron-down"></i></span></a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                            <a class="dropdown-item" href="{{ route('dashboarduser.index') }}">Dashboard
                                ({{ $notify_menu }})</a>
                            <a class="dropdown-item" href="{{ route('dashboarduser.message') }}">Messages ({{$cMessages}})</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                {{ __('Log Out') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> @csrf </form>
                        </div>
                    </li>

                    <!-- Mobile Menu, removes dropdown -->
                    <li class="mobile-nav"><a href="{{ route('dashboarduser.index') }}">Dashboard ({{$notify_menu}})</a></li>
                    <li class="mobile-nav"><a href="{{ route('dashboarduser.message') }}">Messages</a></li>
                    <li class="mobile-nav"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-mobile').submit();">
                            {{ __('Log Out') }}</a><form id="logout-form-mobile" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf </form>
                    </li>
                    <!-- End Mobile Menu -->

                @endguest
            </ul>
        </div>
    </div>
</nav>

<main class="py-4">
    <div class="alerts" style="margin-top: 80px; margin-bottom: -55px; text-align: center;">
        @if(session()->has('message'))
            <div class="alert alert-success" >
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @endif
        @if(Auth::user())
            @if(Auth::user()->verified == 0)
                <div class="alert alert-danger">
                    <p>Please verify your email! You won't be able to use your account until it is verified
                        <a href="{{route('dashboarduser.sendverification')}}" class="btn btn-success">
                            Send Verification
                        </a>.</p>
                </div>
            @endif
        @endif
    </div>

    @yield('content')

</main>
    @include('layouts.footer')
</div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132854726-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-132854726-1');
</script>

</body>
</html>