@if(Auth::user()->avatar == null)
	<img src="/images/avatar.jpg" width ="40" height="40">
@else
	<img id="avatar_small" src="{{ Auth::user()->avatar }}" width ="40" height="40">
@endif