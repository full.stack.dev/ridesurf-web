@extends('layouts.app')


@section('content')
<style type="text/css">
	.starrr {
  display: inline-block; }
  .starrr a {
    font-size: 16px;
    padding: 0 1px;
    cursor: pointer;
    color: #FFD119;
    text-decoration: none; }

</style>
	<div class="container-fluid offer-ride">
 	<div class="container">
		@if ($message = Session::get('success'))
		<div class="alert alert-success">
		  <p>{{ $message }}</p>
		</div>
		@endif
		<div class="row">
			<div class="offset-md-1 col-md-12 mobile-pad-20">
				<div class="widget text-center">

				{!! Form::model($ride, ['method' => 'PUT','route' => ['reviews.storeRate'] ]) !!}
					<div clas="form-control" style="margin:25px;">
						<div class="profile-pic">
			                <img src="{{ $loggedUser->avatar }}" class="img-circle">
			            </div>
						<h4>Please rate your ride with {{ $loggedUser->first_name }}:</h4>
						<input type="number" hidden id="choice" name="rating" >
						<input type="number" hidden value="{{ $ride->user->id }}" name="driver">
						<input type="number" hidden value="{{ Auth::id() }}" name="user">
						<input type="number" hidden value="{{ $ride->id }}" name="ride">
						<div class='starrr' id='star1'></div>
					<div class="form-group" style="margin:25px;">
						<h4>Please leave a review for your ride with {{ $loggedUser->first_name }}:</h4>
						<textarea class="form-control" rows="5" name="description"></textarea>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 text-center" id="buttonsChange">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>

				{!! Form::close() !!}
			</div>
	</div>
</div>
	<script type="text/javascript">
		$('#star1').starrr({
	      change: function(e, value){
	        if (value) {
	          $('#choice').val(value);
	        }
	      }
    	});
	</script>
@endsection