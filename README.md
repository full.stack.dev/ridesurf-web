<p align="center"><img src="https://ridesurf.io/images/ridesurf.png"></p>


## About Ridesurf

Ridesurf is a long-distance ride sharing app. It’s a safe and easy way for two people or more who are going the same direction to share a ride with one another. It works best for road trips and drives that are pre-planned. 

<strong>Ridesurf V1.0 Beta</strong>

- Beta Version of Ridesurf Web: containing Peer-To-Peer Features
- Contains Driver Side: with MVC of Driver, Ride Creation and Driver Dashboard
- Contains User Side: with MVC of User, Ride Search and User Dashboard
- Contains Admin Dashboard: MVC of User Administration, Ride Administration, Payment Administration.

## Instalation

Ridesurf uses Laravel 5.5 with MySQL 5.7 and PHP 7.1 based in a VM of Linux Ubuntu

<strong> Start Application </strong>

* Make sure that application is connected with database via the <i> .env </i> file 
* The .env file should contain the followings keys 
	* Database Info
	* Mailing Info
	* Admin Emails
	* Google Client for emails
	* Paypal Client

#### Start Application via : 

<code> git clone ... </code> <br> 
<code> composer update</code> <br> 
<code> composer dump-autoload</code> <br> 
<code> php artisan migrate</code> <br> 
<code> php artisan db:seed </code> <br>
<code> php artisan import:cars</code> <br>
<code> php artisan passport:install </code> <br>
<code> php artisan serve </code> <br> 

### Testing 

<code> php artisan db:seed --class=UsersTableSeeder </code> <br>
<code> php artisan db:seed --class=RideTableSeeder </code> <br>
<code> php artisan db:seed --class=ReviewsTestSeeder </code> <br>

### Testing Accounts

* Admin : admin@ridesurf.io 	pass: test
* Driver : pedro@testing.com 	pass: test
* User1 : loranzo@testing.com 	pass: test
* User2 : lisa@testing.com 		pass: test

### Live Site Push 

1- merge master in to prod, run in the terminal npm run production 
2- push the changes to prod
3- login in the console of google cloud platform, go to the instance of Ridesurf (compute engine), and open the ssh terminal 
4- navigate to the Ridesurf folder /var/www/html/ridesurf-web and do a git pull, merge the changes.
5- php artisan cache:clear & php artisan view:clear


