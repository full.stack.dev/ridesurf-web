<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
	use RefreshDatabase,WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {

        $user = new \App\User([
	        'id' => 1,
	        'first_name' => 'Eli',
	        'last_name' => 've',
	        'email' => 'eliman341@gmail.com',
	        'password' => 'password',
	    ]);

	    $this->be($user);

        $response = $this->get('/dashboarduser/dashboarduser');
        $response->assertStatus(200);
    }
}
