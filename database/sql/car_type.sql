-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 20, 2019 at 05:49 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ridesurf`
--

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `iCarTypeId` int(5) NOT NULL,
  `eStatus` enum('Active','Inactive') DEFAULT 'Active',
  `vTitle_EN` varchar(100) DEFAULT NULL,
  `vTitle_ES` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`iCarTypeId`, `eStatus`, `vTitle_EN`, `vTitle_ES`) VALUES
(1, 'Inactive', 'Saloon', 'salón'),
(2, 'Active', 'Hatchback', 'hatchback'),
(3, 'Active', 'Sedan', 'sedán'),
(4, 'Active', 'SUV', 'SUV'),
(5, 'Active', 'Coupe', 'Coupe'),
(6, 'Active', 'Truck', 'camión'),
(7, 'Active', 'Van', 'furgoneta'),
(8, 'Active', 'Wagon', 'vagón'),
(9, 'Active', 'Sport', 'deporte'),
(10, 'Active', 'Convertible', 'convertible'),
(11, 'Active', 'Hybrid', 'híbrido'),
(12, 'Active', 'Luxury', 'lujo'),
(13, 'Active', 'Crossover', 'crossover'),
(14, 'Active', 'Electric', 'eléctrico'),
(15, 'Active', 'Utility', 'utilidad');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`iCarTypeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `iCarTypeId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
