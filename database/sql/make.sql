-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 20, 2019 at 05:49 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ridesurf`
--

-- --------------------------------------------------------

--
-- Table structure for table `make`
--

CREATE TABLE `make` (
  `iMakeId` int(10) NOT NULL,
  `vMake` varchar(255) COLLATE utf8_bin NOT NULL,
  `eStatus` enum('Active','Inactive') COLLATE utf8_bin NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `make`
--

INSERT INTO `make` (`iMakeId`, `vMake`, `eStatus`) VALUES
(13, 'Ford', 'Active'),
(14, 'Fiat', 'Active'),
(15, 'Renault', 'Active'),
(124, 'Volvo', 'Active'),
(138, 'Honda', 'Active'),
(144, 'Suzuki', 'Active'),
(147, 'Nissan', 'Active'),
(148, 'Subaru', 'Active'),
(149, 'Land Rover', 'Active'),
(150, 'Mitsubishi', 'Active'),
(151, 'BMW', 'Active'),
(152, 'Audi', 'Active'),
(153, 'Toyota', 'Active'),
(154, 'Isuzu', 'Active'),
(156, 'Ssangyong', 'Active'),
(157, 'Jeep', 'Active'),
(158, 'Daihatsu', 'Active'),
(159, 'Kia', 'Active'),
(161, 'Skoda', 'Active'),
(163, 'Hyundai', 'Active'),
(165, 'Iveco', 'Active'),
(168, 'Volkswagen', 'Active'),
(169, 'Mercedes Benz', 'Active'),
(217, 'Other', 'Active'),
(218, 'Alfa Romeo', 'Active'),
(219, 'Aston Martin', 'Active'),
(220, 'Austin', 'Active'),
(221, 'Bentley', 'Active'),
(222, 'Buddy', 'Active'),
(223, 'Buick', 'Active'),
(225, 'Chevrolet', 'Active'),
(226, 'Chrysler', 'Active'),
(227, 'Citroen', 'Active'),
(228, 'Dacia', 'Active'),
(229, 'Daewoo', 'Active'),
(230, 'Dodge', 'Active'),
(231, 'Bugatti', 'Active'),
(232, 'Cadillac', 'Active'),
(233, 'Caparo', 'Active'),
(234, 'Caterham', 'Active'),
(235, 'Corvette', 'Active'),
(236, 'Ferrari', 'Active'),
(237, 'Fisker', 'Active'),
(238, 'GMC', 'Active'),
(239, 'Gordon Murray', 'Active'),
(240, 'Hummer', 'Active'),
(241, 'Infiniti', 'Active'),
(242, 'Jaguar', 'Active'),
(243, 'Jensen', 'Active'),
(244, 'Koenigsegg', 'Active'),
(245, 'KTM', 'Active'),
(246, 'Lamborghini', 'Active'),
(247, 'Lancia', 'Active'),
(248, 'Lada', 'Active'),
(249, 'Lexus', 'Active'),
(250, 'Lotus', 'Active'),
(251, 'Lincoln', 'Active'),
(252, 'Maserati', 'Active'),
(253, 'Maybach', 'Active'),
(254, 'Mazda', 'Active'),
(255, 'Mercury', 'Active'),
(256, 'MG', 'Active'),
(257, 'Mini', 'Active'),
(258, 'Morgan', 'Active'),
(259, 'Morris', 'Active'),
(260, 'McLaren', 'Active'),
(261, 'Noble', 'Inactive'),
(262, 'Pagani', 'Active'),
(263, 'Peugeot', 'Active'),
(264, 'Porsche', 'Active'),
(265, 'Proton', 'Active'),
(266, 'Packard', 'Active'),
(267, 'Plymouth', 'Active'),
(268, 'Pontiac', 'Active'),
(269, 'Oldsmobile', 'Active'),
(270, 'Opel', 'Active'),
(271, 'Reva', 'Active'),
(272, 'Rolls Royce', 'Active'),
(273, 'Rover', 'Active'),
(274, 'Seat', 'Active'),
(275, 'Smart', 'Active'),
(276, 'Saab', 'Active'),
(277, 'Tesla', 'Active'),
(278, 'Think', 'Active'),
(279, 'Triumph', 'Active'),
(280, 'Tata', 'Active'),
(281, 'Vauxhall', 'Active'),
(283, 'Abarth', 'Inactive'),
(284, 'Buggati', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `make`
--
ALTER TABLE `make`
  ADD PRIMARY KEY (`iMakeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `make`
--
ALTER TABLE `make`
  MODIFY `iMakeId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285;
