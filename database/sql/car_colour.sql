-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 20, 2019 at 05:48 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ridesurf`
--

-- --------------------------------------------------------

--
-- Table structure for table `car_colour`
--

CREATE TABLE `car_colour` (
  `iColourId` int(5) NOT NULL,
  `eStatus` enum('Active','Inactive') DEFAULT 'Active',
  `vColour_EN` varchar(100) DEFAULT NULL,
  `vColour_ES` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_colour`
--

INSERT INTO `car_colour` (`iColourId`, `eStatus`, `vColour_EN`, `vColour_ES`) VALUES
(1, 'Active', 'Red', 'rojo'),
(2, 'Active', 'Green', 'verde'),
(3, 'Active', 'White', 'blanco'),
(4, 'Active', 'Blue', 'azul'),
(5, 'Active', 'Black', 'negro'),
(6, 'Active', 'Silver', 'plata'),
(7, 'Active', 'Grey', 'Gris'),
(8, 'Active', 'Yellow', 'amarillo'),
(9, 'Active', 'Gun Metal', 'Gris oscuro'),
(10, 'Active', 'Gold', 'oro'),
(11, 'Active', 'Beige', 'De blanco'),
(12, 'Active', 'Purple', 'púrpura'),
(13, 'Active', 'Pink', 'rosa'),
(14, 'Active', 'Orange', 'naranja'),
(15, 'Active', 'Brown', 'Marrón'),
(16, 'Active', 'Other', 'otro');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car_colour`
--
ALTER TABLE `car_colour`
  ADD PRIMARY KEY (`iColourId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car_colour`
--
ALTER TABLE `car_colour`
  MODIFY `iColourId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
