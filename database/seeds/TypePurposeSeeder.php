<?php

use Illuminate\Database\Seeder;
use App\TripPurpose;

class TypePurposeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TripPurpose::create(['type' => 'Leisure', 'description' => 'Purpose for Leisure']);
        TripPurpose::create(['type' => 'Business', 'description' => 'Purpose for Business']);
        TripPurpose::create(['type' => 'Mountains Visit', 'description' => 'Purpose for Mountains']);
        TripPurpose::create(['type' => 'Ocean Visit', 'description' => 'Purpose for Ocean Visit']);
        TripPurpose::create(['type' => 'Desert Visit', 'description' => 'Purpose for Desert visit']);
        TripPurpose::create(['type' => 'Camping Visit', 'description' => 'Purpose for Desert visit']);
        TripPurpose::create(['type' => 'Other', 'description' => 'Other type of trip']);
    }
}
