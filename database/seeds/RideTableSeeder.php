<?php

use Illuminate\Database\Seeder;
use App\Rides;
use App\User;
use App\UserRides;
use Carbon\Carbon; 

class RideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $driver_id = User::where('first_name', '=', 'Pedro')->get()->first()->id;
        $passenger1 = User::where('first_name', '=', 'Carlos')->get()->first()->id;
        $passenger2 = User::where('first_name', '=', 'Lisa')->get()->first()->id;
        
    	DB::table('rides')->truncate();
    	DB::table('user_rides')->truncate();
        DB::table('threads')->truncate();
        DB::table('messages')->truncate();
        DB::table('participants')->truncate();
        
        $ride1 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->subDays(5), 'price' => 20, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '17:00:00', 'seats_available' => 1 , 'location_from' => 'Orlando, FL, USA', 'user_id' => $driver_id,
        	'location_to' => 'Miami, FL, USA', 'city_from' => 'Orlando, FL', 'city_to' => 'Miami, FL', 'duration' => 12664, 'ladies_only' => 'null']);

        $ride2 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->addDays(15), 'price' => 20, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '17:00:00', 'seats_available' => 1, 'location_from' => 'Orlando, FL, USA', 'user_id' => $driver_id,
        	'location_to' => 'Miami, FL, USA', 'city_from' => 'Orlando, FL', 'city_to' => 'Miami, FL', 'duration' => 12664, 'ladies_only' => 'null']);

        $ride3 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->addDays(10), 'price' => 20, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '17:00:00', 'seats_available' => 2, 'location_to' => 'Orlando, FL, USA', 'user_id' => $driver_id,
            'location_from' => 'Miami, FL, USA', 'city_to' => 'Orlando, FL', 'city_from' => 'Miami, FL', 'duration' => 12664, 'ladies_only' => 'null']);

        $ride4 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->addDays(12), 'price' => 20, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '17:00:00', 'seats_available' => 2, 'location_to' => 'Tampa, FL, USA', 'user_id' => $driver_id,
            'location_from' => 'Miami, FL, USA', 'city_to' => 'Tampa, FL', 'city_from' => 'Miami, FL', 'duration' => 12664, 'ladies_only' => 'null']);

        $ride5 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->addDays(7), 'price' => 22, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '12:00:00', 'seats_available' => 2, 'location_from' => 'Orlando, FL, USA', 'user_id' => $driver_id,
            'location_to' => 'Miami, FL, USA', 'city_from' => 'Orlando, FL', 'city_to' => 'Miami, FL', 'duration' => 12664, 'ladies_only' => 'null']);

        $ride6 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->addDays(8), 'price' => 22, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '12:00:00', 'seats_available' => 2, 'location_from' => 'Orlando, FL, USA', 'user_id' => $driver_id,
            'location_to' => 'Miami, FL, USA', 'city_from' => 'Orlando, FL', 'city_to' => 'Miami, FL', 'duration' => 12664, 'ladies_only' => 'null']);

        $ride7 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->addDays(22), 'price' => 22, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '12:00:00', 'seats_available' => 2, 'location_from' => 'Orlando, FL, USA', 'user_id' => $driver_id,
            'location_to' => 'Miami, FL, USA', 'city_from' => 'Orlando, FL', 'city_to' => 'Miami, FL', 'duration' => 12664, 'ladies_only' => 'null']);

        $ride8 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->addDays(7), 'price' => 22, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '10:00:00', 'seats_available' => 2, 'location_from' => 'Orlando, FL, USA', 'user_id' => $driver_id,
            'location_to' => 'Tampa, FL, USA', 'city_from' => 'Orlando, FL', 'city_to' => 'Tampa, FL', 'duration' => 12664, 'ladies_only' => 'null']);

        $ride9 = Rides::create(['description' => 'Going from Orlando to Miami from XZY to XZY leaving early ! ', 'dateFrom' => Carbon::now()->addDays(7), 'price' => 22, 'seats' => 2, 'completed' => 'Ongoing', 'startHour' => '12:00:00', 'seats_available' => 2, 'location_from' => 'Orlando, FL, USA', 'user_id' => $driver_id,
            'location_to' => 'Atlanta, GA, USA', 'city_from' => 'Orlando, FL', 'city_to' => 'Atlanta, GA', 'duration' => 15664, 'ladies_only' => 'null']);

        UserRides::create(['rides_id' => $ride1->id, 'user_id' => $passenger1, 'description' => 'Hello I would like to ride with you!', 'status' => 'Approved', 
        	'booking_number' => 'Test', 'booker_payment_paid' => 'Yes', 'price_paid' => ($ride1->price), 'city_from' => $ride1->city_from, 'seats' => 1, 'ride_cost' => ($ride1->price + 1.50),
        	'city_to' => $ride1->city_to, 'transaction_id' => 'test']);

        UserRides::create(['rides_id' => $ride7->id, 'user_id' => $passenger1, 'description' => 'Hello I would like to ride with you!', 'status' => 'Approved', 
            'booking_number' => '', 'booker_payment_paid' => 'No', 'price_paid' => ($ride7->price), 'city_from' => $ride1->city_from, 'seats' => 1, 'ride_cost' => ($ride7->price + 1.50),
            'city_to' => $ride7->city_to, 'transaction_id' => '']);


        UserRides::create(['rides_id' => $ride2->id, 'user_id' => $passenger1, 'description' => 'Hello I would like to ride with you!', 'status' => 'Approved', 
        	'booking_number' => 'Test', 'booker_payment_paid' => 'Yes', 'price_paid' => ($ride2->price), 'city_from' => $ride2->city_from, 'seats' => 1, 'ride_cost' => ($ride2->price + 1.50),
        	'city_to' => $ride1->city_to, 'transaction_id' => 'test']);

        UserRides::create(['rides_id' => $ride1->id, 'user_id' => $passenger2, 'description' => 'Hello I would like to ride with you!', 'status' => 'Approved', 'booking_number' => 'Test', 'booker_payment_paid' => 'Yes', 'price_paid' => ($ride1->price), 'city_from' => $ride1->city_from, 'city_to' => $ride1->city_to, 'seats' => 1, 'ride_cost' => ($ride1->price + 1.50),
    		'transaction_id' => 'test']);

    }
}
