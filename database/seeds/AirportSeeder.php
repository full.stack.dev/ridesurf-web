<?php

use Illuminate\Database\Seeder;
use App\Airports;
use App\AirportMeetings;

class AirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $LA = Airports::create(['name' => 'LAX', 'address' => '1 World Way', 'city' => 'Los Angeles', 'state' => 'CA', 'zip' => '90045']);
        AirportMeetings::create(['airport_id' => $LA->id, 'meeting_point' => 'Terminal 1']);
        AirportMeetings::create(['airport_id' => $LA->id, 'meeting_point' => 'Terminal 2']);
        AirportMeetings::create(['airport_id' => $LA->id, 'meeting_point' => 'Terminal 3']);
        AirportMeetings::create(['airport_id' => $LA->id, 'meeting_point' => 'Terminal 4']);
        AirportMeetings::create(['airport_id' => $LA->id, 'meeting_point' => 'Terminal 5']);
        AirportMeetings::create(['airport_id' => $LA->id, 'meeting_point' => 'Terminal 6']);
        AirportMeetings::create(['airport_id' => $LA->id, 'meeting_point' => 'Terminal 7']);
        AirportMeetings::create(['airport_id' => $LA->id, 'meeting_point' => 'Terminal 8']);

        $Orlando = Airports::create(['name' => 'MCO', 'address' => '1 Jeff Fuqua Blvd', 'city' => 'Orlando', 'state' => 'FL', 'zip'=> '32827']);
        AirportMeetings::create(['airport_id' => $Orlando->id, 'meeting_point' => 'Terminal A']);
        AirportMeetings::create(['airport_id' => $Orlando->id, 'meeting_point' => 'Terminal B']);

        $Miami = Airports::create(['name' => 'MIA', 'address' => '2100 NW 42nd Ave', 'city' => 'Miami', 'state' => 'FL', 'zip'=> '33126']);
        AirportMeetings::create(['airport_id' => $Miami->id, 'meeting_point' => 'Terminal D']);
        AirportMeetings::create(['airport_id' => $Miami->id, 'meeting_point' => 'Terminal E']);
        AirportMeetings::create(['airport_id' => $Miami->id, 'meeting_point' => 'Terminal F']);
        AirportMeetings::create(['airport_id' => $Miami->id, 'meeting_point' => 'Terminal G']);
        AirportMeetings::create(['airport_id' => $Miami->id, 'meeting_point' => 'Terminal H']);
        AirportMeetings::create(['airport_id' => $Miami->id, 'meeting_point' => 'Terminal J']);
    }
}
