<?php

use Illuminate\Database\Seeder;
use App\Reviews;
use App\User;
class ReviewsTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reviews')->truncate();
        $user1 = User::where('first_name' ,'=', 'Pedro')->first();
        $user2 = User::where('first_name','=','Carlos')->first();
        $user3 = User::where('first_name', '=', 'Lisa')->first();
        
        Reviews::create(['user_rating_id' => $user2->id, 'user_rate_id' => $user1->id, 'rate' => 5, 'description' => 'Cool guy had a killer setlist and does not smoke!' ]);
        Reviews::create(['user_rating_id' => $user2->id, 'user_rate_id' => $user1->id, 'rate' => 5, 'description' => 'Good driver and cares about the passengers' ]);
        Reviews::create(['user_rating_id' => $user3->id, 'user_rate_id' => $user1->id, 'rate' => 4, 'description' => 'Average ride! standard car ! ' ]);
    }
}
