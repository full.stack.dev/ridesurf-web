<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Driver;
use App\Role;
use App\Preferences;
use App\UserPreferences;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('user_preferences')->truncate();
        DB::table('drivers')->truncate();
        DB::table('messages')->truncate();
        
        $role_user = Role::where('name','=','User')->first();
        $role_driver = Role::where('name','=', 'Driver')->first();
        $role_admin  = Role::where('name','=','Admin')->first();
        $password = Hash::make('test');
        $preferences = Preferences::all();

        $driver1 = User::create(['first_name' => 'Pedro', 'last_name' => "Perez", 'email' => 'pedro@testing.com', 'password' => 
        			$password	,'Address' => '123 Main St', 'city' => 'Miami', 'country' => 'United States', 'rating' => 4.3,
        			'state' => 'FL','description' => 'I am a test and there for you should not pay attention to me !',
        			'verified' => true, 'gender' => 'Male', 'avatar' => '/images/drivertest/driver.jpg' ]);
        $driver1->assignRole('Driver');
        $driver1->update();        

        $user1 = User::create(['first_name' => 'Carlos', 'last_name' => "Loranzo", 'email' => 'loranzo@testing.com', 'password' => 
        			$password,'Address' => '123 Main St', 'city' => 'Orlando', 'country' => 'United States', 'rating' => 5.0,
        			'state' => 'FL','description' => 'I am a test and there for you should not pay attention to me !',
        			'verified' => true, 'gender' => 'Male', 'avatar' => '/images/drivertest/user1.jpeg' ]);
        $user1->assignRole('User');
        $user1->update(); 
        			

        $user2 = User::create(['first_name' => 'Lisa', 'last_name' => "Mendez", 'email' => 'lisa@testing.com', 'password' => 
        			$password,'Address' => '123 Main St', 'city' => 'Orlando', 'country' => 'United States', 'rating' => 4.8,
        			'state' => 'FL','description' => 'I am a test and there for you should not pay attention to me !',
        			'verified' => true, 'gender' => 'Female', 'avatar' => '/images/drivertest/user2.jpg' ]);
        $user2->assignRole('User');
        $user2->update();

        $admin = User::create(['first_name' => 'Admin', 'last_name' => "Admin", 'email' => 'admin@ridesurf.io', 'password' => 
        			$password,'Address' => '123 Main St', 'city' => 'Miami', 'country' => 'United States', 
        			'state' => 'FL','description' => 'I am a test and there for you should not pay attention to me !',
        			'verified' => true, 'gender' => 'Male', 'avatar' => 'https://ridesurf.io/images/user_information/9/eyJpdiI6InNhVFV5VXRkakp1OXlzZ3JpbVo3bXc9PSIsInZhbHVlIjoiV0R6NkF3SzdlQ1lQQUlmZjhId2c1dz09IiwibWFjIjoiMGRmM2RmYjE4MWVhOWViZDc1ZjBjNGU3MTE2NWI3ODMwNzgwNTNiZDhkMGY3ZmQ5YTJlNmMwNTRjZDU3YjE0NSJ9.png']);
        $admin->assignRole('Admin');
        $admin->save();

        Driver::create([ 'user_id' => $driver1->id, 'permissions_id' => 0, 'vehicle_tag' => 'ELA43C', 'vehicle_registration' => 
                         'AAJDHFGDFGWJBW123', 'make_id' => 153, 'model_id' => 984, 'type_id' => 3, 'color_id' => 5, 'year' => 2018,
                         'path_image' => "/images/drivertest/car_image.jpg", 'status' => 'Approved', 'status_message' => 
                         'User has been approved', 'registration_path' => '/images/drivertest/car_registration.png', 
                         'insurance_path' => 
                         '/images/drivertest/car_insurance.jpg', 'driver_license' => '/images/drivertest/driver_license.png'
        ]);

        // Add Preferemces
        foreach($preferences as $preference){
            if($preference->id % 2 == 0){
                $user_preference = new UserPreferences;
                $user_preference->user_id = $driver1->id;
                $user_preference->preference_id = $preference->id;
                $user_preference->save();
            }
        }
        // Add Preferemces
        foreach($preferences as $preference){
            if($preference->id % 2 == 0){
                $user_preference = new UserPreferences;
                $user_preference->user_id = $user1->id;
                $user_preference->preference_id = $preference->id;
                $user_preference->save();
            }
        }
        // Add Preferemces
        foreach($preferences as $preference){
            if($preference->id % 2 == 0){
                $user_preference = new UserPreferences;
                $user_preference->user_id = $user2->id;
                $user_preference->preference_id = $preference->id;
                $user_preference->save();
            }
        }
        // Add Preferemces
        foreach($preferences as $preference){
            if($preference->id % 2 == 0){
                $user_preference = new UserPreferences;
                $user_preference->user_id = $admin->id;
                $user_preference->preference_id = $preference->id;
                $user_preference->save();
            }
        }

    }
}
