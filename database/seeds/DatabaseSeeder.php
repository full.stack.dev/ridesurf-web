<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Seed the database
        $this->call('PermissionTableSeeder');
        $this->call('PreferencesTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('StatesSeeder');

        //Testing 
        //$this->call('UsersTableSeeder');
        //$this->call('ReviewsTestSeeder');
        //$this->call('RideTableSeeder');
        
    }
}
