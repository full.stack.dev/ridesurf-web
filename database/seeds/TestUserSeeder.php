<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Driver;
use App\Role;
use App\Reviews;

class TestUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$password = Hash::make('testing');

        $user = User::create(['first_name' => 'Pedro', 'last_name' => "Perez", 'email' => 'pedro@testing.com', 'password' => 
        			$password	,'Address' => '123 Main St', 'city' => 'Miami', 'country' => 'United States', 'rating' => 5,
        			'state' => 'FL','description' => 'I am a test and there for you should not pay attention to me !', 'birthday' => 1990,
        			'verified' => true, 'gender' => 'Male', 'avatar' => '/images/drivertest/driver.jpg' ]);

        Reviews::create(['user_rating_id' => 946, 'user_rate_id' => $user->id, 'rate' => 5, 
        	'description' => 'Cool guy had a killer setlist and does not smoke!' ]);
        Reviews::create(['user_rating_id' => 1078, 'user_rate_id' => $user->id, 'rate' => 5, 
        	'description' => 'Cool guy, Good Driver' ]);

        $user->assignRole('User');
        $user->update(); 
    }
}
