<?php

use Illuminate\Database\Seeder;
use App\Http\Traits\Notifications; 
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use App\User;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reviews')->truncate();
        $admin = User::where('first_name', '=', 'admin')->get()->first();
        $driver = User::where('first_name', '=', 'Pedro')->get()->first();
        $passenger1 = User::where('first_name', '=', 'Carlos')->get()->first();

        $user_ride = $passenger1->UserRide[0];

        $thread = Thread::create([
            'subject' => 'Approved for ride to ' . $user_ride->ride->location_to,
            'rides_id' => $user_ride->rides_id,
            'isNotification' => 1,
        ]);
       
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $admin->id,
            'body' => $message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $passenger1->id,
            'last_read' => new Carbon,
        ]);
    }
}