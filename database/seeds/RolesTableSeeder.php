<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'Admin', 'guard_name' => 'web']);
        Role::create(['name' => 'Driver', 'guard_name' => 'web']);
        Role::create(['name' => 'User', 'guard_name' => 'web']);
        Role::create(['name' => 'Moderator', 'guard_name' => 'web']);

        for($i=1; $i <= 13; $i++){
	        DB::table('role_has_permissions')->insert([
	        	'permission_id' => $i,
	        	'role_id' => 1
	        ]);
        }
        for($j= 5; $j <= 9; $j++){
        	DB::table('role_has_permissions')->insert([
	        	'permission_id' => $j,
	        	'role_id' => 2
	        ]);
        }
        for($k= 9; $k <= 12; $k++){
        	DB::table('role_has_permissions')->insert([
	        	'permission_id' => $k,
	        	'role_id' => 3
	        ]);
        }


    }
}
