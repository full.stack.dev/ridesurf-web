<?php

use Illuminate\Database\Seeder;
use App\Preferences;

class PreferencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('preferences')->truncate();
        Preferences::create(['title' => 'Smoking', 'description' => "Smoking doesn't bother me.", 'category' => 'Smoking', 'action' => 'Yes']);
        Preferences::create(['title' => 'No Smoking', 'description' => "No smoking please.", 'category' => 'Smoking', 'action' => 'No']);
        Preferences::create(['title' => 'Pets', 'description' => "Pets welcome!", 'category' => 'Pets', 'action' => 'Yes']);
        Preferences::create(['title' => 'No Pets', 'description' => "No pets in my car please!", 'category' => 'Pets', 'action' => 'No']);
        Preferences::create(['title' => 'Chattiness', 'description' => "I love to chat!", 'category' => 'Chattiness', 'action' => 'Yes']);
        Preferences::create(['title' => 'No Chattiness', 'description' => "I'm the quiet type :)", 'category' => 'Chattiness', 'action' => 'No']);
        Preferences::create(['title' => 'Music', 'description' => "It's all about the playlist!", 'category' => 'Music', 'action' => 'Yes']);
        Preferences::create(['title' => 'No Music', 'description' => "Silence is golden.", 'category' => 'Music', 'action' => 'No']);
    }
}
