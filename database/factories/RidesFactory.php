<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Rides::class, function (Faker $faker) {
    return [
        'location_from' => 'Pembroke Pines, FL, USA',
        'location_to' => 'Orlando, FL, USA',
        'city_from' => 'Pembroke Pines, FL',
        'city_to' => 'Orlando, FL',
        'price' => 20,
        'user_id' => 943,
        'dateFrom' => Carbon::now()->format('Y-m-d'),
        'seats' => 3,
        'startHour' => '10:00:00',
        'seats_available' => 3,
        'duration' => 11761,
        'description' => 'test',
    ];
});
