<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeletedRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('deleted_rides');
        Schema::create('deleted_rides', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('rides_id');
            $table->integer('user_id');
            $table->string('locaton_to');
            $table->string('location_from');
            $table->decimal('price');
            $table->integer('seats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
