<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rides', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('rides_id');
            $table->integer('seats');
            $table->string('description')->nullable();
            $table->enum('status', ['Request', 'Approved', 'Denied'])->nullable();
            $table->string('booking_number')->nullable();
            $table->string('verification_code')->nullable();
            $table->string('payerID')->nullable();
            $table->string('orderID')->nullable();
            $table->string('paymentToken')->nullable();
            $table->string('returnUrl')->nullable();
            $table->string('sale_id')->nullable();
            $table->float('price_paid')->nullable();
            $table->float('ride_cost')->nullable();
            $table->date('booking_date')->nullable();
            $table->string('transaction_id')->nullable();
            $table->enum('booker_payment_paid', ['Yes', 'No'])->default('No');
            $table->enum('cancel_by', ['driver', 'passenger'])->nullable();
            $table->string('cancel_reason')->nullable();
            $table->boolean('reviewed')->nullable();
            $table->boolean('isStop')->nullable();
            $table->string('city_from')->nullable();
            $table->string('city_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rides');
    }
}
