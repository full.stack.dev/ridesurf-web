<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_rides', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id');
            $table->integer('price');
            $table->integer('seats');
            $table->integer('seats_available');
            $table->date('open_date_from');
            $table->date('open_date_to');
            $table->enum('luggage_size', ['Small', 'Medium', 'Large'])->nullable();;
            $table->integer('luggage_amount')->nullable();;
            $table->string('location_from');
            $table->string('location_to');
            $table->string('city_from');
            $table->string('city_to');
            $table->integer('duration');
            $table->text('description')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('status')->nullable();
            $table->enum('completed', ['Completed', 'Ongoing'])->default('Ongoing');
            $table->enum('paid_driver', ['Yes', 'No'])->default('No');
            $table->float('totalToPay')->nullable();
            $table->enum('ladies_only', ['Yes', 'No'])->nullable();;
            $table->integer('trip_purpose_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_rides');
    }
}
