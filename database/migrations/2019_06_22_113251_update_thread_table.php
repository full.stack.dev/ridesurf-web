<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateThreadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::table('threads', function (Blueprint $table) {
            $table->enum('isNotification',['Yes','No'])->default('Yes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('threads', 'isNotification'))

        {

            Schema::table('threads', function (Blueprint $table)

            {

                $table->dropColumn('isNotification');

            });

        }
        
    }
}
