<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('dateFrom');
            $table->integer('price');
            $table->integer('seats');
            $table->integer('seats_available');
            $table->time('startHour');
            $table->boolean('status')->nullable();
            $table->enum('completed', ['Completed', 'Ongoing']);
            $table->enum('paid_driver', ['Yes', 'No'])->default('No');
            $table->float('totalToPay')->nullable();
            $table->enum('luggage_size', ['Small', 'Medium', 'Large']);
            $table->integer('luggage_amount');
            $table->boolean('rated');
            $table->string('location_from');
            $table->string('location_to');
            $table->string('city_from');
            $table->string('city_to');
            $table->integer('duration');
            $table->text('description')->nullable();
            $table->enum('ladies_only', ['Yes', 'No']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rides');
    }
}
