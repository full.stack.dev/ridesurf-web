<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airport_details', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('ride_id');
            $table->integer('airport_id');
            $table->enum('type', ['Picking','Droping', 'Parked'])->default('Picking');
            $table->text('airport');
            $table->text('terminal')->nullable();
            $table->text('parking')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('airport_details');
    }
}
