<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('permissions_id');
            $table->string('vehicle_tag');
            $table->string('vehicle_registration');
            $table->integer('make_id');
            $table->integer('model_id');
            $table->integer('type_id');
            $table->integer('color_id');
            $table->integer('year');
            $table->string('path_image');
            $table->enum('status',['Requested', 'Approved', 'None', 'Denied', 'Changed']);
            $table->string('status_message');
            $table->string('registration_path');
            $table->string('insurance_path');
            $table->string('driver_license');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
