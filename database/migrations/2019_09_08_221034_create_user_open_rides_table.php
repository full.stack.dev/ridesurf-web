<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOpenRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_open_rides', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('open_ride_id');
            $table->integer('user_id');
            $table->enum('status', ['Requested', 'Approved','Denied']);
            $table->time('hour');
            $table->date('date');
            $table->string('message');
            $table->integer('seats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_open_rides');
    }
}
