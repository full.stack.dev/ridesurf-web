<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCancelUserRideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cancel_user_rides', function ( $table) {
            $table->longText('reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cancel_user_rides', function (Blueprint $table) {
            Schema::dropColumn('sales_id');
            Schema::dropColumn('price_refound');
            $table->string('sales_id')->nullable();
            $table->float('price_refound')->nullable();
        });
    }
}
