<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state', 2)->nullable();
            $table->string('country')->nullable();
            $table->integer('zipcode')->length(5)->nullable();
            $table->string('description')->nullable();
            $table->float('rating')->nullable();
            $table->bigInteger('phone_number')->nullable();
            $table->integer('facebook_id')->nullable();
            $table->string('avatar')->nullable();
            $table->string('avatar_original')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->boolean('active')->nullable();
            $table->string('email_token')->nullable();
            $table->boolean('verified')->nullable();
            $table->integer('birthday')->length(4)->nullable();
            $table->integer('newsletter')->nullable();
            $table->enum('gender', ['Male', 'Female'])->nullable();
            $table->string('paypal_email')->nullable();
            $table->boolean('is24Hours')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
