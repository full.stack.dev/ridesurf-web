<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReUpdateDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            if (!Schema::hasColumn('drivers', 'registration_expire')) {
                $table->timestamp('registration_expire');
            } 
            if (!Schema::hasColumn('drivers', 'license_expire')) {
                $table->timestamp('license_expire');
            } 
            if (!Schema::hasColumn('drivers', 'insurance_expire')) {
                $table->timestamp('insurance_expire');
            } 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
