<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancelUserRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancel_user_rides', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('rides_id');
            $table->integer('passenger_id')->nullable();
            $table->string('sales_id')->nullable();
            $table->decimal('price_refound')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancel_user_rides');
    }
}
