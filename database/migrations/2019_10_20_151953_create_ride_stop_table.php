<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRideStopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('rides_stops')) {
            Schema::create('rides_stops', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->integer('rides_id');
                $table->integer('user_id');
                $table->string('address');
                $table->string('city_name')->nullable();
                $table->integer('duration')->nullable();
                $table->float('price_from')->nullable();
                $table->float('price_to')->nullable();
                $table->integer('seats_from')->nullable();
                $table->integer('seats_to')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rides_stops');
    }
}
