@servers(['web' => 'ridesurfapp@34.73.109.97'])

@setup
$repository = 'git@gitlab.com:roska541/ridesurf-web.git';
$app_dir = '/var/www/html/ridesurf-qa-web';
$complete_dir = '/var/www/html/ridesurf-qa-web/ridesurf-web';
$release = date('YmdHis');
$images = '/var/www/html/ridesurf-qa-web/ridesurf-web/public/images';
@endsetup

@story('deploy')
pull-git
@endstory

@task('pull-git')
echo 'pulling'
cd {{$complete_dir}}
git pull
sudo composer update
@endtask
